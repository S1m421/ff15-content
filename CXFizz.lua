local Fizz = {}
local version = 1
if tonumber(GetInternalWebResult("CXFizz.version")) > version then
    DownloadInternalFile("CXFizz.lua", SCRIPT_PATH .. "CXFizz.lua")
    PrintChat("New version:" .. tonumber(GetInternalWebResult("CXFizz.version")) .. " Press F5")
end
require "FF15Menu"
require "utils"
local DreamTS = require("DreamTS")
local dmgLib = require("FF15DamageLib")

function OnLoad()
    if not _G.Prediction then
        LoadPaidScript(PaidScript.DREAM_PRED)
    end
end

function Fizz:__init()
    self.e = {
        type = "circular",
        speed = 1000,
        range = 700,
        delay = 0.25,
        radius = 200
    }
    self.r = {
        type = "linear",
        speed = 1300,
        range = 1300,
        delay = 0.25,
        width = 140
    }
    self:Menu()
    self.QlvlDmg = {[1] = 10,[2] = 25,[3] = 40,[4] = 55,[5] = 70}
    self.TS =
        DreamTS(
        self.menu.dreamTs,
        {
            ValidTarget = function(unit)
                return _G.Prediction.IsValidTarget(unit)
            end,
            Damage = function(unit)
                return dmgLib:CalculateMagicDamage(myHero, unit, 100)
            end
        }
    )
    AddEvent(Events.OnTick, function() self:OnTick() end)
    AddEvent(Events.OnDraw, function() self:OnDraw() end)
    AddEvent(Events.OnExecuteCastFrame, function(unit, spell) self:OnExecuteCastFrame(unit, spell) end)
    PrintChat("<font color=\"#E41B17\">[<b>¤ Cyrex ¤</b>]:</font>" .. " <font color=\"#" .. "FFFFFF" .. "\">" .. "Fizz Loaded" .. "</font>")
    self.font = DrawHandler:CreateFont("Calibri", 12)
end

function Fizz:Menu()
    self.menu = Menu("cxFizz", "Cyrex Fizz")
    self.menu:sub("dreamTs", "Target Selector")

    self.menu:sub("Key", "Key Settings")
        self.menu.Key:key("run", "Flee", string.byte("S"))
        self.menu.Key:key("manual", "Manual R Aim", string.byte("T"))

    self.menu:sub("combo", "Combo Settings")
        self.menu.combo:label("xd", "Q Settings")
        self.menu.combo:checkbox("q", "Use Q", true)
        self.menu.combo:slider("qr", "Min. Q Range", 0, 550, 350, 25)
        self.menu.combo:label("xd1", "W Settings")
        self.menu.combo:checkbox("w", "Use W", true)
        self.menu.combo:label("xd2", "E Settings")
        self.menu.combo:checkbox("e", "Use E", true)
        self.menu.combo:checkbox("e2", "Delay E2 For More Damage", true)
        self.menu.combo:list("ed", "Choose Mode: ", 2, {"Mouse Pos", "With Prediction"})            

    self.menu:sub("harass", "Harass Settings")
        self.menu.harass:checkbox("q", "Use Q", true)
        self.menu.harass:checkbox("w", "Use W", true)
        self.menu.harass:slider("mana", "Min. Mana Percent: ", 0, 100, 10, 5)

    self.menu:sub("auto", "Automatic Settings")
        self.menu.auto:label("dx", "Killsteal Settings")
        self.menu.auto:checkbox("uks", "Use Killsteal", true)
        self.menu.auto:checkbox("urks", "Use R in Killsteal", true)

    self.menu:sub("draws", "Draw")
        self.menu.draws:checkbox("q", "Q", true)
        self.menu.draws:checkbox("r", "R", true)
    self.menu:label("version", "Version: " .. version .. "")
    self.menu:label("author", "Author: Coozbie")
end

function Fizz:OnDraw()
    if self.menu.draws.q:get() and myHero.spellbook:CanUseSpell(0) == 0 and myHero.visibleOnScreen then
        DrawHandler:Circle3D(myHero.position, self.menu.combo.qr:get(), self:Hex(255,255,255,255))
    end
    if self.menu.draws.r:get() and myHero.spellbook:CanUseSpell(3) == 0 and myHero.visibleOnScreen then
        DrawHandler:Circle3D(myHero.position, 1300, self:Hex(255,255,255,255))
    end
end

function Fizz:GetPercentHealth(obj)
  local obj = obj or myHero
  return (obj.health / obj.maxHealth) * 100
end

local delayedActions, delayedActionsExecuter = {}, nil
function Fizz:DelayAction(func, delay, args) --delay in seconds
  if not delayedActionsExecuter then
    function delayedActionsExecuter()
      for t, funcs in pairs(delayedActions) do
        if t <= os.clock() then
          for i = 1, #funcs do
            local f = funcs[i]
            if f and f.func then
              f.func(unpack(f.args or {}))
            end
          end
          delayedActions[t] = nil
        end
      end
    end
    AddEvent(Events.OnTick, delayedActionsExecuter)
  end
  local t = os.clock() + (delay or 0)
  if delayedActions[t] then
    delayedActions[t][#delayedActions[t] + 1] = {func = func, args = args}
  else
    delayedActions[t] = {{func = func, args = args}}
  end
end

function Fizz:GetAARange(target)
  return myHero.characterIntermediate.attackRange + myHero.boundingRadius + (target and target.boundingRadius or 0)
end

function Fizz:MoveToMouse()
    local pos = pwHud.hudManager.virtualCursorPos
    myHero:IssueOrder(GameObjectOrder.MoveTo, pos)
end

function Fizz:ValidTarget(object, distance)
    return object and object.isValid and object.team ~= myHero.team and object.isVisible and not object.buffManager:HasBuff('SionPassiveZombie') and not object.buffManager:HasBuff('FioraW') and not object.isDead and not object.isInvulnerable and (not distance or GetDistanceSqr(object) <= distance * distance)
end

function Fizz:GetEnemyHeroesInRange(range, pos)
  local pos = pos or myHero.position
  local h = {}
  local enemies = ObjectManager:GetEnemyHeroes()
  for i = 1, #enemies do
    local hero = enemies[i]
    if hero and hero.team ~= myHero.team and not hero.isInvulnerable and not hero.isDead and hero.isVisible and hero.isTargetable and GetDistanceSqr(hero) < range * range then
      h[#h + 1] = hero
    end
  end
  return h
end

function Fizz:GetAllyHeroesInRange(range, pos)
  local pos = pos or myHero.position
  local h = {}
  local allies = ObjectManager:GetAllyHeroes()
  for i = 1, #allies do
    local hero = allies[i]
    if hero and hero.team == myHero.team and not hero.isInvulnerable and not hero.isDead and hero.isVisible and hero.isTargetable and GetDistanceSqr(hero) < range * range then
      h[#h + 1] = hero
    end
  end
  return h
end

function Fizz:GetTotalAP(obj)
  local obj = obj or myHero
  return obj.characterIntermediate.flatMagicDamageMod * obj.characterIntermediate.percentMagicDamageMod
end

function Fizz:TotalAD(unit)
    return unit.characterIntermediate.flatPhysicalDamageMod + unit.characterIntermediate.baseAttackDamage
end


-- Returns magic damage multiplier on @target from @damageSource or player
function Fizz:MagicReduction(target, damageSource)
  local damageSource = damageSource or myHero
  local magicResist = (target.characterIntermediate.spellBlock * damageSource.characterIntermediate.percentMagicPenetration) - damageSource.characterIntermediate.flatMagicPenetration
  return magicResist >= 0 and (100 / (100 + magicResist)) or (2 - (100 / (100 - magicResist)))
end

function Fizz:PhysicalReduction(target, damageSource)
  local damageSource = damageSource or myHero
  local armor =
    ((target.characterIntermediate.bonusArmor * damageSource.characterIntermediate.percentBonusArmorPenetration) + (target.characterIntermediate.armor - target.characterIntermediate.bonusArmor)) *
    damageSource.characterIntermediate.percentArmorPenetration
  local lethality =
    (damageSource.characterIntermediate.physicalLethality * .4) + ((damageSource.characterIntermediate.physicalLethality * .6) * (damageSource.experience.level / 18))
  return armor >= 0 and (100 / (100 + (armor - lethality))) or (2 - (100 / (100 - (armor - lethality))))
end

-- Returns damage reduction multiplier on @target from @damageSource or player
function Fizz:DamageReduction(damageType, target, damageSource)
  local damageSource = damageSource or myHero
  local reduction = 1
  if damageType == "AD" then
  end
  if damageType == "AP" then
  end
  return reduction
end

function Fizz:CalculateMagicDamage(target, damage, damageSource)
  local damageSource = damageSource or myHero
  if target then
    return (damage * self:MagicReduction(target, damageSource)) * self:DamageReduction("AP", target, damageSource)
  end
  return 0
end

function Fizz:CalculatePhysicalDamage(target, damage, damageSource)
  local damageSource = damageSource or myHero
  if target then
    return (damage * self:PhysicalReduction(target, damageSource)) *
      self:DamageReduction("AD", target, damageSource)
  end
  return 0
end

function Fizz:OnExecuteCastFrame(unit, spell)
    if unit ~= myHero or not spell.spellData.name:lower():find("attack") then return end
    local target = spell.target
    if not target then return end
    if target and (LegitOrbwalker:GetMode() == "Harass" or LegitOrbwalker:GetMode() == "Combo") then
        if (self.menu.combo.w:get() or self.menu.harass.w:get()) and myHero.spellbook:CanUseSpell(1) == 0 and self:ValidTarget(target) and GetDistance(target) <= self:GetAARange() + 50 then
            myHero.spellbook:CastSpell(1, myHero.networkId)
        end
    end
end

function Fizz:qDmg(target)
    local qDamage = self.QlvlDmg[myHero.spellbook:Spell(0).level] + (self:GetTotalAP() * .55)
    local ad = self:TotalAD(myHero)
    return self:CalculateMagicDamage(target, qDamage) + self:CalculatePhysicalDamage(target, ad)
end

function Fizz:rDmg(target)
    local rDamage = (125 + (100 * myHero.spellbook:Spell(3).level) + (self:GetTotalAP() * 1))
    return self:CalculateMagicDamage(target, rDamage)
end


function Fizz:CastQ(target)
    if myHero.spellbook:CanUseSpell(0) == 0 then
        if GetDistance(target) < self.menu.combo.qr:get() then
            myHero.spellbook:CastSpell(0, target.networkId)
        end
    end
end

function Fizz:CastE(target)
    if myHero.spellbook:Spell(2).name == "FizzE" then
        if myHero.spellbook:CanUseSpell(2) == 0 and GetDistance(target) < 700 and GetDistance(target) > 400 then
            myHero.spellbook:CastSpell(2, target.position)
        end
    elseif myHero.spellbook:Spell(2).name == "FizzEBuffer" then
        if myHero.spellbook:CanUseSpell(2) == 0 and GetDistanceSqr(target) < (700 * 700) then
            local pred = _G.Prediction.GetPrediction(target, self.e, myHero)
            if pred and pred.castPosition and GetDistanceSqr(pred.castPosition) <= (700 * 700) then
                if self.menu.combo.e2:get() then
                    self:DelayAction(function() myHero.spellbook:CastSpell(2, pred.castPosition) end, 0.75)
                elseif not self.menu.combo.e2:get() then
                    myHero.spellbook:CastSpell(2, pred.castPosition)
                end
            end
        end
    end
end

function Fizz:CastR(target)
    if myHero.spellbook:CanUseSpell(3) == 0 then
        local pred = _G.Prediction.GetPrediction(target, self.r, myHero)
        if pred and pred.castPosition and GetDistanceSqr(pred.castPosition) <= self.r.range * self.r.range and not pred:windWallCollision() then
            if GetDistance(target) > 700 then
                myHero.spellbook:CastSpell(3, pred.castPosition)
            end
        end
    end
end

function Fizz:KillSteal()
    for i, enemy in pairs(ObjectManager:GetEnemyHeroes()) do
        if enemy and enemy.team ~= myHero.team and not enemy.isInvulnerable and not enemy.isDead and not enemy.buffManager:HasBuff('SionPassiveZombie') and enemy.isTargetable then
            local hp = enemy.health
            local dist = GetDistanceSqr(enemy)
            local q = myHero.spellbook:CanUseSpell(0) == 0
            local r = myHero.spellbook:CanUseSpell(3) == 0
            if q and dist <= (550 * 550) and self:qDmg(enemy) > hp then
                self:CastQ(enemy)
            elseif r and self:rDmg(enemy) > hp and self.menu.auto.urks:get() and dist < (1300 * 1300) and dist >= (600 * 600) and not enemy.buffManager:HasBuff("FioraW") and not enemy.buffManager:HasBuff("UndyingRage") and not enemy.buffManager:HasBuff("KarthusDeathDefiedBuff") and not enemy.buffManager:HasBuff("KayleR") and not enemy.buffManager:HasBuff("KindredRNoDeathBuff") and not enemy.buffManager:HasBuff("WillRevive") then
                self:CastR(enemy)
            end
        end
    end
end

function Fizz:MR()
    for i, enemy in pairs(ObjectManager:GetEnemyHeroes()) do
        if enemy and enemy.team ~= myHero.team and not enemy.isInvulnerable and not enemy.isDead and not enemy.buffManager:HasBuff('SionPassiveZombie') and enemy.isTargetable then
            if myHero.spellbook:CanUseSpell(3) == 0 then
                self:CastR(enemy)
            end
        end
    end
end

function Fizz:Run()
    if self.menu.Key.run:get() then
        self:MoveToMouse()
        if myHero.spellbook:CanUseSpell(2) == 0 then
            myHero.spellbook:CastSpell(2, pwHud.hudManager.virtualCursorPos)
        end
    end
end


function Fizz:OnTick()
    local target = self:GetTarget(600)
    if LegitOrbwalker:GetMode() == "Combo" then
        local q = myHero.spellbook:CanUseSpell(0) == 0
        local w = myHero.spellbook:CanUseSpell(1) == 0
        local e = myHero.spellbook:CanUseSpell(2) == 0
        local d = GetDistance(target)
        if target and self:ValidTarget(target) then
            if self.menu.combo.q:get() and q then
                self:CastQ(target)
            end
            if self.menu.combo.e:get() and e then
                if self.menu.combo.ed:get() == 1 then
                    if e and d <= 600 then
                        self:DelayAction(function()myHero.spellbook:CastSpell(2, pwHud.hudManager.virtualCursorPos) end, 0.5)
                    end
                elseif self.menu.combo.ed:get() == 2 then
                    self:CastE(target)
                end
            end
        end
    end
    if LegitOrbwalker:GetMode() == "Harass" then
        if target and ValidTarget(target) then
            if self.menu.harass.q:get() then         
                self:CastQ(target)
            end          
        end
    end
    self:KillSteal()
    self:Run()
    if self.menu.Key.manual:get() and myHero.spellbook:CanUseSpell(3) == 0 then
        self:MoveToMouse()
        self:MR()
    end
end


function Fizz:Hex(a, r, g, b)
    return string.format("0x%.2X%.2X%.2X%.2X", a, r, g, b)
end

function Fizz:GetTarget(dist, all)
    self.TS.ValidTarget = function(unit)
        return _G.Prediction.IsValidTarget(unit, dist)
    end
    local res = self.TS:update()
    if all then
        return res
    else
        if res and res[1] then
            return res[1]
        end
    end
end

if myHero.charName == "Fizz" then
    Fizz:__init()
end
