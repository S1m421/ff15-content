local Kog = {}

local version = 1
if tonumber(GetInternalWebResult("QWER-Kog.version")) > version then 
    DownloadInternalFile("QWER-Kog.lua", SCRIPT_PATH.."QWER-Kog.lua")
    PrintChat("New version! V: "..tonumber(GetInternalWebResult("QWER-Kog.version")).." Press F5")
end

require'FF15-PremiumPrediction'
require"FF15Menu"
require"Vector"

function Kog:__init()
    self.Skills = {
        [0] = 
        {
            Data = {Speed = 1650, Range = 1175, Width = 70, Delay = 0.25}, 
            Dmg = function(Unit) return self:CalcDmg(myHero, Unit, 0, 30+50*myHero.spellbook:Spell(0).level+self:TotalAp(myHero)*0.5) end,
        },
        [1] = 
        {
            Data = {Range = function(Unit) return (myHero.characterIntermediate.attackRange+110+20*myHero.spellbook:Spell(1).level) end, Delay = 0.25},
            Dmg = function(Unit) return 0 end,

        },
        [2] = 
        {
            Data = {Speed = 1400, Range = 1280, Width = 120, Delay = 0.25}, 
            Dmg = function(Unit) return self:CalcDmg(myHero, Unit, 0, 30+30*myHero.spellbook:Spell(0).level+self:TotalAp(myHero)*0.4) end,
        },
        [3] = 
        {
            Data = {Speed = 1600, Range = function() return 1050+250*myHero.spellbook:Spell(3).level end, Width = 50, Delay = 0.25},
            Dmg = function(Unit) return Unit.healthPercent <= 40 and self:CalcDmg(myHero, Unit, 0, ((60+40*myHero.spellbook:Spell(3).level+self:TotalAd(myHero)*0.65+self:TotalAp(myHero)*0.25)+(60+40*myHero.spellbook:Spell(3).level+self:TotalAd(myHero)*0.65+self:TotalAp(myHero)*0.25*math.min(0.60,((100-Unit.healthPercent))*0.833)/100))*2) or 
                self:CalcDmg(myHero, Unit, 0, (60+40*myHero.spellbook:Spell(3).level+self:TotalAd(myHero)*0.65+self:TotalAp(myHero)*0.25)+(60+40*myHero.spellbook:Spell(3).level+self:TotalAd(myHero)*
                0.65+self:TotalAp(myHero)*0.25*math.min(0.6,((100-Unit.healthPercent))*0.833/100))) end,
        },
    }
    self.AM = nil
    self.CDS ={
        [0] = true,
        [1] = true,
        [2] = true,
        [3] = true
    }
    self.prio = 
    {
        [1] = {"Ahri", "Anivia", "Annie", "Ashe", "Azir", "Brand", "Caitlyn", "Cassiopeia", "Corki", "Draven", "Ezreal", "Graves", "Jinx", "Kalista", "Karma", "Karthus", "Katarina", "Kennen", "KogMaw", "Kindred", "Leblanc", "Lucian", "Lux", "Malzahar", "MasterYi", "MissFortune", "Orianna", "Quinn", "Sivir", "Syndra", "Talon", "Teemo", "Tristana", "TwistedFate", "Twitch", "Varus", "Vayne", "Veigar", "Velkoz", "Viktor", "Xerath", "Zed", "Ziggs", "Jhin", "Soraka", "Zoe", "Kaisa"},
        [2] = {"Akali", "Diana", "Ekko", "FiddleSticks", "Fiora", "Gangplank", "Fizz", "Heimerdinger", "Jayce", "Kassadin", "Kayle", "Khazix", "Lissandra", "Mordekaiser", "Nidalee", "Riven", "Shaco", "Vladimir", "Yasuo", "Zilean", "Zyra", "Ryze", "Rengar"},
        [3] = {"Kled","Aatrox", "Darius", "Elise", "Evelynn", "Galio", "Gragas", "Irelia", "Jax", "Lee Sin", "Morgana", "Nocturne", "Pantheon", "Rumble", "Swain", "Trundle", "Tryndamere", "Udyr", "Urgot", "Vi", "XinZhao", "RekSai", "Bard", "Nami", "Sona", "Camille", "JarvanIV", "Kayn"},
        [4] = {"PracticeTool_TargetDummy","Alistar", "Amumu", "Blitzcrank", "Braum", "Chogath", "DrMundo", "Garen", "Gnar", "Maokai", "Hecarim", "Janna", "Leona", "Lulu", "Malphite", "Nasus", "Nautilus", "Nunu", "Olaf", "Rammus", "Renekton", "Sejuani", "Shen", "Shyvana", "Singed", "Sion", "Skarner", "Taric", "TahmKench", "Thresh", "Volibear", "Warwick", "MonkeyKing", "Yorick", "Zac", "Poppy", "Ornn"},
    }
    self:Menu()
    self.target = nil
    AddEvent(Events.OnTick,function() self:OnTick() end)
    AddEvent(Events.OnDraw, function() self:OnDraw() end)
    AddEvent(Events.OnBuffUpdate, function(Unit, buff) self:OnBuffUpdate(Unit, buff) end)
    AddEvent(Events.OnBuffGain, function(Unit, buff) self:OnBuffGain(Unit, buff) end)
    AddEvent(Events.OnBuffLost, function(Unit, buff) self:OnBuffLost(Unit, buff) end)
    PrintChat("Welcome to QWER-Kog")
    self.font = DrawHandler:CreateFont("Calibri", 10)
    self.Rbuff = 0
end

function Kog:Menu()
    self.AM = Menu("AM", "QWER-Kog")
    self.AM:sub("CO", "Champion Options")
        self.AM.CO:checkbox("Q", "Use Q", true)
        self.AM.CO:slider("QS", "Q Hitchance", 1, 100, 25)
        self.AM.CO:checkbox("W", "Use W", true)
        self.AM.CO:checkbox("E", "Use E", true)
        self.AM.CO:slider("ES", "E Hitchance", 1, 100, 25)--self.AM.CO.ES:get()
        self.AM.CO:sub("RO", "R Options")
            self.AM.CO.RO:checkbox("R", "Use R", true)
            self.AM.CO.RO:slider("RS", "R Hitchance", 1, 100, 25)--self.AM.CO.ES:get()
            self.AM.CO.RO:slider("RC", "Stop using R at ", 1, 10, 4)
        self.AM.CO:sub("KS", "Ks options")
            self.AM.CO.KS:checkbox("Q", "Use Q", true)
            self.AM.CO.KS:checkbox("E", "Use E", true)
            self.AM.CO.KS:checkbox("R", "Use R", true)
    self.AM:sub("DO", "Draw Options")
        self.AM.DO:sub("QO", "Q Options")
            self.AM.DO.QO:checkbox("Q", "Draw Q", true)
            self.AM.DO.QO:slider("QA", "Apha", 1, 255, 150)
            self.AM.DO.QO:slider("QR", "Red", 1, 255, 150)
            self.AM.DO.QO:slider("QG", "Green", 1, 255, 150)
            self.AM.DO.QO:slider("QB", "Blue", 1, 255, 150)
        self.AM.DO:sub("WO", "W Options")
            self.AM.DO.WO:checkbox("W", "Draw W", true)
            self.AM.DO.WO:slider("WA", "Apha", 1, 255, 150)
            self.AM.DO.WO:slider("WR", "Red", 1, 255, 150)
            self.AM.DO.WO:slider("WG", "Green", 1, 255, 150)
            self.AM.DO.WO:slider("WB", "Blue", 1, 255, 150)
        self.AM.DO:sub("EO", "E Options")
            self.AM.DO.EO:checkbox("E", "Draw E", true)
            self.AM.DO.EO:slider("EA", "Apha", 1, 255, 150)
            self.AM.DO.EO:slider("ER", "Red", 1, 255, 150)
            self.AM.DO.EO:slider("EG", "Green", 1, 255, 150)
            self.AM.DO.EO:slider("EB", "Blue", 1, 255, 150)
        self.AM.DO:sub("RO", "R Options")
            self.AM.DO.RO:checkbox("R", "Draw R", true)
            self.AM.DO.RO:slider("RA", "Apha", 1, 255, 150)
            self.AM.DO.RO:slider("RR", "Red", 1, 255, 150)
            self.AM.DO.RO:slider("RG", "Green", 1, 255, 150)
            self.AM.DO.RO:slider("RB", "Blue", 1, 255, 150)
    self.AM:sub("TS", "Target Selector")
            for k, v in ipairs(ObjectManager:GetEnemyHeroes()) do
                if v then
                    for z, x in ipairs(self.prio) do
                        for a = 1, #x, 1 do
                            if x[a] == v.charName then 
                                self.AM.TS:slider("K"..v.charName, "Priority on "..v.charName, 1, 4, z)
                            end
                        end
                    end
                end
            end
end

function Kog:OnTick()
    for i =0, 3, 1 do
        if self:CanUseSpell(i) then
            self.CDS[i] = true
        else
            self.CDS[i] = false
        end
    end

    if LegitOrbwalker:GetMode() == "Combo" then
        self:CastE(LegitOrbwalker:GetTarget(self.Skills[2].Data.Range,"AD",myHero.position))
        if LegitOrbwalker:GetTarget(self.Skills[0].Data.Range,"AD",myHero.position) and (self:GetDistance(LegitOrbwalker:GetTarget(self.Skills[0].Data.Range,"AD",myHero.position)) > (myHero.characterIntermediate.attackRange+myHero.boundingRadius)) or LegitOrbwalker:AutoAttackCooldown() > 0.25 and self.AM.CO.Q:get() then
            self:CastQ(LegitOrbwalker:GetTarget(self.Skills[0].Data.Range,"AD",myHero.position))
        end
        self:CastW(LegitOrbwalker:GetTarget(self.Skills[1].Data.Range(),"AD",myHero.position))
        if LegitOrbwalker:GetTarget(self.Skills[3].Data.Range(),"HYB",myHero.position) then
            if self:GetDistance(LegitOrbwalker:GetTarget(self.Skills[3].Data.Range(),"HYB",myHero.position)) > (myHero.characterIntermediate.attackRange+myHero.boundingRadius) or LegitOrbwalker:AutoAttackCooldown() > 0.25 then
                self:CastR(LegitOrbwalker:GetTarget(self.Skills[3].Data.Range(),"HYB",myHero.position))
            end
        end
    end

    if LegitOrbwalker:GetMode() == "Harass" then
        self:CastR(LegitOrbwalker:GetTarget(self.Skills[3].Data.Range(),"HYB",myHero.position))      
    end
    self:Ks()
    print(self.Rbuff)
end



function Kog:OnDraw()
    if LegitOrbwalker:GetTarget() then
        DrawHandler:Circle3D(LegitOrbwalker:GetTarget().position, 50,self:Hex(255,255,255,255))
    end

    if self.AM.DO.QO.Q:get() then
        DrawHandler:Circle3D(myHero.position, self.Skills[0].Data.Range,self:Hex(self.AM.DO.QO.QA:get(),self.AM.DO.QO.QR:get(),self.AM.DO.QO.QG:get(),self.AM.DO.QO.QB:get()))
    end

    if self.AM.DO.WO.W:get() then
        DrawHandler:Circle3D(myHero.position, self.Skills[1].Data.Range(),self:Hex(self.AM.DO.WO.WA:get(),self.AM.DO.WO.WR:get(),self.AM.DO.WO.WG:get(),self.AM.DO.WO.WB:get()))
    end

    if self.AM.DO.EO.E:get() then
        DrawHandler:Circle3D(myHero.position, self.Skills[2].Data.Range,self:Hex(self.AM.DO.EO.EA:get(),self.AM.DO.EO.ER:get(),self.AM.DO.EO.EG:get(),self.AM.DO.EO.EB:get()))
    end

    if self.AM.DO.RO.R:get() then
        DrawHandler:Circle3D(myHero.position, self.Skills[3].Data.Range(),self:Hex(self.AM.DO.RO.RA:get(),self.AM.DO.RO.RR:get(),self.AM.DO.RO.RG:get(),self.AM.DO.RO.RB:get()))
    end    
end

function Kog:Ks()
    for k, v in ipairs(ObjectManager:GetEnemyHeroes()) do
        if v and not self:ValidTarget(v, self.Skills[3].Data.Range()) then return end
        if v.health < self.Skills[0].Dmg(v) and self.AM.CO.KS.Q:get() and self:ValidTarget(v, self.Skills[0].Data.Range) then
            self:CastQ(v)
        end
        if v.health < self.Skills[2].Dmg(v) and self.AM.CO.KS.E:get() and self:ValidTarget(v, self.Skills[2].Data.Range) then
            self:CastE(v)
        end
        if v.health < self.Skills[3].Dmg(v) and self.AM.CO.KS.R:get() and self:ValidTarget(v, self.Skills[3].Data.Range()) then
            self:CastR(v)
        end
    end
end

function Kog:CalcDmg(Unit, target, physical, magical, tdmg)
    local AArmor = target.characterIntermediate.armor
    local BArmor = target.characterIntermediate.bonusArmor
    local AMResist = target.characterIntermediate.spellBlock
    local Armor = AArmor + BArmor
    local Dmg = 0
    local truedmg = tdmg or 0
    if physical > 0 then
        Armor = Armor - Unit.characterIntermediate.flatArmorPenetration
        if Armor > 0 then
            Armor = AArmor * Unit.characterIntermediate.percentArmorPenetration + BArmor * Unit.characterIntermediate.percentArmorPenetration
            Armor = Armor - Unit.characterIntermediate.physicalLethality * (0.6 + 0.4 * myHero.experience.level / 18)
            if Armor < 0 then Armor = 0 end
        end
        if Armor > 0 then
            Dmg = Dmg + math.floor(physical*100/(100+Armor))
        else
            Dmg = Dmg + math.floor(physical*(2-(100/(100-Armor))))
        end
    end
    if magical ~= nil then
        AMResist = AMResist - Unit.characterIntermediate.flatMagicReduction
        if AMResist > 0 then
            if Unit.characterIntermediate.percentMagicReduction > 0 then
                AMResist = AMResist*Unit.characterIntermediate.percentMagicReduction
            end
            AMResist = AMResist*Unit.characterIntermediate.percentMagicPenetration
        end
        if AMResist - Unit.characterIntermediate.flatMagicPenetration >= 0 then
            AMResist = AMResist - Unit.characterIntermediate.flatMagicPenetration
        end
        if AMResist > 0 then
            Dmg = Dmg + math.floor(magical*100/(100+AMResist))
        else
            Dmg = Dmg + math.floor(magical*(2-(100/(100-AMResist))))
        end
    end
    return Dmg + truedmg
end

function Kog:CastQ(Unit)
    if not Unit then return end
    if not self:ValidTarget(Unit, self.Skills[0].Data.Range) or not self.CDS[0] then return end
    local CastPos, PredPos, HitChance, TimeToHit = PremiumPrediction:GetPrediction(myHero, Unit, self.Skills[0].Data.Speed, self.Skills[0].Data.Range, self.Skills[0].Data.Delay, self.Skills[0].Data.Width, 0, true)
    if CastPos and PredPos and HitChance >= self.AM.CO.QS:get()/100 then
        myHero.spellbook:CastSpell(0, CastPos:ToDX3())
    end
end

function Kog:CastW(Unit)
    if not Unit then return end
    if not self:ValidTarget(Unit, self.Skills[1].Data.Range()) or not self.CDS[1] or not self.AM.CO.W:get() then return end
    local CastPos, PredPos, HitChance, TimeToHit = PremiumPrediction:GetPrediction(myHero, Unit, math.huge, self.Skills[1].Data.Range(), self.Skills[1].Data.Delay, 0, 0, false)
    if self:GetDistance(PredPos) < self.Skills[1].Data.Range() then
        myHero.spellbook:CastSpell(1, myHero.networkId)
    end
end

function Kog:CastE(Unit)
    if not Unit then return end
    if not self:ValidTarget(Unit, self.Skills[2].Data.Range) or not self.CDS[2] or not self.AM.CO.E:get() then return end
    local CastPos, PredPos, HitChance, TimeToHit = PremiumPrediction:GetPrediction(myHero, Unit, self.Skills[2].Data.Speed, self.Skills[2].Data.Range, self.Skills[2].Data.Delay, self.Skills[2].Data.Width, 0, false)
    if CastPos and PredPos and HitChance >= self.AM.CO.ES:get()/100 then
        myHero.spellbook:CastSpell(2, CastPos:ToDX3())
    end
end

function Kog:CastR(Unit)
    if not Unit then return end
    if not self:ValidTarget(Unit, self.Skills[3].Data.Range()) or not self.CDS[3] then return end
    local CastPos, PredPos, HitChance, TimeToHit = PremiumPrediction:GetPrediction(myHero, Unit, self.Skills[3].Data.Speed, self.Skills[3].Data.Range(), self.Skills[3].Data.Delay, self.Skills[3].Data.Width, 0, false)
    if CastPos and PredPos and HitChance >= self.AM.CO.RO.RS:get()/100 then
        myHero.spellbook:CastSpell(3, CastPos:ToDX3())
    end    
end

function Kog:ValidTarget(unit, distance)
    return unit.isValid and not unit.isDead and not unit.isInvulnerable and unit.isTargetable and not unit.isZombie and unit.team ~= myHero.team and (self:GetDistance(unit) <= distance)
end

function Kog:GetDistance(v1,v2)
    v1 = v1.position or Vector(v1)
    v2 = v2 or myHero.position
    return math.sqrt((v2.x-v1.x)^2 + (v2.z-v1.z)^2)
end

function Kog:Hex(a,r,g,b)
	return string.format("0x%.2X%.2X%.2X%.2X",a,r,g,b)
end

function Kog:TotalAp(unit)
    return math.ceil(unit.characterIntermediate.flatMagicDamageMod + unit.characterIntermediate.flatMagicDamageMod*unit.characterIntermediate.percentMagicDamageMod)
end

function Kog:TotalAd(Unit)
    return math.floor(myHero.characterIntermediate.baseAttackDamage+myHero.characterIntermediate.flatPhysicalDamageMod)
end

function Kog:OnBuffUpdate(Unit, buff)
    if Unit.charName == myHero.charName and buff.name == "kogmawlivingartillerycost" then
       self.Rbuff = math.min(10, buff.count + 1)
    end
end 
function Kog:OnBuffGain(Unit, buff)
    if Unit.charName == myHero.charName and buff.name == "kogmawlivingartillerycost" then
        self.Rbuff = 1
    end
end

function Kog:OnBuffLost(Unit, buff)
    if Unit == myHero and buff.name == "kogmawlivingartillerycost" then
        self.Rbuff = 0
    end
end

function Kog:CanUseSpell(int)
    return myHero.spellbook:CanUseSpell(int) == 0
end

if myHero.charName == "KogMaw" then
    Kog:__init()
end