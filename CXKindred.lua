local Kindred = {}
local version = 1
if tonumber(GetInternalWebResult("CXKindred.version")) > version then
    DownloadInternalFile("CXKindred.lua", SCRIPT_PATH .. "CXKindred.lua")
    PrintChat("New version:" .. tonumber(GetInternalWebResult("CXKindred.version")) .. " Press F5")
end
require "FF15Menu"
require "utils"
local DreamTS = require("DreamTS")
local dmgLib = require("FF15DamageLib")

function OnLoad()
    if not _G.Prediction then
        LoadPaidScript(PaidScript.DREAM_PRED)
    end
end

function Kindred:__init()
    self.w = {
        type = "circular",
        speed = 1400,
        range = 500,
        delay = 0.25,
        radius = 100
    }
    self:Menu()
    self.TS =
        DreamTS(
        self.menu.dreamTs,
        {
            ValidTarget = function(unit)
                return _G.Prediction.IsValidTarget(unit)
            end,
            Damage = function(unit)
                return dmgLib:CalculateMagicDamage(myHero, unit, 100)
            end
        }
    )
    AddEvent(Events.OnTick, function() self:OnTick() end)
    AddEvent(Events.OnDraw, function() self:OnDraw() end)
    AddEvent(Events.OnExecuteCastFrame, function(unit, spell) self:OnExecuteCastFrame(unit, spell) end)
    PrintChat("<font color=\"#E41B17\">[<b>¤ Cyrex ¤</b>]:</font>" .. " <font color=\"#" .. "FFFFFF" .. "\">" .. "Kindred Loaded" .. "</font>")
    self.font = DrawHandler:CreateFont("Calibri", 12)
end

function Kindred:Menu()
    self.menu = Menu("cxKindred", "Cyrex Kindred")
    self.menu:sub("dreamTs", "Target Selector")

    self.menu:sub("Key", "Key Settings")
        self.menu.Key:key("run", "Flee", string.byte("S"))
        self.menu.Key:key("manual", "Panic R", string.byte("T"))
        self.menu.Key:checkbox("time", "Time", true, string.byte("A"))

    self.menu:sub("combo", "Combo Settings")
        self.menu.combo:label("xd", "Q Settings")
        self.menu.combo:checkbox("q", "Use Q", true)
        self.menu.combo:list("mode", "Choose Mode: ", 1, {"To Mouse", "To Target"})
        self.menu.combo:label("xd1", "W Settings")
        self.menu.combo:checkbox("w", "Use W", true)
        self.menu.combo:label("xd2", "E Settings")
        self.menu.combo:checkbox("e", "Use E", true)
        self.menu.combo:list("modee", "Choose Mode: ", 1, {"Smart", "Always"})
        self.menu.combo:label("sd", "R Settings")
        self.menu.combo:checkbox("r", "Use R", true)
        self.menu.combo:slider("rhp", "What HP% to Ult", 0, 100, 10, 5)
        self.menu.combo:slider("rxe", "R if X Enemys in Range", 0, 5, 1, 1)
        self.menu.combo:slider("rxa", "R if X Allys in Range", 0, 5, 1, 1)

    self.menu:sub("jg", "Jungle Clear Settings")
        self.menu.jg:label("xd", "Jungle Settings")
        self.menu.jg:checkbox("q", "Use Q", true)
        self.menu.jg:checkbox("w", "Use W", true)
        self.menu.jg:checkbox("e", "Use E", true)
        self.menu.jg:slider("Mana", "Min. Mana Percent: ", 10, 0, 100, 5)

    self.menu:sub("draws", "Draw")
        self.menu.draws:checkbox("q", "Q", true)
        self.menu.draws:checkbox("e", "E", true)
    self.menu:label("version", "Version: " .. version .. "")
    self.menu:label("author", "Author: Coozbie")
end

function Kindred:OnDraw()
    if self.menu.draws.q:get() and myHero.spellbook:CanUseSpell(0) == 0 and myHero.visibleOnScreen then
        DrawHandler:Circle3D(myHero.position, 340, self:Hex(255,255,255,255))
    end
    if self.menu.draws.e:get() and myHero.spellbook:CanUseSpell(2) == 0 and myHero.visibleOnScreen then
        DrawHandler:Circle3D(myHero.position, (myHero.characterIntermediate.attackRange + myHero.boundingRadius), self:Hex(255,255,255,255))
    end
end

function Kindred:GetPercentHealth(obj)
  local obj = obj or myHero
  return (obj.health / obj.maxHealth) * 100
end

function Kindred:MoveToMouse()
    local pos = pwHud.hudManager.virtualCursorPos
    myHero:IssueOrder(GameObjectOrder.MoveTo, pos)
end

function Kindred:ValidTarget(object, distance)   --Please dont change this -RMAN
    return object and object.isValid and object.team ~= myHero.team and object.isVisible and not object.buffManager:HasBuff('SionPassiveZombie') and not object.buffManager:HasBuff('FioraW') and not object.isDead and not object.isInvulnerable and (not distance or GetDistanceSqr(object) <= distance * distance)
end

function Kindred:GetEnemyHeroesInRange(range, pos)
  local pos = pos or myHero.position
  local h = {}
  local enemies = ObjectManager:GetEnemyHeroes()
  for i = 1, #enemies do
    local hero = enemies[i]
    if hero and hero.team ~= myHero.team and not hero.isInvulnerable and not hero.isDead and hero.isVisible and hero.isTargetable and GetDistanceSqr(hero) < range * range then
      h[#h + 1] = hero
    end
  end
  return h
end

function Kindred:GetAllyHeroesInRange(range, pos)
  local pos = pos or myHero.position
  local h = {}
  local allies = ObjectManager:GetAllyHeroes()
  for i = 1, #allies do
    local hero = allies[i]
    if hero and hero.team == myHero.team and not hero.isInvulnerable and not hero.isDead and hero.isVisible and hero.isTargetable and GetDistanceSqr(hero) < range * range then
      h[#h + 1] = hero
    end
  end
  return h
end

function Kindred:OnExecuteCastFrame(unit, spell)
    if unit ~= myHero or not spell.spellData.name:lower():find("attack") then return end
    local target = spell.target
    if not target then return end
    local range = myHero.characterIntermediate.attackRange + (myHero.boundingRadius + target.boundingRadius)
    if LegitOrbwalker:GetMode() == "Harass" or LegitOrbwalker:GetMode() == "Combo" then
        if self.menu.combo.q:get() and myHero.spellbook:CanUseSpell(0) == 0 and self:ValidTarget(target) and GetDistance(myHero, target) <= range then
            myHero.spellbook:CastSpell(0, pwHud.hudManager.virtualCursorPos)
        end
    end
end

function Kindred:CastQ(target)
    if myHero.spellbook:CanUseSpell(0) == 0 then
        local range = myHero.characterIntermediate.attackRange + (myHero.boundingRadius + target.boundingRadius)
        if GetDistance(target) < range then
            myHero.spellbook:CastSpell(0, target.position)
        end
    end
end

function Kindred:CastW(target)
    if myHero.spellbook:CanUseSpell(1) == 0 then
        local pred = _G.Prediction.GetPrediction(target, self.w, myHero)
        if pred and pred.castPosition and GetDistanceSqr(pred.castPosition) <= self.w.range * self.w.range then
            myHero.spellbook:CastSpell(1, pred.castPosition)
        end
    end
end

function Kindred:AutoUlt()
    if self.menu.combo.r:get() and myHero.spellbook:CanUseSpell(3) == 0 then
        if #self:GetEnemyHeroesInRange(500) >= self.menu.combo.rxe:get() and #self:GetAllyHeroesInRange(500) >= self.menu.combo.rxa:get() and self:GetPercentHealth() <=  self.menu.combo.rhp:get() then
            myHero.spellbook:CastSpell(3, myHero.networkId)
        elseif #self:GetEnemyHeroesInRange(500) > 0 and self:GetPercentHealth(player) <= self.menu.combo.rhp:get() then
            myHero.spellbook:CastSpell(3, myHero.networkId)
        end
    end
end

function Kindred:OutAA()
    if LegitOrbwalker:GetMode() == "Harass" or LegitOrbwalker:GetMode() == "Combo" then
        for i, enemy in pairs(ObjectManager:GetEnemyHeroes()) do
            local range = 320 + myHero.characterIntermediate.attackRange + (myHero.boundingRadius + 70)
            local range2 = myHero.characterIntermediate.attackRange + (myHero.boundingRadius + 65)
            if enemy and enemy.team ~= myHero.team and not enemy.isInvulnerable and not enemy.isDead and enemy.isVisible and enemy.isTargetable then
                if self.menu.combo.q:get() and myHero.spellbook:CanUseSpell(0) == 0 and GetDistance(enemy) < range and GetDistance(enemy) > range2 then
                    myHero.spellbook:CastSpell(0, pwHud.hudManager.virtualCursorPos)
                end
            end
        end
    end
end

function Kindred:Run()
    if self.menu.Key.run:get() then
        self:MoveToMouse()
        if myHero.spellbook:CanUseSpell(0) == 0 then
            myHero.spellbook:CastSpell(0, pwHud.hudManager.virtualCursorPos)
        end
    end
end


function Kindred:OnTick()
    local target = self:GetTarget(myHero.characterIntermediate.attackRange + (myHero.boundingRadius + 65))
    if LegitOrbwalker:GetMode() == "Combo" then
        local q = myHero.spellbook:CanUseSpell(0) == 0
        local w = myHero.spellbook:CanUseSpell(1) == 0
        local e = myHero.spellbook:CanUseSpell(2) == 0
        local range = myHero.characterIntermediate.attackRange + (myHero.boundingRadius)
        local d = GetDistance(target)
        if target and self:ValidTarget(target) then
            if self.menu.combo.q:get() and q then
                if self.menu.combo.mode:get() == 2 then
                    self:CastQ(target)
                end
            end
            if self.menu.combo.w:get() and w and d < 500 then
                if myHero.spellbook:CanUseSpell(0) ~= 0 then         
                    self:CastW(target)
                elseif myHero.spellbook:CanUseSpell(2) ~= 0 and myHero.spellbook:CanUseSpell(0) ~= 0 then
                    self:CastW(target)
                end
            end
            if self.menu.combo.e:get() and e and d <= range then
                if self.menu.combo.modee:get() == 1 and d <= myHero.characterIntermediate.attackRange + (myHero.boundingRadius + target.boundingRadius) and d > 200 then
                    myHero.spellbook:CastSpell(2, target.networkId)
                elseif self.menu.combo.modee:get() == 2 then
                    myHero.spellbook:CastSpell(2, target.networkId)
                end
            end
        end
    end
    self:AutoUlt()
    self:OutAA()
    self:Run()
    if self.menu.Key.manual:get() then
        if self:GetPercentHealth() <=  40 and #self:GetEnemyHeroesInRange(700) > 0 then
            myHero.spellbook:CastSpell(3, myHero.networkId)
        end
    end
end


function Kindred:Hex(a, r, g, b)
    return string.format("0x%.2X%.2X%.2X%.2X", a, r, g, b)
end

function Kindred:GetTarget(dist, all)
    self.TS.ValidTarget = function(unit)
        return _G.Prediction.IsValidTarget(unit, dist)
    end
    local res = self.TS:update()
    if all then
        return res
    else
        if res and res[1] then
            return res[1]
        end
    end
end

if myHero.charName == "Kindred" then
    Kindred:__init()
end
