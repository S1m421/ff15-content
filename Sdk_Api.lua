Global Funcs
DWORD GetTickCount()
void PrintChat(const std::string& Message) 
bool IsKeyDown(int vKey) 
bool IsKeyPressed(int vKey) D3DXVECTOR2 GetCursorPos() 
void CreateFolder(std::string Path) 
bool DownloadFile(std::string Url, std::string Path) 
std::string GetWebResult(std::string Url) 
bool DownloadInternalFile(std::string ScriptName, std::string Path) 
std::string GetInternalWebResult(std::string ScriptName) 
void BlockOrder() (can only be used inside OnIssueOrder) 
void BlockCast() (can only be used inside OnSpellbookCastSpell) 

Global Vars
_G.SCRIPT_PATH (string returns scripts directory) 
_G.COMMON_PATH (string returns common directory) 
ScriptName  (string returns script name) 
myHero (Instance of class GameObject) 
ObjectManager (Instance of class ObjectManager) 
TacticalMap (Instance of class TacticalMap) 
Camera (Instance of class Camera) 
pwHud (Instance of class pwHud) 
MenuGUI (Instance of class MenuGUI) 
ChatConsole (Instance of class ChatConsole) 
MissionInfo (Instance of class MissionInfo) 
ClientFacade (Instance of class ClientFacade) 
RiotClock (Instance of class RiotClock) 
NavMesh (Instance of class NavMesh) 
NetClient (Instance of class NetClient) 
DrawHandler (Instance of class DrawHandler) 
HudPlayerInventory (Instance of class HudPlayerInventory)
 
Events
 OnLoad()
 OnUnload()
 AddEvent(EventId,function Callback)

Events enum :
 enum Events
 OnTick
 OnDraw
 OnReset
 OnProcessSpell
 OnNewPath
 OnBasicAttack
 OnCreateObject
 OnDeleteObject
 OnBuffUpdate
 OnBuffGain
 OnBuffLost
 OnVisionGain
 OnVisionLost
 OnTeleport
 OnIssueOrder
 OnWndProc
 OnSpellbookCastSpell
 OnPlayAnimation
 OnUpdateChargeableSpell
 OnExecuteCastFrame
 OnStopCastSpell

Events params :
OnTick()
OnDraw()
OnReset()
OnProcessSpell(GameObject* Source, SpellCastInfo* Args) 
OnNewPath(GameObject* Source, lua_table Paths, bool isWalk, float dashSpeed) 
OnBasicAttack(GameObject* Source, SpellCastInfo *Spell) 
OnCreateObject(GameObject* Obj, int NetworkId) 
OnDeleteObject(GameObject* Obj) 
OnBuffUpdate(GameObject* Obj, BuffInstance * Buff) 
OnBuffGain(GameObject* Obj, BuffInstance * Buff) 
OnBuffLost(GameObject* Obj, BuffInstance * Buff)
OnVisionGain(GameObject* Obj) 
OnVisionLost(GameObject* Obj) 
OnTeleport(GameObject* Obj, std::string recallType, std::string recallName) 
OnIssueOrder(DWORD Order, D3DXVECTOR3 ToPosition, GameObject * Target) 
OnWndProc(HWND hWnd, UINT Message, WPARAM wParam, LPARAM lParam) 
OnSpellbookCastSpell(int Slot, D3DXVECTOR3 StartPos, D3DXVECTOR3 EndPos, GameObject * Target) 
OnPlayAnimation(GameObject* Source, std::string Name) 
OnUpdateChargeableSpell(int Slot, D3DXVECTOR3 Position, bool releaseCast) 
OnExecuteCastFrame(GameObject* Obj, SpellCastInfo* SpellInfo) 
OnStopCastSpell(GameObject* Obj, SpellCastInfo* SpellCast)

Classes
 class D3DXVECTOR4
 float x
 float y
 float z
 float w

 class D3DXVECTOR3
 float x
 float y
 float z

 class D3DXVECTOR2
 float x
 float y

 class Renderer
 DWORD width
 DWORD height
 D3DXVECTOR2 WorldToScreen(D3DXVECTOR3 p_World)

 class ObjectManager
 GameObject* GetUnitByNetworkId(int netID) 
 GameObject* GetUnitByIndex(int idx) 
 lua_table GameObject GetEnemyHeroes() 
 lua_table GameObject  GetAllyHeroes() 
 lua_table GameObject GetEnemyMinions() 
 lua_table GameObject  GetAllyMinions() 
 lua_table GameObject GetEnemyTurrets() 
 lua_table GameObject  GetAllyTurrets() 
 lua_table GameObject  GetObjectsByType(int type) 

class TacticalMap
 float minimapX // READ+WRITE 
 float minimapY // READ+WRITE 
 float width // READ+WRITE 
 float height // READ+WRITE 
 float scaleX // READ+WRITE 
 float scaleY // READ+WRITE 
 D3DXVECTOR2 WorldToMinimap(D3DXVECTOR3 worldCoord) 

class Camera
 float x // READ+WRITE 
 float y // READ+WRITE 
 float pitch // READ+WRITE 
 float yawX // READ+WRITE 
 float yawY // READ+WRITE 
 float viewportDistance // READ+WRITE 
 float zoomDistance

class pwHud
 bool isLocked // READ+WRITE 
 bool isWindowFocused 
 HudManager* hudManager 
 TargetManager* targetManager
 
class HudManager
 D3DXVECTOR3 activeVirtualCursorPos 
 D3DXVECTOR3 virtualCursorPos
 
class TargetManager
 GameObject* target
 class ChatConsole
 bool isChatOpen

class MenuGUI
 empty

class MissionInfo
 int gameType
 int mapId
 int gameId

class ClientFacade
 std::string region
 int port
 DWORD gameID
 std::string IP
 class RiotClock
 float time

class NavMesh
 float cellWidth
 float cellHeight
 int heightDelta
 int widthDelta
 int GetCollisionFlags(D3DXVECTOR3 position) 
 float QueryHeightForPosition(D3DXVECTOR3 Position) 
 bool IsWall(D3DXVECTOR3 position) 
 bool IsBuilding(D3DXVECTOR3 position) 
 bool IsGrass(D3DXVECTOR3 position)
 
class NetClient
 int ping

class DrawHandler
 FontHandle defaultFont 
 FontHandle CreateFont(std::string family, long size) 
 SpriteHandle CreateSprite(std::string filepath, float width, float height) 
 void FilledRect(D3DXVECTOR4 rect, D3DCOLOR color) 
 void Rect(D3DXVECTOR4 rect, float stroke_width, D3DCOLOR color) 
 void OutlinedRect(D3DXVECTOR4 rect, float stroke_width, D3DCOLOR outline, D3DCOLOR rectc) 
 void Line(D3DXVECTOR2 from, D3DXVECTOR2 to, D3DCOLOR color) 
 void Circle(D3DXVECTOR2 position, float radius, D3DCOLOR color) 
 void Circle3D(D3DXVECTOR3 position, float radius, D3DCOLOR color) 
 void Pixel(D3DXVECTOR2 position, D3DCOLOR color) 
 void Pixels(D3DXVECTOR2 position, float square, D3DCOLOR color) 
 void Text(FontHandle font, D3DXVECTOR2 position, std::string text, D3DCOLOR color) 
 void Sprite(SpriteHandle sprite, D3DXVECTOR2 position, float alpha) 
 D3DXVECTOR2 GetTextExtent(FontHandle font, std::string text) 

class SpellCastInfo
 D3DXVECTOR3 startPos 
 D3DXVECTOR3 endPos
 int spellSlot
 GameObject* target
 int level
 SpellData * spellData 

class SpellData
 std::string name
 float speed
 float width
 float overrideCastRange 

class GameObject
 MissileClientObj* asMissile 
 bool isValid
 bool isDead
 bool visibleOnScreen 
 float attackShield
 float health
 bool isInvulnerable
 bool isTargetable
 bool isZombie
 float allShield
 float magicShield
 float mana
 float maxHealth
 float maxMana
 D3DXVECTOR3 direction 
 bool isMelee
 bool isRanged
 float healthPercent
 float manaPercent
 BuffManager* buffManager 
 int networkId
 float attackCastDelay 
 float attackDelay
 int characterActionState 
 CharacterIntermediate* characterIntermediate 
 DWORD combatType
 int evolvePoints
 float gold
 float goldTotal
 std::string skinName 
 float percentDamageToBarracksMinionMod 
 float flatDamageReductionFromBarracksMinionMod 
 Experience* experience 
 std::string charName 
 int neutralMinionsKilled 
 int campNumber
 int minionLevel
 int originalState
 int roamState
 std::string dampenerState 
 D3DXVECTOR3 position 
 DWORD team
 std::string name
 int type
 HeroInventory* inventory 
 int index
 SpellData * basicAttack 
 AIManager_Client * aiManagerClient 
 UnitInfoComponent * infoComponent 
 void IssueOrder(DWORD order, D3DXVECTOR3 toPosition) 
 void IssueOrder(DWORD order, GameObject* target) 
 void IssueOrder(DWORD order, D3DXVECTOR3 toPosition, GameObject* target, bool isPetCommand) 
 void UseObject(GameObject* target) 
 Spellbook * spellbook 
 float boundingRadius 
 bool isVisible
 bool canAttack
 bool canCast
 bool canMove
 Avatar * avatar

class MissileClientObj 
 D3DXVECTOR3 launchPos 
 D3DXVECTOR3 destPos
 std::string name
 DWORD slot
 GameObject* spellCaster 
 GameObject* target
 D3DXVECTOR3 position 
 float speed

class BuffManager
 lua_table BuffInstance buffs 
 BuffInstance * HasBuff(std::string buffName) 
 bool HasBuffOfType(int Type) 

class BuffInstance
 std::string name
 byte type
 int count
 float remainingTime
 bool isPermanent
 GameObject* caster

class CharacterIntermediate 
 float baseAttackDamage 
 float baseAbilityDamage 
 float dodge
 float crit
 float armor
 float spellBlock
 float hpRegenRate
 float parRegenRate
 float movementSpeed
 float attackRange
 float flatPhysicalDamageMod 
 float flatMagicDamageMod 
 float percentMagicDamageMod 
 float flatMagicReduction 
 float percentMagicReduction 
 float baseAttackSpeed 
 float attackSpeedMod 
 float flatCastRangeMod 
 float percentCooldownMod  
 float passiveCooldownEndTime 
 float passiveCooldownTotalTime 
 float flatArmorPenetration 
 float percentArmorPenetration 
 float flatMagicPenetration 
 float percentMagicPenetration 
 float percentLifeStealMod 
 float percentSpellVampMod 
 float percentBonusArmorPenetration 
 float percentCritBonusArmorPenetration 
 float percentCritTotalArmorPenetration 
 float percentBonusMagicPenetration 
 float physicalLethality 
 float magicLethality 
 float baseHPRegenRate 
 float primaryARBaseRegenRateRep 
 float secondaryARRegenRateRep 
 float secondaryARBaseRegenRateRep 
 float percentCooldownCapMod 
 float percentCCReduction 
 float percentEXPBonus 
 float flatBaseAttackDamageMod 
 float percentBaseAttackDamageMod 
 float baseAttackDamageSansPercentScale 
 float bonusArmor
 float bonusSpellBlock
 
class Experience
 float experience
 int level
 int spellTrainingPoints

class HeroInventory
 InventorySlot * InventorySlot(int slot)
 ItemNode* HasItem(int itemId)

class InventorySlot
 InventorySlotNode* inventorySlotNode 
 int ammo

class InventorySlotNode 
 ItemNode * itemNode

class ItemNode
 int itemId
 int price
 int maxStacks
 int spellSlot

 class AIManager_Client
NavigationPath* navPath
 
class NavigationPath 
 lua_table D3DXVECTOR3 paths 
 bool isMoving
 D3DXVECTOR3 PathIndex(int idx) 
 float dashSpeed

class UnitInfoComponent 
 D3DXVECTOR3 drawBasePosition 
 D3DXVECTOR2 hpBarScreenPosition 
 
class Spellbook
 int activeSpellSlot
 int selectedSpellSlot
 bool SpellSlotCanBeUpgraded(int spellSlot) 
 void LevelSpell(int spellSlot) 
 SpellDataInst* Spell(int spellSlot) 
 void EvolveSpell(int spellSlot) 
 void CastSpell(int slot, DWORD networkId) 
 void CastSpell(int slot, D3DXVECTOR3& position) 
 void CastSpell(int slot, D3DXVECTOR3& startPos, D3DXVECTOR3& endPos) 
 void UpdateChargeableSpell(int slot, D3DXVECTOR3& position, bool releaseCast) 
 int CanUseSpell(int spellSlot) 
 GameObject* owner
 SpellCasterClient* spellCasterClient 

class SpellDataInst
 int level
 float cooldownTimeRemaining 
 int currentAmmoCount 
 float ammoRechargeTimeRemaining 
 int toggleState
 std::string name
 SpellData * spellData 
 float castRange
 float castRadius

class SpellCasterClient 
 float castEndTime
 bool isAutoAttacking 
 bool isCastingSpell
 bool isChanneling
 bool isCharging
 bool isStopped
 bool spellWasCast

class Avatar
 lua_table Perk perks 
 Perk * HasPerk(int Id) 

class Perk
 int id
 std::string name

class HudPlayerInventory 
 int GetItemAmmo(int Slot) 


Enums

 enum SpellSlot
 UnknownSlot
 Q W E R Summoner1
 Summoner2
 Item1
 Item2
 Item3
 Item4
 Item5
 Item6
 Trinket
 Recall
 OathSworn
 CapturePoint
 Internal
 enum SpellState
 Ready
 NotAvailable
 Surpressed
 NotLearned
 Disabled
 Cooldown
 NoMana
 UnknownState

 enum GameObjectType
 NeutralMinionCamp
 FollowerObject
 FollowerObjectWithLerpMovement
 AIHeroClient
 obj_AI_Marker
 obj_AI_Minion
 LevelPropAI
 obj_AI_Turret
 obj_AI_TurretCommon
 obj_GeneralParticleEmitter
 TypeGameObject
 MissileClient
 DrawFX
 UnrevealedTarget
 obj_BarracksDampener
 obj_Barracks
 obj_AnimatedBuilding
 obj_Building
 obj_Lake
 obj_Levelsizer
 obj_NavPoint
 obj_SpawnPoint
 obj_LampBulb
 GrassObject
 obj_HQ
 obj_InfoPoint
 LevelPropGameObject
 LevelPropSpawnerPoint
 obj_Shop
 obj_Turret

 enum GameObjectCharacterState
 CanAttack
 CanCast
 CanMove
 Immovable
 IsStealth
 RevealSpecificUnit
 Taunted
 Feared
 Fleeing
 StateSurpressed
 Asleep
 NearSight
 Ghosted
 GhostProof
 Charmed
 NoRender
 DodgePiercing
 DisableAmbientGold
 DisableAmbientXP
 ForceRenderParticles

 enum GameObjectOrder 
 HoldPosition
 MoveTo
 AttackUnit
 AutoAttackPet
 AutoAttack
 MovePet
 AttackTo
 Stop

 enum CollisionFlags
 None
 Grass
 Wall
 Building
 Prop
 GlobalVision

 enum BuffType
 InternalBuffType
 Aura
 CombatEnchancer
 CombatDehancer
 SpellShield
 Stun
 Invisibility
 Silence
 Taunt
 Polymorph
 Slow
 Snare
 Damage
 Heal
 Haste
 SpellImmunity
 PhysicalImmunity
 Invulnerability
 SleepBuffType
 NearSightBuffType
 Frenzy
 Fear
 Charm
 Poison
 Suppression
 Blind
 Counter
 Shred
 Flee
 Knockup
 Knockback
 Disarm