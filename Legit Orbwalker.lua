local _orbVERSION = 0.03056
local _orbVERSIONONLINE = GetInternalWebResult("LegitOrbwalker.version") 

local function DownloadNewVersion(currentVersion,onlineVersion)
	local currentVersion,onlineVersion = tonumber(currentVersion),tonumber(onlineVersion)
	if onlineVersion > currentVersion then
		PrintChat("<font color=\"#50ed8f\">[UPDATE]</font> <font color=\"#8f70f4\"><b>[Legit - Orbwalker: Rewrite2]</b></font><b> <font color=\"#50ed8f\"> v" ..onlineVersion.. " is available!</font>")
		DownloadInternalFile("Legit%20Orbwalker%20-%20Rewrite2.lua", SCRIPT_PATH .. "Legit Orbwalker - Rewrite2.lua")
	end
end
DownloadNewVersion(_orbVERSION,_orbVERSIONONLINE)


local FileExists = function(path)
	local file = io.open(path, 'r')
	if file then
		io.close(file)
		return true
	end
	return false
end

local damageLib
if FileExists(COMMON_PATH .. "FF15Menu.lua") then require 'FF15Menu'else print("[Legit - Orbwalker] - requires FF15Menu.lua in your //COMMON folder.") return end
if FileExists(COMMON_PATH .. "FF15DamageLib.lua") then damageLib = require 'FF15DamageLib' else print("[Legit - Orbwalker] - Downloading FF15DamageLib.lua in your //COMMON folder.") DownloadInternalFile("FF15DamageLib.lua", COMMON_PATH .. "FF15DamageLib.lua") return end

local huge, floor, sqrt, max, min = math.huge , math.floor , math.sqrt , math.max , math.min 
local abs, deg, acos = math.abs , math.deg , math.acos 
local format, find, lower = string.format , string.find , string.lower
local insert, remove, sort = table.insert, table.remove, table.sort
local Q, W, E, R =  0, 1, 2, 3
local TEAM_ALLY = myHero.team
local TEAM_JUNGLE = 300
local TEAM_ENEMY = 300 - TEAM_ALLY

local LegitCommon = {}
local LegitOrb = {}

-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------- C O M M O N -------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

function LegitCommon:Vec3(vec)
	return D3DXVECTOR3(vec.x, vec.y, vec.z)
end    

function LegitCommon:GetDistanceSqr(p1, p2)
	local dx, dy, dz = p1.x - p2.x, p1.y - p2.y, p1.z - p2.z 
	return dx * dx + dy * dy + dz * dz
end

function LegitCommon:GetDistance(p1, p2)
	return sqrt(self:GetDistanceSqr(p1, p2))
end	

function LegitCommon:GetDistanceToZero(p1,p2)
	return math.sqrt(math.pow((p1.position.x - p2.position.x),2) + 0 + math.pow((p1.position.z - p2.position.z),2))
end

function LegitCommon:GetDistanceToZeroPos(p1,p2)
	return math.sqrt(math.pow((p1.x - p2.x),2) + 0 + math.pow((p1.z - p2.z),2))
end

function LegitCommon:OnScreen(pos)
	local maxX,maxY = Renderer.width, Renderer.height
	local toScreen = Renderer:WorldToScreen(D3DXVECTOR3(pos.x, pos.y, pos.z))
	return toScreen.x < maxX and toScreen.x > 0 and toScreen.y < maxY and toScreen.y > 0
end

function LegitCommon:IsRunning(unit)
	return unit.position.x - floor(unit.position.x) ~= 0
end

function LegitCommon:Hex(a,r,g,b)
	return tonumber(format("0x%.2X%.2X%.2X%.2X",a,r,g,b))
end

-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------- O R B W A L K E R -------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

function LegitOrb:AA_ResetSpell()
	return {
	["ONSPELL"] = {	["Ashe"] = 			function (spell) if spell.spellSlot == 0 then
											local castDelay = myHero.attackCastDelay*(0.45 + self.attackSpeed/25)
											local cooldown = 1/self.attackSpeed - castDelay
											local passedTime = self.time - self.lastAttackCast - myHero.attackCastDelay*2
											local saveTime = cooldown - (cooldown/100)*40
											if saveTime > passedTime  then
												self.lastAttackCast = self.lastAttackCast - saveTime + castDelay*1.25  self.attackTick = self.attackTick - saveTime + castDelay*1.25
											end
										end end,
					["Blitzcrank"] = 	function (spell) if spell.spellSlot == 2 then
											self.aaReset = true
											self.lastAttackCast = 0
											self.attackTick = 0
										end end,
					["Camille"] = 		function (spell) if spell.spellSlot == 0 then
											self.aaReset = true
											self.lastAttackCast = 0
											self.attackTick = 0
										end end,
					["Darius"] = 		function (spell) if spell.spellSlot == 1 then
											self.aaReset = true
											self.lastAttackCast = 0
											self.attackTick = 0
										end end,
					["DrMundo"] =  		function (spell) if spell.spellSlot == 2 then
											self.aaReset = true
											self.lastAttackCast = 0
											self.attackTick = 0
										end end,
					["Ekko"] =   		function (spell) if spell.spellSlot == 2 then
											self.aaReset = true
											self.lastAttackCast = 0
											self.attackTick = 0
										end end,
					["Elise"] =   		function (spell) if spell.spellSlot == 1 and myHero.characterIntermediate.attackRange < 300 then
											self.aaReset = true
											self.lastAttackCast = 0
											self.attackTick = 0
										end end,
					["Evelynn"] =   	function (spell) if spell.spellSlot == 2 then
											self.aaReset = true
											self.lastAttackCast = 0
											self.attackTick = 0
										end end,
					["Fiora"] =   		function (spell) if spell.spellSlot == 2 then
											self.aaReset = true
											self.lastAttackCast = 0
											self.attackTick = 0
										end end,
					["Fizz"] =   		function (spell) if spell.spellSlot == 1 then
											self.aaReset = true
											self.lastAttackCast = 0
											self.attackTick = 0
										end end,
					["Garen"] =   		function (spell) if spell.spellSlot == 0 then
											self.aaReset = true
											self.lastAttackCast = 0
											self.attackTick = 0
										end end,
					["Graves"] =   		function (spell) if spell.spellSlot == 2 then
											self.aaReset = true
											self.lastAttackCast = 0
											self.attackTick = 0
										end end,
					["Illaoi"] =   		function (spell) if spell.spellSlot == 1 then
											self.aaReset = true
											self.lastAttackCast = 0
											self.attackTick = 0
										end end,
					["Jax"] =    		function (spell) if spell.spellSlot == 1 then
											self.aaReset = true
											self.lastAttackCast = 0
											self.attackTick = 0
										end end,
					["Jayce"] =    		function (spell) if spell.spellSlot == 1 and myHero.characterIntermediate.attackRange > 300 then
											self.aaReset = true
											self.lastAttackCast = 0
											self.attackTick = 0
										end end,
					["Kassadin"] =    	function (spell) if spell.spellSlot == 1 then
											--PrintChat("Reset AA: ".. self.time - self.lastAttackOrderTick)
											self.aaReset = true
											self.lastAttackCast = 0
											self.attackTick = 0
										end end,
					["Katarina"] =    	function (spell) if spell.spellSlot == 2 then
											self.aaReset = true
											self.lastAttackCast = 0
											self.attackTick = 0
										end end,
					["Kayle"] =    		function (spell) if spell.spellSlot == 2 then
											self.aaReset = true
											self.lastAttackCast = 0
											self.attackTick = 0
										end end,
					["Kindred"] =    	function (spell) if spell.spellSlot == 0 then
											self.aaReset = true
											self.lastAttackCast = 0
											self.attackTick = 0
										end end,
					["Leona"] =    		function (spell) if spell.spellSlot == 0 then
											self.aaReset = true
											self.lastAttackCast = 0
											self.attackTick = 0
										end end,
					["Lucian"] =    	function (spell) if spell.spellSlot == 2 then
											self.aaReset = true
											self.lastAttackCast = 0
											self.attackTick = 0
										end end,
					["MasterYi"] = 		function (spell) if spell.spellSlot == 1 then
											self.aaReset = true
											self.lastAttackCast = 0
											self.attackTick = 0
										end end,
					["Nasus"] =     	function (spell) if spell.spellSlot == 0 then
											self.aaReset = true
											self.lastAttackCast = 0
											self.attackTick = 0
										end end,
					["Nautilus"] =     	function (spell) if spell.spellSlot == 1 then
											self.aaReset = true
											self.lastAttackCast = 0
											self.attackTick = 0
										end end,
					["Nidalee"] = 		function (spell) if spell.spellSlot == 0 and myHero.characterIntermediate.attackRange < 300 then
											self.aaReset = true
											self.lastAttackCast = 0
											self.attackTick = 0
										end end,
					["Mordekaiser"] =   function (spell) if spell.spellSlot == 0 then
											self.aaReset = true
											self.lastAttackCast = 0
											self.attackTick = 0
										end end,
					["RekSai"] =     	function (spell) if spell.spellSlot == 0 then
											self.aaReset = true
											self.lastAttackCast = 0
											self.attackTick = 0
										end end,
					["Renekton"] =     	function (spell) if spell.spellSlot == 1 then
											self.aaReset = true
											self.lastAttackCast = 0
											self.attackTick = 0
										end end,
					["Rengar"] =     	function (spell) if spell.spellSlot == 0 then
											self.aaReset = true
											self.lastAttackCast = 0
											self.attackTick = 0
										end end,
					["Riven"] = 		function (spell) if spell.spellSlot == 0 then
											self.aaReset = true
											self.lastAttackCast = 0
											self.attackTick = 0
										end end,
					["Sejuani"] =     	function (spell) if spell.spellSlot == 2 then
											self.aaReset = true
											self.lastAttackCast = 0
											self.attackTick = 0
										end end,
					["Shyvana"] =     	function (spell) if spell.spellSlot == 0 then
											self.aaReset = true
											self.lastAttackCast = 0
											self.attackTick = 0
										end end,
					["Sivir"] =     	function (spell) if spell.spellSlot == 1 then
											self.aaReset = true
											self.lastAttackCast = 0
											self.attackTick = 0
										end end,
					["Trundle"] =     	function (spell) if spell.spellSlot == 0 then
											self.aaReset = true
											self.lastAttackCast = 0
											self.attackTick = 0
										end end,
					["Vayne"] =     	function (spell) if spell.spellSlot == 0 then
											self.aaReset = true
											self.lastAttackCast = 0
											self.attackTick = 0
										end end,
					["Vi"] =     		function (spell) if spell.spellSlot == 2 then
											self.aaReset = true
											self.lastAttackCast = 0
											self.attackTick = 0
										end end,
					["Volibear"] =     	function (spell) if spell.spellSlot == 0 then
											self.aaReset = true
											self.lastAttackCast = 0
											self.attackTick = 0
										end end,				
					["MonkeyKing"] =    function (spell) if spell.spellSlot == 0 then
											self.aaReset = true
											self.lastAttackCast = 0
											self.attackTick = 0
										end end,
					["XinZhao"] =     	function (spell) if spell.spellSlot == 0 then
											self.aaReset = true
											self.lastAttackCast = 0
											self.attackTick = 0
										end end,
					["Yorick"] =     	function (spell) if spell.spellSlot == 0 then
											self.aaReset = true
											self.lastAttackCast = 0
											self.attackTick = 0
										end end,
					},
	["ONBUFF"] = {
					["sonapassiveattack"] = function(buff)
											--PrintChat("AA RESET")
											self.aaReset = true
											self.lastAttackCast = 0
											self.attackTick = 0 
											end,					
					["itemtitanichydracleavebuff"] = function(buff)
											--PrintChat("AA RESET")
											self.cantCancelAA = true
											self.aaReset = true
											self.lastAttackCast = 0
											self.attackTick = 0 
											end,

					},
				}
end

function LegitOrb:cantCancel()
	return 	{	["DariusNoxianTacticsONHAttack"] = true,

			}
end

function LegitOrb:specialAttacks()
	return {
		["BlueCardPreAttack"] = "TwistedFate",
		["RedCardPreAttack"] = "TwistedFate",
		["GoldCardPreAttack"] = "TwistedFate",
		["CaitlynHeadshotMissile"] = "Caitlyn",
		["ViktorQBuff"] = "Viktor",
		["YasuoQE1"] = "Yasuo",
		["YasuoQE2"] = "Yasuo",
		["YasuoQE3"] = "Yasuo",
	}
end

function LegitOrb:specialAttacksObjects()
	return {
		["CaitlynHeadshotMissile"] = "Caitlyn",
	}
end

function LegitOrb:__init()

	self.time =    RiotClock.time
	self.minionRefreshTick, self.EnemyMinions = self.time, {}
	self.Nexus = {}
	self.Inhib = {}
	self.Turrets = {}


	self.attackTick, self.lastAttackOrderTick, self.lastAttackCast = self.time, self.time, self.time
	self.movementTick, self.lastMoveOrderTick, self.lastPathTick = self.time, self.time, self.time
	self.ignoreNextWaypoint = true

	self.isAttacking, self.canAttack, self.aaCanceled, self.aaReset, self.cantCancelAA = false, true, false, false, false
	self.lastTarget = myHero

	self.attackSpeed = self:GetAttackSpeed()

	self.AttackSpeedBuffsNames = {
		[1] = {
			["ASSETS/Perks/Styles/Precision/LethalTempo/LethalTempoEmpowered.lua"] = {state = true, name = nil},
			["ASSETS/Perks/Styles/Domination/HailOfBlades/HailOfBladesBuff.lua"] =  {state = true, name = nil},
			["jinxpassivekill"] =  {state = true, name = "nil"},
			["ASSETS/Perks/Styles/Precision/LethalTempo/LethalTempoCooldown.lua"] =  {state = false, name = "ASSETS/Perks/Styles/Precision/LethalTempo/LethalTempoEmpowered.lua"},
		},
		[2] = {
			["jinxpassivekill"] = {state = false, name = "jinxpassivekill"},
			["ASSETS/Perks/Styles/Domination/HailOfBlades/HailOfBladesBuff.lua"] = {state = false, name = "ASSETS/Perks/Styles/Domination/HailOfBlades/HailOfBladesBuff.lua"},
		}
	}
	self.AttackSpeedBuffs = {}
	self.lethalTempo = false

	self.currentAttackDamage = 0
	self.myAttackWindup = {}
	self.myMissiles = {}
	self.allyFocus = {}
	self.allyAttacksWindup = {}
	self.allyMissile = {}
	self.Turret_DMG = {
		["SRU_ChaosMinionRanged"] = 0.7,
		["SRU_ChaosMinionMelee"] = 0.4325,
		["SRU_ChaosMinionSiege"] = 0.14,
		["SRU_ChaosMinionSuper"] = 0.05,
		["SRU_OrderMinionRanged"] = 0.7,
		["SRU_OrderMinionMelee"] = 0.4325,
		["SRU_OrderMinionSiege"] = 0.14,
		["SRU_OrderMinionSuper"] = 0.05,
		-- ARAM
		["HA_ChaosMinionRanged"] = 0.7,
		["HA_ChaosMinionMelee"] = 0.4325,
		["HA_ChaosMinionSiege"] = 0.14,
		["HA_ChaosMinionSuper"] = 0.05,
		["HA_OrderMinionRanged"] = 0.7,
		["HA_OrderMinionMelee"] = 0.4325,
		["HA_OrderMinionSiege"] = 0.14,
		["HA_OrderMinionSuper"] = 0.05
	}

	self.legitMenu = nil

	self.CaitlynCustomAA = {[1] = {}, [2] = {}}

	-- ForDevs-----------

	self.currentMode = "nil"
	self.blockMove, self.blockAttack = false, false
	self.forceTarget = nil
	self.aaCooldownTimer = 0

	---------------------

	self:Menu()

	AddEvent(Events.OnTick, 		function() 			self:OnTick() end)
	AddEvent(Events.OnDraw, 		function() 			self:OnDraw() end)
	AddEvent(Events.OnBasicAttack, 	function(a,b) 		self:OnBasicAttack(a,b) end)
	AddEvent(Events.OnProcessSpell, function(a,b) 		self:OnProcessSpell(a,b) end)
	AddEvent(Events.OnExecuteCastFrame, function(a,b) 	self:OnExecuteCastFrame(a,b) end)
	--AddEvent(Events.OnStopCastSpell,function(a,b,c) 	self:OnStopCastSpell(a,b,c) end)
	AddEvent(Events.OnCreateObject, function(a,b) 		self:OnCreateObject(a,b) end)
	AddEvent(Events.OnDeleteObject, function(a) 		self:OnDeleteObject(a) end)
	AddEvent(Events.OnBuffGain, 	function(a,b) 		self:OnBuffGain(a,b) end)
	AddEvent(Events.OnBuffLost, 	function(a,b) 		self:OnBuffLost(a,b) end)
	AddEvent(Events.OnNewPath, 		function(a,b,c,d) 	self:OnNewPath(a,b,c,d) end)
	
	--***
	AddEvent(Events.OnIssueOrder, 	function(a,b,c)	self:OnIssueOrder(a,b,c) end)

	AddEvent(Events.OnWndProc, 		function(a,b,c,d) 	self:OnWndProc(a,b,c,d) end)

	PrintChat("<font color=\"#8f70f4\"><b>[Legit - Orbwalker: Rewrite2]</b></font><b><font color=\"#FFFFFF\"> ALPHA v" .._orbVERSION.. " loaded!</font>")


	self:CollectTurrets()
end

function LegitOrb:OnUnLoad()
	PrintChat("[Legit - Orb] unloaded")
end

function LegitOrb:Menu()
	self.legitMenu = Menu("_legitOrbMenu", "[Legit Orbwalker] v" .. _orbVERSION)

	self.legitMenu:sub("keySettings", "Key - Settings")
		self.legitMenu.keySettings:key("Combo", "Combo Key" , 0x20, false)
		self.legitMenu.keySettings:key("Harass", "Harass Key" , 0x43, false)
		self.legitMenu.keySettings:key("Lasthit", "Lasthit Key" , 0x58, false)
		self.legitMenu.keySettings:key("Waveclear", "Waveclear Key" , 0x56, false)
		--self.legitMenu:key("Jungleclear", "Jungleclear Key" , 0x56, false)
		--self.legitMenu.keySettings:key("Freeze", "Freeze Key" , 0x59, false)

		self.legitMenu:list("tsMethod","Target Selector",1,{"New","Old"})
		

	self.legitMenu:sub("_draw", "Drawings")

		self.legitMenu._draw:checkbox("drawAARange","Draw Auto-Attack Range",true)
		self.legitMenu._draw:checkbox("drawAARangeGucci","Thicc Auto-Attack Drawing",true)
		self.legitMenu._draw:checkbox("drawDMG","Draw Auto-Attack DMG on minions",true)


	self.legitMenu:slider("extraWaitTime", "Extra wait time for lasthits" , 0, 0.5, 0.15, 0.01)
	self.legitMenu:slider("moveDelay", "Delay between clicks" , 0.05, 0.25, 0.125, 0.001)
	self.legitMenu:slider("holdRadius", "Hold Radius" , 0, 200, 75, 1)

end

-- Core
function LegitOrb:OnBasicAttack(unit,attack)
	
	if unit.team == TEAM_ALLY and unit ~= myHero and LegitCommon:GetDistanceToZeroPos(unit.position,myHero.position) < 3000 and find(lower(attack.spellData.name),"attack") then
		if self.allyAttacksWindup[unit.networkId] == nil and unit.team == TEAM_ALLY then self.allyAttacksWindup[unit.networkId] = {} end
		--PrintChat(attack.spellData.name)
		self.allyAttacksWindup[unit.networkId] = {source = unit ,name = attack.spellData.name, target = attack.target, data = attack.spellData.spellDataInfo, windUp = unit.attackCastDelay, attackTime = unit.attackDelay, startTime = RiotClock.time}
		self.allyFocus[unit.networkId] = {source = unit ,name = attack.spellData.name, target = attack.target, data = attack.spellData.spellDataInfo, windUp = unit.attackCastDelay, attackTime = unit.attackDelay, startTime = RiotClock.time}
	end
	if unit == myHero then
		if attack.target.type == GameObjectType.obj_AI_Minion then
			self.currentAttackDamage = damageLib:GetAutoAttackDamage(myHero, attack.target)
		end
		self.ignoreNextWaypoint = true
		self.aaReset = false
		self.myAttackWindup = {source = unit ,name = attack.spellData.name, target = attack.target, data = attack.spellData.spellDataInfo, windUp = unit.attackCastDelay, attackTime = unit.attackDelay, startTime = RiotClock.time}
		self.attackTick = 0
		self.lastAttackCast = self.time
		self.lastTarget = attack.target
		self.isAttacking = true
		self.cantCancelAA = false
		if self.cantCancel()[attack.spellData.name] then
			self.cantCancelAA = true
		end
	end 
	return 1
end

function LegitOrb:OnProcessSpell(unit,spell)
	if unit == myHero then
		--PrintChat("Spell: "..spell.spellData.name)

		if self:specialAttacks()[spell.spellData.name] ~= nil then
			self.aaReset = false
			if self:specialAttacks()[spell.spellData.name] == "Yasuo" then
				self.lastAttackCast = self.time - ((1/self.attackSpeed) - myHero.attackCastDelay*(0.025 + self.attackSpeed/25)) + 0.55
				return 1
			else
			self.myAttackWindup = {source = unit ,name = spell.spellData.name, target = spell.target, data = spell.spellData.spellDataInfo, windUp = unit.attackCastDelay, attackTime = unit.attackDelay, startTime = RiotClock.time}
			self.lastAttackCast = self.time
			end
			self.attackTick = 0
			self.lastTarget = spell.target
			self.isAttacking = true
			self.cantCancelAA = false
			if self.cantCancel()[spell.spellData.name] then
				self.cantCancelAA = true
			end
		end

		local reset = self:AA_ResetSpell()["ONSPELL"][myHero.charName]
		if reset then reset(spell) end	
		end
	return 1
end

function LegitOrb:OnExecuteCastFrame(unit,spell)

	if unit.team == myHero.team and self.allyAttacksWindup[unit.networkId] ~= nil and self:projectileSpeed(unit) > 10000 then self.allyAttacksWindup[unit.networkId] = nil end
	if unit == myHero and (find(lower(spell.spellData.name),"attack") or self:specialAttacks()[spell.spellData.name] ~= nil or spell.spellSlot == 64 or spell.spellSlot == 73) then
		self.myAttackWindup = {}
		self.isAttacking = false
	end
	return 1
end

-- Seems to be buggy for some Attacks // Darius W, TitanicItem
function LegitOrb:OnStopCastSpell(unit,animation,frame)
	if self.allyFocus[unit.networkId] ~= nil then self.allyFocus[unit.networkId] = nil end
	if self.allyAttacksWindup[unit.networkId] ~= nil then self.allyAttacksWindup[unit.networkId] = nil end
	if unit == myHero then
		if self.isAttacking == true and not self.cantCancelAA then 
			--PrintChat("Canceled")
			--PrintChat("animation: " .. tostring(animation))
			--self.myAttackWindup = {}
			--self.aaCanceled = true
			--self.isAttacking = false
			--self.canAttack = true
			--self.lastAttackCast = 0
		end
	end
end

function LegitOrb:OnNewPath(unit,path,isWalk,dashSpeed)
	if self.allyAttacksWindup[unit.networkId] ~= nil and #path > 1 then self.allyAttacksWindup[unit.networkId] = {} end
	if self.allyFocus[unit.networkId] ~= nil and #path > 1 then self.allyFocus[unit.networkId] = nil end
	--if unit == myHero and #path > 1 and self.ignoreNextWaypoint == true then self.ignoreNextWaypoint = false return end
	if unit == myHero and #path > 1 and self.isAttacking == true and self.time - self.lastAttackOrderTick > NetClient.ping/1000 then
		--PrintChat("Yikes")
		self.myAttackWindup = {}
		self.aaCanceled = true -- ?
		self.aaReset = true
		
		self.isAttacking = false
		--self.canAttack = true
		--self.lastAttackCast = 0
	end
	return 1
end

function LegitOrb:OnCreateObject(o,networkId)
	if o.type == GameObjectType.MissileClient then
		if o.asMissile.spellCaster == nil then return end
		  if self.allyAttacksWindup[o.asMissile.spellCaster.networkId] and find(lower(o.name),"attack") and o.asMissile.spellCaster ~= myHero then
			self.allyMissile[o.asMissile.spellCaster.networkId] = o
			self.allyAttacksWindup[o.asMissile.spellCaster.networkId] = {}
		  end
	end
	--if self.isAttacking == true then
		if (o.type == GameObjectType.MissileClient and o.asMissile.spellCaster.networkId == myHero.networkId) then
			if find(lower(o.name),"attack") or self:specialAttacksObjects()[o.name] ~= nil then
				self.myAttackWindup = {}
				if o.asMissile.target ~= nil then
					if self.myMissiles[o.asMissile.target.networkId] == nil then self.myMissiles[o.asMissile.target.networkId] = {} end
					insert(self.myMissiles[o.asMissile.target.networkId],{missile = o, dmg = self.currentAttackDamage})
				end
				self.isAttacking = false
			end
		end
	--end
end

function LegitOrb:OnDeleteObject(o)
	if self.Turrets[o.networkId] then self.Turrets[o.networkId] = nil end
	if self.allyFocus[o.networkId] ~= nil then self.allyFocus[o.networkId] = nil end
	if o.type == GameObjectType.MissileClient then
		if o.asMissile.spellCaster == nil then return end
		if self.allyMissile[o.asMissile.spellCaster.networkId] then
			self.allyMissile[o.asMissile.spellCaster.networkId] = nil
		end
		
		if o.asMissile.target ~= nil then
			local myMissiles = self.myMissiles[o.asMissile.target.networkId]
			if myMissiles then
				for i, m in pairs(myMissiles) do
					if m.missile.networkId == o.networkId then
						remove(self.myMissiles[o.asMissile.target.networkId],i)
						if #self.myMissiles[o.asMissile.target.networkId] == 0 then
							self.myMissiles[o.asMissile.target.networkId] = nil
						end
					end
				end
			end
		end
	end
end

function LegitOrb:OnBuffGain(unit, buff)
	if unit == myHero then
		local name = buff.name
		local speedBuff = self.AttackSpeedBuffsNames[1][name]
		if speedBuff then
			if speedBuff.state == true then
				self.AttackSpeedBuffs[name] = speedBuff
			elseif speedBuff.state == false then
				if speedBuff.name == nil then
					self.AttackSpeedBuffs[name] = nil
				else
					if self.AttackSpeedBuffs[speedBuff.name] ~= nil then
						self.AttackSpeedBuffs[speedBuff.name] = nil
					end
				end
			end
		end
		
		local reset = self:AA_ResetSpell()["ONBUFF"][name]
		if reset then reset(buff) end
	end
	
end

function LegitOrb:OnBuffLost(unit, buff)
	if unit == myHero then
		local name = buff.name
		local speedBuff = self.AttackSpeedBuffsNames[2][name]
		if speedBuff then
			if speedBuff.state == false then
				self.AttackSpeedBuffs[name] = nil
			elseif speedBuff.state == true then
				self.AttackSpeedBuffs[name] = speedBuff
			end
		end
	end
end

function LegitOrb:OnIssueOrder(order)
	if order == 3 then
		if self.lastAttackOrderTick + 0.05 > self.time then
			BlockOrder()
		end
		self.lastAttackOrderTick = self.time
	end
	if order == 2 then
		self.lastMoveOrderTick = self.time
	end
end

function LegitOrb:OnWndProc(hWnd,msg,wP,lP)
	-- 258 keydown
	if msg == 13 or msg == 132 or msg == 32 or msg == 512 or msg == 258 then return end
	if msg == 513 then
		local mousePos = pwHud.hudManager.activeVirtualCursorPos
		local forceTarget = self.forceTarget
		for i,v in pairs(ObjectManager:GetEnemyHeroes()) do
			if LegitCommon:GetDistanceToZeroPos(mousePos,v.position) < v.boundingRadius and not v.isDead then
				if forceTarget == nil then
					PrintChat("<font color=\"#8f70f4\"><b>[Legit - Orbwalker]</b></font><b><font color=\"#FFFFFF\"> <font color=\"#f23040\"><b>Forced Target: </b></font><b><font color=\"#FFFFFF\">".. v.charName .."</font>")
				elseif forceTarget ~= v then
					PrintChat("<font color=\"#8f70f4\"><b>[Legit - Orbwalker]</b></font><b><font color=\"#FFFFFF\"> <font color=\"#f23040\"><b>Forced Target: </b></font><b><font color=\"#FFFFFF\">".. v.charName .."</font>")
				end
					self.forceTarget = v
				return
			end
		end
		self.forceTarget = nil
	end
	--PrintChat(msg)
end

function LegitOrb:OnTick()

	self:CollectData()
	if ChatConsole.isChatOpen then return end
	self:Orbwalker()
end

--local font = DrawHandler:CreateFont("Calibri", 20)
function LegitOrb:OnDraw()

	
	--DrawHandler:Text(font, Renderer:WorldToScreen(myHero.position), tostring(myHero.canMove), LegitCommon:Hex(250,154,250,147))

	--[[
	for i,turret in pairs(self.Inhib) do
		if LegitCommon:OnScreen(turret.object.position) then
			DrawHandler:Circle3D(turret.object.position,turret.hitbox,LegitCommon:Hex(255,254,50,47))
		end
	end
	]]

	if self.legitMenu._draw.drawDMG:get() then
		for i,minion in pairs(self.EnemyMinions) do
			if not minion.isDead and minion.team == TEAM_ENEMY and LegitCommon:GetDistance(myHero,minion) < myHero.characterIntermediate.attackRange + 400 then
			local minionHPBar = minion.infoComponent.hpBarScreenPosition
			--print(minionHPBar)
			local dmg = damageLib:GetAutoAttackDamage(myHero,minion)
			local hpBarSize = find(minion.charName,"Super") and 100 or 70
			local hpBarStartPos = find(minion.charName,"Super") and 50 or 35
			local pHP = floor(hpBarSize*minion.health/minion.maxHealth+1)
			local pDMG = floor(hpBarSize*dmg/minion.maxHealth + 0.5)
			pDMG = pHP > pDMG and pDMG or pHP
			if minion.health < dmg then
			DrawHandler:OutlinedRect(D3DXVECTOR4(minionHPBar.x-hpBarStartPos-1,minionHPBar.y-7,hpBarSize+1,6), 2, LegitCommon:Hex(250,9, 229, 148), LegitCommon:Hex(100,9, 229, 148))
			end
			DrawHandler:OutlinedRect(D3DXVECTOR4(minionHPBar.x-hpBarStartPos+pHP-pDMG,minionHPBar.y-7,pDMG,6), 1, LegitCommon:Hex(255,9, 229, 148), LegitCommon:Hex(125,9, 229, 148))
			end
		end
	end

	if self.forceTarget ~= nil then
		if LegitCommon:OnScreen(self.forceTarget.position) and not self.forceTarget.isDead then
			DrawHandler:Circle3D(self.forceTarget.position,self.forceTarget.boundingRadius + 20,LegitCommon:Hex(255,254,50,47))
			DrawHandler:Circle3D(self.forceTarget.position,self.forceTarget.boundingRadius + 21,LegitCommon:Hex(150,254,50,47))
			DrawHandler:Circle3D(self.forceTarget.position,self.forceTarget.boundingRadius + 19,LegitCommon:Hex(150,254,50,47))
		end
	end

	if self.legitMenu._draw.drawAARange:get() then
		if LegitCommon:OnScreen(myHero.position) then
			DrawHandler:Circle3D(myHero.position,myHero.characterIntermediate.attackRange + myHero.boundingRadius + 39,LegitCommon:Hex(255,154,250,147))
			if self.legitMenu._draw.drawAARangeGucci:get() then
				DrawHandler:Circle3D(myHero.position,myHero.characterIntermediate.attackRange + myHero.boundingRadius + 39.5,LegitCommon:Hex(250/2,154,250,147))
				DrawHandler:Circle3D(myHero.position,myHero.characterIntermediate.attackRange + myHero.boundingRadius + 38.5,LegitCommon:Hex(255/2,154,250,147))
			end
		end
	end

end




--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- [[ COMMON ]] ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- good
function LegitOrb:CollectData()
	self.time = RiotClock.time
	self.attackSpeed = self:GetAttackSpeed()
	self.canAttack = self:CanAttack()
	self.EnemyMinions = self:MinionManager()
end

local specialChamps = {
	["Jayce"] = 2000,
	["Poppy"] = 1600
}
function LegitOrb:projectileSpeed(unit)
	local speed = (unit.basicAttack.speed > 0 and unit.basicAttack.speed) or math.huge
	if unit.characterIntermediate.attackRange <= 350 and speed < 600 then speed = math.huge end
	if specialChamps[unit.charName] and unit.characterIntermediate.attackRange > 350 then
		return specialChamps[unit.charName]
	end
	return speed
end

-- seem k
function LegitOrb:travelTime(from,to,speed,distancePuffer)
	local distancePuffer = distancePuffer or 0
	local distance = (LegitCommon:GetDistanceToZero(from,to)) + distancePuffer
	return distance/speed
end

function LegitOrb:travelTimeTurret(from,to,speed,distancePuffer)
	local distancePuffer = distancePuffer or 0
	local distance = (LegitCommon:GetDistance(from,to)) + distancePuffer
	return distance/speed
end

--good
function LegitOrb:CalcPhysicalDamage(source, target, dmg)
	if target.isInvulnerable then return 0 end
	
  local result = 0
  local baseArmor = target.characterIntermediate.armor
  local Lethality = source.characterIntermediate.physicalLethality * (0.6 + 0.4 * source.experience.level / 18)
  baseArmor = baseArmor - Lethality
  
  if baseArmor < 0 then baseArmor = 0 end
  if (baseArmor >= 0 ) then
	local armorPenetration = source.characterIntermediate.percentArmorPenetration
	local armor = baseArmor - ((armorPenetration*baseArmor) / 100)
	result = dmg * (100 / (100 + armor))
  end
  
  return result
end

--good
function LegitOrb:CalcMagicalDamage(source, target, dmg)
	if target.isInvulnerable then return 0 end
	
	local result = 0
  
  local baseArmor = target.characterIntermediate.spellBlock
  local Lethality = source.characterIntermediate.flatMagicPenetration
	baseArmor = baseArmor - Lethality
  
  if baseArmor < 0 then baseArmor = 0 end
  if (baseArmor >= 0 ) then
	local armorPenetration = source.characterIntermediate.percentMagicPenetration
	local armor = baseArmor - ((armorPenetration*baseArmor) / 100)
	result = dmg * (100 / (100 + armor))
  end
  
  return result
end

-- good
function LegitOrb:Priority(charName)
	local p1 = {"Alistar", "Amumu", "Blitzcrank", "Braum", "Cho'Gath", "DrMundo", "Garen", "Gnar", "Maokai", "Hecarim", "Janna", "Leona", "Lulu", "Malphite", "Nasus", "Nautilus", "Nunu", "Olaf", "Rammus", "Renekton", "Sejuani", "Shen", "Shyvana", "Singed", "Sion", "Skarner", "Taric", "TahmKench", "Thresh", "Volibear", "Warwick", "MonkeyKing", "Yorick", "Zac", "Poppy", "Ornn"}
	local p2 = {"Aatrox", "Darius", "Elise", "Evelynn", "Galio", "Gragas", "Irelia", "Jax", "LeeSin", "Morgana", "Nocturne", "Pantheon", "Rumble", "Swain", "Trundle", "Tryndamere", "Udyr", "Urgot", "Vi", "XinZhao", "RekSai", "Bard", "Nami", "Sona", "Camille", "Jarvan IV", "Kayn"}
	local p3 = {"Akali", "Diana", "Ekko", "FiddleSticks", "Fiora", "Gangplank", "Fizz", "Heimerdinger", "Jayce", "Kassadin", "Kayle", "Kha'Zix", "Lissandra", "Mordekaiser", "Nidalee", "Riven", "Shaco", "Vladimir", "Yasuo", "Zilean", "Zyra", "Ryze", "Rengar"}
	local p4 = {"Ahri", "Anivia", "Annie", "Ashe", "Azir", "Brand", "Caitlyn", "Cassiopeia", "Corki", "Draven", "Ezreal", "Graves", "Jinx", "Kalista", "Karma", "Karthus", "Katarina", "Kennen", "KogMaw", "Kindred", "Leblanc", "Lucian", "Lux", "Malzahar", "MasterYi", "MissFortune", "Orianna", "Quinn", "Sivir", "Syndra", "Talon", "Teemo", "Tristana", "TwistedFate", "Twitch", "Varus", "Vayne", "Veigar", "Velkoz", "Viktor", "Xerath", "Zed", "Ziggs", "Jhin", "Soraka", "Zoe", "Kaisa"}
   
	for i,n in pairs(p1) do if n == charName then return 0.85 end end 
	for i,n in pairs(p2) do if n == charName then return 1 end end 
	for i,n in pairs(p3) do if n == charName then return 1.25 end end 
	for i,n in pairs(p4) do if n == charName then return 1.5 end end 
	return 1.05
end

-- good
function LegitOrb:GetTarget(range,t,pos,orb)
	local range = range or myHero.characterIntermediate.attackRange
	local t = t or "AD"
	local pos = pos or myHero.position
	local orb = orb or false
	if self.forceTarget ~= nil then
		if self.forceTarget.team == TEAM_ENEMY and not self.forceTarget.isInvulnerable and not self.forceTarget.isDead and self.forceTarget.isVisible and self.forceTarget.isTargetable then
			if math.floor(LegitCommon:GetDistanceToZeroPos(pos,self.forceTarget.position)) <= range + myHero.boundingRadius + self.forceTarget.boundingRadius/1.5 then
				return self.forceTarget
			end
		end
	end
	local target = {}
			for i,v in pairs(ObjectManager:GetEnemyHeroes()) do
					local hero = v
					if hero.team == TEAM_ENEMY and not hero.isInvulnerable and not hero.isDead and hero.isVisible and hero.isTargetable then
							local caitlynRange = 0
							if myHero.charName == "Caitlyn" then 
								local trapBuff = hero.buffManager:HasBuff("caitlynyordletrapinternal")
								if trapBuff ~= nil and trapBuff.name == "caitlynyordletrapinternal" and trapBuff.caster == myHero then caitlynRange = 425 end 
							end
							local heroPos = hero.position
							local range = math.floor(range + myHero.boundingRadius + hero.boundingRadius/1.5) + caitlynRange
							if math.floor(LegitCommon:GetDistanceToZeroPos(pos,heroPos)) <= range then
								if self.legitMenu.tsMethod:get() == 1 then
									if t == "AD" then
											target[math.floor(((hero.health / damageLib:GetAutoAttackDamage(myHero, hero)) - self:Priority(hero.charName))/self:Priority(hero.charName))] = hero
									elseif t == "AP" then
											target[math.floor(((hero.health / self:CalcMagicalDamage(myHero,hero,100)) - self:Priority(hero.charName))/self:Priority(hero.charName))] = hero
									elseif t == "HYB" then
											target[math.floor(((hero.health / (self:CalcMagicalDamage(myHero,hero,50) + self:CalcPhysicalDamage(myHero,hero,50))) - self:Priority(hero.charName))/self:Priority(hero.charName))] = hero
									end
								elseif self.legitMenu.tsMethod:get() == 2 then
									if t == "AD" then
											target[(damageLib:GetAutoAttackDamage(myHero, hero) / hero.health)*self:Priority(hero.charName)] = hero
									elseif t == "AP" then
											target[(self:CalcMagicalDamage(myHero,hero,100) / hero.health)*self:Priority(hero.charName)] = hero
									elseif t == "HYB" then
											target[((self:CalcMagicalDamage(myHero,hero,50) + self:CalcPhysicalDamage(myHero,hero,50))/ hero.health)*self:Priority(hero.charName)] = hero
									end
								end
							end
					end
			end
			local bT
			if self.legitMenu.tsMethod:get() == 1 then
				bT = math.huge
				for d,v in pairs(target) do
					if d < bT then
							bT = d
					end
				end
			elseif self.legitMenu.tsMethod:get() == 2 then
				bT = 0
				for d,v in pairs(target) do
						if d > bT then
								bT = d
						end
				end
			end

			if target[bT] then return target[bT] end
end

--good
function LegitOrb:MinionManager()
	if self.time > self.minionRefreshTick + 0.25 then

		local EnemyMinions = {}
		for i,minion in pairs(ObjectManager:GetEnemyMinions()) do
			if LegitCommon:GetDistance(myHero.position,minion.position) < 2500 then
				if not minion.isDead and minion.isTargetable and minion.isValid and minion.health > 0 and minion.maxHealth > 5 then
					insert(EnemyMinions,minion)
				end
			end
		end
		self.minionRefreshTick = self.time
		return EnemyMinions

	end
	return self.EnemyMinions
end

function LegitOrb:CollectTurrets()
	for i,turret in pairs(ObjectManager:GetEnemyTurrets()) do
		self.Turrets[turret.networkId] = {object = turret, hitbox = 90}
	end
	for i, hq in pairs(ObjectManager:GetObjectsByType(GameObjectType.obj_HQ)) do
		if hq.team == TEAM_ENEMY then
			self.Nexus = {object = hq, hitbox = 300}
		end
	end
	for i, inhib in pairs(ObjectManager:GetObjectsByType(GameObjectType.obj_BarracksDampener)) do
		if inhib.team == TEAM_ENEMY then
			self.Inhib[inhib.networkId] = {object = inhib, hitbox = 200}
		end
	end
end

function LegitOrb:HpPred(unit,delta)
	local delta = delta or (self:travelTime(myHero,unit,self:projectileSpeed(myHero),-myHero.boundingRadius) + myHero.attackCastDelay + NetClient.ping/1000 - 0.05)
	local dmg = 0
	local trueDmg = 0
	local dmgReduction = (100/(100+unit.characterIntermediate.armor))

	for n,attack in pairs(self.allyAttacksWindup) do
		if attack.target == unit then
			local attacker = ObjectManager:GetUnitByNetworkId(n)
			if attacker ~= nil and not attacker.isDead then
				local isTurret = find(attacker.charName,"Turret")
				local projSpeed = self:projectileSpeed(attacker)
				local untilHit = 0
				if isTurret then
					untilHit = ( attack.startTime + ((max(0,LegitCommon:GetDistance(D3DXVECTOR3(attacker.x,attacker.y+520,attacker.z),unit)-attacker.boundingRadius/2))/projSpeed) + attack.windUp + 0.05) - RiotClock.time
				else
					untilHit = ( attack.startTime + ((max(0,LegitCommon:GetDistanceToZero(unit,attacker)-attacker.boundingRadius*0.5))/projSpeed) + attack.windUp) - RiotClock.time
				end
				if delta > untilHit and untilHit > 0 then
					if isTurret then
						trueDmg = trueDmg + floor(unit.maxHealth*self.Turret_DMG[unit.charName])
					else
						dmg = dmg + floor((attacker.characterIntermediate.flatPhysicalDamageMod + attacker.characterIntermediate.baseAttackDamage)*dmgReduction)
					end
				end
			end
		end
	end
	
	for i,p in pairs(self.allyMissile) do
		local target = p.asMissile.target
		if unit == target then
			local attacker = p.asMissile.spellCaster
			if attacker.isDead or attacker.health < 1 then self.allyMissile[i] = nil break end
			local isTurret = find(attacker.charName,"Turret")
			local projSpeed = self:projectileSpeed(attacker)
			local untilHit = 0
			if isTurret then
				untilHit = self:travelTimeTurret(p,unit,projSpeed,-attacker.boundingRadius) + 0.05
			else
				untilHit = self:travelTime(p,unit,projSpeed,0)
			end
			if delta > untilHit then
				if isTurret then
					trueDmg = trueDmg + floor(unit.maxHealth*(self.Turret_DMG[unit.charName] or 0))
				else
					dmg = dmg + floor((attacker.characterIntermediate.flatPhysicalDamageMod + attacker.characterIntermediate.baseAttackDamage)*dmgReduction)
				end
			end
		end
	end
	
	local myMissiles = self.myMissiles[unit.networkId]
	if myMissiles then
		for i, mTable in pairs(myMissiles) do
			local projSpeed = self:projectileSpeed(myHero)
			local untilHit = self:travelTime(mTable.missile,unit,projSpeed)
			if delta > untilHit then
				local hurricane = find(mTable.missile.name,"Item") and 0.4 or 1
				trueDmg = trueDmg + mTable.dmg*hurricane
			end
		end
	end

	return (unit.health - (dmg + trueDmg)) or 0

end

function LegitOrb:HpPredClear(unit,delta)
	local extraWaitTime = self.legitMenu.extraWaitTime:get()
	local delta = delta or (extraWaitTime + self:travelTime(myHero,unit,self:projectileSpeed(myHero),0) + myHero.attackCastDelay + NetClient.ping/1000 - 0.05)
	local dmg = 0
	local trueDmg = 0
	local dmgReduction = (100/(100+unit.characterIntermediate.armor))

	for n,attack in pairs(self.allyFocus) do
		local attacker = attack.source
		if attacker and not attacker.isDead and attacker.isValid and not LegitCommon:IsRunning(attacker) then
			--DrawHandler:Circle3D(attacker.position,50,0xff00ff00)
			if attack.target == unit then
				local projSpeed = self:projectileSpeed(attacker)
				local untilHit = ( attack.startTime + ((max(0,LegitCommon:GetDistanceToZero(unit,attacker)-attacker.boundingRadius/2))/projSpeed) + attack.windUp) - self.time
				local count = 0
				while delta > untilHit do
					if untilHit > 0 then
						count = count + 1
					end
					untilHit = untilHit + attack.attackTime
				end

				if count > 0 then
					--DrawHandler:Line(Renderer:WorldToScreen(attacker.position), Renderer:WorldToScreen(unit.position), LegitCommon:Hex(200,200,50,150))
					if find(attacker.charName,"Turret") and LegitCommon:GetDistanceToZeroPos(attacker.position,unit.position) < 800 then
						--DrawHandler:Circle3D(D3DXVECTOR3(unit.x,unit.y,unit.z),50,0xff00ff00)
						trueDmg = trueDmg + floor(unit.maxHealth*(self.Turret_DMG[unit.charName] or 0))*count
					else
						dmg = dmg + floor((attacker.characterIntermediate.flatPhysicalDamageMod + attacker.characterIntermediate.baseAttackDamage)*dmgReduction)*count
					end
				end
			end
		end
	end


	local myMissiles = self.myMissiles[unit.networkId]
	if myMissiles then
		for i, mTable in pairs(myMissiles) do
			local projSpeed = self:projectileSpeed(myHero)
			local untilHit = self:travelTime(mTable.missile,unit,projSpeed)
			if delta > untilHit then
				local hurricane = find(mTable.missile.name,"Item") and 0.4 or 1
				trueDmg = trueDmg + mTable.dmg*hurricane
				--[[
				DrawHandler:Line(Renderer:WorldToScreen(mTable.missile.position), Renderer:WorldToScreen(unit.position), LegitCommon:Hex(200,200,50,150))
				DrawHandler:Circle3D(mTable.missile.position,40,LegitCommon:Hex(250,254,50,147))
				PrintChat(unit.networkId..": " .. untilHit)
				]]
			end
		end
	end

	return (unit.health - (dmg + trueDmg)) or 0
end

-- common functions
function LegitOrb:GetAttackSpeed()
	local attackSpeed = (myHero.characterIntermediate.baseAttackSpeed*myHero.characterIntermediate.attackSpeedMod)
	self.lethalTempo = false
	if self.AttackSpeedBuffs ~= nil then
		for i,buff in pairs(self.AttackSpeedBuffs) do
			self.lethalTempo = true
			break
		end
	end
	if self.lethalTempo == false then attackSpeed = min(2.5,attackSpeed) end
	return attackSpeed
end

function LegitOrb:CanAttack()
	if myHero.canAttack == false then return false end
	local spellCast = myHero.spellbook.spellCasterClient
	if spellCast and (spellCast.isCastingSpell or spellCast.isChanneling) then
		return false
	end
	if self.isAttacking then self.movementTick = 0 return false end
	if self.isAttacking and self.lastTarget.health < 1 then self.isAttacking = false self.lastAttackCast = 0 PrintChat("Reset") end
	local attackDelay = (1/self.attackSpeed) - myHero.attackCastDelay*(0.025 + self.attackSpeed/25)
	self.aaCooldownTimer =  ((self.lastAttackCast + attackDelay) - NetClient.ping/1000 ) - self.time
	self.aaCooldownTimer = (self.aaCooldownTimer <= 0 and 0) or (self.aaCooldownTimer) or (self.aaReset and 0)
	return self.time > (self.lastAttackCast + attackDelay) - NetClient.ping/1000 or self.aaReset
end

function LegitOrb:CanMove()
	local try = self.time > self.lastAttackCast + myHero.attackCastDelay*math.max(1 - self.attackSpeed/5,0.35) + NetClient.ping/1000
	--if self.isAttacking and try then PrintChat(myHero.attackCastDelay*math.max(1 - self.attackSpeed/5,0.35) + NetClient.ping/1000) end
	-- and self.time > self.lastAttackCast + myHero.attackCastDelay*0.01
	return try or (self.isAttacking == false) or (myHero.charName == "Kalista" and self.time > self.lastAttackCast + myHero.attackCastDelay*0.001)

end

LegitOrb.forcePos = nil
function LegitOrb:MoveTo(pos)
	--if self:CanMove() and self.time - self.movementTick > self.legitMenu.moveDelay:get() + NetClient.ping/1000 and self.time - self.lastAttackOrderTick > self.legitMenu.moveDelay:get() + NetClient.ping/1000 then
	if self:CanMove() and self.time - self.movementTick > self.legitMenu.moveDelay:get() then
		if (self.time - self.lastAttackOrderTick > self.legitMenu.moveDelay:get() + NetClient.ping/1000) or (myHero.charName == "Kalista") then
			if self.blockMove == false then
				self.movementTick = self.time
				local mousePos = pwHud.hudManager.virtualCursorPos
				local pos = pos or mousePos
				if mousePos == pos then
					local mouseDistance = LegitCommon:GetDistanceToZeroPos(myHero.position,pos)
					local radius = myHero.boundingRadius
					if mouseDistance <= radius + 50 then
						radius = radius + 50 + math.random(1,25)
						local vec = {x = myHero.position.x + (pos.x - myHero.position.x)/mouseDistance * radius  ,    y = pos.y   ,    z = myHero.position.z + (pos.z - myHero.position.z)/mouseDistance * radius}
						pos = D3DXVECTOR3(vec.x,vec.y,vec.z)
					end
				end
				if self.forcePos ~= nil then pos = self.forcePos end
				myHero:IssueOrder(GameObjectOrder.MoveTo, pos)
			end
			self.forcePos = nil
			return
		end
	end
end

function LegitOrb:Attack(unit)
	if self.isAttacking then return end
	if self.time - self.attackTick > 0.1 + NetClient.ping/4000 then
	--if self.time - self.attackTick > 0.05 + NetClient.ping/4000 then
		if self.blockAttack == false then
			self.attackTick = self.time
			myHero:IssueOrder(GameObjectOrder.AttackUnit, unit)
		else
			if self.legitMenu.holdRadius:get() < LegitCommon:GetDistanceToZeroPos(myHero.position,pwHud.hudManager.virtualCursorPos) then
				self:MoveTo()
			end
		end
		--self.blockAttack = false
		return
	end
end


-- functions
function LegitOrb:Orbwalker()
	self:Combo()
	self:Harass()
	self:LastHit()
	self:Waveclear()
	self:JungleClear()
	--self:Freeze()

	if not self.legitMenu.keySettings.Combo:get() and not self.legitMenu.keySettings.Harass:get() and not self.legitMenu.keySettings.Lasthit:get() and not self.legitMenu.keySettings.Waveclear:get() then
		self.currentMode = "nil"
	end

end

function LegitOrb:Combo()
	if self.legitMenu.keySettings.Combo:get() then 
		self.currentMode = "Combo"

		if self.canAttack and not self.isAttacking then
			local target = self:GetTarget(myHero.characterIntermediate.attackRange,"AD",myHero.position,true)
			if target then
				self:Attack(target)
				return
			end
		end
		if self.legitMenu.holdRadius:get() < LegitCommon:GetDistanceToZeroPos(myHero.position,pwHud.hudManager.virtualCursorPos) then
			self:MoveTo()
		end
		return
	end
end

function LegitOrb:Harass()
	if self.legitMenu.keySettings.Harass:get() then 
		self.currentMode = "Harass"

		if self.canAttack and not self.isAttacking then

			local wait = false
			local mustLasthit = {}
			local canLasthit = {}
			local waitMinion = {}
			local missLasthit = {}
			
			for i,minion in pairs(self.EnemyMinions) do
				if LegitCommon:GetDistance(minion.position,myHero.position) < myHero.characterIntermediate.attackRange + myHero.boundingRadius + minion.boundingRadius then
						local hpPred = self:HpPred(minion)
						if hpPred ~= minion.health then
							local hpPred2 = self:HpPredClear(minion,myHero.attackDelay*2 + myHero.attackCastDelay + ((max(0,myHero.characterIntermediate.attackRange + myHero.boundingRadius + 50)*2)/self:projectileSpeed(myHero) + NetClient.ping/500))
							local dmg = damageLib:GetAutoAttackDamage(myHero, minion)
							if hpPred <= dmg and hpPred2 < 0 and hpPred > -1 then
								insert( mustLasthit, minion )
							elseif hpPred <= dmg and hpPred > -1 and hpPred2 > 0 then
								insert( canLasthit, minion )
							end
							if hpPred2 < 0 then
								wait = true
							end
						elseif hpPred <= damageLib:GetAutoAttackDamage(myHero, minion) and hpPred > 0 then
							insert( canLasthit, minion )
						end
					end
				end

			sort(mustLasthit, function(a, b)
				local first = a.maxHealth
				local second = b.maxHealth
				return first > second
			end)

			if mustLasthit[1] ~= nil then
				self:Attack(mustLasthit[1])
				return
			end

			sort(canLasthit, function(a, b)
				local first = a.maxHealth
				local second = b.maxHealth
				return first > second
			end)

			if canLasthit[1] ~= nil and wait == false then
				self:Attack(canLasthit[1])
				return
			end

			local target = self:GetTarget(myHero.characterIntermediate.attackRange,"AD",myHero.position,true)

			if target and wait == false then
				self:Attack(target)
				return
			end

		end

		if self.legitMenu.holdRadius:get() < LegitCommon:GetDistanceToZeroPos(myHero.position,pwHud.hudManager.virtualCursorPos) then
			self:MoveTo()
		end
		return
	end
end

function LegitOrb:LastHit()
	if self.legitMenu.keySettings.Lasthit:get() then
		self.currentMode = "Lasthit"

		local wait = false
		local mustLasthit = {}
		local canLasthit = {}
		local waitMinion = {}
		local missLasthit = {}

		if self.canAttack and not self.isAttacking then

			
			for i,minion in pairs(self.EnemyMinions) do
				local distance = LegitCommon:GetDistanceToZeroPos(minion.position,myHero.position)
				if distance < myHero.characterIntermediate.attackRange + myHero.boundingRadius + minion.boundingRadius then
					local hpPred = self:HpPred(minion)
					if hpPred ~= minion.health then
						local hpPred2 = self:HpPredClear(minion,myHero.attackDelay*2 + myHero.attackCastDelay + ((max(0,myHero.characterIntermediate.attackRange + myHero.boundingRadius + 50)*2)/self:projectileSpeed(myHero) + NetClient.ping/500))
						local dmg = damageLib:GetAutoAttackDamage(myHero, minion)
						if hpPred <= dmg and hpPred2 < 0 and hpPred > 0 then
							insert( mustLasthit, minion )
						elseif hpPred <= dmg and hpPred > 0 then
							insert( canLasthit, minion )
						end
						if hpPred2 < 0 then
							wait = true
						end
					elseif hpPred <= damageLib:GetAutoAttackDamage(myHero, minion) and hpPred > 0 then
						insert( canLasthit, minion )
					end
				end
			end

		sort(mustLasthit, function(a, b)
			local first = a.maxHealth
			local second = b.maxHealth
			return first > second
		end)

		if mustLasthit[1] ~= nil then
			self:Attack(mustLasthit[1])
			return
		end

		sort(canLasthit, function(a, b)
			local first = a.maxHealth
			local second = b.maxHealth
			return first > second
		end)

		if canLasthit[1] ~= nil and wait == false then
			self:Attack(canLasthit[1])
			return
		end

		end

		if self.legitMenu.holdRadius:get() < LegitCommon:GetDistanceToZeroPos(myHero.position,pwHud.hudManager.virtualCursorPos) then
			self:MoveTo()
		end
		return

	end
end

function LegitOrb:Waveclear()
	if self.legitMenu.keySettings.Waveclear:get() then 
		self.currentMode = "Waveclear"

		local wait = false
		local mustLasthit = {}
		local canLasthit = {}
		local jungleOrb = {}
		local canOrb = {}
		local missLasthit = {}

		if self.canAttack and not self.isAttacking then

			if self.Nexus then
				local nexusObj = self.Nexus.object
				if nexusObj and nexusObj.isTargetable and not nexusObj.isDead then
					if myHero.characterIntermediate.attackRange + myHero.boundingRadius + self.Nexus.hitbox > LegitCommon:GetDistanceToZero(myHero,nexusObj) then
						self:Attack(nexusObj)
						return
					end
				end
			end

			for i, turret in pairs(self.Inhib) do
				local turretObj = turret.object
				if turretObj and turretObj.isTargetable and not turretObj.isDead then
					if myHero.characterIntermediate.attackRange + myHero.boundingRadius + turret.hitbox > LegitCommon:GetDistanceToZero(myHero,turretObj) then
						self:Attack(turretObj)
						return
					end
				end
			end
			
			for i,minion in pairs(self.EnemyMinions) do
				if minion.health > 0 and minion.isValid and LegitCommon:GetDistance(minion.position,myHero.position) < myHero.characterIntermediate.attackRange + myHero.boundingRadius + minion.boundingRadius and minion.isTargetable then
						if minion.team == TEAM_ENEMY then
							local hpPred2 = self:HpPredClear(minion,myHero.attackDelay + myHero.attackCastDelay*3 + ((max(0,myHero.characterIntermediate.attackRange + myHero.boundingRadius + 50)*2)/self:projectileSpeed(myHero) + NetClient.ping/500))
							if hpPred2 ~= minion.health then
								local hpPred = self:HpPred(minion)
								local dmg = damageLib:GetAutoAttackDamage(myHero, minion)
								if hpPred <= dmg and hpPred2 <= 0 and hpPred > 0 then
									insert( mustLasthit, minion )
								elseif hpPred <= dmg and hpPred > 0 then
									insert( canLasthit, minion )
								elseif hpPred2 - dmg > 0 then
									insert( canOrb, minion )
								end

								if hpPred2 < 0 then
									DrawHandler:Circle3D(minion.position,50,LegitCommon:Hex(250,254,150,107))
									wait = true
								end
							else
								local hpPred = self:HpPred(minion)
								if hpPred <= damageLib:GetAutoAttackDamage(myHero, minion) and hpPred > 0 then
									insert( canLasthit, minion )
								elseif hpPred - damageLib:GetAutoAttackDamage(myHero, minion) > 0 then
									insert( canOrb, minion )
								end
							end
						else
							local dmg = damageLib:GetAutoAttackDamage(myHero, minion)
							local hp = minion.health
							if hp < dmg then 
								if minion.maxHealth >= 700 then
									insert( mustLasthit, minion )
								else
									insert( canLasthit, minion )
								end
							else
								insert( jungleOrb, minion )
							end
						end
				end
			end

			table.sort(mustLasthit, function(a, b)
				local first = a.maxHealth
				local second = b.maxHealth
				return first > second
			end)

			if mustLasthit[1] ~= nil then
				self:Attack(mustLasthit[1])
				return
			end

			table.sort(canLasthit, function(a, b)
				local first = a.maxHealth
				local second = b.maxHealth
				return first > second
			end)

			if canLasthit[1] ~= nil and wait == false then
				self:Attack(canLasthit[1])
				return
			end

			
			for i, turret in pairs(self.Turrets) do
				local turretObj = turret.object
				if turretObj and turretObj.isTargetable and not turretObj.isDead then
					if myHero.characterIntermediate.attackRange + myHero.boundingRadius + turret.hitbox > LegitCommon:GetDistanceToZero(myHero,turretObj) then
						self:Attack(turretObj)
						return
					end
				end
			end

			table.sort(jungleOrb, function(a, b)
				if find(a.charName,"Razor") and find(b.charName,"Razor") then
					local first = a.maxHealth
					local second = b.maxHealth
					return first < second
				else
					local first = a.maxHealth
					local second = b.maxHealth
					return first > second
				end
			end)

			if jungleOrb[1] ~= nil then
				self:Attack(jungleOrb[1])
				return
			end

			table.sort(canOrb, function(a, b)
				local first = a.health
				local second = b.health
				return first < second
			end)

			if canOrb[1] ~= nil and wait == false then
				self:Attack(canOrb[1])
				return
			end

		end

		if self.legitMenu.holdRadius:get() < LegitCommon:GetDistanceToZeroPos(myHero.position,pwHud.hudManager.virtualCursorPos) then
			self:MoveTo()
		end
		return

	end
end

function LegitOrb:JungleClear()
	--
end

function LegitOrb:Freeze()
	if self.legitMenu.keySettings.Freeze:get() then
		self.currentMode = "Freeze"
		--

		local wait = false
		local mustLasthit = {}
		local canLasthit = {}

		if self.canAttack then

			for i,minion in pairs(self.EnemyMinions) do
				if LegitCommon:GetDistance(minion.position,myHero.position) < myHero.characterIntermediate.attackRange + myHero.boundingRadius/2 + minion.boundingRadius/2 then
					local hpPred = self:HpPred(minion) or 0
					local hpPredHit = self:HpPred(minion,0.2 + myHero.attackCastDelay + (max(0,LegitCommon:GetDistance(minion.position,myHero.position) - myHero.boundingRadius))/(myHero.characterIntermediate.attackRange > 350 and myHero.basicAttack.spellDataInfo.missileSpeed or math.huge) + NetClient.ping/1000 - 0.05) or 0
					local hpPred2Hit = self:HpPredClear(minion,myHero.attackCastDelay*3 + ((max(0,myHero.characterIntermediate.attackRange - myHero.boundingRadius)*2)/(myHero.basicAttack.spellDataInfo.missileSpeed > 0 and myHero.basicAttack.spellDataInfo.missileSpeed or math.huge) - 0.1 + NetClient.ping/1000 + 0.07) + myHero.attackDelay ) or 0
					--local dmg = floor((myHero.characterIntermediate.flatPhysicalDamageMod + myHero.characterIntermediate.baseAttackDamage)*(100/(100+minion.characterIntermediate.armor)))
					local dmg = damageLib:GetAutoAttackDamage(myHero, minion)
					if hpPred < 0 then
					
					elseif hpPred <= dmg and hpPred > 0 and hpPredHit <= 5 then
						insert( mustLasthit, minion )
					elseif hpPred <= dmg and hpPred > 0 and hpPred2Hit <= 0 then
						insert( canLasthit, minion )
					end
					if find(minion.charName,"Siege") and hpPred2Hit <= 0 then
						wait = true
					end

				end
			end

			sort(mustLasthit, function(a, b)
				local first = a.maxHealth
				local second = b.maxHealth
				return first > second
			end)
			sort(canLasthit, function(a, b)
				local first = a.maxHealth
				local second = b.maxHealth
				return first > second
			end)


			if mustLasthit[1] ~= nil and (wait == false or (wait == true and find(mustLasthit[1].charName,"Siege"))) then
				self:Attack(mustLasthit[1])
				return
			end

			if canLasthit[1] ~= nil and wait == false then
				self:Attack(canLasthit[1])
				return
			end

		end

		self:MoveTo()
		return

	end
end


function LegitOrb:setAttack(bool)
	self.blockAttack = bool
end

function LegitOrb:setMove(bool)
	self.blockMove = bool
end


LegitOrb:__init()

----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----DEVELOPERS--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

_G.LegitOrbwalker = {
    IsAttacking = function() return LegitOrb.isAttacking end,
    CanAttack = function() return LegitOrb.canAttack end,
    CanMove = function() return LegitOrb:CanMove() end,
    MoveTo = function(self,pos) if pos then LegitOrb.forcePos = pos end local pos = pos or pwHud.hudManager.activeVirtualCursorPos LegitOrb:MoveTo(pos) end,
    Attack = function(self,unit) LegitOrb:Attack(unit) end,
    BlockAttack = function(self,bool) LegitOrb:setAttack(bool) end,
		BlockMove = function(self,bool) LegitOrb:setMove(bool) end,
		IsAttackBlocked = function() return LegitOrb.blockAttack end,
		IsMoveBlocked = function() return LegitOrb.blockMove end,
    GetMode = function() return LegitOrb.currentMode end,
		HpPred = function(self,unit,delta) return LegitOrb:HpPred(unit,delta) end,
		GetTarget = function(self,range,type,pos) local range = range or myHero.characterIntermediate.attackRange return LegitOrb:GetTarget(range,type,pos) end,
		IsTargetForced = function() return LegitOrb.forceTarget ~= nil end,
		GetForcedTarget = function() return LegitOrb.forceTarget end,
		SetForcedTarget = function(self,unit) LegitOrb.forceTarget = unit end,
		UnsetForcedTarget = function() LegitOrb.forceTarget = nil end,
		AutoAttackCooldown = function() return LegitOrb.aaCooldownTimer end,
		AttackSpeed = function() return LegitOrb.attackSpeed end
}
--[[
	LegitOrbwalker:IsAttacking() -- returns boolean
	LegitOrbwalker:CanAttack() -- returns boolean
	LegitOrbwalker:CanMove() -- returns boolean 
	LegitOrbwalker:MoveTo(position) -- Moves to passed position, if nothing is passed moves to mousePos
	LegitOrbwalker:Attack(unit) -- Attacks passed unit (without range check)
	LegitOrbwalker:BlockAttack(bool) -- Blocks attacks // true or false
	LegitOrbwalker:BlockMove(bool) -- Blocks movement // true or false
	LegitOrbwalker:IsAttackBlocked()
	LegitOrbwalker:IsMoveBlocked()
	LegitOrbwalker:GetMode() -- returns current mode as a string (Combo,Harass,Lasthit,Waveclear,nil)
	LegitOrbwalker:HpPred(unit,time) -- returns predicted unit HP
	LegitOrbwalker:GetTarget(range,type,position) --returns target, can be used without arguments ( will return based on your AA range) | type = "AD", "AP" , "HYB" | position = from which position it should search for the target (usefull for Zed W, Oriana Ball, ...)
	LegitOrbwalker:IsTargetForced() -- returns boolean
	LegitOrbwalker:GetForcedTarget() -- returns unit
	LegitOrbwalker:SetForcedTarget(unit) -- forcing the orb to target passed unit
	LegitOrbwalker:UnsetForcedTarget() -- sets forced target to nil
	LegitOrbwalker:AutoAttackCooldown()
	LegitOrbwalker:AttackSpeed()
	]]