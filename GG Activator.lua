-----------------------------------------------------------------
--					Some weird stuff
-----------------------------------------------------------------

--[[


Update History:
27.03.2019 -- Added Strukture, Ignite,Heal,Barrier, Cleanse and OnBuffGain/Lose
27.03.2019 -- Summs and most items
28.03.2019 -- Fixed all Items added QSS
28.03.2019 -- Added more Items, Added Struktur for Items and Smite
29.03.2019 -- Added Exaust Beta and Pots
29.03.2019 -- Fixed Pots, disabled Exhaust, Rework starting slowly

 

-----------------------------------------------------------------
-----------------------------------------------------------------
-----------------------------------------------------------------
--]]
-----------------------------------------------------------------
---------------Update and Version--------------------------------
-----------------------------------------------------------------
local version = 0.1000
local author = "GGK (till now)"

local newVersion = GetInternalWebResult('ggActivator.version')

if tonumber(newVersion) > version then
	DownloadInternalFile('ggActivator.lua', SCRIPT_PATH .. "ggActivator.lua")
	PrintChat("ggActivator updated to: "..version..", please press 2x F5") 
else
	print("You are using the latest Version of ggActivator)")
end

-----------------------------------------------------------------
---------------Summoners and Locals------------------------------
-----------------------------------------------------------------
local Ignite
if myHero.spellbook:Spell(SpellSlot.Summoner1).name == "SummonerDot" then
	Ignite = SpellSlot.Summoner1
elseif myHero.spellbook:Spell(SpellSlot.Summoner2).name == "SummonerDot" then
	Ignite = SpellSlot.Summoner2
end

local Heal
if myHero.spellbook:Spell(SpellSlot.Summoner1).name == "SummonerHeal" then
	Heal = SpellSlot.Summoner1
elseif myHero.spellbook:Spell(SpellSlot.Summoner2).name == "SummonerHeal" then
	Heal = SpellSlot.Summoner2
end

local Barrier
if myHero.spellbook:Spell(SpellSlot.Summoner1).name == "SummonerBarrier" then
	Barrier = SpellSlot.Summoner1
elseif myHero.spellbook:Spell(SpellSlot.Summoner2).name == "SummonerBarrier" then
	Barrier = SpellSlot.Summoner2
end

local Cleanse
if myHero.spellbook:Spell(SpellSlot.Summoner1).name == "SummonerBoost" then
	Cleanse = SpellSlot.Summoner1
elseif myHero.spellbook:Spell(SpellSlot.Summoner2).name == "SummonerBoost" then
	Cleanse = SpellSlot.Summoner2
end

local Exaust
if myHero.spellbook:Spell(SpellSlot.Summoner1).name == "SummonerExhaust" then
	Exaust = SpellSlot.Summoner1
elseif myHero.spellbook:Spell(SpellSlot.Summoner2).name == "SummonerExhaust" then
	Exaust = SpellSlot.Summoner2
end

local lastCC =  nil
local stun = nil
local Suppression = nil
local taunt = nil
local polymorph = nil
local snare = nil
local fear = nil
local charm = nil
local blind = nil
local disarm = nil
local flee = nil
local sleep = nil
local knockback = nil

local map = MissionInfo.mapId
local team = myHero.team
local base =
	map == 11 and (
		team == 100 and {x = 416, y = 183, z = 424} or
		team == 200 and {x = 14304, y = 171, z = 14396}
	)

local delays = {}
AddEvent(Events.OnTick, function()
    local time = RiotClock.time

    for i = 1, #delays do
        local data = delays[i]
        
        if data then
            if time - data.delay >= 0 then 
                data.f()
                delays[i] = nil
            end
        end
    end
end)

--[[ Waiting for Damagelib all Champs
local Smite
if myHero.spellbook:Spell(SpellSlot.Summoner1).name == "SummonerSmite" or myHero.spellbook:Spell(SpellSlot.Summoner1).name == "S5_SummonerSmitePlayerGanker" or myHero.spellbook:Spell(SpellSlot.Summoner1).name == "S5_SummonerSmiteDuel" then
	Smite = SpellSlot.Summoner1
elseif myHero.spellbook:Spell(SpellSlot.Summoner2).name == "SummonerSmite" or myHero.spellbook:Spell(SpellSlot.Summoner2).name == "S5_SummonerSmitePlayerGanker" or myHero.spellbook:Spell(SpellSlot.Summoner2).name == "S5_SummonerSmiteDuel" then
	Smite = SpellSlot.Summoner2
end
--]]
-----------------------------------------------------------------
--					For Rework #FPS Gain and stuff
-----------------------------------------------------------------

--[[


local CCList = {
    ['__05'] = {name = "Stun", id = 5, stun},
    ['__07'] = {name = "Silence", id = 7, silence},
    ['__08'] = {name = "Taunt", id = 8, taunt},
    ['__09'] = {name = "Polymorph", id = 9, polymorph},
    ['__11'] = {name = "Snare", id = 11, snare},
    ['__18'] = {name = "Sleep", id = 18, asleep},
    ['__21'] = {name = "Fear", id = 21, fear},
    ['__22'] = {name = "Charm", id = 22, charm},
    ['__24'] = {name = "Suppression", id = 24, Suppression},
    ['__25'] = {name = "Blind", id = 25, blind},
    ['__28'] = {name = "Flee", id = 28, flee},
    ['__29'] = {name = "Knockup", id = 29, Knockup},
    ['__30'] = {name = "Knockback", id = 30, knockback},
    ['__31'] = {name = "Disarm", id = 31, disarm},
}

local Items = {
	--TODO ItemDamage, (when AfterAttack, Killsteal) , isTarget = true
	--Items
	['__3153'] = {name = "Blade of the RuinedKing", id = 3153, range = 550, type = "DMG"}, 
	['__3144'] = {name = "Bilgewater Cutlass", id = 3144, range = 550, type = "DMG"},
    ['__3146'] = {name = "Hextech Gunblade", id = 3146, range = 550, type = "DMG"},
	['__2139'] = {name = "Zhonyas", id = 2139, type = "DEF"},
	['__2420'] = {name = "Stopwatch" , id = 2420, type = "DEF"},
	['__3190'] = {name = "Locket of the Solari", id = 3190, range = 550, type = "TEAMDEF"}
	['__3140'] = {name = "Quicksilver Sash", id = 3140, type = "QSS"},
	['__3139'] = {name = "Mercurial Scimitar", id = 3139, type = "QSS"},
	['__3222'] = {name = "Mikaels Crucible", id = 3222, range = 850, type = "TEAMQSS"},
}
--]]
local Pots = {
	--Pots
	['__2010'] = {name = "Biscuit of Rejeventation", id = 2010, buffName ="ItemMiniRegenPotion", type = ""},
	['__2033'] = {name = "Corrupting Potion", id = 2033, buffname = "ItemDarkCrystalFlask", type = "POTMANA"},
	['__2003'] = {name = "Health Potion", id = 2003, buffName = "RegenerationPotion", type = "" },
	['__2032'] = {name = "Hunters Potion", id = 2032, buffName = "ItemCrystalFlaskJungle", type = "POTMANA"},
	['__2031'] = {name = "Refillable Potion", id = 2031, buffName = "ItemCrystalFlask", type = ""},

}


-----------------------------------------------------------------
-------------------Requirements----------------------------------
require'FF15Menu'
--DamageLib = require'FF15DamageLib'

local Activator = setmetatable({}, {
	__call = function(self)
		self:init()
	end
})

-----------------------------------------------------------------
--				Menu
-----------------------------------------------------------------

--REWORK THIS SHIT
function Activator:loadMenu()
	self.Menu = Menu("ggActivator", "ggActivator")
		self.Menu:checkbox("Enable", "Use Activator on Combo", true) 
		self.Menu:sub("Summoner", "Summoners")
			if Ignite then self.Menu.Summoner:checkbox("Ignite", "Use Ignite", true) end --Add Killable Logic for Champ Req DmgLib with EveryDamage Calcs
			--if Ignite then self.Menu.Summoner.checkbox("autoIgnite", "Auto Ignite", false) end Auto Ignite for Every Target without any Logic just DMG >= enemy.healthpercent 
			if Heal then self.Menu.Summoner:checkbox("Heal", "Use Heal", false) end
			if Heal then self.Menu.Summoner:checkbox("HealAllies", "Use Heal Allies", true) end
			if Heal then self.Menu.Summoner:slider("minHeal", "min Health to heal", 1, 100, 10) end
			if Heal then self.Menu.Summoner:slider("minHealAllie", "min Health to heal Allie", 1, 100, 10) end
			if Barrier then self.Menu.Summoner:checkbox("Barrier", "Use Barrier", false) end
			if Barrier then self.Menu.Summoner:slider("minBarrier", "min Health to Barrier", 1, 100, 10) end
			--if Exaust then self.Menu.Summoner:checkbox("Exaust", "Use Exaust BETA", true) end
			--if Exaust then self.Menu.Summoner:slider("ExhaustSlider", "BETA", 1, 40, 20, 1) end
			-- if Smite then self.Menu.Summoner:checkbox("Smite", "Use Smite", true)
			-- if smite then self.Menu.Summoner:key("Key", "Smite key", 0x20)
			-- if Smite then self.Menu.Summoner:permashow()
		--Need TO Rework
			if Cleanse then  
				self.Menu.Summoner:checkbox("Cleanse", "Use Cleanse", true)
				self.Menu.Summoner:slider("Delay", "Delay", 0, 100, 1, 0.1)
				self.Menu.Summoner:sub("listClean", "Clean")
				self.Menu.Summoner.listClean:checkbox("stun", "On Stun", true) 
				self.Menu.Summoner.listClean:checkbox("Suppression", "On Silence", true)
				self.Menu.Summoner.listClean:checkbox("taunt", "On Taunt", true)
				self.Menu.Summoner.listClean:checkbox("polymorph", "On Polymorph", true)
				self.Menu.Summoner.listClean:checkbox("fear", "On Fear", true)
				self.Menu.Summoner.listClean:checkbox("charm", "On Charm", true)
				self.Menu.Summoner.listClean:checkbox("blind", "On Blind", true)
				self.Menu.Summoner.listClean:checkbox("disarm", "On Disarm", true)
				self.Menu.Summoner.listClean:checkbox("flee", "On Flee", true)
				self.Menu.Summoner.listClean:checkbox("asleep", "On Sleep", true)
				self.Menu.Summoner.listClean:checkbox("snare", "On Snare", true)
				self.Menu.Summoner.listClean:checkbox("knockback", "On Knockback", false)
			end
	
			self.Menu:sub("Items", "Items")
				self.Menu.Items:sub("BOTRK", "Blade of the RuinedKing")
				self.Menu.Items.BOTRK:checkbox("Enable", "Enable", true)
				self.Menu.Items.BOTRK:slider("minHPHero", "min Hero HP to use", 0, 100, 70, 1)
				self.Menu.Items.BOTRK:slider("minHPEnemy", "min Enemy HP to use", 1, 100, 80, 1)

				self.Menu.Items:sub("BC", "Bilgewater Cutlass")
				self.Menu.Items.BC:checkbox("Enable", "Enable", true)
				self.Menu.Items.BC:slider("minHPHero", "min Hero HP to use", 0, 100, 70, 1)
				self.Menu.Items.BC:slider("minHPEnemy", "min Enemy HP to use", 1, 100, 80, 1)		

				self.Menu.Items:sub("HG", "Hextech Gunblade")
				self.Menu.Items.HG:checkbox("Enable", "Enable", true)
				self.Menu.Items.HG:slider("minHPHero", "min Hero HP to use", 0, 100, 70, 1)
				self.Menu.Items.HG:slider("minHPEnemy", "min Enemy HP to use", 1, 100, 80, 1)			


				self.Menu.Items:sub("Zhonyas", "Zhonyas")
				self.Menu.Items.Zhonyas:checkbox("Enable", "Enable", true)
				self.Menu.Items.Zhonyas:slider("minHPHero", "min Hero HP to use", 1, 100, 20, 1)
				--TO ADD On Risky spells	

				self.Menu.Items:sub("Stopwatch", "Stopwatch")
				self.Menu.Items.Stopwatch:checkbox("Enable", "Enable", false)
				self.Menu.Items.Stopwatch:slider("minHPHero", "min Hero HP to use", 1, 100, 10, 1)

				self.Menu.Items:sub("Archangel", "Archangel and ManaMune")
				self.Menu.Items.Archangel:checkbox("Enable", "Enable", true)
				self.Menu.Items.Archangel:slider("minHPHero", "min Hero HP to use", 1, 100, 40, 1)


				self.Menu.Items:sub("Locket", "Locket of Solari")
				self.Menu.Items.Locket:checkbox("Enable", "Enable", true)
				self.Menu.Items.Locket:slider("minHPAllie", "min A	llie HP to use", 1, 100, 40, 1)
				
				local nameTable = {}
                local Allie = ObjectManager:GetAllyHeroes()
				
                for _, Allie in pairs(Allie) do
                    nameTable[_] = Allie.charName
					
                end
                self.Menu.Items.Locket:multiList("LocketList", "Select Champs", nameTable, nameTable)
					
--[[
				self.Menu.Items:sub("Mikaels", "Mikales")
				self.Menu.Items.Mikaels:checkbox("Enable", "Enable", true)
				local nameTable = {}
                local Allie = ObjectManager:GetAllyHeroes()
                for _, Allie in pairs(Allie) do
                    nameTable[_] = Allie.charName
					
                end
                self.Menu.Items.Mikaels:priorityList("MikaelsList", "Select Champs", nameTable, nameTable)
				self.Menu.Items.Mikaels:sub("listClean", "Clean List")
				self.Menu.Items.Mikaels.listClean:checkbox("stun", "On Stun", true) 
				self.Menu.Items.Mikaels.listClean:checkbox("Suppression", "On Silence", false)
				self.Menu.Items.Mikaels.listClean:checkbox("taunt", "On Taunt" ,true)
				self.Menu.Items.Mikaels.listClean:checkbox("polymorph", "On Polymorph", true)
				self.Menu.Items.Mikaels.listClean:checkbox("fear", "On Fear", true)
				self.Menu.Items.Mikaels.listClean:checkbox("charm", "On Charm", true)
				self.Menu.Items.Mikaels.listClean:checkbox("blind", "On Blind", true)
				self.Menu.Items.Mikaels.listClean:checkbox("disarm", "On Disarm", true)
				self.Menu.Items.Mikaels.listClean:checkbox("flee", "On Flee", true)
				self.Menu.Items.Mikaels.listClean:checkbox("asleep", "On Sleep", true)
				self.Menu.Items.Mikaels.listClean:checkbox("snare", "On Snare", true)
				self.Menu.Items.Mikaels.listClean:checkbox("knockback", "On Knockback", false)

--]]
				self.Menu.Items:sub("QSS", "QSS Items")
				self.Menu.Items.QSS:checkbox("Enable", "Enable", true)
				self.Menu.Items.QSS:slider("Delay", "Delay", 0, 100, 1, 0.1)
				self.Menu.Items.QSS:sub("listClean", "Clean List")
				self.Menu.Items.QSS.listClean:checkbox("stun", "On Stun", true) 
				self.Menu.Items.QSS.listClean:checkbox("Suppression", "On Silence", false)
				self.Menu.Items.QSS.listClean:checkbox("taunt", "On Taunt" ,true)
				self.Menu.Items.QSS.listClean:checkbox("polymorph", "On Polymorph", true)
				self.Menu.Items.QSS.listClean:checkbox("fear", "On Fear", true)
				self.Menu.Items.QSS.listClean:checkbox("charm", "On Charm", true)
				self.Menu.Items.QSS.listClean:checkbox("blind", "On Blind", true)
				self.Menu.Items.QSS.listClean:checkbox("disarm", "On Disarm", true)
				self.Menu.Items.QSS.listClean:checkbox("flee", "On Flee", true)
				self.Menu.Items.QSS.listClean:checkbox("asleep", "On Sleep", true)
				self.Menu.Items.QSS.listClean:checkbox("snare", "On Snare", true)
				self.Menu.Items.QSS.listClean:checkbox("knockback", "On Knockback", false)




--types: DMG,DEF,TEAMDEF (locket), QSS, TEAMQSS Mikaels
			self.Menu:sub("Pots", "Pots")
			self.Menu.Pots:checkbox("Enable", "Enable", true)

			for id, Pots in pairs(Pots) do
				self.Menu.Pots:sub(id, Pots.name)
				self.Menu.Pots[id]:checkbox("Enable", "Enable", true)
				self.Menu.Pots[id]:slider("minHPHero", "min HP ", 1, 100, 50, 1)

				if Pots.type == 'POTMANA'then
					self.Menu.Pots[id]:slider("minMPHero", "min Mana", 1, 100, 50, 1)
					self.Menu.Pots[id]:slider("HPCheck", "Only Use when HP is also under X", 1, 100, 70, 1)
				end
			end

end
-----------------------------------------------------------------
--			Common
-----------------------------------------------------------------
function Activator:isValidTarget(Unit, range, from)
	local range = range
	local from = from or myHero
	return Unit.isValid and Unit.isVisible and not Unit.isDead and not Unit.isInvulnerable and not Unit.isZombie and self:GetDistanceSqr(Unit, from) <= range*range
end

function Activator:GetDistanceSqr(p1, p2)
        p2 = p2 or myHero
        p1 = p1.position or p1
        p2 = p2.position or p2
        
        local dx = p1.x - p2.x
        local dz = p1.z - p2.z
        return dx * dx + dz * dz
    end
    
    local sqrt = math.sqrt
    local function GetDistance(p1, p2)
        return sqrt(GetDistanceSqr(p1, p2))
end 

function Activator:delayAction(func, time)
    delays[#delays + 1] = {delay = time + RiotClock.time, f = func}
end

function Activator:TotalAD(Unit)
	if not Unit then return end
	return Unit.characterIntermediate.flatPhysicalDamageMod + Unit.characterIntermediate.baseAttackDamage
end

function Activator:TotalAP(Unit)
	if not Unit then return end
	return Unit.characterIntermediate.flatMagicDamageMod + Unit.characterIntermediate.baseAbilityDamage
end

function Activator:Base(a, b, v)
	local dx, dy, dz = a.x - b.x, a.y - b.y, a.z - b.z

	return (dx * dx + dy * dy + dz * dz) <= v * v
end


--Thx to Hanndel
function Activator:Selector(charName)
	
	local p0 = {"testData"}
	local p1 = {"Alistar", "Amumu", "Blitzcrank", "Braum", "Cho'Gath", "DrMundo", "Garen", "Gnar", "Maokai", "Hecarim", "Janna", "Leona", "Lulu", "Malphite", "Nasus", "Nautilus", "Nunu", "Olaf", "Rammus", "Renekton", "Sejuani", "Shen", "Shyvana", "Singed", "Sion", "Skarner", "Taric", "TahmKench", "Thresh", "Volibear", "Warwick", "MonkeyKing", "Yorick", "Zac", "Poppy", "Ornn"}
	local p2 = {"Aatrox", "Darius", "Elise", "Evelynn", "Galio", "Gragas", "Irelia", "Jax", "LeeSin", "Morgana", "Nocturne", "Pantheon", "Rumble", "Swain", "Trundle", "Tryndamere", "Udyr", "Urgot", "Vi", "XinZhao", "RekSai", "Bard", "Nami", "Sona", "Camille", "Jarvan IV", "Kayn"}
	local p3 = {"Akali", "Diana", "Ekko", "FiddleSticks", "Fiora", "Gangplank", "Fizz", "Heimerdinger", "Jayce", "Kassadin", "Kayle", "Kha'Zix", "Lissandra", "Mordekaiser", "Nidalee", "Riven", "Shaco", "Vladimir", "Yasuo", "Zilean", "Zyra", "Ryze", "Rengar"}
	local p4 = {"PracticeTool_TargetDummy", "Ahri", "Anivia", "Annie", "Ashe", "Azir", "Brand", "Caitlyn", "Cassiopeia", "Corki", "Draven", "Ezreal", "Graves", "Jinx", "Kalista", "Karma", "Karthus", "Katarina", "Kennen", "KogMaw", "Kindred", "Leblanc", "Lucian", "Lux", "Malzahar", "MasterYi", "MissFortune", "Orianna", "Quinn", "Sivir", "Syndra", "Talon", "Teemo", "Tristana", "TwistedFate", "Twitch", "Varus", "Vayne", "Veigar", "Velkoz", "Viktor", "Xerath", "Zed", "Ziggs", "Jhin", "Soraka", "Zoe", "Kaisa"}
   
	for i,n in pairs(p0) do if n == charName then return 0.5 end end
	for i,n in pairs(p1) do if n == charName then return 0.85 end end 
	for i,n in pairs(p2) do if n == charName then return 1 end end 
	for i,n in pairs(p3) do if n == charName then return 1.25 end end 
	for i,n in pairs(p4) do if n == charName then return 1.75 end end 
	return 1.05
end

function Activator:EnemiesAround(range, pos)
    local pos = pos or myHero.position
    local count = 0
    for i = 1 ,#self.Enemies do
    	local Enemies = self.Enemies
        if Enemies.isVisible and not Enemies.isDead and Enemies.isValid then
            if self:GetDistanceSqr(pos, Enemies.position) <= range then
                count = count + 1
            end
        end
    end
    return count
end


function Activator:AlliesAround(range, pos)
    local pos = pos or myHero.position
    local count = 0
    for i = 1 ,#self.Allies do
    	local Allie = self.Allies[i]
        if Allie.isVisible and not Allie.isDead and Allie.isValid then
            if self:GetDistanceSqr(Allie.position, pos) <= range then
                count = count + 1
            end
        end
    end
    return count
end
----------------------------------------------------------------
--				init and Callbacks and Tables
-----------------------------------------------------------------

function Activator:init()
	self:loadTables()
	self:loadMenu()
	self:loadCallbacks()
end

function Activator:loadCallbacks()
	AddEvent(Events.OnTick, function() self:OnTick() end)
	AddEvent(Events.OnBuffGain, function(Obj, Buff) self:OnBuffGain(Obj, Buff) end)
	AddEvent(Events.OnBuffLost, function(Obj, Buff) self:OnBuffLost(Obj, Buff) end)
end

function Activator:OnBuffGain(Obj, Buff)
	if Obj == myHero then
		if Buff.type == 5 then stun = true end
		if Buff.type == 24 then Suppression = true end
		if Buff.type == 8 then taunt = true end
		if Buff.type == 9 then polymorph = true end
		if Buff.type == 11 then snare = true end
		if Buff.type == 21 then fear = true end
		if Buff.type == 22 then charm = true end
		if Buff.type == 25 then blind = true end
		if Buff.type == 31 then disarm = true end
		if Buff.type == 28 then flee = true end
		if Buff.type == 18 then sleep = true end
		if Buff.type == 30 then knockback = true end
	end
end



function Activator:OnBuffLost(Obj, Buff)
	if Obj == myHero then
		if Buff.type == 5 then stun = false end
		if Buff.type == 24 then Suppression = false end
		if Buff.type == 8 then taunt = false end
		if Buff.type == 9 then polymorph = false end
		if Buff.type == 11 then snare = false end
		if Buff.type == 21 then fear = false end
		if Buff.type == 22 then charm = false end
		if Buff.type == 25 then blind = false end
		if Buff.type == 31 then disarm = false end
		if Buff.type == 28 then flee = false end
		if Buff.type == 18 then sleep = false end
		if Buff.type == 30 then knockback = false end
	end
end

function Activator:OnTick()

	--PrintChat(self:Priority)
	
	if self.Menu.Enable:get() and not myHero.isDead then
		self:Klepto()
		if LegitOrbwalker:GetMode() == "Combo" then
			local Unit = LegitOrbwalker:GetTarget(1000)
				if Unit then			
-----------------------------------------------------------------
--OnTick Summoners
-----------------------------------------------------------------
				if Ignite then self:Ignite(Unit) end
				if Heal then self:Heal() end
				if Barrier then self:Barrier() end
				if Cleanse then self:Cleanse() end
				--if Exaust then self:Exaust() end
-----------------------------------------------------------------
--OnTick Item
-----------------------------------------------------------------
				self:Items(Unit);
-----------------------------------------------------------------
--OnTick Pots
-----------------------------------------------------------------
				end


				self:QSS()
				self:Zhonyas()
				self:StopWatch()
				self:Archangel()
				self:Pot()
				self:Locket()
		end
	end
end

function Activator:loadTables()
	self.Enemies = {}
	self.Allies = {}
	self:loadUnits()
	self.Spells = {}

	self.Spells.Ignite =
	{
		range = 575,
		isReady = function()
			return Ignite and myHero.spellbook:Spell(spellSlot).cooldownTimeRemaining == 0
		end,
		damage = function()
			return 20 * myHero.experience.level + 50
		end
	}

	self.Spells.Heal =
	{
		range = 850,
		isReady = function()
			return Heal and myHero.spellbook:Spell(spellSlot).cooldownTimeRemaining == 0
		end	
	}

	self.Spells.Cleanse = 
	{
		isReady = function()
			return Cleanse and myHero.spellbook:Spell(spellSlot).cooldownTimeRemaining == 0
		end
	}

	self.Spells.Barrier =
	{
		isReady = function()
		return Barrier and myHero.spellbook:Spell(spellSlot).cooldownTimeRemaining == 0
		end
	}

	self.Spells.Exaust = 
	{
		range = 650,
		isReady = function()
			return Exaust and myHero.spellbook:Spell(spellSlot).cooldownTimeRemaining == 0
		end
	}

--[[When DamageLib full
	self.Spells.Smite = {
		range = 500,
		isReady = function()
			return Exaust and myHero.spellbook:Spell(spellSlot).cooldownTimeRemaining == 0
		end
		damagePerLevel = {
		0, 20, 40, 60, 90, 120, 150, 180, 210, 250, 290, 330, 370, 410, 460, 510, 550, 610 
		},
		rawDamge = functio()
			local i = myHero.experience.level
			return 370 + myHero.experience.leve[i]
	end
	}
--]]

end


function Activator:loadUnits()
	self.Allies = ObjectManager:GetAllyHeroes()
	self.Enemies = ObjectManager:GetEnemyHeroes()
end

-----------------------------------------------------------------
--					Summoners 
-----------------------------------------------------------------

--Ignite
-----------------------------------------------------------------
function Activator:Ignite(Unit)
	if Ignite then
		if self.Menu.Summoner.Ignite:get() then
			if self.Spells.Ignite:isReady() and self:isValidTarget(Unit, self.Spells.Ignite.range) and self.Spells.Ignite:damage() >= Unit.health then
				myHero.spellbook:CastSpell(Ignite, Unit.networkId)
			end
			for i = 1 ,#self.Enemies do
				local Enemie = self.Enemies[i]
				if self:isValidTarget(Enemie, self.Spells.Ignite.range) and self.Spells.Ignite:damage() >= Enemie.health then
					myHero.spellbook:CastSpell(Ignite, Enemie.networkId)
				end
			end
		end
	end
end

--Heal
-----------------------------------------------------------------
function Activator:Heal()
	if Heal then
		if self.Menu.Summoner.Heal:get() then
			if self.Spells.Heal:isReady() and myHero.health/myHero.maxHealth*100 <= self.Menu.Summoner.minHeal:get() then 
				myHero.spellbook:CastSpell(Heal, myHero.networkId)
			end
		end
	end
--?? Only Heal when Allie lower then hero?	
	if Heal then
		if self.Menu.Summoner.HealAllies:get() then
			if self.Spells.Heal:isReady() and self:AlliesAround(self.Spells.Heal.range, myHero.position) > 1 then
				for i = 1, #self.Allies do
					local Ally = self.Allies[i]
						if Ally.health/Ally.maxHealth * 100 <= self.Menu.Summoner.minHealAllie:get() then
							myHero.spellbook:CastSpell(Heal, myHero.networkId)
						end
				end
			end
		end
	end
end


--Barrier
-----------------------------------------------------------------
function Activator:Barrier()
	if Barrier then
		if self.Menu.Summoner.Barrier:get() then
			if self.Spells.Heal:isReady() and myHero.health/myHero.maxHealth*100 <= self.Menu.Summoner.minBarrier:get() then 
				myHero.spellbook:CastSpell(Heal, myHero.networkId)
			end
		end
	end
end

--Cleanse
-----------------------------------------------------------------
function Activator:Cleanse()
	if Cleanse then
		if self.Menu.Summoner.Cleanse:get() then

			if self.Menu.Summoner.listClean.stun:get() and stun then
				myHero.spellbook:CastSpell(Cleanse, myHero.networkId) end
			if self.Menu.Summoner.listClean.Suppression:get() and Suppression then
				myHero.spellbook:CastSpell(Cleanse, myHero.networkId) end
			if self.Menu.Summoner.listClean.taunt:get() and taunt then
				myHero.spellbook:CastSpell(Cleanse, myHero.networkId) end
			if self.Menu.Summoner.listClean.polymorph:get() and polymorph then
				myHero.spellbook:CastSpell(Cleanse, myHero.networkId) end
			if self.Menu.Summoner.listClean.snare:get() and snare then
				myHero.spellbook:CastSpell(Cleanse, myHero.networkId) end
			if self.Menu.Summoner.listClean.fear:get() and fear then
				myHero.spellbook:CastSpell(Cleanse, myHero.networkId) end
			if self.Menu.Summoner.listClean.charm:get() and charm then
				myHero.spellbook:CastSpell(Cleanse, myHero.networkId) end
			if self.Menu.Summoner.listClean.blind:get() and blind then
				myHero.spellbook:CastSpell(Cleanse, myHero.networkId) end
			if self.Menu.Summoner.listClean.disarm:get() and disarm then
				myHero.spellbook:CastSpell(Cleanse, myHero.networkId) end
			if self.Menu.Summoner.listClean.flee:get() and flee then
				myHero.spellbook:CastSpell(Cleanse, myHero.networkId) end
			if self.Menu.Summoner.listClean.asleep:get() and sleep then
				myHero.spellbook:CastSpell(Cleanse, myHero.networkId) end
			if self.Menu.Summoner.listClean.knockback:get() and knockback then
				myHero.spellbook:CastSpell(Cleanse, myHero.networkId) end
		end
	end
end








-----------------------------------------------------------------
-----------------------------------------------------------------



--Items
function Activator:Items(Unit)

-----------------------------------------------------------------
--Blade of The Ruin King
-----------------------------------------------------------------
	if self.Menu.Items.BOTRK.Enable:get() then
		local item = myHero.inventory:HasItem(3153)
		if item and Unit and self:isValidTarget(Unit, 550, myHero) then
			local spellSlot=item.spellSlot
			if Unit.health/Unit.maxHealth * 100 <= self.Menu.Items.BOTRK.minHPEnemy:get() or self.Menu.Items.BOTRK.minHPEnemy:get() == 1 and myHero.health/myHero.maxHealth * 100 <= self.Menu.Items.BOTRK.minHPHero:get() or self.Menu.Items.BOTRK.minHPHero:get() == 0 then
				myHero.spellbook:CastSpell(spellSlot, Unit.networkId)
			end
		end
	end
-----------------------------------------------------------------
--BilgeWater Cutlass
-----------------------------------------------------------------
	if self.Menu.Items.BC.Enable:get() then
		local item = myHero.inventory:HasItem(3144)
		if item and Unit and  self:isValidTarget(Unit, 550, myHero) then
			local spellSlot=item.spellSlot
			if Unit.health/Unit.maxHealth * 100 <= self.Menu.Items.BC.minHPEnemy:get() or self.Menu.Items.BC.minHPEnemy:get() == 0 and myHero.health/myHero.maxHealth * 100 <= self.Menu.Items.BC.minHPHero:get() or self.Menu.Items.BC.minHPHero:get() == 0 then
				myHero.spellbook:CastSpell(spellSlot, Unit.networkId)
			end
		end
	end
-----------------------------------------------------------------
--hextechgunblade
-----------------------------------------------------------------	
	if self.Menu.Items.HG.Enable:get() then
		local item = myHero.inventory:HasItem(3146)
		if item and Unit and self:isValidTarget(Unit, 600, myHero) then
			local spellSlot=item.spellSlot
			if Unit.health/Unit.maxHealth * 100 <= self.Menu.Items.HG.minHPEnemy:get() or self.Menu.Items.HG.minHPEnemy:get() == 0 and myHero.health/myHero.maxHealth * 100 <= self.Menu.Items.HG.minHPHero:get() or self.Menu.Items.HG.minHPHero:get() == 0 then
				myHero.spellbook:CastSpell(spellSlot, Unit.networkId)
			end
		end
	end
end




-----------------------------------------------------------------
--QSS
-----------------------------------------------------------------
function Activator:QSS()
	if self.Menu.Items.QSS.Enable:get() then
			local item = myHero.inventory:HasItem(3140) or myHero.inventory:HasItem(3139)
			if item then
				local spellSlot = item.spellSlot
				if self.Menu.Items.QSS.listClean.stun:get() and stun then
					self:delayAction(myHero.spellbook:CastSpell(spellSlot, myHero.networkId), self.Menu.Items.QSS.Delay:get())end
				if self.Menu.Items.QSS.listClean.Suppression:get() and Suppression then
					myHero.spellbook:CastSpell(spellSlot, myHero.networkId) end
				if self.Menu.Items.QSS.listClean.taunt:get() and taunt then
					myHero.spellbook:CastSpell(spellSlot, myHero.networkId) end
				if self.Menu.Items.QSS.listClean.polymorph:get() and polymorph then
					myHero.spellbook:CastSpell(spellSlot, myHero.networkId) end
				if self.Menu.Items.QSS.listClean.snare:get() and snare then	
					myHero.spellbook:CastSpell(spellSlot, myHero.networkId) end 
				if self.Menu.Items.QSS.listClean.fear:get() and fear then
					myHero.spellbook:CastSpell(spellSlot, myHero.networkId) end 
				if self.Menu.Items.QSS.listClean.charm:get() and charm then
					myHero.spellbook:CastSpell(spellSlot, myHero.networkId) end
				if self.Menu.Items.QSS.listClean.blind:get() and blind then
					myHero.spellbook:CastSpell(spellSlot, myHero.networkId) end
				if self.Menu.Items.QSS.listClean.disarm:get() and disarm then
					myHero.spellbook:CastSpell(spellSlot, myHero.networkId) end
				if self.Menu.Items.QSS.listClean.flee:get() and flee then
					myHero.spellbook:CastSpell(spellSlot, myHero.networkId) end
				if self.Menu.Items.QSS.listClean.asleep:get() and sleep then
					myHero.spellbook:CastSpell(spellSlot, myHero.networkId) end
				if self.Menu.Items.QSS.listClean.knockback:get() and knockback then
					myHero.spellbook:CastSpell(spellSlot, myHero.networkId) end

			end
	end
end


function Activator:Zhonyas()
	if self.Menu.Items.Zhonyas.Enable:get() then
		local item = myHero.inventory:HasItem(3157)
		if item then
			
			local spellSlot = item.spellSlot
			if myHero.health/myHero.maxHealth*100 <= self.Menu.Items.Zhonyas.minHPHero:get() then
				myHero.spellbook:CastSpell(spellSlot, myHero.networkId)
			end
		end
	end
end

function Activator:StopWatch()
	if self.Menu.Items.Stopwatch.Enable:get() then
		local item = myHero.inventory:HasItem(2420)	
		if item then
			local spellSlot = item.spellSlot
			if myHero.health/myHero.maxHealth * 100 <= self.Menu.Items.Stopwatch.minHPHero:get() then
				myHero.spellbook:CastSpell(spellSlot, myHero.networkId)
			end
		end
	end
end

function Activator:Archangel()
	if self.Menu.Items.Archangel.Enable:get() then
		local item = myHero.inventory:HasItem(3003)
		local item2 = myHero.inventory:HasItem(3004)
		if item or item2 then
			local spellSlot = item.spellSlot
			local spellSlot2 = item2.spellSlot
			if item and myHero.heal/myHero.maxHealth * 100 <= self.Menu.Items.Archangel.minHPHero:get() then
				myHero.spellbook:CastSpell(spellSlot, myHero.networkId)	
			elseif item2 and myHero.heal/myHero.maxHealth * 100 <= self.Menu.Items.Archangel.minHPHero:get() then
				myHero.spellbook:CastSpell(spellSlot2, myHero.networkId)	
			end
			if item and item2 and myHero.heal/myHero.maxHealth * 100 <= self.Menu.Items.Archangel.minHPHero:get() then
				myHero.spellbook:CastSpell(spellSlot, myHero.networkId)
				myHero.spellbook:CastSpell(spellSlot2, myHero.networkId)
			end
		end
	end
end	





function Activator:Locket()
	if self.Menu.Items.Locket.Enable:get() then
		local item = myHero.inventory:HasItem(3190)
		if item then
			local spellSlot = item.spellSlot
			for i = 1, #self.Allies do
				local Ally = self.Allies[i]
				for key, value in pairs(self.Menu.Items.Locket.LocketList:get()) do
					local listName = tostring(key)

				if Ally.charName == tostring(key) and value and self.Menu.Items.Locket.minHPAllie:get() >= Ally.healthPercent then
					myHero.spellbook:CastSpell(spellSlot, Ally.networkId)
				end
				end
			end
		end
	end
end

function Activator:Klepto()
	local money = myHero.inventory:HasItem(2319)
	local hppot = myHero.inventory:HasItem(2061)
	local manapot = myHero.inventory:HasItem(2004)
	local adpot = myHero.inventory:HasItem(2062)
		if money then
			local moneyspellSlot = money.spellSlot 
			myHero.spellbook:CastSpell(moneyspellSlot, myHero.networkId)
		end

		if hppot then
			local hppotspellSlot = hppot.spellSlot 
			myHero.spellbook:CastSpell(hppotspellSlot, myHero.networkId)
		end

		if manapot then
			local manapotspellSlot = manapot.spellSlot 
			myHero.spellbook:CastSpell(manapotspellSlot, myHero.networkId)
		end

		if adpot  then
			local adpotspellSlot = adpot.spellSlot 
			myHero.spellbook:CastSpell(adpotspellSlot, myHero.networkId)
		end

end		

-----------------------------------------------------------------
--				Pots
-----------------------------------------------------------------

function Activator:Pot()
	if self.Menu.Pots.Enable:get() and not self:Base(myHero.position, base, 1200) then

		for i = 0, 5 do
			local item = myHero.inventory:InventorySlot(i)
			local slot = item.inventorySlotNode and item.inventorySlotNode.itemNode.spellSlot
			local ammo = item.ammo

			if ammo > 0 then
				local data = item.inventorySlotNode.itemNode
				local id = '__' .. data.itemId
				local pot = Pots[id]

				if pot and self.Menu.Pots[id].Enable:get() then
					local minHP = self.Menu.Pots[id].minHPHero:get()
					local lowHP = myHero.healthPercent <= minHP
					local cast = false


					if Pots.type == 'POTMANA' then
						local minMP = self.Menu.Pots[id].minMPHero:get()
						local both = self.Menu.Pots[id].HPCheck:get()

						lowHPCheck = myHero.healthPercent <= both
						lowMP = myHero.manaPercent <= minMP

						cast = lowHPCheck and lowMP or lowHPCheck and not lowMP

						
						else
							cast = lowHP
					end

					if cast and not myHero.buffManager:HasBuff(pot.buffName) then
						local slot = i + 6

						myHero.spellbook:CastSpell(slot, myHero.position)
					end
				end
			end
		end
	end
end







-----------------------------------------------------------------
--OnLoad
-----------------------------------------------------------------
function OnLoad()
	Activator()
	PrintChat("[WIP] Activator loaded by GGK " ..version.. " ")
end
