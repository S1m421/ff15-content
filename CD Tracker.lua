local CdTracker = {}

local version = 1.14
if tonumber(GetInternalWebResult("CdTracker.version")) > version then 
    DownloadInternalFile("CdTracker.lua", SCRIPT_PATH.."CdTracker.lua")
    PrintChat("New version! V: "..tonumber(GetInternalWebResult("CdTracker.version")).." Press F5")
end

require"FF15Menu"
function CdTracker:__init()
    self.kek = {"q", "w", "e", "r"}
    self:Menu()
    self.champtable = {}
    self.SpriteBar = nil
    self.Sprites = {}
    self:LoadSprites()
    self.positions = {[1]={[1] = -70, [2] = -37.9, [3] = 31, [4] = 4}, [2]={[1] = -37, [2] = -37.9, [3] = 31, [4] = 4},[3]={[1] = -4, [2] = -37.9, [3] = 31, [4] = 4},[4]={[1] = 29, [2] = -37.9, [3] = 31, [4] = 4}, [5] = {[1] = 63, [2] = -38}, [6] = {[1] = 63, [2] = -20} }--, [4] = {[1] = }
    self.exp = {[1] = 0, [2] = 280, [3] = 660, [4] = 1140, [5] = 1720, [6] = 2400, [7] = 3180, [8] = 4060, [9] = 5040, [10] = 6120, [11] = 7300, [12] = 8580, [13] = 9960, [14] = 11440, [15] = 13020, [16] = 14700, [17] = 16480, [18] = 18360}
    self.champs = {}
    self:Create()
    AddEvent(Events.OnTick,function() self:OnTick() end)
    AddEvent(Events.OnDraw, function() self:OnDraw() end)
    AddEvent(Events.OnProcessSpell, function(unit, spell) self:OnProcessSpell(unit, spell) end)
    PrintChat("CdTracker loaded")
    print"loaded"
    self.font = DrawHandler:CreateFont("Calibri", 10)
end

function CdTracker:Menu()
    self.CD = Menu("CD", "Cd Tracker")
    self.CD:slider("SS", "Sprite Scale", -10, 10, 1, 1)
    self.CD:sub("SP", "Sprite position")
        self.CD.SP:slider("x", "X positions", -1920, 1920, -74, 1) --self.CD.SS:get()
        self.CD.SP:slider("y", "Y positions", -1920, 1920, -41, 1)
    self.CD:sub("B", "Bars options")
        self.CD.B:sub("X", "Bar x positions")
            self.CD.B.X:slider("bx0", "Position Q bar", -1920, 1920, -70, 1)
            self.CD.B.X:slider("bx1", "Position W bar", -1920, 1920, -37, 1)
            self.CD.B.X:slider("bx2", "Position E bar", -1920, 1920, -4, 1)
            self.CD.B.X:slider("bx3", "Position R bar", -1920, 1920, 29, 10)
        self.CD.B:sub("Y", "Bar y positions")
            self.CD.B.Y:slider("by0", "Position Q bar", -1080, 1080, -37.9, 1)
            self.CD.B.Y:slider("by1", "Position W bar", -1080, 1080, -37.9, 1)
            self.CD.B.Y:slider("by2", "Position E bar", -1080, 1080, -37.9, 1)
            self.CD.B.Y:slider("by3", "Position R bar", -1080, 1080, -37.9, 1)
        self.CD.B:slider("BL", "Bar longitude", 0, 50, 31, 1) -- self.CD.SM.S:get()
        self.CD.B:slider("BA", "Bar amplitude", 0, 50, 4, 1)
    self.CD:sub("SM", "Summoners positions")
        self.CD.SM:slider("S", "summoners scale", -10, 10, 1, 1)
        self.CD.SM:slider("Sx5", "Summoner 1 x", -1920, 1920, 63, 1)
        self.CD.SM:slider("Sy5", "Summoner 1 y", -1080, 1080, -38, 1)
        self.CD.SM:slider("Sx6", "Summoner 2 x", -1920, 1920, 63, 1)
        self.CD.SM:slider("Sy6", "Summoner 2 y", -1080, 1080, -20, 1)
    self.CD:sub("EM", "Experience bar options")
        self.CD.EM:slider("EX", "Experience X Position", -1920, 1920, -46, 1) --self.CD.EM.EX:get()
        self.CD.EM:slider("EY", "Experience Y Position", -1080, 1080, -31, 1)
        self.CD.EM:slider("EL", "Experience longitude", 0, 300, 105, 1)
        self.CD.EM:slider("EA", "Experience amplitude", 0, 50, 1, 1)
    self.CD:checkbox("a", "PRESS F5 AFTER CHANGING ANYTHING")
    --print(self.CD.SM["Sx"..5]:get())
end

function CdTracker:OnScreen(pos) --Goddy Kappa
	local maxX,maxY = Renderer.width, Renderer.height
	local toScreen = Renderer:WorldToScreen(D3DXVECTOR3(pos.x, pos.y, pos.z))
	return toScreen.x < maxX and toScreen.x > 0 and toScreen.y < maxY and toScreen.y > 0
end

function CdTracker:OnTick()
    local clock = os.clock()
    for k, v in ipairs(ObjectManager:GetEnemyHeroes()) do
        for i=0,3,1 do
            if self.champs[v.charName][i][1] ~= nil and self.champs[v.charName][i][2] then
                self.champs[v.charName][i][3] = (clock - self.champs[v.charName][i][2])*self.champs[v.charName][i][1]
                self.champs[v.charName][i][4] = self.champs[v.charName][i][5] - clock
                if self.champs[v.charName][i][3] > self.CD.B.BL:get() then 
                    self.champs[v.charName][i][3] = self.CD.B.BL:get()
                    self.champs[v.charName][i][2] = nil
                    self.champs[v.charName][i][1] = nil
                end
                if self.champs[v.charName][i][3] < 4 then
                    self.champs[v.charName][i][3] = 4
                end
                if self.champs[v.charName][i][3] == self.CD.B.BL:get() then
                    self.champs[v.charName][i][2] = nil
                    self.champs[v.charName][i][1] = nil
                end
                if self.champs[v.charName][i][4] < 0 then
                    self.champs[v.charName][i][4] = 0
                end
            end
            if i+2 == 4 and self.champs[v.charName][i+2][1] or i+2 == 5 and self.champs[v.charName][i+2][1] then
                self.champs[v.charName][i+2][3] =  self.champs[v.charName][i+2][2] - clock
                --print(self.champs[v.charName][i+2][3])
                if self.champs[v.charName][i+2][3] <= 0 then
                    self.champs[v.charName][i+2][3] = 0
                end
            end
        end
    end
end

function CdTracker:OnDraw()
    for k, v in ipairs(ObjectManager:GetEnemyHeroes()) do
        if self:ValidTarget(v) then
            DrawHandler:Sprite(self.SpriteBar, D3DXVECTOR2(v.infoComponent.hpBarScreenPosition.x+self.CD.SP.x:get(),v.infoComponent.hpBarScreenPosition.y+self.CD.SP.y:get()), self.CD.SS:get())
            for i = 0,3,1 do
                if self:IsReady(v, i) then
                    if self.champs[v.charName][i][3] >= self.CD.B.BL:get() then
                        DrawHandler:OutlinedRect(D3DXVECTOR4(v.infoComponent.hpBarScreenPosition.x+self.CD.B.X["bx"..i]:get(),v.infoComponent.hpBarScreenPosition.y+self.CD.B.Y["by"..i]:get(), self.CD.B.BL:get(), self.positions[i+1][4]), self.CD.B.BA:get(), self:Hex(255,0,255,0), self:Hex(255,0,255,0))
                    else
                        DrawHandler:OutlinedRect(D3DXVECTOR4(v.infoComponent.hpBarScreenPosition.x+self.CD.B.X["bx"..i]:get(),v.infoComponent.hpBarScreenPosition.y+self.CD.B.Y["by"..i]:get(), self.champs[v.charName][i][3], self.positions[i+1][4]), self.CD.B.BA:get(), self:Hex(255,245,119,10), self:Hex(255,245,119,10))
                    end
                    if self.champs[v.charName][i][4] ~= 0 then
                        DrawHandler:Text(self.font, D3DXVECTOR2(v.infoComponent.hpBarScreenPosition.x+self.positions[i+1][1]+12,v.infoComponent.hpBarScreenPosition.y+self.positions[i+1][2]-20), string.format("%.f",self.champs[v.charName][i][4]),self:Hex(255,255,255,255))
                    end
                end
                if i+2 >= 4 then
                    if self.champs[v.charName][i+2][3] <= 0 then
                        DrawHandler:Sprite(self.champtable[v.charName][i+2], D3DXVECTOR2(v.infoComponent.hpBarScreenPosition.x+self.CD.SM["Sx"..i+3]:get(),v.infoComponent.hpBarScreenPosition.y+self.CD.SM["Sy"..i+3]:get()), self.CD.SM.S:get())
                    else
                        DrawHandler:Sprite(self.champtable[v.charName][i+4], D3DXVECTOR2(v.infoComponent.hpBarScreenPosition.x+self.CD.SM["Sx"..i+3]:get(),v.infoComponent.hpBarScreenPosition.y+self.CD.SM["Sy"..i+3]:get()), self.CD.SM.S:get())
                        DrawHandler:Text(self.font, D3DXVECTOR2(v.infoComponent.hpBarScreenPosition.x+self.CD.SM["Sx"..i+3]:get()+10,v.infoComponent.hpBarScreenPosition.y+self.CD.SM["Sy"..i+3]:get()), string.format("%.f",self.champs[v.charName][i+2][3]),self:Hex(255,255,255,255))
                        DrawHandler:Text(self.font, D3DXVECTOR2(v.infoComponent.hpBarScreenPosition.x+self.CD.SM["Sx"..i+3]:get()+10,v.infoComponent.hpBarScreenPosition.y+self.CD.SM["Sy"..i+3]:get()), 15,self:Hex(255,255,255,255))
                    end
                end
            end
            if v.experience.level ~= 18 then
                DrawHandler:OutlinedRect(D3DXVECTOR4(v.infoComponent.hpBarScreenPosition.x+self.CD.EM.EX:get(),v.infoComponent.hpBarScreenPosition.y+self.CD.EM.EY:get(), self.CD.EM.EL:get()*(((v.experience.experience - self.exp[v.experience.level])*100/(self.exp[v.experience.level+1] - self.exp[v.experience.level]))/100), 3), self.CD.EM.EA:get(), self:Hex(255,157,14,186), self:Hex(255,157,14,186))
            end
        end
    end
end

function CdTracker:Create()
    for k, v in ipairs(ObjectManager:GetEnemyHeroes()) do
        self.champs[v.charName] = {}
        for i = 0,5,1 do
            self.champs[v.charName][i] = {}
            --table.insert(self.champs[v.charName][i], ([1] = nil, [2] = nil, [3] = 31))
            self.champs[v.charName][i][1] = nil
            self.champs[v.charName][i][2] = nil
            if i == 4 or i == 5 then
                self.champs[v.charName][i][3] = 0
            else 
                self.champs[v.charName][i][3] = self.CD.B.BL:get()
                self.champs[v.charName][i][4] = 0
                self.champs[v.charName][i][5] = 0
            end
        end
    end
end

function CdTracker:LoadSprites()
    self.SpriteBar = DrawHandler:CreateSprite(COMMON_PATH.."\\Sprites\\hpbar.png", 155, 40)
    for k, v in ipairs(ObjectManager:GetEnemyHeroes()) do
        self.champtable[v.charName] = {}
        for i = 4, 5, 1 do
            self.champtable[v.charName][i] = {}
            self.champtable[v.charName][i] = DrawHandler:CreateSprite(COMMON_PATH.."\\Sprites\\Summoners\\"..v.spellbook:Spell(i).spellData.name..".jpg", 14, 15)
            self.champtable[v.charName][i+2] = DrawHandler:CreateSprite(COMMON_PATH.."\\Sprites\\Summoners\\"..v.spellbook:Spell(i).spellData.name.."-grey.jpg", 15, 15)
        end
    end
end

function CdTracker:IsReady(unit, int)
    if unit.spellbook:Spell(int).level >= 1 --[[and unit.spellbook:Spell(unit).cooldownTimeRemaining == 0]] then 
        return true
    end
    return false
end

function CdTracker:CanUseSpell(unit, int)
    return unit.spellbook:CanUseSpell(int) == 0
end

function CdTracker:CanUseSpell(Unit, int)
    return Unit.spellbook:CanUseSpell(int) == 0
end

function CdTracker:Hex(a,r,g,b)
	return string.format("0x%.2X%.2X%.2X%.2X",a,r,g,b)
end

function CdTracker:OnProcessSpell(unit, spell)
    --0 if unit == myHero and spell then print(spell.spellSlot) end

    for k, v in ipairs(ObjectManager:GetEnemyHeroes()) do
        if unit == v and spell then
            if self.champs[unit.charName][spell.spellSlot] and self.champs[unit.charName][spell.spellSlot][1] == nil and (spell.spellSlot ~= 4 or spell.spellSlot ~= 5) then
                self.champs[unit.charName][spell.spellSlot][1] = self.CD.B.BL:get()/unit.spellbook:Spell(spell.spellSlot).baseCdTime+unit.spellbook:Spell(spell.spellSlot).baseCdTime*math.sqrt(unit.characterIntermediate.percentCooldownMod^2)
                self.champs[unit.charName][spell.spellSlot][2] = math.floor(os.clock()+0.5)
                self.champs[unit.charName][spell.spellSlot][5] = math.floor(os.clock()+0.5) + unit.spellbook:Spell(spell.spellSlot).baseCdTime
            end
            if (spell.spellSlot == 4 or spell.spellSlot == 5) then
                self.champs[unit.charName][spell.spellSlot][1] = unit.spellbook:Spell(spell.spellSlot).baseCdTime
                self.champs[unit.charName][spell.spellSlot][2] = math.floor(os.clock()+0.5) + unit.spellbook:Spell(spell.spellSlot).baseCdTime
            end
        end
    end
end

function CdTracker:ValidTarget(unit)
    return unit ~= nil and unit.isValid and not unit.isDead and not unit.isInvulnerable and unit.isTargetable and not unit.isZombie and unit.team ~= myHero.team and unit.isVisible and self:OnScreen(unit)
end

CdTracker:__init()