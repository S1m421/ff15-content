--[[
		____                      _                _____           _          
	   / __ \________  ____ ___  (_)_  ______ ___ / ___/___  _____(_)__  _____
	  / /_/ / ___/ _ \/ __ `__ \/ / / / / __ `__ \\__ \/ _ \/ ___/ / _ \/ ___/
	 / ____/ /  /  __/ / / / / / / /_/ / / / / / /__/ /  __/ /  / /  __(__  ) 
	/_/   /_/   \___/_/ /_/ /_/_/\__,_/_/ /_/ /_/____/\___/_/  /_/\___/____/  
	                                                       Powered by FF15!

	Credits: Gamsteron, Timmy, Maxxxel, Noddy, Quasar

	Changelog:

	v1.0.5 BETA
	+ Added Yasuo
	+ Added Xerath's R range drawing on minimap

	v1.0.4 BETA
	+ Small fix

	v1.0.3 BETA
	+ Fixed DreamPred callbacks

	v1.0.2 BETA
	+ Fixed Auto func
	+ Fixed Target Selector

	v1.0.1 BETA
	+ Improved LaneClear

	v1.0 BETA
	+ Initial release

--]]

local a, b, c = 1.05, "1.0.5", GetInternalWebResult("FF15-PremiumSeries.version")
local d = function()
	if tonumber(c) > a then
		PrintChat("<font color=\"#00ced1\"><b>PremiumSeries:</font></b> <font color=\"#87cefa\">Found update! Downloading...</font>")
		DownloadInternalFile("FF15-PremiumSeries.lua", SCRIPT_PATH .. "FF15-PremiumSeries.lua")
		PrintChat("<font color=\"#00ced1\"><b>PremiumSeries:</font></b> <font color=\"#87cefa\">Updated! Have fun!</font>")
	end
end
local e, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t = math.abs, math.atan, math.atan2, math.acos, math.ceil, math.cos, math.deg, math.floor, math.huge, math.max, math.min, math.pi, math.random, math.rad, math.sin, math.sqrt
local u, v, w, x, y, z = SpellSlot.Q, SpellSlot.W, SpellSlot.E, SpellSlot.R, SpellSlot.Summoner1, SpellSlot.Summoner2
local A, B, C = table.insert, table.remove, table.sort
local D, E, F, G, H, I = {}, {}, {}, nil, nil, nil
local J = {Champion = nil, TargetSelector = nil}
local K = {Champion = nil, TargetSelector = nil}
local L = {
	Champion = nil,
	Orbwalker = nil,
	Utility = nil
}
local M, N = {Champion = nil}, {Champion = nil}
local O, P = {Champion = nil}, {Champion = nil}
local Q, R = {Champion = nil}, {TargetSelector = nil}
local S = function(T)
	local U = io.open(T, "r")
	if U then
		io.close(U)
		return true
	end
	return false
end
local V = {}
function DelayAction(W, X, Y)
	if not delayedActionsExecuter then
		function delayedActionsExecuter()
			for Z, _ in pairs(V) do
				if Z <= RiotClock.time then
					for a0, a1 in ipairs(_) do
						a1.func(unpack(a1.args or {}))
					end
					V[Z] = nil
				end
			end
		end
		AddEvent(Events.OnTick, delayedActionsExecuter)
	end
	local a2 = RiotClock.time + (X or 0)
	if V[a2] then
		table.insert(V[a2], {func = W, args = Y})
	else
		V[a2] = {
			{func = W, args = Y}
		}
	end
end
local a3 = function()
	for Z, a4 in pairs(ObjectManager:GetEnemyHeroes()) do
		A(F, a4)
	end
	for Z, a5 in pairs(ObjectManager:GetAllyHeroes()) do
		A(E, a5)
	end
end
local a6 = {
	Viktor = {
		{
			slot = 0,
			state = 0,
			damage = function(a7)
				return Manager:CalcMagicalDamage(myHero, a7, ({
					60,
					80,
					100,
					120,
					140
				})[Manager:GetCastLevel(myHero, u)] + 0.4 * Manager:GetBonusAP(myHero))
			end
		},
		{
			slot = 0,
			state = 1,
			damage = function(a7)
				return Manager:CalcMagicalDamage(myHero, a7, ({
					20,
					50,
					80,
					110,
					140
				})[Manager:GetCastLevel(myHero, u)] + Manager:GetTotalDmg(myHero) + 0.5 * GetBonusAP(myHero))
			end
		},
		{
			slot = 2,
			state = 0,
			damage = function(a7)
				return Manager:CalcMagicalDamage(myHero, a7, ({
					70,
					110,
					150,
					190,
					230
				})[Manager:GetCastLevel(myHero, w)] + 0.5 * Manager:GetBonusAP(myHero))
			end
		},
		{
			slot = 2,
			state = 1,
			damage = function(a7)
				return Manager:CalcMagicalDamage(myHero, a7, ({
					20,
					60,
					100,
					140,
					180
				})[Manager:GetCastLevel(myHero, w)] + 0.7 * Manager:GetBonusAP(myHero))
			end
		},
		{
			slot = 3,
			state = 0,
			damage = function(a7)
				return Manager:CalcMagicalDamage(myHero, a7, ({
					550,
					925,
					1300
				})[Manager:GetCastLevel(myHero, x)] + 2.6 * Manager:GetBonusAP(myHero))
			end
		}
	},
	Xerath = {
		{
			slot = 0,
			state = 0,
			damage = function(a7)
				return Manager:CalcMagicalDamage(myHero, a7, ({
					80,
					120,
					160,
					200,
					240
				})[Manager:GetCastLevel(myHero, u)] + 0.75 * Manager:GetBonusAP(myHero))
			end
		},
		{
			slot = 1,
			state = 0,
			damage = function(a7)
				return Manager:CalcMagicalDamage(myHero, a7, ({
					90,
					135,
					180,
					225,
					270
				})[Manager:GetCastLevel(myHero, v)] + 0.9 * Manager:GetBonusAP(myHero))
			end
		},
		{
			slot = 2,
			state = 0,
			damage = function(a7)
				return Manager:CalcMagicalDamage(myHero, a7, ({
					80,
					110,
					140,
					170,
					200
				})[Manager:GetCastLevel(myHero, w)] + 0.45 * Manager:GetBonusAP(myHero))
			end
		},
		{
			slot = 3,
			state = 0,
			damage = function(a7)
				return Manager:CalcMagicalDamage(myHero, a7, ({
					200,
					240,
					280
				})[Manager:GetCastLevel(myHero, x)] + 0.43 * Manager:GetBonusAP(myHero))
			end
		}
	},
	Yasuo = {
		{
			slot = 2,
			state = 0,
			damage = function(a7)
				return Manager:CalcMagicalDamage(myHero, a7, ({
					60,
					70,
					80,
					90,
					100
				})[Manager:GetCastLevel(myHero, u)] + 0.2 * Manager:GetBonusDmg(myHero) + 0.6 * Manager:GetBonusAP(myHero))
			end
		}
	}
}
local a8 = {
	Viktor = {
		[0] = {range = 600},
		[1] = {
			speed = m,
			range = 800,
			delay = 1.75,
			radius = 270,
			angle = 0,
			collision = false,
			type = "circular"
		},
		[2] = {
			speed = 1050,
			range = 1225,
			delay = 0,
			radius = 80,
			width = 160,
			angle = 0,
			collision = false,
			type = "linear"
		},
		[3] = {
			speed = m,
			range = 700,
			delay = 0.25,
			radius = 325,
			angle = 0,
			collision = false,
			type = "circular"
		}
	},
	Xerath = {
		[0] = {
			speed = m,
			range = 1400,
			delay = 0.5,
			radius = 72.5,
			width = 145,
			angle = 0,
			collision = false,
			type = "linear"
		},
		[1] = {
			speed = m,
			range = 1100,
			delay = 0.5,
			radius = 200,
			angle = 0,
			collision = false,
			type = "circular"
		},
		[2] = {
			speed = 1400,
			range = 1050,
			delay = 0.2,
			radius = 60,
			width = 120,
			angle = 0,
			collision = true,
			type = "linear"
		},
		[3] = {
			speed = m,
			range = 3520,
			delay = 0.6,
			radius = 200,
			angle = 0,
			collision = false,
			type = "circular"
		}
	},
	Yasuo = {
		[0] = {
			speed = m,
			speed2 = 1200,
			range = 520,
			range2 = 1100,
			delay = 0.25,
			radius = 40,
			radius2 = 90,
			angle = 0,
			collision = false,
			type == "linear"
		},
		[2] = {range = 475},
		[3] = {range = 1400},
		["Q"] = {
			speed = m,
			range = 520,
			delay = 0.25,
			radius = 40,
			angle = 0,
			type = "linear"
		},
		["Q3"] = {
			speed = 1200,
			range = 1100,
			delay = 0.25,
			radius = 90,
			angle = 0,
			type = "linear"
		}
	}
}
local a9 = {
	AatroxW = {
		charName = "Aatrox",
		displayName = "Infernal Chains",
		slot = v,
		type = "linear",
		speed = 1800,
		range = 825,
		radius = 80,
		danger = 2,
		cc = true,
		collision = true
	},
	AhriOrbMissile = {
		charName = "Ahri",
		displayName = "Orb of Deception",
		slot = u,
		type = "linear",
		speed = 2500,
		range = 880,
		radius = 100,
		danger = 2,
		cc = false,
		collision = false
	},
	AhriSeduceMissile = {
		charName = "Ahri",
		displayName = "Seduce",
		slot = w,
		type = "linear",
		speed = 1500,
		range = 975,
		radius = 60,
		danger = 1,
		cc = true,
		collision = true
	},
	AkaliEMis = {
		charName = "Akali",
		displayName = "Shuriken Flip",
		slot = w,
		type = "linear",
		speed = 1800,
		range = 825,
		radius = 70,
		danger = 2,
		cc = false,
		collision = true
	},
	SadMummyBandageToss = {
		charName = "Amumu",
		displayName = "Bandage Toss",
		slot = u,
		type = "linear",
		speed = 2000,
		range = 1100,
		radius = 80,
		danger = 3,
		cc = true,
		collision = true
	},
	FlashFrostSpell = {
		charName = "Anivia",
		displayName = "Flash Frost",
		slot = u,
		type = "linear",
		speed = 850,
		range = 1100,
		radius = 110,
		danger = 2,
		cc = true,
		collision = false
	},
	VolleyRightAttack = {
		charName = "Ashe",
		displayName = "Volley",
		slot = v,
		type = "conic",
		speed = 2000,
		range = 1200,
		radius = 20,
		angle = 40,
		danger = 2,
		cc = true,
		collision = true
	},
	EnchantedCrystalArrow = {
		charName = "Ashe",
		displayName = "Enchanted Crystal Arrow",
		slot = x,
		type = "linear",
		speed = 1600,
		range = 12500,
		radius = 130,
		danger = 4,
		cc = true,
		collision = false
	},
	AurelionSolQMissile = {
		charName = "AurelionSol",
		displayName = "Starsurge",
		slot = u,
		type = "linear",
		speed = 850,
		range = 1075,
		radius = 110,
		danger = 2,
		cc = true,
		collision = false
	},
	BardQMissile = {
		charName = "Bard",
		displayName = "Cosmic Binding",
		slot = u,
		type = "linear",
		speed = 1500,
		range = 950,
		radius = 60,
		danger = 2,
		cc = true,
		collision = true
	},
	RocketGrabMissile = {
		charName = "Blitzcrank",
		displayName = "Rocket Grab",
		slot = u,
		type = "linear",
		speed = 1800,
		range = 925,
		radius = 70,
		danger = 3,
		cc = true,
		collision = true
	},
	BrandQMissile = {
		charName = "Brand",
		displayName = "Sear",
		slot = u,
		type = "linear",
		speed = 1600,
		range = 1050,
		radius = 60,
		danger = 1,
		cc = false,
		collision = true
	},
	BraumQMissile = {
		charName = "Braum",
		displayName = "Winter's Bite",
		slot = u,
		type = "linear",
		speed = 1700,
		range = 1000,
		radius = 70,
		danger = 3,
		cc = true,
		collision = true
	},
	CaitlynPiltoverPeacemaker = {
		charName = "Caitlyn",
		displayName = "Piltover Peacemaker",
		slot = u,
		type = "linear",
		speed = 2200,
		range = 1250,
		radius = 60,
		radius2 = 90,
		danger = 1,
		cc = false,
		collision = false
	},
	CaitlynEntrapment = {
		charName = "Caitlyn",
		displayName = "Entrapment",
		slot = w,
		type = "linear",
		speed = 1600,
		range = 750,
		radius = 70,
		danger = 2,
		cc = true,
		collision = true
	},
	CamilleEMissile = {
		charName = "Camille",
		displayName = "Hookshot",
		slot = w,
		type = "linear",
		speed = 1900,
		range = 800,
		radius = 60,
		danger = 1,
		cc = false,
		collision = false
	},
	PhosphorusBombMissile = {
		charName = "Corki",
		displayName = "Phosphorus Bomb",
		slot = u,
		type = "circular",
		speed = 1000,
		range = 825,
		radius = 250,
		danger = 2,
		cc = false,
		collision = false
	},
	MissileBarrageMissile = {
		charName = "Corki",
		displayName = "Missile Barrage [Standard]",
		slot = x,
		type = "linear",
		speed = 2000,
		range = 1300,
		radius = 40,
		danger = 1,
		cc = false,
		collision = true
	},
	MissileBarrageMissile2 = {
		charName = "Corki",
		displayName = "Missile Barrage [Big]",
		slot = x,
		type = "linear",
		speed = 2000,
		range = 1500,
		radius = 40,
		danger = 1,
		cc = false,
		collision = true
	},
	DravenDoubleShotMissile = {
		charName = "Draven",
		displayName = "Double Shot",
		slot = w,
		type = "linear",
		speed = 1600,
		range = 1050,
		radius = 130,
		danger = 3,
		cc = true,
		collision = false
	},
	DravenR = {
		charName = "Draven",
		displayName = "Whirling Death",
		slot = x,
		type = "linear",
		speed = 2000,
		range = 12500,
		radius = 160,
		danger = 4,
		cc = false,
		collision = false
	},
	InfectedCleaverMissile = {
		charName = "DrMundo",
		displayName = "Infected Cleaver",
		slot = u,
		type = "linear",
		speed = 2000,
		range = 975,
		radius = 60,
		danger = 2,
		cc = true,
		collision = true
	},
	EkkoQMis = {
		charName = "Ekko",
		displayName = "Timewinder",
		slot = u,
		type = "linear",
		speed = 1650,
		range = 1175,
		radius = 60,
		danger = 1,
		cc = true,
		collision = false
	},
	EliseHumanE = {
		charName = "Elise",
		displayName = "Cocoon",
		slot = w,
		type = "linear",
		speed = 1600,
		range = 1075,
		radius = 55,
		danger = 1,
		cc = true,
		collision = true
	},
	EvelynnQ = {
		charName = "Evelynn",
		displayName = "Hate Spike",
		slot = u,
		type = "linear",
		speed = 2400,
		range = 800,
		radius = 60,
		danger = 2,
		cc = false,
		collision = true
	},
	EzrealQ = {
		charName = "Ezreal",
		displayName = "Mystic Shot",
		slot = u,
		type = "linear",
		speed = 2000,
		range = 1150,
		radius = 60,
		danger = 1,
		cc = false,
		collision = true
	},
	EzrealW = {
		charName = "Ezreal",
		displayName = "Essence Flux",
		slot = v,
		type = "linear",
		speed = 2000,
		range = 1150,
		radius = 60,
		danger = 1,
		cc = false,
		collision = false
	},
	EzrealR = {
		charName = "Ezreal",
		displayName = "Trueshot Barrage",
		slot = x,
		type = "linear",
		speed = 2000,
		range = 12500,
		radius = 160,
		danger = 4,
		cc = false,
		collision = false
	},
	FizzRMissile = {
		charName = "Fizz",
		displayName = "Chum the Waters",
		slot = x,
		type = "linear",
		speed = 1300,
		range = 1300,
		radius = 150,
		danger = 5,
		cc = true,
		collision = false
	},
	GalioQMissileR = {
		charName = "Galio",
		displayName = "Winds of War",
		slot = u,
		type = "circular",
		speed = 1150,
		range = 825,
		radius = 235,
		danger = 2,
		cc = false,
		collision = false
	},
	GnarQMissile = {
		charName = "Gnar",
		displayName = "Boomerang Throw",
		slot = u,
		type = "linear",
		speed = 2500,
		range = 1125,
		radius = 55,
		danger = 2,
		cc = true,
		collision = false
	},
	GnarBigQMissile = {
		charName = "Gnar",
		displayName = "Boulder Toss",
		slot = u,
		type = "linear",
		speed = 2100,
		range = 1125,
		radius = 90,
		danger = 2,
		cc = true,
		collision = true
	},
	GragasQMissile = {
		charName = "Gragas",
		displayName = "Barrel Roll",
		slot = u,
		type = "circular",
		speed = 1000,
		range = 850,
		radius = 275,
		danger = 2,
		cc = true,
		collision = false
	},
	GragasRBoom = {
		charName = "Gragas",
		displayName = "Explosive Cask",
		slot = x,
		type = "circular",
		speed = 1800,
		range = 1000,
		radius = 400,
		danger = 5,
		cc = true,
		collision = false
	},
	GravesClusterShotSoundMissile = {
		charName = "Graves",
		displayName = "End of the Line",
		slot = u,
		type = "linear",
		speed = 2000,
		range = 700,
		radius = 20,
		danger = 1,
		cc = false,
		collision = false
	},
	GravesSmokeGrenadeBoom = {
		charName = "Graves",
		displayName = "Smoke Grenade",
		slot = v,
		type = "circular",
		speed = 1500,
		range = 950,
		radius = 250,
		danger = 2,
		cc = true,
		collision = false
	},
	GravesChargeShotShot = {
		charName = "Graves",
		displayName = "Charge Shot",
		slot = x,
		type = "linear",
		speed = 2100,
		range = 1000,
		radius = 100,
		danger = 5,
		cc = false,
		collision = false
	},
	HeimerdingerWAttack2 = {
		charName = "Heimerdinger",
		displayName = "Hextech Micro-Rockets",
		slot = v,
		type = "linear",
		speed = 2050,
		range = 1325,
		radius = 100,
		danger = 2,
		cc = false,
		collision = false
	},
	HeimerdingerESpell = {
		charName = "Heimerdinger",
		displayName = "CH-2 Electron Storm Grenade",
		slot = w,
		type = "circular",
		speed = 1200,
		range = 970,
		radius = 250,
		danger = 2,
		cc = true,
		collision = false
	},
	HeimerdingerESpell_ult = {
		charName = "Heimerdinger",
		displayName = "CH-2 Electron Storm Grenade",
		slot = w,
		type = "circular",
		speed = 1200,
		range = 970,
		radius = 250,
		danger = 3,
		cc = true,
		collision = false
	},
	IllaoiEMis = {
		charName = "Illaoi",
		displayName = "Test of Spirit",
		slot = w,
		type = "linear",
		speed = 1900,
		range = 900,
		radius = 50,
		danger = 1,
		cc = false,
		collision = true
	},
	IreliaR = {
		charName = "Irelia",
		displayName = "Vanguard's Edge",
		slot = x,
		type = "linear",
		speed = 2000,
		range = 950,
		radius = 160,
		danger = 4,
		cc = true,
		collision = false
	},
	IvernQ = {
		charName = "Ivern",
		displayName = "Rootcaller",
		slot = u,
		type = "linear",
		speed = 1300,
		range = 1075,
		radius = 80,
		danger = 1,
		cc = true,
		collision = true
	},
	HowlingGaleSpell = {
		charName = "Janna",
		displayName = "Howling Gale",
		slot = u,
		type = "linear",
		speed = 667,
		range = 1750,
		radius = 100,
		danger = 2,
		cc = true,
		collision = false
	},
	JayceShockBlastMis = {
		charName = "Jayce",
		displayName = "Shock Blast",
		slot = u,
		type = "linear",
		speed = 1450,
		range = 1050,
		radius = 70,
		danger = 1,
		cc = false,
		collision = true
	},
	JhinRShotMis = {
		charName = "Jhin",
		displayName = "Curtain Call",
		slot = x,
		type = "linear",
		speed = 5000,
		range = 3500,
		radius = 80,
		danger = 2,
		cc = true,
		collision = false
	},
	JinxWMissile = {
		charName = "Jinx",
		displayName = "Zap!",
		slot = v,
		type = "linear",
		speed = 3300,
		range = 1450,
		radius = 60,
		danger = 1,
		cc = true,
		collision = true
	},
	JinxEHit = {
		charName = "Jinx",
		displayName = "Flame Chompers!",
		slot = w,
		type = "circular",
		speed = 1100,
		range = 900,
		radius = 120,
		danger = 1,
		cc = true,
		collision = false
	},
	JinxR = {
		charName = "Jinx",
		displayName = "Super Mega Death Rocket!",
		slot = x,
		type = "linear",
		speed = 1700,
		range = 12500,
		radius = 140,
		danger = 4,
		cc = false,
		collision = false
	},
	KaisaW = {
		charName = "Kaisa",
		displayName = "Void Seeker",
		slot = v,
		type = "linear",
		speed = 1750,
		range = 3000,
		radius = 100,
		danger = 1,
		cc = false,
		collision = true
	},
	KalistaMysticShotMisTrue = {
		charName = "Kalista",
		displayName = "Pierce",
		slot = u,
		type = "linear",
		speed = 2400,
		range = 1150,
		radius = 40,
		danger = 1,
		cc = false,
		collision = true
	},
	KarmaQMissile = {
		charName = "Karma",
		displayName = "Inner Flame",
		slot = u,
		type = "linear",
		speed = 1700,
		range = 950,
		radius = 60,
		danger = 2,
		cc = true,
		collision = true
	},
	KarmaQMissileMantra = {
		charName = "Karma",
		displayName = "Inner Flame [Mantra]",
		slot = u,
		origin = "linear",
		type = "linear",
		speed = 1700,
		range = 950,
		radius = 80,
		danger = 2,
		cc = true,
		collision = true
	},
	KayleQMisVFX = {
		charName = "Kayle",
		displayName = "Radiant Blast",
		slot = u,
		type = "linear",
		speed = 2000,
		range = 850,
		radius = 60,
		danger = 1,
		cc = true,
		collision = false
	},
	KennenShurikenHurlMissile1 = {
		charName = "Kennen",
		displayName = "Shuriken Hurl",
		slot = u,
		type = "linear",
		speed = 1700,
		range = 1050,
		radius = 50,
		danger = 2,
		cc = false,
		collision = true
	},
	KhazixWMissile = {
		charName = "Khazix",
		displayName = "Void Spike",
		slot = v,
		type = "linear",
		speed = 1700,
		range = 1000,
		radius = 70,
		danger = 1,
		cc = false,
		collision = true
	},
	KledQMissile = {
		charName = "Kled",
		displayName = "Beartrap on a Rope",
		slot = u,
		type = "linear",
		speed = 1600,
		range = 800,
		radius = 45,
		danger = 1,
		cc = true,
		collision = true
	},
	KledRiderQMissile = {
		charName = "Kled",
		displayName = "Pocket Pistol",
		slot = u,
		type = "conic",
		speed = 3000,
		range = 700,
		radius = 0,
		angle = 25,
		danger = 3,
		cc = false,
		collision = false
	},
	KogMawQ = {
		charName = "KogMaw",
		displayName = "Caustic Spittle",
		slot = u,
		type = "linear",
		speed = 1650,
		range = 1175,
		radius = 70,
		danger = 1,
		cc = false,
		collision = true
	},
	KogMawVoidOozeMissile = {
		charName = "KogMaw",
		displayName = "Void Ooze",
		slot = w,
		type = "linear",
		speed = 1400,
		range = 1360,
		radius = 120,
		danger = 2,
		cc = true,
		collision = false
	},
	LeblancEMissile = {
		charName = "Leblanc",
		displayName = "Ethereal Chains [Standard]",
		slot = w,
		type = "linear",
		speed = 1750,
		range = 925,
		radius = 55,
		danger = 1,
		cc = true,
		collision = true
	},
	LeblancREMissile = {
		charName = "Leblanc",
		displayName = "Ethereal Chains [Ultimate. Missile]",
		slot = w,
		type = "linear",
		speed = 1750,
		range = 925,
		radius = 55,
		danger = 1,
		cc = true,
		collision = true
	},
	BlindMonkQOne = {
		charName = "LeeSin",
		displayName = "Sonic Wave",
		slot = u,
		type = "linear",
		speed = 1800,
		range = 1100,
		radius = 60,
		danger = 1,
		cc = false,
		collision = true
	},
	LeonaZenithBladeMissile = {
		charName = "Leona",
		displayName = "Zenith Blade",
		slot = w,
		type = "linear",
		speed = 2000,
		range = 875,
		radius = 70,
		danger = 2,
		cc = true,
		collision = false
	},
	LissandraQMissile = {
		charName = "Lissandra",
		displayName = "Ice Shard",
		slot = u,
		type = "linear",
		speed = 2200,
		range = 750,
		radius = 75,
		danger = 2,
		cc = true,
		collision = false
	},
	LissandraEMissile = {
		charName = "Lissandra",
		displayName = "Glacial Path",
		slot = w,
		type = "linear",
		speed = 850,
		range = 1025,
		radius = 125,
		danger = 2,
		cc = false,
		collision = false
	},
	LucianW = {
		charName = "Lucian",
		displayName = "Ardent Blaze",
		slot = v,
		type = "linear",
		speed = 1600,
		range = 900,
		radius = 80,
		danger = 2,
		cc = false,
		collision = true
	},
	LuluQMissile = {
		charName = "Lulu",
		displayName = "Glitterlance",
		slot = u,
		type = "linear",
		speed = 1450,
		range = 925,
		radius = 60,
		danger = 1,
		cc = true,
		collision = false
	},
	LuxLightBindingDummy = {
		charName = "Lux",
		displayName = "Light Binding",
		slot = u,
		type = "linear",
		speed = 1200,
		range = 1175,
		radius = 70,
		danger = 1,
		cc = true,
		collision = true
	},
	LuxLightStrikeKugel = {
		charName = "Lux",
		displayName = "Light Strike Kugel",
		slot = w,
		type = "circular",
		speed = 1200,
		range = 1100,
		radius = 300,
		danger = 3,
		cc = true,
		collision = true
	},
	MaokaiQMissile = {
		charName = "Maokai",
		displayName = "Bramble Smash",
		slot = u,
		type = "linear",
		speed = 1600,
		range = 600,
		radius = 110,
		danger = 2,
		cc = true,
		collision = false
	},
	MorganaQ = {
		charName = "Morgana",
		displayName = "Dark Binding",
		slot = u,
		type = "linear",
		speed = 1200,
		range = 1250,
		radius = 70,
		danger = 1,
		cc = true,
		collision = true
	},
	NamiQMissile = {
		charName = "Nami",
		displayName = "Aqua Prison",
		slot = u,
		type = "circular",
		speed = m,
		range = 875,
		radius = 180,
		danger = 1,
		cc = true,
		collision = false
	},
	NamiRMissile = {
		charName = "Nami",
		displayName = "Tidal Wave",
		slot = x,
		type = "linear",
		speed = 850,
		range = 2750,
		radius = 250,
		danger = 3,
		cc = true,
		collision = false
	},
	NautilusAnchorDragMissile = {
		charName = "Nautilus",
		displayName = "Dredge Line",
		slot = u,
		type = "linear",
		speed = 2000,
		range = 925,
		radius = 90,
		danger = 3,
		cc = true,
		collision = true
	},
	NeekoQ = {
		charName = "Neeko",
		displayName = "Blooming Burst",
		slot = u,
		type = "circular",
		speed = 1500,
		range = 800,
		radius = 200,
		danger = 2,
		cc = true,
		collision = false
	},
	NeekoE = {
		charName = "Neeko",
		displayName = "Tangle-Barbs",
		slot = w,
		type = "linear",
		speed = 1300,
		range = 1000,
		radius = 70,
		danger = 1,
		cc = true,
		collision = false
	},
	JavelinToss = {
		charName = "Nidalee",
		displayName = "Javelin Toss",
		slot = u,
		type = "linear",
		speed = 1300,
		range = 1500,
		radius = 40,
		danger = 1,
		cc = false,
		collision = true
	},
	NocturneDuskbringer = {
		charName = "Nocturne",
		displayName = "Duskbringer",
		slot = u,
		type = "linear",
		speed = 1600,
		range = 1200,
		radius = 60,
		danger = 2,
		cc = false,
		collision = false
	},
	OlafAxeThrow = {
		charName = "Olaf",
		displayName = "Undertow",
		slot = u,
		type = "linear",
		speed = 1600,
		range = 1000,
		radius = 90,
		danger = 2,
		cc = true,
		collision = false
	},
	PoppyRMissile = {
		charName = "Poppy",
		displayName = "Keeper's Verdict",
		slot = x,
		type = "linear",
		speed = 2000,
		range = 1200,
		radius = 100,
		danger = 3,
		cc = true,
		collision = false
	},
	PykeQRange = {
		charName = "Pyke",
		displayName = "Bone Skewer [Range]",
		slot = u,
		type = "linear",
		speed = 2000,
		range = 1100,
		radius = 70,
		danger = 2,
		cc = true,
		collision = true
	},
	QuinnQ = {
		charName = "Quinn",
		displayName = "Blinding Assault",
		slot = u,
		type = "linear",
		speed = 1550,
		range = 1025,
		radius = 60,
		danger = 1,
		cc = false,
		collision = true
	},
	RakanQMis = {
		charName = "Rakan",
		displayName = "Gleaming Quill",
		slot = u,
		type = "linear",
		speed = 1850,
		range = 850,
		radius = 65,
		danger = 1,
		cc = false,
		collision = true
	},
	RekSaiQBurrowedMis = {
		charName = "RekSai",
		displayName = "Prey Seeker",
		slot = u,
		type = "linear",
		speed = 1950,
		range = 1625,
		radius = 65,
		danger = 2,
		cc = false,
		collision = true
	},
	RengarEMis = {
		charName = "Rengar",
		displayName = "Bola Strike",
		slot = w,
		type = "linear",
		speed = 1500,
		range = 1000,
		radius = 70,
		danger = 1,
		cc = true,
		collision = true
	},
	RumbleGrenadeMissile = {
		charName = "Rumble",
		displayName = "Electro Harpoon",
		slot = w,
		type = "linear",
		speed = 2000,
		range = 850,
		radius = 60,
		danger = 2,
		cc = true,
		collision = true
	},
	RyzeQ = {
		charName = "Ryze",
		displayName = "Overload",
		slot = u,
		type = "linear",
		speed = 1700,
		range = 1000,
		radius = 55,
		danger = 1,
		cc = false,
		collision = true
	},
	SejuaniRMissile = {
		charName = "Sejuani",
		displayName = "Glacial Prison",
		slot = x,
		type = "linear",
		speed = 1600,
		range = 1300,
		radius = 120,
		danger = 5,
		cc = true,
		collision = false
	},
	ShyvanaFireballMissile = {
		charName = "Shyvana",
		displayName = "Flame Breath [Standard]",
		slot = w,
		type = "linear",
		speed = 1575,
		range = 925,
		radius = 60,
		danger = 1,
		cc = false,
		collision = false
	},
	ShyvanaFireballDragonMissile = {
		charName = "Shyvana",
		displayName = "Flame Breath [Dragon]",
		slot = w,
		type = "linear",
		speed = 1575,
		range = 975,
		radius = 60,
		danger = 2,
		cc = false,
		collision = false
	},
	SionEMissile = {
		charName = "Sion",
		displayName = "Roar of the Slayer",
		slot = w,
		type = "linear",
		speed = 1800,
		range = 800,
		radius = 80,
		danger = 2,
		cc = true,
		collision = false
	},
	SivirQMissile = {
		charName = "Sivir",
		displayName = "Boomerang Blade",
		slot = u,
		type = "linear",
		speed = 1350,
		range = 1250,
		radius = 90,
		danger = 2,
		cc = false,
		collision = false
	},
	SkarnerFractureMissile = {
		charName = "Skarner",
		displayName = "Fracture",
		slot = w,
		type = "linear",
		speed = 1500,
		range = 1000,
		radius = 70,
		danger = 1,
		cc = true,
		collision = false
	},
	SonaRMissile = {
		charName = "Sona",
		displayName = "Crescendo",
		slot = x,
		type = "linear",
		speed = 2400,
		range = 1000,
		radius = 140,
		danger = 4,
		cc = true,
		collision = false
	},
	SyndraE = {
		charName = "Syndra",
		displayName = "Scatter the Weak",
		slot = w,
		type = "conic",
		speed = 1600,
		range = 700,
		radius = 0,
		angle = 40,
		danger = 3,
		cc = true,
		collision = false
	},
	TahmKenchQMissile = {
		charName = "TahmKench",
		displayName = "Tongue Lash",
		slot = u,
		type = "linear",
		speed = 2800,
		range = 800,
		radius = 70,
		danger = 2,
		cc = true,
		collision = true
	},
	TaliyahQMis = {
		charName = "Taliyah",
		displayName = "Threaded Volley",
		slot = u,
		type = "linear",
		speed = 3600,
		range = 1000,
		radius = 100,
		danger = 2,
		cc = false,
		collision = true
	},
	TalonWMissileOne = {
		charName = "Talon",
		displayName = "Rake",
		slot = v,
		type = "conic",
		speed = 2500,
		range = 650,
		radius = 75,
		angle = 26,
		danger = 2,
		cc = true,
		collision = false
	},
	ThreshQMissile = {
		charName = "Thresh",
		displayName = "Death Sentence",
		slot = u,
		type = "linear",
		speed = 1900,
		range = 1100,
		radius = 70,
		danger = 1,
		cc = true,
		collision = true
	},
	SealFateMissile = {
		charName = "TwistedFate",
		displayName = "Wild Cards",
		slot = u,
		type = "threeway",
		speed = 1000,
		range = 1450,
		radius = 40,
		angle = 28,
		danger = 1,
		cc = false,
		collision = false
	},
	UrgotQMissile = {
		charName = "Urgot",
		displayName = "Corrosive Charge",
		slot = u,
		type = "circular",
		speed = m,
		range = 800,
		radius = 180,
		danger = 2,
		cc = true,
		collision = false
	},
	UrgotR = {
		charName = "Urgot",
		displayName = "Fear Beyond Death",
		slot = x,
		type = "linear",
		speed = 3200,
		range = 1600,
		radius = 80,
		danger = 4,
		cc = true,
		collision = false
	},
	VarusQMissile = {
		charName = "Varus",
		displayName = "Piercing Arrow",
		slot = u,
		type = "linear",
		speed = 1900,
		range = 1525,
		radius = 70,
		danger = 1,
		cc = false,
		collision = false
	},
	VarusEMissile = {
		charName = "Varus",
		displayName = "Hail of Arrows",
		slot = w,
		type = "linear",
		speed = 1500,
		range = 925,
		radius = 260,
		danger = 3,
		cc = true,
		collision = false
	},
	VarusRMissile = {
		charName = "Varus",
		displayName = "Chain of Corruption",
		slot = x,
		type = "linear",
		speed = 1950,
		range = 1200,
		radius = 120,
		danger = 4,
		cc = true,
		collision = false
	},
	VeigarBalefulStrikeMis = {
		charName = "Veigar",
		displayName = "Baleful Strike",
		slot = u,
		type = "linear",
		speed = 2200,
		range = 900,
		radius = 70,
		danger = 2,
		cc = false,
		collision = true
	},
	VelkozQMissile = {
		charName = "Velkoz",
		displayName = "Plasma Fission",
		slot = u,
		type = "linear",
		speed = 1300,
		range = 1050,
		radius = 50,
		danger = 1,
		cc = true,
		collision = true
	},
	VelkozWMissile = {
		charName = "Velkoz",
		displayName = "Void Rift",
		slot = v,
		type = "linear",
		speed = 1700,
		range = 1050,
		radius = 87.5,
		danger = 1,
		cc = false,
		collision = false
	},
	ViktorDeathRayMissile = {
		charName = "Viktor",
		displayName = "Death Ray",
		slot = w,
		type = "linear",
		speed = 1050,
		range = 1025,
		radius = 80,
		danger = 2,
		cc = false,
		collision = false
	},
	XerathMageSpearMissile = {
		charName = "Xerath",
		displayName = "Mage Spear",
		slot = w,
		type = "linear",
		speed = 1400,
		range = 1050,
		radius = 60,
		danger = 1,
		cc = true,
		collision = true
	},
	YasuoQ3Mis = {
		charName = "Yasuo",
		displayName = "Gathering Storm",
		slot = u,
		type = "linear",
		speed = 1200,
		range = 1000,
		radius = 90,
		danger = 2,
		cc = true,
		collision = false
	},
	ZacQMissile = {
		charName = "Zac",
		displayName = "Stretching Strikes",
		slot = u,
		type = "linear",
		speed = 2800,
		range = 800,
		radius = 120,
		danger = 2,
		cc = true,
		collision = false
	},
	ZedQMissile = {
		charName = "Zed",
		displayName = "Razor Shuriken",
		slot = u,
		type = "linear",
		speed = 1700,
		range = 900,
		radius = 50,
		danger = 1,
		cc = false,
		collision = false
	},
	ZiggsQSpell = {
		charName = "Ziggs",
		displayName = "Bouncing Bomb",
		slot = u,
		type = "circular",
		speed = 1700,
		range = 850,
		radius = 120,
		danger = 1,
		cc = false,
		collision = true
	},
	ZiggsW = {
		charName = "Ziggs",
		displayName = "Satchel Charge",
		slot = v,
		type = "circular",
		speed = 1750,
		range = 1000,
		radius = 240,
		danger = 2,
		cc = true,
		collision = false
	},
	ZiggsE = {
		charName = "Ziggs",
		displayName = "Hexplosive Minefield",
		slot = w,
		type = "circular",
		speed = 1800,
		range = 900,
		radius = 250,
		danger = 2,
		cc = true,
		collision = false
	},
	ZileanQMissile = {
		charName = "Zilean",
		displayName = "Time Bomb",
		slot = u,
		type = "circular",
		speed = m,
		range = 900,
		radius = 150,
		danger = 2,
		cc = true,
		collision = false
	},
	ZoeQMissile = {
		charName = "Zoe",
		displayName = "Paddle Star [First]",
		slot = u,
		type = "linear",
		speed = 1200,
		range = 800,
		radius = 50,
		danger = 1,
		cc = false,
		collision = true
	},
	ZoeQMis2 = {
		charName = "Zoe",
		displayName = "Paddle Star [Second]",
		slot = u,
		type = "linear",
		speed = 2500,
		range = 1600,
		radius = 70,
		danger = 2,
		cc = false,
		collision = true
	},
	ZoeEMis = {
		charName = "Zoe",
		displayName = "Sleepy Trouble Bubble",
		slot = w,
		type = "linear",
		speed = 1700,
		range = 800,
		radius = 50,
		danger = 2,
		cc = true,
		collision = true
	},
	ZyraE = {
		charName = "Zyra",
		displayName = "Grasping Roots",
		slot = w,
		type = "linear",
		speed = 1150,
		range = 1100,
		radius = 70,
		danger = 1,
		cc = true,
		collision = false
	}
}
local aa = function()
	local ab = {}
	ab.__index = ab
	return setmetatable(ab, {
		__call = function(ac, ...)
			local ad = setmetatable({}, ab)
			if ab.__init then
				ab.__init(ad, ...)
			end
			return ad
		end
	})
end
function OnLoad()
	if S(COMMON_PATH .. "FF15Menu.lua") then
		require("FF15Menu")
	else
		PrintChat("<font color=\"#00ced1\"><b>Missing:</font></b> <font color=\"#87cefa\">FF15Menu</font>")
		return
	end
	if S(COMMON_PATH .. "FF15-PremiumPrediction.lua") then
		require("FF15-PremiumPrediction")
	else
		PrintChat("<font color=\"#00ced1\"><b>Missing:</font></b> <font color=\"#87cefa\">PremiumPrediction</font>")
		return
	end
	if S(COMMON_PATH .. "GeometryLib.lua") then
		H = require("GeometryLib")
	else
		PrintChat("<font color=\"#00ced1\"><b>Missing:</font></b> <font color=\"#87cefa\">GeometryLib</font>")
		return
	end
	if not _G.Prediction then
		LoadPaidScript(PaidScript.DREAM_PRED)
	end
	I = H.Vector
	J.Geometry = Geometry()
	J.Manager = Manager()
	J.TargetSelector = TargetSelector()
	if myHero.spellbook:Spell(y).name == "SummonerDot" then
		G = y
	elseif myHero.spellbook:Spell(z).name == "SummonerDot" then
		G = z
	end
	if myHero.charName == "Xerath" then
		Xerath()
	elseif myHero.charName == "Viktor" then
		Viktor()
	elseif myHero.charName == "Yasuo" then
		Yasuo()
	end
	a3()
	d()
end
Geometry = aa()
function Geometry:__init()
end
function Geometry:CalculateCollisionTime(ae, af, ag, ah, ai)
	local aj = I(ae):extended(af, ai * (RiotClock.time - ah))
	return self:GetDistance(ag, aj) / ai
end
function Geometry:CalculateEndPos(ae, ak, ag, al, am, an, type)
	local af = I(ae):extended(ak, al)
	if type == "linear" or type == "threeway" or type == "conic" then
		if an then
			local ae, ao = I(ae):extended(ak, 45), {}
			for Z, ap in pairs(ObjectManager:GetAllyMinions()) do
				if ap and ap.team == myHero.team and al >= self:GetDistance(ap.position, ae) and ap.maxHealth > 5 and not ap.isDead then
					local aq = self:VectorPointProjectionOnLineSegment(ae, ak, ap.position)
					if aq then
						if self:GetDistance(aq, ap.position) < (ap.boundingRadius or 45) / 2 + am then
							A(ao, ap.position)
						end
					end
				end
			end
			if #ao > 0 then
				C(ao, function(ar, as)
					return self:GetDistanceSqr(ar, ae) < self:GetDistanceSqr(as, ae)
				end)
				local at = self:GetDistance(ae, ao[1])
				local af = ae:extended(ak, at)
				do return af, at end
				elseif al > 0 then
					if al > self:GetDistance(ag, ak) then
						af = ak
					end
				else
					af = ag
				end
			end
		else
		end
	return af, al
end
function Geometry:GetBestCircularAOEPos(au, am, av)
end
function Geometry:GetBestLinearAOEPos(au, al, am)
	local aw, ax = nil, 0
	local ay, az = {}, {}
	for Z, aA in ipairs(au) do
		ay[Z] = I(aA.position)
		az[Z] = I(aA.position)
	end
	local aB = #au
	for Z = 1, aB do
		for aC = 1, aB do
			if ay[aC] ~= ay[Z] then
				A(ay, I(ay[aC] + ay[Z]) / 2)
			end
		end
	end
	for Z, aj in ipairs(ay) do
		local aD = myHero.position
		if Geometry:GetDistanceSqr(aj, aD) <= al * al then
			local aE = I(aD):extended(aj, al)
			local aF = 0
			for Z = 1, #az do
				local ag = az[Z]
				local aG, aH, aI = Geometry:VectorPointProjectionOnLineSegment(aD, aE, ag)
				if aI and Geometry:GetDistanceSqr(aG, ag) < am * am then
					aF = aF + 1
				end
			end
			if ax <= aF then
				aw, ax = aE, aF
			end
		end
	end
	return aw, ax
end
function Geometry:GetDistance(aJ, aK)
	return t(self:GetDistanceSqr(aJ, aK))
end
function Geometry:GetDistanceSqr(aJ, aK)
	local aK = aK and aK or myHero.position
	local aL, aM = aK.x - aJ.x, (aK.z or aK.y) - (aJ.z or aJ.y)
	return aL * aL + aM * aM
end
function Geometry:GetMousePos()
	return pwHud.hudManager.activeVirtualCursorPos
end
function Geometry:IsInRange(aJ, aK, al)
	local aL, aM = aK.x - aJ.x, (aK.z or aK.y) - (aJ.z or aJ.y)
	return aL * aL + aM * aM <= al * al
end
function Geometry:To2D(aj)
	return Renderer:WorldToScreen(Geometry:To3D(aj))
end
function Geometry:To3D(aj)
	return D3DXVECTOR3(aj.x, aj.y, aj.z)
end
function Geometry:VectorPointProjectionOnLineSegment(aN, aO, aP)
	local aQ, aR, aS, aT, aU, aV = aP.x, aP.z, aN.x, aN.z, aO.x, aO.z
	local aW = ((aQ - aS) * (aU - aS) + (aR - aT) * (aV - aT)) / ((aU - aS) ^ 2 + (aV - aT) ^ 2)
	local aX = {
		x = aS + aW * (aU - aS),
		y = aT + aW * (aV - aT)
	}
	local aY = aW < 0 and 0 or aW > 1 and 1 or aW
	local aZ = aY == aW
	local a_ = aZ and aX or {
		x = aS + aY * (aU - aS),
		y = aT + aY * (aV - aT)
	}
	return a_, aX, aZ
end
Manager = aa()
function Manager:__init()
end
function Manager:CalcMagicalDamage(b0, a7, b1)
	local b2 = a7.characterIntermediate.spellBlock
	local b3 = b0.characterIntermediate.flatMagicPenetration
	b2 = b2 - b3
	if b2 < 0 then
		b2 = 0
	end
	local b4 = 0
	if b2 > 0 then
		local b5 = b0.characterIntermediate.percentMagicPenetration
		b4 = b2 - b5 * b2 / 100
	end
	return n(0, b1 * 100 / (100 + b4))
end
function Manager:CalcPhysicalDamage(b0, a7, b1)
	local b2 = a7.characterIntermediate.armor
	local b3 = b0.characterIntermediate.physicalLethality * (0.6 + 0.4 * b0.experience.level / 18)
	b2 = b2 - b3
	if b2 < 0 then
		b2 = 0
	end
	local b4 = 0
	if b2 > 0 then
		local b5 = b0.characterIntermediate.percentArmorPenetration
		b4 = b2 - b5 * b2 / 100
	end
	return n(0, b1 * 100 / (100 + b4))
end
function Manager:DrawCircle(aj, am, b6)
	DrawHandler:Circle3D(Geometry:To3D(I(aj)), am, b6)
end
function Manager:DrawText(b7, b8, aj, b9, ba, b6)
	DrawHandler:Text(b8, Geometry:To2D(I(aj.x + b9, 0, aj.z + ba)), b7, b6)
end
function Manager:GetBonusAP(aA)
	return aA.characterIntermediate.flatMagicDamageMod
end
function Manager:GetBonusDmg(aA)
	return aA.characterIntermediate.flatPhysicalDamageMod
end
function Manager:GetCastLevel(aA, bb)
	return aA.spellbook:Spell(bb).level or 1
end
function Manager:GetDamage(a7, bc, bd)
	local bd = bd or 0
	if bc >= 0 and bc <= 3 and a6[myHero.charName] then
		for Z, be in pairs(a6[myHero.charName]) do
			if be.slot == bc and be.state == bd then
				return be.damage(a7)
			end
		end
	end
end
function Manager:GetHeroesAround(aj, al, bf)
	local au, bg = {}, 0
	local bh = bf == "allies" and E or F
	for Z = 1, #bh do
		local aA = bh[Z]
		if self:ValidTarget(aA, al) then
			A(au, aA)
			bg = bg + 1
		end
	end
	return au, bg
end
function Manager:GetMinionsAround(aj, al, bf)
	local au, bg = {}, 0
	local ao = bf == "allies" and ObjectManager:GetAllyMinions() or ObjectManager:GetEnemyMinions()
	for Z = 1, #ao do
		local ap = ao[Z]
		if ap.maxHealth > 5 and self:ValidTarget(ap, al) then
			A(au, ap)
			bg = bg + 1
		end
	end
	return au, bg
end
function Manager:GetOrbwalkerMode()
	return _G.LegitOrbwalker and _G.LegitOrbwalker:GetMode() or nil
end
function Manager:GetTotalDmg(aA)
	return aA.characterIntermediate.flatPhysicalDamageMod + aA.characterIntermediate.baseAttackDamage
end
function Manager:IsReady(bb)
	return myHero.spellbook:CanUseSpell(bb) == 0
end
function Manager:ValidTarget(a7, al)
	local al = al or m
	return a7 and a7.isValid and a7.isVisible and not a7.isDead and a7.maxHealth > 1 and a7.health > 0 and a7.isTargetable and Geometry:GetDistanceSqr(myHero.position, a7.position) <= al * al
end
TargetSelector = aa()
function TargetSelector:__init()
	self.Timer, self.SelectedTarget = 0, nil
	self.DamageType = {
		Viktor = "AP",
		Xerath = "AP",
		Yasuo = "AD"
	}
	self.Damage = function(a7, bi, bj) return bi == "AP" and Manager:CalcMagicalDamage(myHero, a7, bj) or Manager:CalcPhysicalDamage(myHero, a7, bj) end
	self.Modes = {
		[1] = function(ar, as)
			return self.Damage(ar, self.DamageType[myHero.charName], 100) / (1 + ar.health) * self:GetPriority(ar) > self.Damage(as, self.DamageType[myHero.charName], 100) / (1 + as.health) * self:GetPriority(as)
		end,
		[2] = function(ar, as)
			return self:GetPriority(ar) > self:GetPriority(as)
		end,
		[3] = function(ar, as)
			return self.Damage(ar, "AD", 100) / (1 + ar.health) * self:GetPriority(ar) > self.Damage(as, "AD", 100) / (1 + as.health) * self:GetPriority(as)
		end,
		[4] = function(ar, as)
			return self.Damage(ar, "AP", 100) / (1 + ar.health) * self:GetPriority(ar) > self.Damage(as, "AP", 100) / (1 + as.health) * self:GetPriority(as)
		end,
		[5] = function(ar, as)
			return self.Damage(ar, "AD", 100) / (1 + ar.health) > self.Damage(as, "AD", 100) / (1 + as.health)
		end,
		[6] = function(ar, as)
			return self.Damage(ar, "AP", 100) / (1 + ar.health) > self.Damage(as, "AP", 100) / (1 + as.health)
		end,
		[7] = function(ar, as)
			return ar.health < as.health
		end,
		[8] = function(ar, as)
			return Manager:GetTotalDmg(ar) > Manager:GetTotalDmg(as)
		end,
		[9] = function(ar, as)
			return Manager:GetBonusAP(ar) > Manager:GetBonusAP(as)
		end,
		[10] = function(ar, as)
			return Geometry:GetDistance(ar.position, myHero.position) < Geometry:GetDistance(as.position, myHero.position)
		end,
		[11] = function(ar, as)
			return Geometry:GetDistance(ar.position, Geometry:GetMousePos()) < Geometry:GetDistance(as.position, Geometry:GetMousePos())
		end
	}
	self.Priorities = {
		Aatrox = 3,
		Ahri = 4,
		Akali = 4,
		Alistar = 1,
		Amumu = 1,
		Anivia = 4,
		Annie = 4,
		Ashe = 5,
		AurelionSol = 4,
		Azir = 4,
		Bard = 3,
		Blitzcrank = 1,
		Brand = 4,
		Braum = 1,
		Caitlyn = 5,
		Camille = 3,
		Cassiopeia = 4,
		Chogath = 1,
		Corki = 5,
		Darius = 2,
		Diana = 4,
		DrMundo = 1,
		Draven = 5,
		Ekko = 4,
		Elise = 3,
		Evelynn = 4,
		Ezreal = 5,
		Fiddlesticks = 3,
		Fiora = 3,
		Fizz = 4,
		Galio = 1,
		Gangplank = 4,
		Garen = 1,
		Gnar = 1,
		Gragas = 2,
		Graves = 4,
		Hecarim = 2,
		Heimerdinger = 3,
		Illaoi = 3,
		Irelia = 3,
		Ivern = 1,
		Janna = 2,
		JarvanIV = 3,
		Jax = 3,
		Jayce = 4,
		Jhin = 5,
		Jinx = 5,
		Kaisa = 5,
		Kalista = 5,
		Karma = 4,
		Karthus = 4,
		Kassadin = 4,
		Katarina = 4,
		Kayle = 4,
		Kayn = 4,
		Kennen = 4,
		Khazix = 4,
		Kindred = 4,
		Kled = 2,
		KogMaw = 5,
		Leblanc = 4,
		LeeSin = 3,
		Leona = 1,
		Lissandra = 4,
		Lucian = 5,
		Lulu = 3,
		Lux = 4,
		Malphite = 1,
		Malzahar = 3,
		Maokai = 2,
		MasterYi = 5,
		MissFortune = 5,
		MonkeyKing = 3,
		Mordekaiser = 4,
		Morgana = 3,
		Nami = 3,
		Nasus = 2,
		Nautilus = 1,
		Neeko = 4,
		Nidalee = 4,
		Nocturne = 4,
		Nunu = 2,
		Olaf = 2,
		Orianna = 4,
		Ornn = 2,
		Pantheon = 3,
		Poppy = 2,
		Pyke = 5,
		Quinn = 5,
		Rakan = 3,
		Rammus = 1,
		RekSai = 2,
		Renekton = 2,
		Rengar = 4,
		Riven = 4,
		Rumble = 4,
		Ryze = 4,
		Sejuani = 2,
		Shaco = 4,
		Shen = 1,
		Shyvana = 2,
		Singed = 1,
		Sion = 1,
		Sivir = 5,
		Skarner = 2,
		Sona = 3,
		Soraka = 3,
		Swain = 3,
		Sylas = 4,
		Ashe = 4,
		TahmKench = 1,
		Taliyah = 4,
		Talon = 4,
		Taric = 1,
		Teemo = 4,
		Thresh = 1,
		Tristana = 5,
		Trundle = 2,
		Tryndamere = 4,
		TwistedFate = 4,
		Twitch = 5,
		Udyr = 2,
		Urgot = 2,
		Varus = 5,
		Vayne = 5,
		Veigar = 4,
		Velkoz = 4,
		Vi = 2,
		Viktor = 4,
		Vladimir = 3,
		Volibear = 2,
		Warwick = 2,
		Xayah = 5,
		Xerath = 4,
		XinZhao = 3,
		Yasuo = 4,
		Yorick = 2,
		Zac = 1,
		Zed = 4,
		Ziggs = 4,
		Zilean = 3,
		Zoe = 4,
		Zyra = 2
	}
	self.TargetSelectorMenu = Menu("TargetSelector", "[Premium Series] Target Selector")
	self.TargetSelectorMenu:list("TS", "Target Selector Mode", 1, {
		"Auto",
		"Priority",
		"Less Attack Priority",
		"Less Cast Priority",
		"Less Attack",
		"Less Cast",
		"Lowest HP",
		"Most AD",
		"Most AP",
		"Closest",
		"Near Mouse"
	})
	self.TargetSelectorMenu:sub("PR", "Priority Menu")
	self.TargetSelectorMenu:key("ST", "Selected Target", string.byte("Z"))
	DelayAction(function()
		for Z = 1, #F do
			local a4 = F[Z]
			self.TargetSelectorMenu.PR:slider("Level" .. a4.charName, a4.charName, 1, 5, self.Priorities[a4.charName] or 3, 1)
		end
	end, 0.01)
	function K.TargetSelector()
		self:Draw()
	end
	function R.TargetSelector(...)
		self:WndProc(...)
	end
end
function TargetSelector:Draw()
	if myHero.isDead then
		return
	end
	if self.SelectedTarget then
		self:DrawCircle(self.SelectedTarget.position, 100, 3237992192)
	end
end
function TargetSelector:WndProc(bk, bl, bm, bn)
	if RiotClock.time > self.Timer + 0.1 then
		if self.TargetSelectorMenu.ST:get() == bm then
			if self.SelectedTarget == nil then
				local bo = Geometry:GetMousePos()
				for Z = 1, #F do
					local a4 = F[Z]
					if a4 and Manager:ValidTarget(a4) and Geometry:IsInRange(bo, a4.position, 50) then
						self.SelectedTarget = a4
						break
					end
				end
			else
				self.SelectedTarget = nil
			end
		end
		self.Timer = RiotClock.time
	end
end
function TargetSelector:GetPriority(a4)
	local bp = 1
	if self.TargetSelectorMenu == nil then
		return bp
	end
	if self.TargetSelectorMenu.PR["Level" .. a4.charName]:get() ~= nil then
		bp = self.TargetSelectorMenu.PR["Level" .. a4.charName]:get()
	end
	if bp == 2 then
		return 1.5
	elseif bp == 3 then
		return 1.75
	elseif bp == 4 then
		return 2
	elseif bp == 5 then
		return 2.5
	end
	return bp
end
function TargetSelector:GetTarget(al, bf)
	if self.SelectedTarget and al >= Geometry:GetDistance(myHero.position, self.SelectedTarget.position) then
		return self.SelectedTarget
	end
	local bq = {}
	for Z = 1, #F do
		local a4 = F[Z]
		if Manager:ValidTarget(a4, al) then
			A(bq, a4)
		end
	end
	local br = bf or self.TargetSelectorMenu.TS:get() or 1
	C(bq, self.Modes[br])
	return #bq > 0 and bq[1] or nil
end
Viktor = aa()
function Viktor:__init()
	self.Target1, self.Target2 = nil, nil
	self.Range = myHero.characterIntermediate.attackRange + myHero.boundingRadius * 2
	self.QData, self.WData = a8[myHero.charName][0], a8[myHero.charName][1]
	self.EData, self.RData = a8[myHero.charName][2], a8[myHero.charName][3]
	self.ViktorMenu = Menu("Viktor", "[Premium Series] Viktor")
	self.ViktorMenu:sub("Auto", "Auto")
	self.ViktorMenu.Auto:checkbox("UseE", "Use E [Death Ray]", true)
	self.ViktorMenu.Auto:slider("Mana", "Mana Manager", 0, 100, 35, 5)
	self.ViktorMenu:sub("Combo", "Combo")
	self.ViktorMenu.Combo:checkbox("UseQ", "Use Q [Siphon Power]", true)
	self.ViktorMenu.Combo:checkbox("UseW", "Use W [Gravity Field]", true)
	self.ViktorMenu.Combo:checkbox("UseE", "Use E [Death Ray]", true)
	self.ViktorMenu.Combo:checkbox("UseR", "Use E [Chaos Storm]", true)
	self.ViktorMenu.Auto:slider("ER", "Minimum Hits: R", 1, 5, 2, 1)
	self.ViktorMenu:sub("Harass", "Harass")
	self.ViktorMenu.Harass:checkbox("UseQ", "Use Q [Siphon Power]", true)
	self.ViktorMenu.Harass:checkbox("UseW", "Use W [Gravity Field]", true)
	self.ViktorMenu.Harass:checkbox("UseE", "Use E [Death Ray]", true)
	self.ViktorMenu.Harass:slider("Mana", "Mana Manager", 0, 100, 35, 5)
	self.ViktorMenu:sub("KillSteal", "KillSteal")
	self.ViktorMenu.KillSteal:checkbox("UseIgn", "Use Ignite", true)
	self.ViktorMenu:sub("LaneClear", "LaneClear")
	self.ViktorMenu.LaneClear:checkbox("UseE", "Use E [Death Ray]", true)
	self.ViktorMenu.LaneClear:slider("MHE", "Minimum Hits: E", 1, 15, 5, 1)
	self.ViktorMenu.LaneClear:slider("Mana", "Mana Manager", 0, 100, 35, 5)
	self.ViktorMenu:sub("AntiGapcloser", "Anti-Gapcloser")
	self.ViktorMenu.AntiGapcloser:checkbox("UseW", "Use W [Gravity Field]", true)
	self.ViktorMenu.AntiGapcloser:slider("Distance", "Distance: W", 50, 500, 250, 5)
	self.ViktorMenu:sub("HitChance", "HitChance")
	self.ViktorMenu.HitChance:list("Pred", "Prediction", 2, {
		"Dream Prediction",
		"Premium Prediction"
	})
	self.ViktorMenu.HitChance:slider("HCW", "HitChance: W", 0, 100, 70, 1)
	self.ViktorMenu.HitChance:slider("HCE", "HitChance: E", 0, 100, 40, 1)
	self.ViktorMenu.HitChance:slider("HCR", "HitChance: R", 0, 100, 50, 1)
	self.ViktorMenu:sub("Drawings", "Drawings")
	self.ViktorMenu.Drawings:checkbox("DrawQ", "Draw Q Range", true)
	self.ViktorMenu.Drawings:checkbox("DrawW", "Draw W Range", true)
	self.ViktorMenu.Drawings:checkbox("DrawE", "Draw E Range", true)
	self.ViktorMenu.Drawings:checkbox("DrawR", "Draw R Range", true)
	function K.Champion()
		self:Draw()
	end
	function L.Champion()
		self:Tick()
	end
	function Q.Champion(...)
		self:ProcessSpell(...)
	end
end
function Viktor:Draw()
	if self.ViktorMenu.Drawings.DrawQ:get() then
		Manager:DrawCircle(myHero.position, self.QData.range, 2147549183)
	end
	if self.ViktorMenu.Drawings.DrawW:get() then
		Manager:DrawCircle(myHero.position, self.WData.range, 2147532799)
	end
	if self.ViktorMenu.Drawings.DrawE:get() then
		Manager:DrawCircle(myHero.position, self.EData.range, 2149486847)
	end
	if self.ViktorMenu.Drawings.DrawR:get() then
		Manager:DrawCircle(myHero.position, self.RData.range, 2147483903)
	end
end
function Viktor:Tick()
	if myHero.isDead or _G.JustEvade then
		return
	end
	self:Auto2()
	if Manager:GetOrbwalkerMode() == "Waveclear" then
		self:LaneClear()
		return
	end
	self.Target1 = J.TargetSelector:GetTarget(self.WData.range, nil)
	self.Target2 = J.TargetSelector:GetTarget(self.EData.range, nil)
	if self.Target2 == nil then
		return
	end
	if Manager:GetOrbwalkerMode() == "Combo" then
		self:Combo(self.Target1, self.Target2)
	elseif Manager:GetOrbwalkerMode() == "Harass" then
		self:Harass(self.Target1, self.Target2)
	elseif Manager:GetOrbwalkerMode() == "Waveclear" then
		self:LaneClear()
	else
		self:Auto(self.Target2)
	end
end
function Viktor:ProcessSpell(aA, bc)
	if bc and aA == myHero and bc.name == "ViktorQ" then
		return
	end
end
function Viktor:Auto(a7)
	if a7 == nil or myHero.manaPercent < self.ViktorMenu.Auto.Mana:get() then
		return
	end
	if self.ViktorMenu.Auto.UseE:get() and Manager:IsReady(w) and Manager:ValidTarget(a7, self.EData.range) then
		self:UseE(a7)
	end
end
function Viktor:Auto2()
	for Z, a4 in pairs(F) do
		if self.ViktorMenu.AntiGapcloser.UseW:get() and Manager:ValidTarget(a4, self.ViktorMenu.AntiGapcloser.Distance:get()) and Manager:IsReady(v) then
			self:UseW(a4)
		end
		if self.ViktorMenu.KillSteal.UseIgn:get() and G and Manager:IsReady(G) and Manager:ValidTarget(a4, 600) then
			local bs = 50 + 20 * myHero.experience.level - 3 * (a4.characterIntermediate.hpRegenRate + a4.characterIntermediate.baseHPRegenRate)
			if bs > a4.health then
				myHero.spellbook:CastSpell(G, a4.networkId)
			end
		end
	end
end
function Viktor:Combo(bt, bu)
	if _G.LegitOrbwalker and _G.LegitOrbwalker:IsAttacking() then
		return
	end
	if bu and Manager:IsReady(w) and self.ViktorMenu.Combo.UseE:get() and Manager:ValidTarget(bu, self.EData.range) then
		self:UseE(bu)
	elseif bt then
		if Manager:IsReady(u) and self.ViktorMenu.Combo.UseQ:get() and Manager:ValidTarget(bt, self.QData.range) then
			myHero.spellbook:CastSpell(u, bt.networkId)
			return
		end
		if Manager:IsReady(v) and self.ViktorMenu.Combo.UseW:get() and Manager:ValidTarget(bt, self.WData.range) then
			self:UseW(bt)
			return
		end
		if Manager:IsReady(x) and not Manager:IsReady(w) and self.ViktorMenu.Combo.UseR:get() and Manager:ValidTarget(bt, self.Range) then
			local bv = Manager:GetHeroesAround(myHero.position, self.RData.range)
			local bw, bx = Geometry:GetBestCircularAOEPos(bv, self.RData.range, self.RData.radius - 5)
			if bw and bx >= self.XerathMenu.LaneClear.MHQ:get() then
				self:UseR(bw)
			end
		end
	end
end
function Viktor:Harass(bt, bu)
	if _G.LegitOrbwalker and _G.LegitOrbwalker:IsAttacking() or myHero.manaPercent < self.XerathMenu.Harass.Mana:get() then
		return
	end
	if bu and Manager:IsReady(w) and self.ViktorMenu.Harass.UseE:get() and Manager:ValidTarget(bu, self.EData.range) then
		self:UseE(bu)
	elseif bt then
		if Manager:IsReady(u) and self.ViktorMenu.Harass.UseQ:get() and Manager:ValidTarget(bt, self.QData.range) then
			myHero.spellbook:CastSpell(u, bt.networkId)
			return
		end
		if Manager:IsReady(v) and self.ViktorMenu.Harass.UseW:get() and Manager:ValidTarget(bt, self.WData.range) then
			self:UseW(bt)
		end
	end
end
function Viktor:LaneClear()
	if _G.LegitOrbwalker and _G.LegitOrbwalker:IsAttacking() or myHero.manaPercent < self.ViktorMenu.LaneClear.Mana:get() then
		return
	end
	if self.ViktorMenu.LaneClear.UseE:get() and Manager:IsReady(w) then
		local ao, bg = Manager:GetMinionsAround(myHero.position, self.EData.range)
		if bg > 0 then
			local ae, af, bx = self:GetBestLaserFarmPos(ao)
			if ae and af and bx >= self.ViktorMenu.LaneClear.MHE:get() then
				myHero.spellbook:CastSpell(w, Geometry:To3D(ae), Geometry:To3D(af))
			end
		end
	end
end
function Viktor:GetBestLaserFarmPos(ao)
	local by, bz, ax = nil, nil, 0
	local ay, bA = {}, {}
	for Z, ap in ipairs(ao) do
		ay[Z] = I(ap.position)
		bA[Z] = I(ap.position)
	end
	local aB = #ao
	for Z = 1, aB do
		for aC = 1, aB do
			if ay[aC] ~= ay[Z] then
				A(ay, I(ay[aC] + ay[Z]) / 2)
			end
		end
	end
	for Z, aD in ipairs(ao) do
		if Geometry:GetDistance(myHero.position, aD) < 525 then
			for Z, aj in ipairs(ay) do
				if Geometry:GetDistanceSqr(aj, aD) <= 490000 then
					local aE = I(aD):extended(aj, 700)
					local aF = 0
					for Z = 1, #bA do
						local bB = bA[Z]
						local aG, aH, aI = Geometry:VectorPointProjectionOnLineSegment(aD, aE, bB)
						if aI and Geometry:GetDistanceSqr(aG, bB) < 6400 then
							aF = aF + 1
						end
					end
					if ax <= aF then
						by, bz, ax = aD, aE, aF
					end
				end
			end
		end
	end
	return by, bz, ax
end
function Viktor:UseW(aA)
	local CP, bC, bD, bE, bF
	if self.ViktorMenu.HitChance.Pred:get() == 2 then
		CP, bC, bD, bE = PremiumPrediction:GetPrediction(myHero, aA, self.WData.speed, self.WData.range, self.WData.delay, self.WData.radius, self.WData.angle, self.WData.collision)
	else
		bF = _G.Prediction.GetPrediction(aA, self.WData, myHero)
		CP, bD = bF.castPosition, bF.hitChance
	end
	if CP and bD >= self.ViktorMenu.HitChance.HCW:get() / 100 then
		myHero.spellbook:CastSpell(v, Geometry:To3D(CP))
	end
end
function Viktor:UseE(aA)
	local CP, bC, bD, bE, bF
	local ae = I(myHero.position):extended(aA.position, 525)
	if self.ViktorMenu.HitChance.Pred:get() == 2 then
		bC, CP, bD, bE = PremiumPrediction:GetPrediction(ae, aA, self.EData.speed, self.EData.range, self.EData.delay, self.EData.radius, self.EData.angle, self.EData.collision)
	else
		bF = _G.Prediction.GetPrediction(aA, self.EData, ae)
		CP, bD = bF.castPosition, bF.hitChance
	end
	if CP and bD >= self.ViktorMenu.HitChance.HCE:get() / 100 then
		myHero.spellbook:CastSpell(w, Geometry:To3D(ae), Geometry:To3D(CP))
	end
end
function Viktor:UseR(aA)
	local CP, bC, bD, bE, bF
	if self.ViktorMenu.HitChance.Pred:get() == 2 then
		CP, bC, bD, bE = PremiumPrediction:GetPrediction(myHero, aA, self.RData.speed, self.RData.range, self.RData.delay, self.RData.radius, self.RData.angle, self.RData.collision)
	else
		bF = _G.Prediction.GetPrediction(aA, self.RData, myHero)
		CP, bD = bF.castPosition, bF.hitChance
	end
	if CP and bD >= self.ViktorMenu.HitChance.HCR:get() / 100 then
		myHero.spellbook:CastSpell(x, Geometry:To3D(CP))
	end
end
Xerath = aa()
function Xerath:__init()
	self.Target1, self.Target2 = nil, nil
	self.QData, self.WData = a8[myHero.charName][0], a8[myHero.charName][1]
	self.EData, self.RData = a8[myHero.charName][2], a8[myHero.charName][3]
	self.RRanges = {
		[1] = 3520,
		[2] = 4840,
		[3] = 6160
	}
	self.Charged, self.Channeled, self.QBuff = false, false, false
	self.ChargedTime, self.ChargedRange, self.MinRange, self.RTimer = 0, 750, 750, 0
	self.XerathMenu = Menu("Xerath", "[Premium Series] Xerath")
	self.XerathMenu:sub("Auto", "Auto")
	self.XerathMenu.Auto:checkbox("UseW", "Use W [Eye of Destruction]", true)
	self.XerathMenu.Auto:checkbox("UseR", "Use R [Rite of the Arcane]", true)
	self.XerathMenu.Auto:checkbox("RandomR", "Random Delay: R", true)
	self.XerathMenu.Auto:list("CastR", "Target Selector: R", 2, {"Default", "Near Mouse"})
	self.XerathMenu.Auto:slider("DelayR", "Custom Delay: R", 0.25, 1.5, 0.75, 0.05)
	self.XerathMenu.Auto:slider("Mana", "Mana Manager: W", 0, 100, 35, 5)
	self.XerathMenu:sub("Combo", "Combo")
	self.XerathMenu.Combo:checkbox("UseQ", "Use Q [Arcanopulse]", true)
	self.XerathMenu.Combo:checkbox("UseW", "Use W [Eye of Destruction]", true)
	self.XerathMenu.Combo:checkbox("UseE", "Use E [Shocking Orb]", true)
	self.XerathMenu:sub("Harass", "Harass")
	self.XerathMenu.Harass:checkbox("UseQ", "Use Q [Arcanopulse]", true)
	self.XerathMenu.Harass:checkbox("UseW", "Use W [Eye of Destruction]", true)
	self.XerathMenu.Harass:checkbox("UseE", "Use E [Shocking Orb]", true)
	self.XerathMenu.Harass:slider("Mana", "Mana Manager", 0, 100, 35, 5)
	self.XerathMenu:sub("KillSteal", "KillSteal")
	self.XerathMenu.KillSteal:checkbox("UseW", "Use W [Eye of Destruction]", true)
	self.XerathMenu:sub("LaneClear", "LaneClear")
	self.XerathMenu.LaneClear:checkbox("UseQ", "Use Q [Arcanopulse]", true)
	self.XerathMenu.LaneClear:slider("MHQ", "Minimum Hits: Q", 1, 15, 6, 1)
	self.XerathMenu.LaneClear:slider("Mana", "Mana Manager", 0, 100, 35, 5)
	self.XerathMenu:sub("AntiGapcloser", "Anti-Gapcloser")
	self.XerathMenu.AntiGapcloser:checkbox("UseE", "Use E [Shocking Orb]", true)
	self.XerathMenu.AntiGapcloser:slider("Distance", "Distance: E", 50, 500, 350, 5)
	self.XerathMenu:sub("HitChance", "HitChance")
	self.XerathMenu.HitChance:list("Pred", "Prediction", 2, {
		"Dream Prediction",
		"Premium Prediction"
	})
	self.XerathMenu.HitChance:slider("HCQ", "HitChance: Q", 0, 100, 20, 1)
	self.XerathMenu.HitChance:slider("HCW", "HitChance: W", 0, 100, 30, 1)
	self.XerathMenu.HitChance:slider("HCE", "HitChance: E", 0, 100, 50, 1)
	self.XerathMenu.HitChance:slider("HCR", "HitChance: R", 0, 100, 10, 1)
	self.XerathMenu:sub("Drawings", "Drawings")
	self.XerathMenu.Drawings:checkbox("DrawQ", "Draw Q Range", true)
	self.XerathMenu.Drawings:checkbox("DrawW", "Draw W Range", true)
	self.XerathMenu.Drawings:checkbox("DrawE", "Draw E Range", true)
	self.XerathMenu.Drawings:checkbox("DrawR", "Draw R Range", true)
	function K.Champion()
		self:Draw()
	end
	function L.Champion()
		self:Tick()
	end
	function M.Champion(...)
		self:BuffGain(...)
	end
	function N.Champion(...)
		self:BuffLost(...)
	end
end
function Xerath:Draw()
	if self.XerathMenu.Drawings.DrawQ:get() then
		Manager:DrawCircle(myHero.position, self.QData.range, 2147549183)
	end
	if self.XerathMenu.Drawings.DrawW:get() then
		Manager:DrawCircle(myHero.position, self.WData.range, 2147532799)
	end
	if self.XerathMenu.Drawings.DrawE:get() then
		Manager:DrawCircle(myHero.position, self.EData.range, 2149486847)
	end
	if self.XerathMenu.Drawings.DrawR:get() then
		self.RRange = self.RRanges[Manager:GetCastLevel(myHero, x)]
		if self.RRange then
			Manager:DrawCircle(myHero.position, self.RRange, 4278190335)
			local bG = TacticalMap:WorldToMinimap(myHero.position)
			local bH = self.RRange / 14641 * TacticalMap.width
			DrawHandler:Circle(bG, bH, 4278190335)
		end
	end
end
function Xerath:Tick()
	if myHero.isDead or _G.JustEvade then
		return
	end
	self:Auto2()
	self:AutoR()
	if Manager:GetOrbwalkerMode() == "Waveclear" then
		self:LaneClear()
		return
	end
	self.Target1 = J.TargetSelector:GetTarget(self.QData.range, nil)
	self.Target2 = J.TargetSelector:GetTarget(self.EData.range, nil)
	if self.Target1 == nil then
		return
	end
	if Manager:GetOrbwalkerMode() == "Combo" then
		self:Combo(self.Target1, self.Target2)
	elseif Manager:GetOrbwalkerMode() == "Harass" then
		self:Harass(self.Target1, self.Target2)
	else
		self:Auto(self.Target2)
	end
end
function Xerath:BuffGain(aA, bI)
	if aA == myHero then
		if bI.name == "XerathArcanopulseChargeUp" then
			self.QBuff, self.Charged, self.ChargedTime = true, true, GetTickCount()
			if _G.LegitOrbwalker then
				_G.LegitOrbwalker:BlockAttack(true)
			end
		elseif bI.name == "XerathLocusOfPower2" then
			self.QBuff, self.Charged, self.Channeled = false, false, true
		end
	end
end
function Xerath:BuffLost(aA, bI)
	if aA == myHero then
		if bI.name == "XerathArcanopulseChargeUp" then
			self.QBuff, self.Charged, self.ChargedRange = false, false, 750
			if _G.LegitOrbwalker then
				_G.LegitOrbwalker:BlockAttack(false)
			end
		elseif bI.name == "XerathLocusOfPower2" then
			self.Channeled = false
		end
	end
end
function Xerath:Auto(a7)
	if myHero.manaPercent >= self.XerathMenu.Auto.Mana:get() and self.XerathMenu.Auto.UseW:get() and Manager:IsReady(v) and not self.Charged and not self.Channeled and Manager:ValidTarget(a7, self.WData.range) then
		self:UseW(a7)
	end
end
function Xerath:Auto2()
	if not self.Charged and self.ChargedRange ~= self.MinRange then
		self.ChargedRange = self.MinRange
	end
	if self.Charged and self.ChargedTime + 1500 + 3000 + 1000 < GetTickCount() then
		self.Charged, self.ChargedRange = false, self.MinRange
	end
	if self.Charged then
		self.ChargedRange = l(o(self.MinRange + (1500 - self.MinRange) * (GetTickCount() - self.ChargedTime) / 1500 - 100, 1500))
	end
	for Z, a4 in pairs(F) do
		if self.XerathMenu.AntiGapcloser.UseE:get() and Manager:ValidTarget(a4, self.XerathMenu.AntiGapcloser.Distance:get()) and Manager:IsReady(w) and not self.Charged then
			self:UseE(a4)
		end
		if self.XerathMenu.KillSteal.UseW:get() and Manager:IsReady(v) and not self.Charged and Manager:ValidTarget(a4, self.WData.range) then
			local bJ = Manager:GetDamage(a4, 1)
			if bJ > a4.health then
				self:UseW(a4)
			end
		end
	end
end
function Xerath:AutoR()
	if self.Channeled and self.XerathMenu.Auto.UseR:get() then
		if self.XerathMenu.Auto.CastR:get() ~= 2 or not 11 then
			local bf
		end
		local bK = J.TargetSelector:GetTarget(self.RRange, bf)
		if bK == nil then
			return
		end
		local bL = self.XerathMenu.Auto.RandomR:get() and q(250, 1000) / 1000 or self.XerathMenu.Auto.DelayR:get()
		if RiotClock.time > self.RTimer + bL then
			self:UseR(bK)
			self.RTimer = RiotClock.time
		end
	end
end
function Xerath:Combo(bt, bu)
	if _G.LegitOrbwalker and _G.LegitOrbwalker:IsAttacking() then
		return
	end
	if bt and Manager:IsReady(u) and self.XerathMenu.Combo.UseQ:get() and Manager:ValidTarget(bt, self.QData.range) then
		self:UseQ(bt)
	elseif bu and not self.Charged then
		if Manager:IsReady(v) and self.XerathMenu.Combo.UseW:get() and Manager:ValidTarget(bu, self.WData.range) then
			self:UseW(bu)
		end
		if Manager:IsReady(w) and self.XerathMenu.Combo.UseE:get() and Manager:ValidTarget(bu, self.EData.range) then
			self:UseE(bu)
		end
	end
end
function Xerath:Harass(bt, bu)
	if myHero.manaPercent < self.XerathMenu.Harass.Mana:get() then
		return
	end
	if bt and Manager:IsReady(u) and self.XerathMenu.Harass.UseQ:get() and Manager:ValidTarget(bt, self.QData.range) then
		self:UseQ(bt)
	elseif bu and not self.Charged then
		if Manager:IsReady(v) and self.XerathMenu.Harass.UseW:get() and Manager:ValidTarget(bu, self.WData.range) then
			self:UseW(bu)
		end
		if Manager:IsReady(w) and self.XerathMenu.Harass.UseE:get() and Manager:ValidTarget(bu, self.EData.range) then
			self:UseE(bu)
		end
	end
end
function Xerath:LaneClear()
	if _G.LegitOrbwalker and _G.LegitOrbwalker:IsAttacking() or myHero.manaPercent < self.XerathMenu.LaneClear.Mana:get() then
		return
	end
	if self.XerathMenu.LaneClear.UseQ:get() and Manager:IsReady(u) then
		do
			local ao = Manager:GetMinionsAround(myHero.position, self.QData.range)
			local bw, bx = Geometry:GetBestLinearAOEPos(ao, self.QData.range, self.QData.radius - 5)
			if bw and bx >= self.XerathMenu.LaneClear.MHQ:get() and not self.Charged then
				myHero.spellbook:CastSpell(u, Geometry:GetMousePos())
			end
			if self.Charged then
				bw = Geometry:GetBestLinearAOEPos(ao, self.QData.range, self.QData.radius - 5)
				if bw and Geometry:GetDistance(myHero.position, bw) > self.ChargedRange then
					DelayAction(function()
						bw = Geometry:GetBestLinearAOEPos(ao, self.QData.range, self.QData.radius - 5)
						myHero.spellbook:UpdateChargeableSpell(u, Geometry:To3D(bw), 1)
					end, 1)
				else
					myHero.spellbook:UpdateChargeableSpell(u, Geometry:To3D(Geometry:GetMousePos()), 1)
				end
			end
		end
	else
	end
end
function Xerath:UseQ(aA)
	if self.Charged then
		if Geometry:GetDistance(aA.position, myHero.position) > self.ChargedRange then
			DelayAction(function()
				local CP, bC, bD, bE, bF
				if self.XerathMenu.HitChance.Pred:get() == 2 then
					CP, bC, bD, bE = PremiumPrediction:GetPrediction(myHero, aA, self.QData.speed, self.QData.range, self.QData.delay, self.QData.radius, self.QData.angle, self.QData.collision)
				else
					bF = _G.Prediction.GetPrediction(aA, self.QData, myHero)
					CP, bD = bF.castPosition, bF.hitChance
				end
				if CP and bD >= self.XerathMenu.HitChance.HCQ:get() / 100 then
					myHero.spellbook:UpdateChargeableSpell(u, Geometry:To3D(CP), 1)
				end
			end, 1)
		else
			DelayAction(function()
				local CP, bC, bD, bE, bF
				if self.XerathMenu.HitChance.Pred:get() == 2 then
					CP, bC, bD, bE = PremiumPrediction:GetPrediction(myHero, aA, self.QData.speed, self.QData.range, self.QData.delay, self.QData.radius, self.QData.angle, self.QData.collision)
				else
					bF = _G.Prediction.GetPrediction(aA, self.QData, myHero)
					CP, bD = bF.castPosition, bF.hitChance
				end
				if CP and bD >= self.XerathMenu.HitChance.HCQ:get() / 100 then
					myHero.spellbook:UpdateChargeableSpell(u, Geometry:To3D(CP), 1)
				end
			end, 0.4)
		end
		if self.ChargedRange >= 1500 then
			if CP then
				Manager:UpdateChargeableSpell(u, Geometry:To3D(CP), 1)
			else
				Manager:UpdateChargeableSpell(u, Geometry:To3D(Geometry:GetMousePos()), 1)
			end
		end
	else
		myHero.spellbook:CastSpell(u, Geometry:GetMousePos())
	end
end
function Xerath:UseW(aA)
	local CP, bC, bD, bE, bF
	if self.XerathMenu.HitChance.Pred:get() == 2 then
		CP, bC, bD, bE = PremiumPrediction:GetPrediction(myHero, aA, self.WData.speed, self.WData.range, self.WData.delay, self.WData.radius, self.WData.angle, self.WData.collision)
	else
		bF = _G.Prediction.GetPrediction(aA, self.WData, myHero)
		CP, bD = bF.castPosition, bF.hitChance
	end
	if CP and bD >= self.XerathMenu.HitChance.HCW:get() / 100 then
		myHero.spellbook:CastSpell(v, Geometry:To3D(CP))
	end
end
function Xerath:UseE(aA)
	local CP, bC, bD, bE, bF
	if self.XerathMenu.HitChance.Pred:get() == 2 then
		CP, bC, bD, bE = PremiumPrediction:GetPrediction(myHero, aA, self.EData.speed, self.EData.range, self.EData.delay, self.EData.radius, self.EData.angle, self.EData.collision)
	else
		bF = _G.Prediction.GetPrediction(aA, self.EData, myHero)
		CP, bD = bF.castPosition, not bF:minionCollision(1) and bF.hitChance or 0
	end
	if CP and bD >= self.XerathMenu.HitChance.HCE:get() / 100 then
		myHero.spellbook:CastSpell(w, Geometry:To3D(CP))
	end
end
function Xerath:UseR(aA)
	local CP, bC, bD, bE, bF
	self.RData.range = self.RRange
	if self.XerathMenu.HitChance.Pred:get() == 2 then
		CP, bC, bD, bE = PremiumPrediction:GetPrediction(myHero, aA, self.RData.speed, self.RRange, self.RData.delay, self.RData.radius, self.RData.angle, self.RData.collision)
	else
		bF = _G.Prediction.GetPrediction(aA, self.RData, myHero)
		CP, bD = bF.castPosition, bF.hitChance
	end
	if CP and bD >= self.XerathMenu.HitChance.HCR:get() / 100 then
		myHero.spellbook:CastSpell(x, Geometry:To3D(CP))
	end
end
Yasuo = aa()
function Yasuo:__init()
	self.Target1, self.Target2, self.Dash, self.DetectedSpells, self.KnockedUp = nil, nil, 0, {}, {}
	self.Range, self.Timer, self.LastDash = myHero.characterIntermediate.attackRange + myHero.boundingRadius * 2, 0, nil
	self.QData, self.EData, self.RData = a8[myHero.charName][0], a8[myHero.charName][2], a8[myHero.charName][3]
	self.SpellSlot = {
		[u] = "Q",
		[v] = "W",
		[w] = "E",
		[x] = "R"
	}
	self.YasuoMenu = Menu("Yasuo", "[Premium Series] Yasuo")
	self.YasuoMenu:sub("Auto", "Auto")
	self.YasuoMenu.Auto:checkbox("UseQ", "Use Q [Steel Tempest]", true)
	self.YasuoMenu.Auto:checkbox("UseQ3", "Use Q3 [Gathering Storm]", true)
	self.YasuoMenu:sub("Combo", "Combo")
	self.YasuoMenu.Combo:checkbox("UseQ", "Use Q [Steel Tempest]", true)
	self.YasuoMenu.Combo:checkbox("UseQ3", "Use Q3 [Gathering Storm]", true)
	self.YasuoMenu.Combo:checkbox("UseE", "Use E [Sweeping Blade]", true)
	self.YasuoMenu.Combo:checkbox("UseR", "Use R [Last Breath]", true)
	self.YasuoMenu.Combo:slider("HP", "HP Manager: R", 0, 100, 35, 5)
	self.YasuoMenu:sub("Harass", "Harass")
	self.YasuoMenu.Harass:checkbox("UseQ", "Use Q [Steel Tempest]", true)
	self.YasuoMenu.Harass:checkbox("UseQ3", "Use Q3 [Gathering Storm]", true)
	self.YasuoMenu.Harass:checkbox("UseE", "Use E [Sweeping Blade]", true)
	self.YasuoMenu:sub("WindWall", "WindWall")
	self.YasuoMenu.WindWall:checkbox("UseW", "Use W [Wind Wall]", true)
	self.YasuoMenu.WindWall:sub("Spells", "Block List")
	self.YasuoMenu:sub("KillSteal", "KillSteal")
	self.YasuoMenu.KillSteal:checkbox("UseE", "Use E [Sweeping Blade]", true)
	self.YasuoMenu.KillSteal:checkbox("UseIgn", "Use Ignite", true)
	self.YasuoMenu:sub("LaneClear", "LaneClear")
	self.YasuoMenu.LaneClear:checkbox("UseQ", "Use Q [Arcanopulse]", true)
	self.YasuoMenu.LaneClear:checkbox("UseE", "Use E [Sweeping Blade]", true)
	self.YasuoMenu:sub("Flee", "Flee")
	self.YasuoMenu.Flee:checkbox("UseE", "Use E [Sweeping Blade]", true)
	self.YasuoMenu.Flee:key("Key", "Flee Key", string.byte("Z"))
	self.YasuoMenu:sub("HitChance", "HitChance")
	self.YasuoMenu.HitChance:list("Pred", "Prediction", 2, {
		"Dream Prediction",
		"Premium Prediction"
	})
	self.YasuoMenu.HitChance:slider("HCQ", "HitChance: Q", 0, 100, 30, 1)
	self.YasuoMenu:sub("Drawings", "Drawings")
	self.YasuoMenu.Drawings:checkbox("DrawQ", "Draw Q Range", true)
	self.YasuoMenu.Drawings:checkbox("DrawE", "Draw E Range", true)
	self.YasuoMenu.Drawings:checkbox("DrawR", "Draw R Range", true)
	DelayAction(function()
		self.YasuoMenu.WindWall.Spells:checkbox("Target", "Targeted Spells", true)
		for Z, bc in pairs(a9) do
			for aC, bM in ipairs(F) do
				if bc.charName == bM.charName then
					self.YasuoMenu.WindWall.Spells:checkbox(Z, "" .. bc.charName .. " " .. self.SpellSlot[bc.slot] .. " - " .. bc.displayName, true)
				end
			end
		end
	end, 0.01)
	function K.Champion()
		self:Draw()
	end
	function L.Champion()
		self:Tick()
	end
	function M.Champion(...)
		self:BuffGain(...)
	end
	function N.Champion(...)
		self:BuffLost(...)
	end
	function O.Champion(...)
		self:CreateObject(...)
	end
	function P.Champion(...)
		self:NewPath(...)
	end
end
function Yasuo:Draw()
	if self.YasuoMenu.Drawings.DrawQ:get() then
		Manager:DrawCircle(myHero.position, self.QData.range2, 2147549183)
		Manager:DrawCircle(myHero.position, self.QData.range, 2147549183)
	end
	if self.YasuoMenu.Drawings.DrawE:get() then
		Manager:DrawCircle(myHero.position, self.EData.range, 2149486847)
	end
	if self.YasuoMenu.Drawings.DrawR:get() then
		Manager:DrawCircle(myHero.position, self.RData.range, 2147483903)
	end
end
function Yasuo:Tick()
	if myHero.isDead or _G.JustEvade then
		return
	end
	self:Auto2()
	if Manager:GetOrbwalkerMode() == "Waveclear" then
		self:LaneClear()
		return
	end
	self.Target1 = J.TargetSelector:GetTarget(self.QData.range2, nil)
	self.Target2 = J.TargetSelector:GetTarget(self.QData.range, nil)
	if self.Target1 == nil then
		return
	end
	if Manager:GetOrbwalkerMode() == "Combo" then
		self:Combo(self.Target1, self.Target2)
	elseif Manager:GetOrbwalkerMode() == "Harass" then
		self:Harass(self.Target1, self.Target2)
	end
	self:Auto(self.Target1, self.Target2)
end
function Yasuo:Auto(bt, bu)
	if Manager:IsReady(u) then
		if self:HasQ3() and bt and self.YasuoMenu.Auto.UseQ3:get() and Manager:ValidTarget(bt, self.QData.range2) then
			if self.LastDash and Geometry:GetDistance(self.LastDash, bt.position) < 300 and not self:CanCastQ() or self:CanCastQ() then
				self:UseQ3(bt)
			end
		elseif not self:HasQ3() and bu and self.YasuoMenu.Auto.UseQ:get() and Manager:ValidTarget(bu, self.QData.range) then
			self:UseQ(bu)
		end
	end
end
function Yasuo:Auto2()
	for Z, bN in pairs(self.DetectedSpells) do
		if bN.startTime + bN.range / bN.speed > RiotClock.time then
			local an = Geometry:VectorPointProjectionOnLineSegment(bN.startPos, bN.endPos, myHero.position)
			if bN.type == "circular" and Geometry:GetDistanceSqr(myHero.position, bN.endPos) < (bN.radius + 65) ^ 2 or Geometry:GetDistanceSqr(myHero.position, an) < (bN.radius + 80) ^ 2 then
				local bO = bN.speed ~= m and Geometry:CalculateCollisionTime(bN.startPos, bN.endPos, myHero.position, bN.startTime, bN.speed) or 0.29
				if bO < 0.3 and Manager:IsReady(v) then
					self:UseW(bN.startPos)
				end
			end
		else
			B(self.DetectedSpells, Z)
		end
	end
	if self.YasuoMenu.Flee.Key:get() and RiotClock.time > self.Timer + 0.25 then
		myHero:IssueOrder(GameObjectOrder.MoveTo, Geometry:GetMousePos())
		local ao = Manager:GetMinionsAround(myHero.position, self.EData.range)
		local bP = {}
		for Z, ap in ipairs(ao) do
			if not self:IsMarked(ap) then
				A(bP, ap)
			end
		end
		if #bP > 0 then
			C(bP, function(ar, as)
				return Geometry:GetDistanceSqr(ar.position, Geometry:GetMousePos()) < Geometry:GetDistanceSqr(as.position, Geometry:GetMousePos())
			end)
			self:UseE(bP[1])
		end
		self.Timer = RiotClock.time
	end
	for Z, a4 in pairs(F) do
		if self.YasuoMenu.KillSteal.UseE:get() and Manager:IsReady(w) and Manager:ValidTarget(a4, self.EData.range) then
			local bQ = Manager:GetDamage(a4, 2)
			if bQ > a4.health then
				self:UseE(a4)
			end
		end
		if self.YasuoMenu.KillSteal.UseIgn:get() and G and Manager:IsReady(G) and Manager:ValidTarget(a4, 600) then
			local bs = 50 + 20 * myHero.experience.level - 3 * (a4.characterIntermediate.hpRegenRate + a4.characterIntermediate.baseHPRegenRate)
			if bs > a4.health then
				myHero.spellbook:CastSpell(G, a4.networkId)
			end
		end
	end
end
function Yasuo:Combo(bt, bu)
	if _G.LegitOrbwalker and _G.LegitOrbwalker:IsAttacking() then
		return
	end
	if bt or bu then
		if bu and not self:HasQ3() and Manager:IsReady(u) and self.YasuoMenu.Combo.UseQ:get() and Manager:ValidTarget(bu, self.QData.range) then
			self:UseQ(bu)
		elseif Manager:IsReady(w) and self.YasuoMenu.Combo.UseE:get() then
			if bu and Manager:ValidTarget(bu, self.EData.range) and not self:IsMarked(bu) and Geometry:GetDistance(myHero.position, bu.position) > self.Range then
				self:UseE(bu)
			elseif bt and Manager:ValidTarget(bt, self.QData.range2) then
				local ao, bg = Manager:GetMinionsAround(myHero.position, self.EData.range)
				if bg > 0 then
					for Z, ap in ipairs(ao) do
						if not self:IsMarked(ap) then
							local bR = I(myHero.position):extended(ap.position, 600)
							if Geometry:GetDistance(bR, bt.position) < Geometry:GetDistance(myHero.position, bt.position) then
								self.LastDash = bR
								self:UseE(ap)
							end
						end
					end
				end
			end
		end
		if bt then
			if self:HasQ3() and Manager:IsReady(u) and self.YasuoMenu.Combo.UseQ3:get() and Manager:ValidTarget(bt, self.QData.range2) then
				if self.LastDash and Geometry:GetDistance(self.LastDash, bt.position) < 300 and not self:CanCastQ() or self:CanCastQ() then
					self:UseQ3(bt)
				end
			elseif Manager:IsReady(x) and self.YasuoMenu.Combo.UseR:get() and self.YasuoMenu.Combo.HP:get() > bt.healthPercent and self.KnockedUp[bt] then
				self:UseR(bt)
			end
		end
	end
end
function Yasuo:Harass(bt, bu)
	if _G.LegitOrbwalker and _G.LegitOrbwalker:IsAttacking() then
		return
	end
	if bt or bu then
		if bu and not self:HasQ3() and Manager:IsReady(u) and self.YasuoMenu.Harass.UseQ:get() and Manager:ValidTarget(bu, self.QData.range) then
			self:UseQ(bu)
		elseif Manager:IsReady(w) and self.YasuoMenu.Harass.UseE:get() then
			if bu and Manager:ValidTarget(bu, self.EData.range) and not self:IsMarked(bu) and Geometry:GetDistance(myHero.position, bu.position) > self.Range and not self:IsDangerousPos(myHero.position) then
				self:UseE(bu)
			elseif bt and Manager:ValidTarget(bt, self.QData.range2) then
				local ao, bg = Manager:GetMinionsAround(myHero.position, self.EData.range)
				if bg > 0 then
					for Z, ap in ipairs(ao) do
						if not self:IsMarked(ap) then
							local bR = I(myHero.position):extended(ap.position, 600)
							if Geometry:GetDistance(bR, bt.position) < Geometry:GetDistance(myHero.position, bt.position) and not self:IsDangerousPos(bR) then
								self.LastDash = bR
								self:UseE(ap)
							end
						end
					end
				end
			end
		end
		if bt and self:HasQ3() and Manager:IsReady(u) and self.YasuoMenu.Harass.UseQ3:get() and Manager:ValidTarget(bt, self.QData.range2) and (self.LastDash and Geometry:GetDistance(self.LastDash, bt.position) < 300 and not self:CanCastQ() or self:CanCastQ()) then
			self:UseQ3(bt)
		end
	end
end
function Yasuo:LaneClear()
	if _G.LegitOrbwalker and _G.LegitOrbwalker:IsAttacking() then
		return
	end
	if self.YasuoMenu.LaneClear.UseQ:get() and Manager:IsReady(u) then
		local bS = self:HasQ3() and self.QData.range2 or self.QData.range
		local bT = self:HasQ3() and self.QData.radius2 or self.QData.radius
		local ao = Manager:GetMinionsAround(myHero.position, bS)
		local bw, bx = Geometry:GetBestLinearAOEPos(ao, bS, bT)
		if bw then
			myHero.spellbook:CastSpell(u, Geometry:To3D(bw))
		end
	elseif self.YasuoMenu.LaneClear.UseE:get() and Manager:IsReady(w) then
		local ao, bg = Manager:GetMinionsAround(myHero.position, self.EData.range)
		if bg > 0 then
			for Z, ap in ipairs(ao) do
				local bQ = Manager:GetDamage(ap, 2)
				if bQ > ap.health then
					self:UseE(ap)
				end
			end
		end
	end
end
function Yasuo:CanCastQ()
	return RiotClock.time - self.Dash > 600 / (750 + 0.6 * myHero.characterIntermediate.movementSpeed)
end
function Yasuo:GetWindUp()
	return myHero.attackDelay
end
function Yasuo:HasQ3()
	return myHero.spellbook:Spell(u).castRange > 3250
end
function Yasuo:IsDangerousPos(aj)
	for Z, bU in pairs(ObjectManager:GetEnemyTurrets()) do
		if bU.health > 0 and Geometry:GetDistance(myHero.position, bU.position) < 900 then
			return true
		end
	end
	return false
end
function Yasuo:IsMarked(aA)
	return aA.buffManager:HasBuff("YasuoE") ~= nil
end
function Yasuo:BuffGain(aA, bI)
	if aA and aA.team ~= myHero.team and bI and bI.type == 29 and self.KnockedUp[aA] == nil then
		A(self.KnockedUp, aA)
	end
end
function Yasuo:BuffLost(aA, bI)
	if aA and aA.team ~= myHero.team and bI and bI.type == 29 and self.KnockedUp[aA] ~= nil then
		B(self.KnockedUp, aA)
	end
end
function Yasuo:CreateObject(bV, bW)
	if not self.YasuoMenu.WindWall.UseW:get() then
		return
	end
	local bc = bV.asMissile
	local aA, bX = bc.spellCaster, bV.name
	if aA and bc and aA.type == GameObjectType.AIHeroClient and myHero.team ~= aA.team then
		local bY = I(aA.position)
		if self.YasuoMenu.WindWall.Spells.Target:get() and bc.target == myHero and not bX:lower():find("attack") then
			if Manager:IsReady(v) then self:UseW(bY) end
		elseif self.YasuoMenu.WindWall.Spells[bX] and self.YasuoMenu.WindWall.Spells[bX]:get() then
			local bN, bZ, b_ = a9[bX], I(bc.launchPos) or bY, I(bc.destPos)
			local c0, c1 = Geometry:CalculateEndPos(bZ, b_, bY, bN.range, bN.radius, bN.collision, bN.type)
			A(self.DetectedSpells, {
				startPos = bZ,
				endPos = c0,
				speed = bN.speed,
				range = c1,
				radius = bN.radius,
				startTime = RiotClock.time,
				type = bN.type,
				cc = bN.cc
			})
		end
	end
end
function Yasuo:NewPath(aA, c2, c3, ai)
	if aA == myHero and not c3 then
		self.Dash = RiotClock.time
	end
end
function Yasuo:UseQ(aA)
	local CP, bC, bD, bE, bF
	if self.YasuoMenu.HitChance.Pred:get() == 2 then
		CP, bC, bD, bE = PremiumPrediction:GetPrediction(myHero, aA, self.QData.speed, self.QData.range, self.QData.delay, self.QData.radius, self.QData.angle, self.QData.collision)
	else
		bF = _G.Prediction.GetPrediction(aA, a8[myHero.charName].Q, myHero)
		CP, bD = bF.castPosition, bF.hitChance
	end
	if CP and bD > 0 then
		myHero.spellbook:CastSpell(u, Geometry:To3D(CP))
	end
end
function Yasuo:UseQ3(aA)
	local CP, bC, bD, bE, bF
	if self.YasuoMenu.HitChance.Pred:get() == 2 then
		CP, bC, bD, bE = PremiumPrediction:GetPrediction(myHero, aA, self.QData.speed2, self.QData.range2, self.QData.delay, self.QData.radius2, self.QData.angle, self.QData.collision)
	else
		bF = _G.Prediction.GetPrediction(aA, a8[myHero.charName].Q3, myHero)
		CP, bD = bF.castPosition, bF.hitChance
	end
	if CP and bD > 0 then
		myHero.spellbook:CastSpell(u, Geometry:To3D(CP))
	end
end
function Yasuo:UseW(aj)
	local c4 = I(myHero.position):extended(aj, 100)
	myHero.spellbook:CastSpell(v, Geometry:To3D(c4))
end
function Yasuo:UseE(aA)
	myHero.spellbook:CastSpell(w, aA.networkId)
end
function Yasuo:UseR(aA)
	myHero.spellbook:CastSpell(x, aA.networkId)
end
AddEvent(Events.OnDraw, function()
	if K.Champion then
		K.Champion()
	end
	if K.TargetSelector then
		K.TargetSelector()
	end
end)
AddEvent(Events.OnTick, function()
	if L.Champion then
		L.Champion()
	end
	if L.Orbwalker then
		L.Orbwalker()
	end
end)
AddEvent(Events.OnBuffGain, function(...)
	if M.Champion then
		M.Champion(...)
	end
end)
AddEvent(Events.OnBuffLost, function(...)
	if N.Champion then
		N.Champion(...)
	end
end)
AddEvent(Events.OnCreateObject, function(...)
	if O.Champion then
		O.Champion(...)
	end
end)
AddEvent(Events.OnNewPath, function(...)
	if P.Champion then
		P.Champion(...)
	end
end)
AddEvent(Events.OnProcessSpell, function(...)
	if Q.Champion then
		Q.Champion(...)
	end
end)
AddEvent(Events.OnWndProc, function(...)
	if R.TargetSelector then
		R.TargetSelector(...)
	end
end)
