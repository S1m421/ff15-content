local Zilean = {}
local version = 1
require "FF15Menu"
require "utils"
local DreamTS = require("DreamTS")
local dmgLib = require("FF15DamageLib")

function OnLoad()
    if not _G.Prediction then
        LoadPaidScript(PaidScript.DREAM_PRED)
    end
end

function Zilean:__init()
    self.q = {
        type = "circular",
        speed = math.huge,
        range = 890,
        delay = 0.8,
        radius = 130
    }
    self:Menu()
    self.QlvlDmg = {[1] = 75,[2] = 115,[3] = 165,[4] = 230,[5] = 300}
    self.QWQCast = false
    self.speed = nil
    self.slow = nil
    self.TS =
        DreamTS(
        self.menu.dreamTs,
        {
            ValidTarget = function(unit)
                return _G.Prediction.IsValidTarget(unit, 890)
            end,
            Damage = function(unit)
                return dmgLib:CalculateMagicDamage(myHero, unit, 100)
            end
        }
    )
    AddEvent(Events.OnTick, function() self:OnTick() end)
    AddEvent(Events.OnDraw, function() self:OnDraw() end)
    AddEvent(Events.OnBuffGain, function(obj, buff) self:OnBuffGain(obj, buff) end)
    AddEvent(Events.OnBuffLost, function(obj, buff) self:OnBuffLost(obj, buff) end)
    PrintChat("<font color=\"#E41B17\">[<b>¤ Cyrex ¤</b>]:</font>" .. " <font color=\"#" .. "FFFFFF" .. "\">" .. "Zilean Loaded" .. "</font>")
    self.font = DrawHandler:CreateFont("Calibri", 12)
end

function Zilean:Menu()
    self.menu = Menu("cxzil", "Cyrex Zilean")
    self.menu:sub("dreamTs", "Target Selector")

    self.menu:sub("Key", "Key Settings")
        self.menu.Key:checkbox("e", "Start Combo With E", true, string.byte("K"))
        self.menu.Key:key("ultself", "Auto-Ult Self", string.byte("T"))
        self.menu.Key:key("run", "Marathon Mode", string.byte("S"))

    self.menu:sub("combo", "Combo Settings")
        self.menu.combo:label("xd", "Q Settings")
        self.menu.combo:checkbox("q", "Use Q", true)
        self.menu.combo:label("xd1", "W Settings")
        self.menu.combo:checkbox("w", "Force QWQ", true)
        self.menu.combo:label("xd2", "E Settings")
        self.menu.combo:checkbox("e", "Smart E", true)
        self.menu.combo:checkbox("ed", "Mid Usage", true)
        self.menu.combo:label("xd3", "R Settings")
        self.menu.combo:sub("rs", "R Settings")
            self.menu.combo.rs:label("xd4", "My Hero Settings")
            self.menu.combo.rs:checkbox("r", "Use Smart R", true)
            self.menu.combo.rs:slider("rx", "R on X Enemys in Range", 0, 5, 1, 1)
            self.menu.combo.rs:slider("rhp", "What HP% to Ult", 0, 100, 10, 5)

            self.menu.combo.rs:label("xd5", "Ally Settings")
            self.menu.combo.rs:checkbox("use", "Use R for Ally", true)
            self.menu.combo.rs:sub("x", "Ally Selection")
            for i, Hero in pairs(ObjectManager:GetAllyHeroes()) do
                local ally = Hero
                if ally ~= myHero then
                    self.menu.combo.rs.x:checkbox(ally.charName, "Revive: "..ally.charName, false)
                end 
            end
            self.menu.combo.rs:slider("ahp", "HP% To Revive Ally", 0, 100, 10, 5)

    self.menu:sub("harass", "Harass Settings")
        self.menu.harass:checkbox("q", "Use Q", true)
        self.menu.harass:checkbox("w", "Use QWQ", true)
        self.menu.harass:checkbox("e", "Use E", true)
        self.menu.harass:slider("mana", "Min. Mana Percent: ", 0, 100, 10, 5)


    self.menu:sub("auto", "Automatic Settings")
        self.menu.auto:checkbox("ig", "Auto Ignite", true)
        self.menu.auto:label("dx", "Killsteal Settings")
        self.menu.auto:checkbox("uks", "Use Killsteal", true)
        self.menu.auto:checkbox("ksq", "Use Q in Killsteal", true) --]]

    self.menu:sub("draws", "Draw")
        self.menu.draws:checkbox("q", "Q", true)
        self.menu.draws:checkbox("e", "E", true)
end

function Zilean:OnDraw()
    if self.menu.draws.q:get() and myHero.visibleOnScreen then
        DrawHandler:Circle3D(myHero.position, 900, self:Hex(255,255,255,255))
    end
    if self.menu.draws.e:get() and myHero.visibleOnScreen then
        DrawHandler:Circle3D(myHero.position, 610, self:Hex(255,255,255,255))
    end    
end

local delayedActions, delayedActionsExecuter = {}, nil
function Zilean:DelayAction(func, delay, args) --delay in seconds
  if not delayedActionsExecuter then
    function delayedActionsExecuter()
      for t, funcs in pairs(delayedActions) do
        if t <= os.clock() then
          for i = 1, #funcs do
            local f = funcs[i]
            if f and f.func then
              f.func(unpack(f.args or {}))
            end
          end
          delayedActions[t] = nil
        end
      end
    end
    AddEvent(Events.OnTick, delayedActionsExecuter)
  end
  local t = os.clock() + (delay or 0)
  if delayedActions[t] then
    delayedActions[t][#delayedActions[t] + 1] = {func = func, args = args}
  else
    delayedActions[t] = {{func = func, args = args}}
  end
end

function Zilean:GetPercentHealth(obj)
  local obj = obj or myHero
  return (obj.health / obj.maxHealth) * 100
end

function Zilean:MoveToMouse()
    local pos = pwHud.hudManager.virtualCursorPos
    myHero:IssueOrder(GameObjectOrder.MoveTo, pos)
end

function Zilean:GetTotalAP(obj)
  local obj = obj or myHero
  return obj.characterIntermediate.flatMagicDamageMod * obj.characterIntermediate.percentMagicDamageMod
end

-- Returns magic damage multiplier on @target from @damageSource or player
function Zilean:MagicReduction(target, damageSource)
  local damageSource = damageSource or myHero
  local magicResist = (target.characterIntermediate.spellBlock * damageSource.characterIntermediate.percentMagicPenetration) - damageSource.characterIntermediate.flatMagicPenetration
  return magicResist >= 0 and (100 / (100 + magicResist)) or (2 - (100 / (100 - magicResist)))
end

-- Returns damage reduction multiplier on @target from @damageSource or player
function Zilean:DamageReduction(damageType, target, damageSource)
  local damageSource = damageSource or myHero
  local reduction = 1
  if damageType == "AD" then
  end
  if damageType == "AP" then
  end
  return reduction
end

function Zilean:CalculateMagicDamage(target, damage, damageSource)
  local damageSource = damageSource or myHero
  if target then
    return (damage * self:MagicReduction(target, damageSource)) * self:DamageReduction("AP", target, damageSource)
  end
  return 0
end

function Zilean:GetEnemyHeroesInRange(range, pos)
  local pos = pos or myHero.position
  local h = {}
  local enemies = ObjectManager:GetEnemyHeroes()
  for i = 1, #enemies do
    local hero = enemies[i]
    if hero and hero.team ~= myHero.team and not hero.isInvulnerable and not hero.isDead and hero.isVisible and hero.isTargetable and GetDistanceSqr(hero) < range * range then
      h[#h + 1] = hero
    end
  end
  return h
end

function Zilean:qDmg(target)
    local qDamage = self.QlvlDmg[myHero.spellbook:Spell(0).level] + (self:GetTotalAP() * .9)
    return self:CalculateMagicDamage(target, qDamage)
end


function Zilean:CastQ(target)
    if myHero.spellbook:CanUseSpell(0) == 0 then
        local pred = _G.Prediction.GetPrediction(target, self.q, myHero)
        if pred and pred.castPosition and (pred.realHitChance == 1 or _G.Prediction.WaypointManager.ShouldCast(target)) and GetDistanceSqr(pred.castPosition) <= self.q.range * self.q.range then
            myHero.spellbook:CastSpell(0, pred.castPosition)
        end
    end
end

function Zilean:QWQ(target)
    if self.QWQCast == false then
        local mcq = 55 + 5 * myHero.spellbook:Spell(0).level
        local mcw = 35
        if myHero.mana >= mcq * 2 + mcw then
            self:CastQ(target)
            if myHero.spellbook:CanUseSpell(0) ~= 0 and GetDistanceSqr(target) < (880 * 880) and myHero.spellbook:CanUseSpell(1) == 0 and myHero.spellbook:Spell(0).cooldownTimeRemaining > 4 then
                myHero.spellbook:CastSpell(1, myHero.networkId)
            end
            if myHero.spellbook:CanUseSpell(1) ~= 0 and myHero.spellbook:CanUseSpell(0) == 0 then
                self:DelayAction(function() self:CastQ(target) end, 0.1)
                self.QWQCast = true
            end
        else
            self:DelayAction(function() self:CastQ(target) end, 0.1)
        end
    elseif myHero.spellbook:CanUseSpell(0) == 0 and myHero.spellbook:CanUseSpell(1) == 0 then
        self.QWQCast = false
    end
end

function Zilean:QWQ2(target)
    if self.QWQCast == false then
        local mcq = 55 + 5 * myHero.spellbook:Spell(0).level
        local mcw = 35
        if myHero.mana >= mcq * 2 + mcw then
            self:DelayAction(function() self:CastQ(target) end, 0.2)
            if myHero.spellbook:CanUseSpell(0) ~= 0 and GetDistanceSqr(target) < (850 * 850) and myHero.spellbook:CanUseSpell(1) == 0 then
                myHero.spellbook:CastSpell(1, myHero.networkId)
            end
        else
            self:DelayAction(function() self:CastQ(target) end, 0.2)
        end
    elseif myHero.spellbook:CanUseSpell(0) == 0 and myHero.spellbook:CanUseSpell(1) == 0 then
        self.QWQCast = false
    end
end

function Zilean:KillSteal()
    for i, enemy in pairs(ObjectManager:GetEnemyHeroes()) do
        if enemy and enemy.team ~= myHero.team and not enemy.isInvulnerable and not enemy.isDead and enemy.isVisible and enemy.isTargetable then
            if self.menu.auto.ksq:get() and myHero.spellbook:CanUseSpell(0) == 0 and enemy.health < self:qDmg(enemy) then
                self:CastQ(enemy)
            end
        end
    end
end

function Zilean:autoUlt()
    if myHero.spellbook:CanUseSpell(3) ~= 0 then return end
    if self.menu.combo.rs.r:get() and #self:GetEnemyHeroesInRange(800) >= self.menu.combo.rs.rx:get() and self:GetPercentHealth(player) <=  self.menu.combo.rs.rhp:get() then
        myHero.spellbook:CastSpell(3, myHero.networkId)
    end
end

function Zilean:autoAllyUlt()
    if self.menu.combo.rs.use:get() then
        for i, Hero in pairs(ObjectManager:GetAllyHeroes()) do
            local ally = Hero
            if myHero.spellbook:CanUseSpell(3) == 0 and ally and not ally.isDead and not myHero.isDead and GetDistance(myHero, ally) <= 900 and self:GetPercentHealth(ally) <= self.menu.combo.rs.ahp:get() and #self:GetEnemyHeroesInRange(800, ally.position) >= 1 then
                if self.menu.combo.rs.x[ally.charName] and self.menu.combo.rs.x[ally.charName]:get() and self:GetPercentHealth(player) > self:GetPercentHealth(ally) then
                    myHero.spellbook:CastSpell(3, ally.networkId)
                end
            end
        end
    end
end


function Zilean:Run()
    if self.menu.Key.run:get() then
        self:MoveToMouse()
        if myHero.spellbook:CanUseSpell(2) == 0 then
            myHero.spellbook:CastSpell(2, myHero.networkId)
        end
        if myHero.spellbook:CanUseSpell(1) and myHero.spellbook:CanUseSpell(2) ~= 0 and not self.speed and myHero.spellbook:Spell(2).cooldownTimeRemaining > 4 then
            myHero.spellbook:CastSpell(1, myHero.networkId)
        end
    end
end

function Zilean:OnTick()
    local target = self:GetTarget(900)
    if LegitOrbwalker:GetMode() == "Combo" then
        if target then
            if self.menu.combo.e:get() then
                if self.menu.combo.ed:get() then
                    if GetDistance(target) > 900 and GetDistance(target) < 1050 and myHero.spellbook:CanUseSpell(0) == 0 and myHero.spellbook:CanUseSpell(1) == 0 then 
                        myHero.spellbook:CastSpell(2, myHero.networkId)
                    elseif GetDistance(target) <= 700 and not self.slow then            
                        myHero.spellbook:CastSpell(2, target.networkId)
                    end
                    for i, enemy in pairs(ObjectManager:GetEnemyHeroes()) do
                        if enemy and enemy.team ~= myHero.team and not enemy.isInvulnerable and not enemy.isDead and enemy.isVisible and enemy.isTargetable then
                            if enemy ~= target and GetDistance(enemy) < 400 then
                                myHero.spellbook:CastSpell(2, enemy.networkId)
                            end
                        end
                    end
                else
                    if self.menu.combo.e:get() and GetDistance(target) <= 610 and myHero.spellbook:CanUseSpell(2) == 0 and not self.slow then
                        myHero.spellbook:CastSpell(2, target.networkId)
                    end
                end
            end
            if self.menu.Key.e:get() and myHero.spellbook:CanUseSpell(2) == 0 then return end 
            if self.menu.combo.q:get() and not self.menu.combo.w:get() then         
                self:CastQ(target)
            elseif myHero.spellbook:CanUseSpell(0) ~= 0 and not self.menu.combo.w:get() then
                myHero.spellbook:CastSpell(1, myHero.networkId)
            elseif self.menu.combo.q:get() and self.menu.combo.w:get() and myHero.spellbook:Spell(1).level >= 4 then
                self:QWQ(target)
            elseif self.menu.combo.q:get() and self.menu.combo.w:get() and myHero.spellbook:Spell(1).level <= 3 then
                self:QWQ2(target)
            end
        end
    end
    if LegitOrbwalker:GetMode() == "Harass" then
        if target then
            if myHero.mana / myHero.maxMana * 100 >= menu.harass.mana:get() then
                if self.menu.harass.e:get() and myHero.spellbook:CanUseSpell(2) == 0 and GetDistance(target) <= 600 then                
                    myHero.spellbook:CastSpell(2, target.networkId)
                end
                if self.menu.harass.q:get() and not self.menu.harass.w:get() then         
                    self:CastQ(target)
                elseif self.menu.harass.q:get() and self.menu.harass.w:get() then
                    self:QWQ(target)
                elseif not myHero.spellbook:CanUseSpell(0) == 0 and not self.menu.combo.w:get() then
                    myHero.spellbook:CastSpell(1, myHero.networkId)
                end
            end        
        end
    end
    self:Run()
    self:autoAllyUlt()
    self:autoUlt()
    self:KillSteal()
    if self.menu.Key.ultself:get() and myHero.spellbook:CanUseSpell(3) == 0 and self:GetPercentHealth(player) <= 30 then myHero.spellbook:CastSpell(3, myHero.networkId) end
end

function Zilean:OnBuffGain(obj, buff)
    if obj and obj.team ~= myHero.team and obj.type == myHero.type and buff.name == "timewarpslow" then
        self.slow = true
    end
    if obj == myHero and buff.name == "timewarp" then
        self.speed = true
    end
end

function Zilean:OnBuffLost(obj, buff)
    if obj and obj.team ~= myHero.team and obj.type == myHero.type and buff.name == "timewarpslow" then
        self.slow = false
    end
    if obj == myHero and buff.name == "timewarp" then
        self.speed = false
    end
end

function Zilean:Hex(a, r, g, b)
    return string.format("0x%.2X%.2X%.2X%.2X", a, r, g, b)
end

function Zilean:GetTarget(dist, all)
    self.TS.ValidTarget = function(unit)
        return _G.Prediction.IsValidTarget(unit, dist)
    end
    local res = self.TS:update()
    if all then
        return res
    else
        if res and res[1] then
            return res[1]
        end
    end
end

if myHero.charName == "Zilean" then
    Zilean:__init()
end
