--[[

	_________                _____ __________                _________      
	______  /____  ____________  /____  ____/___   ________ _______  /_____ 
	___ _  / _  / / /__  ___/_  __/__  __/   __ | / /_  __ `/_  __  / _  _ \
	/ /_/ /  / /_/ / _(__  ) / /_  _  /___   __ |/ / / /_/ / / /_/ /  /  __/
	\____/   \__,_/  /____/  \__/  /_____/   _____/  \__,_/  \__,_/   \___/ 
                                                          Powered by FF15!

	Credits: Feez, Maxxxel, Noddy

	Changelog:

	v1.2.9 BETA
	+ Small fix

	v1.2.8 BETA
	+ Fixed blocking

	v1.2.7 BETA
	+ Temporary fix for blocking AA's

	+1.2.6 BETA
	+ Improved blocking

	v1.2.5 BETA
	+ Fixed FPS drop

	v1.2.4 BETA
	+ Removed support for LegitOrbwalker
	+ Fixed getting alternative evade position

	v1.2.3 BETA
	+ Small fix

	v1.2.2 BETA
	+ Added FOW detection
	+ Smoothed evading

	v1.2.1 BETA
	+ Small changes

	v1.2 BETA
	+ Added IsAboutToHit func for linear spells
	+ Added movement stop while trying to cross skillshot
	+ Made positions of linear skillshots interactive
	+ Prevented spamming of orbwalker functions
	+ Replaced custom vector lib with GeometryLib
	+ Fixed radiuses of linear skillshots
	+ Fixed minor bugs

	v1.1.9 BETA
	+ Added evade status display

	v1.1.8 BETA
	+ Improved movement blocking

	v1.1.7 BETA
	+ Fixed spell removing

	v1.1.6 BETA
	+ Fixed conic spells
	+ Fixed movement block on dodge

	v1.1.5 BETA
	+ Replaced Vector library with custom one

	v1.1.4 BETA
	+ Added OnIssueOrder event
	+ Improved movement blocking
	+ Bugfixing

	v1.1.3 BETA
	+ Probably fixed bug with loading libraries

	v1.1.2 BETA
	+ Bugfixing

	v1.1.1 BETA
	+ Updated Urgot R delay

	v1.1 BETA
	+ Small changes

	v1.0.9 BETA
	+ Published a source

	v1.0.8 BETA
	+ Added some spells to spell data (untested)

	v1.0.7 BETA
	+ Fixed LegitOrbwalker's support

	v1.0.6 BETA
	+ Added IsWall and IsOnScreen check
	+ Improved collision detecting

	v1.0.5 BETA
	+ Fixed disabled spells
	+ Shortened dodging and drawing of linear spells

	v1.0.4 BETA
	+ Updated Kayle and Morgana spell data

	v1.0.3 BETA
	+ Added support for LegitOrbwalker
	+ Fixed dodging and drawing of rectangular spells

	v1.0.2 BETA
	+ Removed 'IsAboutToHit' func
	+ Bugfixing again

	v1.0.1 BETA
	+ Bugfixing

	v1.0 BETA
	+ Initial release
	
	ToDo:
	+ Update spell names

--]]

local a, b, c = 1.29, "1.2.9", GetInternalWebResult("FF15-JustEvade.version")
local
function d() if tonumber(c) > a then PrintChat("<font color=\"#00ced1\"><b>JustEvade:</font></b> <font color=\"#87cefa\">Found update! Downloading...</font>") DownloadInternalFile("FF15-JustEvade.lua", SCRIPT_PATH..
	"FF15-JustEvade.lua") PrintChat("<font color=\"#00ced1\"><b>JustEvade:</font></b> <font color=\"#87cefa\">Updated! Have fun!</font>") end end
local e = require 'GeometryLib'
require 'FF15Menu'
local f, g, h, i, j, k, l, m, n, o, p, q, r, s, t = math.abs, math.atan, math.atan2, math.acos, math.ceil, math.cos, math.deg, math.floor, math.huge, math.max, math.min, math.pi, math.rad, math.sin, math.sqrt
local u, v, w = table.insert, table.remove, table.sort
local x, y, z = e.Circle, e.Polygon, e.Vector
local A = {
	["AatroxQ"] = {
		charName = "Aatrox",
		displayName = "The Darkin Blade [First]",
		slot = SpellSlot.Q,
		type = "linear",
		speed = n,
		range = 650,
		delay = 0.6,
		radius = 130,
		danger = 3,
		cc = true,
		collision = false,
		windwall = false,
		hitbox = false
	},
	["AatroxQ2"] = {
		charName = "Aatrox",
		displayName = "The Darkin Blade [Second]",
		slot = SpellSlot.Q,
		type = "linear",
		speed = n,
		range = 500,
		delay = 0.6,
		radius = 300,
		danger = 3,
		cc = true,
		collision = false,
		windwall = false,
		hitbox = false
	},
	["AatroxQ3"] = {
		charName = "Aatrox",
		displayName = "The Darkin Blade [Third]",
		slot = SpellSlot.Q,
		type = "circular",
		speed = n,
		range = 200,
		delay = 0.6,
		radius = 300,
		danger = 4,
		cc = true,
		collision = false,
		windwall = false,
		hitbox = false
	},
	["AatroxW"] = {
		charName = "Aatrox",
		displayName = "Infernal Chains",
		slot = SpellSlot.W,
		type = "linear",
		speed = 1800,
		range = 825,
		delay = 0.25,
		radius = 80,
		danger = 2,
		cc = true,
		collision = true,
		windwall = true,
		hitbox = true
	},
	["AhriOrbofDeception"] = {
		charName = "Ahri",
		displayName = "Orb of Deception",
		slot = SpellSlot.Q,
		type = "linear",
		speed = 2500,
		range = 880,
		delay = 0.25,
		radius = 100,
		danger = 2,
		cc = false,
		collision = false,
		windwall = true,
		hitbox = true
	},
	["AhriSeduce"] = {
		charName = "Ahri",
		displayName = "Seduce",
		slot = SpellSlot.E,
		type = "linear",
		speed = 1500,
		range = 975,
		delay = 0.25,
		radius = 60,
		danger = 1,
		cc = true,
		collision = true,
		windwall = true,
		hitbox = true
	},
	["AkaliQ"] = {
		charName = "Akali",
		displayName = "Five Point Strike",
		slot = SpellSlot.Q,
		type = "conic",
		speed = 3200,
		range = 550,
		delay = 0.25,
		radius = 60,
		angle = 45,
		danger = 2,
		cc = false,
		collision = false,
		windwall = true,
		hitbox = false
	},
	["AkaliE"] = {
		charName = "Akali",
		displayName = "Shuriken Flip",
		slot = SpellSlot.E,
		type = "linear",
		speed = 1800,
		range = 825,
		delay = 0.25,
		radius = 70,
		danger = 2,
		cc = false,
		collision = true,
		windwall = true,
		hitbox = true
	},
	["AkaliR"] = {
		charName = "Akali",
		displayName = "Perfect Execution [First]",
		slot = SpellSlot.R,
		type = "linear",
		speed = 1800,
		range = 525,
		delay = 0,
		radius = 65,
		danger = 4,
		cc = true,
		collision = false,
		windwall = false,
		hitbox = false
	},
	["AkaliRb"] = {
		charName = "Akali",
		displayName = "Perfect Execution [Second]",
		slot = SpellSlot.R,
		type = "linear",
		speed = 3600,
		range = 525,
		delay = 0,
		radius = 65,
		danger = 4,
		cc = false,
		collision = false,
		windwall = false,
		hitbox = false
	},
	["Pulverize"] = {
		charName = "Alistar",
		displayName = "Pulverize",
		slot = SpellSlot.Q,
		type = "circular",
		speed = n,
		range = 0,
		delay = 0.25,
		radius = 365,
		danger = 3,
		cc = true,
		collision = false,
		windwall = false,
		hitbox = false
	},
	["BandageToss"] = {
		charName = "Amumu",
		displayName = "Bandage Toss",
		slot = SpellSlot.Q,
		type = "linear",
		speed = 2000,
		range = 1100,
		delay = 0.25,
		radius = 80,
		danger = 3,
		cc = true,
		collision = true,
		windwall = true,
		hitbox = false
	},
	["CurseoftheSadMummy"] = {
		charName = "Amumu",
		displayName = "Curse of the Sad Mummy",
		slot = SpellSlot.R,
		type = "circular",
		speed = n,
		range = 0,
		delay = 0.25,
		radius = 550,
		danger = 5,
		cc = true,
		collision = false,
		windwall = false,
		hitbox = false
	},
	["FlashFrost"] = {
		charName = "Anivia",
		displayName = "Flash Frost",
		slot = SpellSlot.Q,
		type = "linear",
		speed = 850,
		range = 1100,
		delay = 0.25,
		radius = 110,
		danger = 2,
		cc = true,
		collision = false,
		windwall = true,
		hitbox = true
	},
	["AnnieW"] = {
		charName = "Annie",
		displayName = "Incinerate",
		slot = SpellSlot.W,
		type = "conic",
		speed = n,
		range = 600,
		delay = 0.25,
		radius = 0,
		angle = 50,
		danger = 2,
		cc = false,
		collision = false,
		windwall = false,
		hitbox = false
	},
	["AnnieR"] = {
		charName = "Annie",
		displayName = "Summon:Tibbers",
		slot = SpellSlot.R,
		type = "circular",
		speed = n,
		range = 600,
		delay = 0.25,
		radius = 0,
		radius = 290,
		danger = 5,
		cc = false,
		collision = false,
		windwall = false,
		hitbox = false
	},
	["Volley"] = {
		charName = "Ashe",
		displayName = "Volley",
		slot = SpellSlot.W,
		type = "conic",
		speed = 2000,
		range = 1200,
		delay = 0.25,
		radius = 20,
		angle = 40,
		danger = 2,
		cc = true,
		collision = true,
		windwall = true,
		hitbox = false
	},
	["EnchantedCrystalArrow"] = {
		charName = "Ashe",
		displayName = "Enchanted Crystal Arrow",
		slot = SpellSlot.R,
		type = "linear",
		speed = 1600,
		range = 12500,
		delay = 0.25,
		radius = 130,
		danger = 4,
		cc = true,
		collision = false,
		windwall = true,
		hitbox = true
	},
	["AurelionSolQ"] = {
		charName = "AurelionSol",
		displayName = "Starsurge",
		slot = SpellSlot.Q,
		type = "linear",
		speed = 850,
		range = 1075,
		delay = 0,
		radius = 110,
		danger = 2,
		cc = true,
		collision = false,
		windwall = true,
		hitbox = true
	},
	["AurelionSolR"] = {
		charName = "AurelionSol",
		displayName = "Voice of Light",
		slot = SpellSlot.R,
		type = "linear",
		speed = 4500,
		range = 1500,
		delay = 0.35,
		radius = 120,
		danger = 5,
		cc = true,
		collision = false,
		windwall = false,
		hitbox = false
	},
	["AzirR"] = {
		charName = "Azir",
		displayName = "Emperor's Divide",
		slot = SpellSlot.R,
		type = "linear",
		speed = 1400,
		range = 500,
		delay = 0.3,
		radius = 250,
		danger = 5,
		cc = true,
		collision = false,
		windwall = false,
		hitbox = false
	},
	["BardQ"] = {
		charName = "Bard",
		displayName = "Cosmic Binding",
		slot = SpellSlot.Q,
		type = "linear",
		speed = 1500,
		range = 950,
		delay = 0.25,
		radius = 60,
		danger = 2,
		cc = true,
		collision = true,
		windwall = true,
		hitbox = true
	},
	["BardR"] = {
		charName = "Bard",
		displayName = "Tempered Fate",
		slot = SpellSlot.R,
		type = "circular",
		speed = 2100,
		range = 3400,
		delay = 0.5,
		radius = 350,
		danger = 2,
		cc = true,
		collision = false,
		windwall = false,
		hitbox = false
	},
	["RocketGrab"] = {
		charName = "Blitzcrank",
		displayName = "Rocket Grab",
		slot = SpellSlot.Q,
		type = "linear",
		speed = 1800,
		range = 925,
		delay = 0.25,
		radius = 70,
		danger = 3,
		cc = true,
		collision = true,
		windwall = true,
		hitbox = true
	},
	["BrandQ"] = {
		charName = "Brand",
		displayName = "Sear",
		slot = SpellSlot.Q,
		type = "linear",
		speed = 1600,
		range = 1050,
		delay = 0.25,
		radius = 60,
		danger = 1,
		cc = false,
		collision = true,
		windwall = true,
		hitbox = true
	},
	["BrandW"] = {
		charName = "Brand",
		displayName = "Pillar of Flame",
		slot = SpellSlot.W,
		type = "circular",
		speed = n,
		range = 900,
		delay = 0.85,
		radius = 250,
		danger = 2,
		cc = false,
		collision = false,
		windwall = false,
		hitbox = false
	},
	["BraumQ"] = {
		charName = "Braum",
		displayName = "Winter's Bite",
		slot = SpellSlot.Q,
		type = "linear",
		speed = 1700,
		range = 1000,
		delay = 0.25,
		radius = 70,
		danger = 3,
		cc = true,
		collision = true,
		windwall = true,
		hitbox = true
	},
	["BraumRWrapper"] = {
		charName = "Braum",
		displayName = "Glacial Fissure",
		slot = SpellSlot.R,
		type = "linear",
		speed = 1400,
		range = 1250,
		delay = 0.5,
		radius = 115,
		danger = 4,
		cc = true,
		collision = false,
		windwall = true,
		hitbox = true
	},
	["CaitlynPiltoverPeacemaker"] = {
		charName = "Caitlyn",
		displayName = "Piltover Peacemaker",
		slot = SpellSlot.Q,
		type = "linear",
		speed = 2200,
		range = 1250,
		delay = 0.625,
		radius = 60,
		radius2 = 90,
		danger = 1,
		cc = false,
		collision = false,
		windwall = true,
		hitbox = true
	},
	["CaitlynYordleTrap"] = {
		charName = "Caitlyn",
		displayName = "Yordle Trap",
		slot = SpellSlot.W,
		type = "circular",
		speed = n,
		range = 800,
		delay = 0.25,
		radius = 75,
		danger = 1,
		cc = true,
		collision = false,
		windwall = false,
		hitbox = false
	},
	["CaitlynEntrapment"] = {
		charName = "Caitlyn",
		displayName = "Entrapment",
		slot = SpellSlot.E,
		type = "linear",
		speed = 1600,
		range = 750,
		delay = 0.15,
		radius = 70,
		danger = 2,
		cc = true,
		collision = true,
		windwall = true,
		hitbox = true
	},
	["CamilleE"] = {
		charName = "Camille",
		displayName = "Hookshot [First]",
		slot = SpellSlot.E,
		type = "linear",
		speed = 1900,
		range = 800,
		delay = 0,
		radius = 60,
		danger = 1,
		cc = false,
		collision = false,
		windwall = true,
		hitbox = true
	},
	["CamilleEDash2"] = {
		charName = "Camille",
		displayName = "Hookshot [Second]",
		slot = SpellSlot.E,
		type = "linear",
		speed = 1900,
		range = 400,
		delay = 0,
		radius = 60,
		danger = 2,
		cc = true,
		collision = false,
		windwall = true,
		hitbox = false
	},
	["CassiopeiaQ"] = {
		charName = "Cassiopeia",
		displayName = "Noxious Blast",
		slot = SpellSlot.Q,
		type = "circular",
		speed = n,
		range = 850,
		delay = 0.75,
		radius = 150,
		danger = 2,
		cc = false,
		collision = false,
		windwall = false,
		hitbox = false
	},
	["CassiopeiaW"] = {
		charName = "Cassiopeia",
		displayName = "Miasma",
		slot = SpellSlot.W,
		type = "circular",
		speed = 2500,
		range = 800,
		delay = 0.75,
		radius = 160,
		danger = 2,
		cc = true,
		collision = false,
		windwall = true,
		hitbox = false
	},
	["CassiopeiaR"] = {
		charName = "Cassiopeia",
		displayName = "Petrifying Gaze",
		slot = SpellSlot.R,
		type = "conic",
		speed = n,
		range = 825,
		delay = 0.5,
		radius = 0,
		angle = 80,
		danger = 5,
		cc = true,
		collision = false,
		windwall = false,
		hitbox = false
	},
	["Rupture"] = {
		charName = "Chogath",
		displayName = "Rupture",
		slot = SpellSlot.Q,
		type = "circular",
		speed = n,
		range = 950,
		delay = 1.2,
		radius = 250,
		danger = 2,
		cc = true,
		collision = false,
		windwall = false,
		hitbox = false
	},
	["FeralScream"] = {
		charName = "Chogath",
		displayName = "Feral Scream",
		slot = SpellSlot.W,
		type = "conic",
		speed = n,
		range = 650,
		delay = 0.5,
		radius = 0,
		angle = 56,
		danger = 2,
		cc = true,
		collision = false,
		windwall = false,
		hitbox = false
	},
	["PhosphorusBomb"] = {
		charName = "Corki",
		displayName = "Phosphorus Bomb",
		slot = SpellSlot.Q,
		type = "circular",
		speed = 1000,
		range = 825,
		delay = 0.25,
		radius = 250,
		danger = 2,
		cc = false,
		collision = false,
		windwall = true,
		hitbox = false
	},
	["MissileBarrage"] = {
		charName = "Corki",
		displayName = "Missile Barrage [Standard]",
		slot = SpellSlot.R,
		type = "linear",
		speed = 2000,
		range = 1300,
		delay = 0.175,
		radius = 40,
		danger = 1,
		cc = false,
		collision = true,
		windwall = true,
		hitbox = true
	},
	["MissileBarrage2"] = {
		charName = "Corki",
		displayName = "Missile Barrage [Big]",
		slot = SpellSlot.R,
		type = "linear",
		speed = 2000,
		range = 1500,
		delay = 0.175,
		radius = 40,
		danger = 1,
		cc = false,
		collision = true,
		windwall = true,
		hitbox = true
	},
	["DianaArc"] = {
		charName = "Diana",
		displayName = "Crescent Strike",
		slot = SpellSlot.Q,
		type = "circular",
		speed = 1400,
		range = 900,
		delay = 0.25,
		radius = 185,
		danger = 2,
		cc = false,
		collision = true,
		windwall = true,
		hitbox = false
	},
	["DravenDoubleShot"] = {
		charName = "Draven",
		displayName = "Double Shot",
		slot = SpellSlot.E,
		type = "linear",
		speed = 1600,
		range = 1050,
		delay = 0.25,
		radius = 130,
		danger = 3,
		cc = true,
		collision = false,
		windwall = true,
		hitbox = true
	},
	["DravenRCast"] = {
		charName = "Draven",
		displayName = "Whirling Death",
		slot = SpellSlot.R,
		type = "linear",
		speed = 2000,
		range = 12500,
		delay = 0.25,
		radius = 160,
		danger = 4,
		cc = false,
		collision = false,
		windwall = true,
		hitbox = true
	},
	["InfectedCleaverMissileCast"] = {
		charName = "DrMundo",
		displayName = "Infected Cleaver",
		slot = SpellSlot.Q,
		type = "linear",
		speed = 2000,
		range = 975,
		delay = 0.25,
		radius = 60,
		danger = 2,
		cc = true,
		collision = true,
		windwall = true,
		hitbox = true
	},
	["EkkoQ"] = {
		charName = "Ekko",
		displayName = "Timewinder",
		slot = SpellSlot.Q,
		type = "linear",
		speed = 1650,
		range = 1175,
		delay = 0.25,
		radius = 60,
		danger = 1,
		cc = true,
		collision = false,
		windwall = true,
		hitbox = true
	},
	["EkkoW"] = {
		charName = "Ekko",
		displayName = "Parallel Convergence",
		slot = SpellSlot.W,
		type = "circular",
		speed = n,
		range = 1600,
		delay = 3.35,
		radius = 400,
		danger = 1,
		cc = true,
		collision = false,
		windwall = false,
		hitbox = false
	},
	["EliseHumanE"] = {
		charName = "Elise",
		displayName = "Cocoon",
		slot = SpellSlot.E,
		type = "linear",
		speed = 1600,
		range = 1075,
		delay = 0.25,
		radius = 55,
		danger = 1,
		cc = true,
		collision = true,
		windwall = true,
		hitbox = true
	},
	["EvelynnQ"] = {
		charName = "Evelynn",
		displayName = "Hate Spike",
		slot = SpellSlot.Q,
		type = "linear",
		speed = 2400,
		range = 800,
		delay = 0.25,
		radius = 60,
		danger = 2,
		cc = false,
		collision = true,
		windwall = true,
		hitbox = true
	},
	["EvelynnR"] = {
		charName = "Evelynn",
		displayName = "Last Caress",
		slot = SpellSlot.R,
		type = "conic",
		speed = n,
		range = 450,
		delay = 0.35,
		radius = 180,
		angle = 180,
		danger = 5,
		cc = false,
		collision = false,
		windwall = false,
		hitbox = false
	},
	["EzrealQ"] = {
		charName = "Ezreal",
		displayName = "Mystic Shot",
		slot = SpellSlot.Q,
		type = "linear",
		speed = 2000,
		range = 1150,
		delay = 0.25,
		radius = 60,
		danger = 1,
		cc = false,
		collision = true,
		windwall = true,
		hitbox = true
	},
	["EzrealW"] = {
		charName = "Ezreal",
		displayName = "Essence Flux",
		slot = SpellSlot.W,
		type = "linear",
		speed = 2000,
		range = 1150,
		delay = 0.25,
		radius = 60,
		danger = 1,
		cc = false,
		collision = false,
		windwall = true,
		hitbox = true
	},
	["EzrealR"] = {
		charName = "Ezreal",
		displayName = "Trueshot Barrage",
		slot = SpellSlot.R,
		type = "linear",
		speed = 2000,
		range = 12500,
		delay = 1,
		radius = 160,
		danger = 4,
		cc = false,
		collision = false,
		windwall = true,
		hitbox = true
	},
	["FioraW"] = {
		charName = "Fiora",
		displayName = "Riposte",
		slot = SpellSlot.W,
		type = "linear",
		speed = 3200,
		range = 750,
		delay = 0.75,
		radius = 70,
		danger = 2,
		cc = true,
		collision = false,
		windwall = true,
		hitbox = false
	},
	["FizzR"] = {
		charName = "Fizz",
		displayName = "Chum the Waters",
		slot = SpellSlot.R,
		type = "linear",
		speed = 1300,
		range = 1300,
		delay = 0.25,
		radius = 150,
		danger = 5,
		cc = true,
		collision = false,
		windwall = true,
		hitbox = true
	},
	["GalioQ"] = {
		charName = "Galio",
		displayName = "Winds of War",
		slot = SpellSlot.Q,
		type = "circular",
		speed = 1150,
		range = 825,
		delay = 0.25,
		radius = 235,
		danger = 2,
		cc = false,
		collision = false,
		windwall = true,
		hitbox = false
	},
	["GalioE"] = {
		charName = "Galio",
		displayName = "Justice Punch",
		slot = SpellSlot.E,
		type = "linear",
		speed = 2300,
		range = 650,
		delay = 0.4,
		radius = 160,
		danger = 3,
		cc = true,
		collision = false,
		windwall = false,
		hitbox = false
	},
	["GnarQ"] = {
		charName = "Gnar",
		displayName = "Boomerang Throw",
		slot = SpellSlot.Q,
		type = "linear",
		speed = 2500,
		range = 1125,
		delay = 0.25,
		radius = 55,
		danger = 2,
		cc = true,
		collision = false,
		windwall = true,
		hitbox = true
	},
	["GnarBigQ"] = {
		charName = "Gnar",
		displayName = "Boulder Toss",
		slot = SpellSlot.Q,
		type = "linear",
		speed = 2100,
		range = 1125,
		delay = 0.5,
		radius = 90,
		danger = 2,
		cc = true,
		collision = true,
		windwall = true,
		hitbox = true
	},
	["GnarBigW"] = {
		charName = "Gnar",
		displayName = "Wallop",
		slot = SpellSlot.W,
		type = "linear",
		speed = n,
		range = 575,
		delay = 0.6,
		radius = 100,
		danger = 3,
		cc = true,
		collision = false,
		windwall = false,
		hitbox = false
	},
	["GnarE"] = {
		charName = "Gnar",
		displayName = "Hop",
		slot = SpellSlot.E,
		type = "circular",
		speed = 900,
		range = 475,
		delay = 0.25,
		radius = 160,
		danger = 2,
		cc = true,
		collision = false,
		windwall = false,
		hitbox = false
	},
	["GnarBigE"] = {
		charName = "Gnar",
		displayName = "Crunch",
		slot = SpellSlot.E,
		type = "circular",
		speed = 800,
		range = 600,
		delay = 0.25,
		radius = 375,
		cc = true,
		collision = false,
		windwall = false,
		hitbox = false
	},
	["GnarR"] = {
		charName = "Gnar",
		displayName = "GNAR!",
		slot = SpellSlot.R,
		type = "circular",
		speed = n,
		range = 0,
		delay = 0.25,
		radius = 475,
		danger = 5,
		cc = true,
		collision = false,
		windwall = false,
		hitbox = false
	},
	["GragasQ"] = {
		charName = "Gragas",
		displayName = "Barrel Roll",
		slot = SpellSlot.Q,
		type = "circular",
		speed = 1000,
		range = 850,
		delay = 0.25,
		radius = 275,
		danger = 2,
		cc = true,
		collision = false,
		windwall = true,
		hitbox = false
	},
	["GragasE"] = {
		charName = "Gragas",
		displayName = "Body Slam",
		slot = SpellSlot.E,
		type = "linear",
		speed = 900,
		range = 600,
		delay = 0.25,
		radius = 170,
		danger = 2,
		cc = true,
		collision = true,
		windwall = false,
		hitbox = false
	},
	["GragasR"] = {
		charName = "Gragas",
		displayName = "Explosive Cask",
		slot = SpellSlot.R,
		type = "circular",
		speed = 1800,
		range = 1000,
		delay = 0.25,
		radius = 400,
		danger = 5,
		cc = true,
		collision = false,
		windwall = true,
		hitbox = false
	},
	["GravesQLineSpell"] = {
		charName = "Graves",
		displayName = "End of the Line",
		slot = SpellSlot.Q,
		type = "linear",
		speed = 2000,
		range = 700,
		delay = 0.095,
		radius = 20,
		danger = 1,
		cc = false,
		collision = false,
		windwall = true,
		hitbox = true
	},
	["GravesSmokeGrenade"] = {
		charName = "Graves",
		displayName = "Smoke Grenade",
		slot = SpellSlot.W,
		type = "circular",
		speed = 1500,
		range = 950,
		delay = 0.15,
		radius = 250,
		danger = 2,
		cc = true,
		collision = false,
		windwall = true,
		hitbox = false
	},
	["GravesChargeShot"] = {
		charName = "Graves",
		displayName = "Charge Shot",
		slot = SpellSlot.R,
		type = "linear",
		speed = 2100,
		range = 1000,
		delay = 0.25,
		radius = 100,
		danger = 5,
		cc = false,
		collision = false,
		windwall = true,
		hitbox = true
	},
	["HecarimUlt"] = {
		charName = "Hecarim",
		displayName = "Onslaught of Shadows",
		slot = SpellSlot.R,
		type = "linear",
		speed = 1100,
		range = 1650,
		delay = 0.2,
		radius = 280,
		danger = 4,
		cc = true,
		collision = false,
		windwall = false,
		hitbox = false
	},
	["HeimerdingerW"] = {
		charName = "Heimerdinger",
		displayName = "Hextech Micro-Rockets",
		slot = SpellSlot.W,
		type = "linear",
		speed = 2050,
		range = 1325,
		delay = 0.25,
		radius = 100,
		danger = 2,
		cc = false,
		collision = false,
		windwall = true,
		hitbox = true
	},
	["HeimerdingerE"] = {
		charName = "Heimerdinger",
		displayName = "CH-2 Electron Storm Grenade",
		slot = SpellSlot.E,
		type = "circular",
		speed = 1200,
		range = 970,
		delay = 0.25,
		radius = 250,
		danger = 2,
		cc = true,
		collision = false,
		windwall = true,
		hitbox = false
	},
	["HeimerdingerEUlt"] = {
		charName = "Heimerdinger",
		displayName = "CH-2 Electron Storm Grenade",
		slot = SpellSlot.E,
		type = "circular",
		speed = 1200,
		range = 970,
		delay = 0.25,
		radius = 250,
		danger = 3,
		cc = true,
		collision = false,
		windwall = true,
		hitbox = false
	},
	["IllaoiQ"] = {
		charName = "Illaoi",
		displayName = "Tentacle Smash",
		slot = SpellSlot.Q,
		type = "linear",
		speed = n,
		range = 850,
		delay = 0.75,
		radius = 100,
		danger = 2,
		cc = false,
		collision = true,
		windwall = false,
		hitbox = false
	},
	["IllaoiE"] = {
		charName = "Illaoi",
		displayName = "Test of Spirit",
		slot = SpellSlot.E,
		type = "linear",
		speed = 1900,
		range = 900,
		delay = 0.25,
		radius = 50,
		danger = 1,
		cc = false,
		collision = true,
		windwall = true,
		hitbox = false
	},
	["IreliaW2"] = {
		charName = "Irelia",
		displayName = "Defiant Dance",
		slot = SpellSlot.W,
		type = "linear",
		speed = n,
		range = 775,
		delay = 0.25,
		radius = 120,
		danger = 3,
		cc = true,
		collision = false,
		windwall = false,
		hitbox = false
	},
	["IreliaR"] = {
		charName = "Irelia",
		displayName = "Vanguard's Edge",
		slot = SpellSlot.R,
		type = "linear",
		speed = 2000,
		range = 950,
		delay = 0.4,
		radius = 160,
		danger = 4,
		cc = true,
		collision = false,
		windwall = true,
		hitbox = false
	},
	["IvernQ"] = {
		charName = "Ivern",
		displayName = "Rootcaller",
		slot = SpellSlot.Q,
		type = "linear",
		speed = 1300,
		range = 1075,
		delay = 0.25,
		radius = 80,
		danger = 1,
		cc = true,
		collision = true,
		windwall = true,
		hitbox = true
	},
	["HowlingGale"] = {
		charName = "Janna",
		displayName = "Howling Gale",
		slot = SpellSlot.Q,
		type = "linear",
		speed = 667,
		range = 1750,
		delay = 0,
		radius = 100,
		danger = 2,
		cc = true,
		collision = false,
		windwall = true,
		hitbox = true
	},
	["JarvanIVDragonStrike"] = {
		charName = "JarvanIV",
		displayName = "Dragon Strike",
		slot = SpellSlot.Q,
		type = "linear",
		speed = n,
		range = 770,
		delay = 0.4,
		radius = 70,
		danger = 2,
		cc = true,
		collision = false,
		windwall = false,
		hitbox = false
	},
	["JarvanIVDemacianStandard"] = {
		charName = "JarvanIV",
		displayName = "Demacian Standard",
		slot = SpellSlot.E,
		type = "circular",
		speed = 3440,
		range = 860,
		delay = 0,
		radius = 175,
		cc = false,
		collision = false,
		windwall = false,
		hitbox = false
	},
	["JayceShockBlast"] = {
		charName = "Jayce",
		displayName = "Shock Blast",
		slot = SpellSlot.Q,
		type = "linear",
		speed = 1450,
		range = 1050,
		delay = 0.214,
		radius = 70,
		danger = 1,
		cc = false,
		collision = true,
		windwall = true,
		hitbox = true
	},
	["JhinW"] = {
		charName = "Jhin",
		displayName = "Deadly Flourish",
		slot = SpellSlot.W,
		type = "linear",
		speed = 5000,
		range = 2550,
		delay = 0.75,
		radius = 40,
		danger = 1,
		cc = true,
		collision = false,
		windwall = true,
		hitbox = false
	},
	["JhinE"] = {
		charName = "Jhin",
		displayName = "Captive Audience",
		slot = SpellSlot.E,
		type = "circular",
		speed = 1600,
		range = 750,
		delay = 0.25,
		radius = 130,
		danger = 1,
		cc = true,
		collision = false,
		windwall = false,
		hitbox = false
	},
	["JhinRShot"] = {
		charName = "Jhin",
		displayName = "Curtain Call",
		slot = SpellSlot.R,
		type = "linear",
		speed = 5000,
		range = 3500,
		delay = 0.25,
		radius = 80,
		danger = 2,
		cc = true,
		collision = false,
		windwall = true,
		hitbox = true
	},
	["JinxW"] = {
		charName = "Jinx",
		displayName = "Zap!",
		slot = SpellSlot.W,
		type = "linear",
		speed = 3300,
		range = 1450,
		delay = 0.6,
		radius = 60,
		danger = 1,
		cc = true,
		collision = true,
		windwall = true,
		hitbox = true
	},
	["JinxE"] = {
		charName = "Jinx",
		displayName = "Flame Chompers!",
		slot = SpellSlot.E,
		type = "circular",
		speed = 1100,
		range = 900,
		delay = 1.5,
		radius = 120,
		danger = 1,
		cc = true,
		collision = false,
		windwall = true,
		hitbox = false
	},
	["JinxR"] = {
		charName = "Jinx",
		displayName = "Super Mega Death Rocket!",
		slot = SpellSlot.R,
		type = "linear",
		speed = 1700,
		range = 12500,
		delay = 0.6,
		radius = 140,
		danger = 4,
		cc = false,
		collision = false,
		windwall = true,
		hitbox = true
	},
	["KaisaW"] = {
		charName = "Kaisa",
		displayName = "Void Seeker",
		slot = SpellSlot.W,
		type = "linear",
		speed = 1750,
		range = 3000,
		delay = 0.4,
		radius = 100,
		danger = 1,
		cc = false,
		collision = true,
		windwall = true,
		hitbox = true
	},
	["KalistaMysticShot"] = {
		charName = "Kalista",
		displayName = "Pierce",
		slot = SpellSlot.Q,
		type = "linear",
		speed = 2400,
		range = 1150,
		delay = 0.25,
		radius = 40,
		danger = 1,
		cc = false,
		collision = true,
		windwall = true,
		hitbox = true
	},
	["KarmaQ"] = {
		charName = "Karma",
		displayName = "Inner Flame",
		slot = SpellSlot.Q,
		type = "linear",
		speed = 1700,
		range = 950,
		delay = 0.25,
		radius = 60,
		danger = 2,
		cc = true,
		collision = true,
		windwall = true,
		hitbox = true
	},
	["KarmaQMantra"] = {
		charName = "Karma",
		displayName = "Inner Flame [Mantra]",
		slot = SpellSlot.Q,
		origin = "linear",
		type = "linear",
		speed = 1700,
		range = 950,
		delay = 0.25,
		radius = 80,
		danger = 2,
		cc = true,
		collision = true,
		windwall = true,
		hitbox = true
	},
	["KarthusLayWasteA1"] = {
		charName = "Karthus",
		displayName = "Lay Waste [1]",
		slot = SpellSlot.Q,
		type = "circular",
		speed = n,
		range = 875,
		delay = 0.9,
		radius = 175,
		danger = 1,
		cc = false,
		collision = false,
		windwall = false,
		hitbox = false
	},
	["KarthusLayWasteA2"] = {
		charName = "Karthus",
		displayName = "Lay Waste [2]",
		slot = SpellSlot.Q,
		type = "circular",
		speed = n,
		range = 875,
		delay = 0.9,
		radius = 175,
		danger = 1,
		cc = false,
		collision = false,
		windwall = false,
		hitbox = false
	},
	["KarthusLayWasteA3"] = {
		charName = "Karthus",
		displayName = "Lay Waste [3]",
		slot = SpellSlot.Q,
		type = "circular",
		speed = n,
		range = 875,
		delay = 0.9,
		radius = 175,
		danger = 1,
		cc = false,
		collision = false,
		windwall = false,
		hitbox = false
	},
	["ForcePulse"] = {
		charName = "Kassadin",
		displayName = "Force Pulse",
		slot = SpellSlot.E,
		type = "conic",
		speed = n,
		range = 600,
		delay = 0.3,
		radius = 0,
		angle = 80,
		danger = 3,
		cc = true,
		collision = false,
		windwall = false,
		hitbox = false
	},
	["RiftWalk"] = {
		charName = "Kassadin",
		displayName = "Rift Walk",
		slot = SpellSlot.R,
		type = "circular",
		speed = n,
		range = 500,
		delay = 0.25,
		radius = 250,
		danger = 3,
		cc = false,
		collision = false,
		windwall = false,
		hitbox = false
	},
	["KayleQ"] = {
		charName = "Kayle",
		displayName = "Radiant Blast",
		slot = SpellSlot.Q,
		type = "linear",
		speed = 2000,
		range = 850,
		delay = 0.5,
		radius = 60,
		danger = 1,
		cc = true,
		collision = false,
		windwall = true,
		hitbox = true
	},
	["KaynQ"] = {
		charName = "Kayn",
		displayName = "Reaping Slash",
		slot = SpellSlot.Q,
		type = "circular",
		speed = n,
		range = 0,
		delay = 0.15,
		radius = 350,
		danger = 2,
		cc = false,
		collision = false,
		windwall = false,
		hitbox = false
	},
	["KaynW"] = {
		charName = "Kayn",
		displayName = "Blade's Reach",
		slot = SpellSlot.W,
		type = "linear",
		speed = n,
		range = 700,
		delay = 0.55,
		radius = 90,
		danger = 2,
		cc = true,
		collision = false,
		windwall = false,
		hitbox = false
	},
	["KennenShurikenHurlMissile1"] = {
		charName = "Kennen",
		displayName = "Shuriken Hurl",
		slot = SpellSlot.Q,
		type = "linear",
		speed = 1700,
		range = 1050,
		delay = 0.175,
		radius = 50,
		danger = 2,
		cc = false,
		collision = true,
		windwall = true,
		hitbox = true
	},
	["KhazixW"] = {
		charName = "Khazix",
		displayName = "Void Spike [Standard]",
		slot = SpellSlot.W,
		type = "linear",
		speed = 1700,
		range = 1000,
		delay = 0.25,
		radius = 70,
		danger = 1,
		cc = false,
		collision = true,
		windwall = true,
		hitbox = true
	},
	["KhazixWLong"] = {
		charName = "Khazix",
		displayName = "Void Spike [Threeway]",
		slot = SpellSlot.W,
		type = "threeway",
		speed = 1700,
		range = 1000,
		delay = 0.25,
		radius = 70,
		angle = 23,
		danger = 2,
		cc = true,
		collision = true,
		windwall = true,
		hitbox = true
	},
	["KledQ"] = {
		charName = "Kled",
		displayName = "Beartrap on a Rope",
		slot = SpellSlot.Q,
		type = "linear",
		speed = 1600,
		range = 800,
		delay = 0.25,
		radius = 45,
		danger = 1,
		cc = true,
		collision = true,
		windwall = true,
		hitbox = true
	},
	["KledRiderQ"] = {
		charName = "Kled",
		displayName = "Pocket Pistol",
		slot = SpellSlot.Q,
		type = "conic",
		speed = 3000,
		range = 700,
		delay = 0.25,
		radius = 0,
		angle = 25,
		danger = 3,
		cc = false,
		collision = false,
		windwall = true,
		hitbox = false
	},
	["KledEDash"] = {
		charName = "Kled",
		displayName = "Jousting",
		slot = SpellSlot.E,
		type = "linear",
		speed = 1100,
		range = 550,
		delay = 0,
		radius = 90,
		danger = 2,
		cc = false,
		collision = false,
		windwall = false,
		hitbox = false
	},
	["KogMawQ"] = {
		charName = "KogMaw",
		displayName = "Caustic Spittle",
		slot = SpellSlot.Q,
		type = "linear",
		speed = 1650,
		range = 1175,
		delay = 0.25,
		radius = 70,
		danger = 1,
		cc = false,
		collision = true,
		windwall = true,
		hitbox = true
	},
	["KogMawVoidOoze"] = {
		charName = "KogMaw",
		displayName = "Void Ooze",
		slot = SpellSlot.E,
		type = "linear",
		speed = 1400,
		range = 1360,
		delay = 0.25,
		radius = 120,
		danger = 2,
		cc = true,
		collision = false,
		windwall = true,
		hitbox = true
	},
	["KogMawLivingArtillery"] = {
		charName = "KogMaw",
		displayName = "Living Artillery",
		slot = SpellSlot.R,
		type = "circular",
		speed = n,
		range = 1300,
		delay = 1.1,
		radius = 200,
		danger = 1,
		cc = false,
		collision = false,
		windwall = false,
		hitbox = false
	},
	["LeblancE"] = {
		charName = "Leblanc",
		displayName = "Ethereal Chains [Standard]",
		slot = SpellSlot.E,
		type = "linear",
		speed = 1750,
		range = 925,
		delay = 0.25,
		radius = 55,
		danger = 1,
		cc = true,
		collision = true,
		windwall = true,
		hitbox = true
	},
	["LeblancRE"] = {
		charName = "Leblanc",
		displayName = "Ethereal Chains [Ultimate]",
		slot = SpellSlot.E,
		type = "linear",
		speed = 1750,
		range = 925,
		delay = 0.25,
		radius = 55,
		danger = 1,
		cc = true,
		collision = true,
		windwall = true,
		hitbox = true
	},
	["BlindMonkQOne"] = {
		charName = "LeeSin",
		displayName = "Sonic Wave",
		slot = SpellSlot.Q,
		type = "linear",
		speed = 1800,
		range = 1100,
		delay = 0.25,
		radius = 60,
		danger = 1,
		cc = false,
		collision = true,
		windwall = true,
		hitbox = true
	},
	["LeonaZenithBlade"] = {
		charName = "Leona",
		displayName = "Zenith Blade",
		slot = SpellSlot.E,
		type = "linear",
		speed = 2000,
		range = 875,
		delay = 0.25,
		radius = 70,
		danger = 2,
		cc = true,
		collision = false,
		windwall = true,
		hitbox = true
	},
	["LeonaSolarFlare"] = {
		charName = "Leona",
		displayName = "Solar Flare",
		slot = SpellSlot.R,
		type = "circular",
		speed = n,
		range = 1200,
		delay = 0.85,
		radius = 300,
		danger = 5,
		cc = true,
		collision = false,
		windwall = false,
		hitbox = false
	},
	["LissandraQ"] = {
		charName = "Lissandra",
		displayName = "Ice Shard",
		slot = SpellSlot.Q,
		type = "linear",
		speed = 2200,
		range = 750,
		delay = 0.25,
		radius = 75,
		danger = 2,
		cc = true,
		collision = false,
		windwall = true,
		hitbox = true
	},
	["LissandraE"] = {
		charName = "Lissandra",
		displayName = "Glacial Path",
		slot = SpellSlot.E,
		type = "linear",
		speed = 850,
		range = 1025,
		delay = 0.25,
		radius = 125,
		danger = 2,
		cc = false,
		collision = false,
		windwall = true,
		hitbox = true
	},
	["LucianQ"] = {
		charName = "Lucian",
		displayName = "Piercing Light",
		slot = SpellSlot.Q,
		type = "linear",
		speed = n,
		range = 900,
		delay = 0.35,
		radius = 65,
		danger = 2,
		cc = false,
		collision = false,
		windwall = false,
		hitbox = false
	},
	["LucianW"] = {
		charName = "Lucian",
		displayName = "Ardent Blaze",
		slot = SpellSlot.W,
		type = "linear",
		speed = 1600,
		range = 900,
		delay = 0.25,
		radius = 80,
		danger = 2,
		cc = false,
		collision = true,
		windwall = true,
		hitbox = true
	},
	["LuluQ"] = {
		charName = "Lulu",
		displayName = "Glitterlance",
		slot = SpellSlot.Q,
		type = "linear",
		speed = 1450,
		range = 925,
		delay = 0.25,
		radius = 60,
		danger = 1,
		cc = true,
		collision = false,
		windwall = true,
		hitbox = true
	},
	["LuxLightBinding"] = {
		charName = "Lux",
		displayName = "Light Binding",
		slot = SpellSlot.Q,
		type = "linear",
		speed = 1200,
		range = 1175,
		delay = 0.25,
		radius = 70,
		danger = 1,
		cc = true,
		collision = true,
		windwall = true,
		hitbox = true
	},
	["LuxLightStrikeKugel"] = {
		charName = "Lux",
		displayName = "Light Strike Kugel",
		slot = SpellSlot.E,
		type = "circular",
		speed = 1200,
		range = 1100,
		delay = 0.25,
		radius = 300,
		danger = 3,
		cc = true,
		collision = true,
		windwall = true,
		hitbox = false
	},
	["LuxMaliceCannon"] = {
		charName = "Lux",
		displayName = "Malice Cannon",
		slot = SpellSlot.R,
		type = "linear",
		speed = n,
		range = 3340,
		delay = 1,
		radius = 120,
		danger = 4,
		cc = false,
		collision = false,
		windwall = false,
		hitbox = false
	},
	["Landslide"] = {
		charName = "Malphite",
		displayName = "Ground Slam",
		slot = SpellSlot.E,
		type = "circular",
		speed = n,
		range = 0,
		delay = 0.242,
		radius = 400,
		danger = 2,
		cc = true,
		collision = false,
		windwall = false,
		hitbox = false
	},
	["UFSlash"] = {
		charName = "Malphite",
		displayName = "Unstoppable Force",
		slot = SpellSlot.R,
		type = "circular",
		speed = 1835,
		range = 1000,
		delay = 0,
		radius = 300,
		danger = 5,
		cc = true,
		collision = false,
		windwall = false,
		hitbox = false
	},
	["MalzaharQ"] = {
		charName = "Malzahar",
		displayName = "Call of the Void",
		slot = SpellSlot.Q,
		type = "rectangular",
		speed = 1600,
		range = 900,
		delay = 0.5,
		radius = 400,
		radius2 = 100,
		danger = 1,
		cc = true,
		collision = false,
		windwall = false,
		hitbox = false
	},
	["MaokaiQ"] = {
		charName = "Maokai",
		displayName = "Bramble Smash",
		slot = SpellSlot.Q,
		type = "linear",
		speed = 1600,
		range = 600,
		delay = 0.375,
		radius = 110,
		danger = 2,
		cc = true,
		collision = false,
		windwall = true,
		hitbox = true
	},
	["MissFortuneBulletTime"] = {
		charName = "MissFortune",
		displayName = "Bullet Time",
		slot = SpellSlot.R,
		type = "conic",
		speed = 2000,
		range = 1400,
		delay = 0.25,
		radius = 100,
		angle = 34,
		danger = 4,
		cc = false,
		collision = false,
		windwall = true,
		hitbox = false
	},
	["MordekaiserSyphonOfDestruction"] = {
		charName = "Mordekaiser",
		displayName = "Syphon Of Destruction",
		slot = SpellSlot.E,
		type = "conic",
		speed = n,
		range = 700,
		delay = 0.25,
		radius = 0,
		angle = 50,
		danger = 2,
		cc = false,
		collision = false,
		windwall = false,
		hitbox = false
	},
	["MorganaQ"] = {
		charName = "Morgana",
		displayName = "Dark Binding",
		slot = SpellSlot.Q,
		type = "linear",
		speed = 1200,
		range = 1250,
		delay = 0.25,
		radius = 70,
		danger = 1,
		cc = true,
		collision = true,
		windwall = true,
		hitbox = true
	},
	["MorganaW"] = {
		charName = "Morgana",
		displayName = "Tormented Soil",
		slot = SpellSlot.W,
		type = "circular",
		speed = n,
		range = 900,
		delay = 0.25,
		radius = 275,
		danger = 2,
		cc = false,
		collision = false,
		windwall = false,
		hitbox = false
	},
	["NamiQ"] = {
		charName = "Nami",
		displayName = "Aqua Prison",
		slot = SpellSlot.Q,
		type = "circular",
		speed = n,
		range = 875,
		delay = 1,
		radius = 180,
		danger = 1,
		cc = true,
		collision = false,
		windwall = true,
		hitbox = false
	},
	["NamiR"] = {
		charName = "Nami",
		displayName = "Tidal Wave",
		slot = SpellSlot.R,
		type = "linear",
		speed = 850,
		range = 2750,
		delay = 0.5,
		radius = 250,
		danger = 3,
		cc = true,
		collision = false,
		windwall = true,
		hitbox = true
	},
	["NautilusAnchorDrag"] = {
		charName = "Nautilus",
		displayName = "Dredge Line",
		slot = SpellSlot.Q,
		type = "linear",
		speed = 2000,
		range = 925,
		delay = 0.25,
		radius = 90,
		danger = 3,
		cc = true,
		collision = true,
		windwall = true,
		hitbox = true
	},
	["NeekoQ"] = {
		charName = "Neeko",
		displayName = "Blooming Burst",
		slot = SpellSlot.Q,
		type = "circular",
		speed = 1500,
		range = 800,
		delay = 0.25,
		radius = 200,
		danger = 2,
		cc = true,
		collision = false,
		windwall = true,
		hitbox = false
	},
	["NeekoE"] = {
		charName = "Neeko",
		displayName = "Tangle-Barbs",
		slot = SpellSlot.E,
		type = "linear",
		speed = 1300,
		range = 1000,
		delay = 0.25,
		radius = 70,
		danger = 1,
		cc = true,
		collision = false,
		windwall = true,
		hitbox = true
	},
	["JavelinToss"] = {
		charName = "Nidalee",
		displayName = "Javelin Toss",
		slot = SpellSlot.Q,
		type = "linear",
		speed = 1300,
		range = 1500,
		delay = 0.25,
		radius = 40,
		danger = 1,
		cc = false,
		collision = true,
		windwall = true,
		hitbox = true
	},
	["Bushwhack"] = {
		charName = "Nidalee",
		displayName = "Bushwhack",
		slot = SpellSlot.W,
		type = "circular",
		speed = n,
		range = 900,
		delay = 1.25,
		radius = 85,
		danger = 1,
		cc = false,
		collision = false,
		windwall = false,
		hitbox = false
	},
	["Swipe"] = {
		charName = "Nidalee",
		displayName = "Swipe",
		slot = SpellSlot.E,
		type = "conic",
		speed = n,
		range = 350,
		delay = 0.25,
		radius = 0,
		angle = 180,
		danger = 2,
		cc = false,
		collision = false,
		windwall = false,
		hitbox = false
	},
	["NocturneDuskbringer"] = {
		charName = "Nocturne",
		displayName = "Duskbringer",
		slot = SpellSlot.Q,
		type = "linear",
		speed = 1600,
		range = 1200,
		delay = 0.25,
		radius = 60,
		danger = 2,
		cc = false,
		collision = false,
		windwall = true,
		hitbox = true
	},
	["NunuR"] = {
		charName = "Nunu",
		displayName = "Absolute Zero",
		slot = SpellSlot.R,
		type = "circular",
		speed = n,
		range = 0,
		delay = 3,
		radius = 650,
		danger = 5,
		cc = true,
		collision = false,
		windwall = false,
		hitbox = false
	},
	["OlafAxeThrowCast"] = {
		charName = "Olaf",
		displayName = "Undertow",
		slot = SpellSlot.Q,
		type = "linear",
		speed = 1600,
		range = 1000,
		delay = 0.25,
		radius = 90,
		danger = 2,
		cc = true,
		collision = false,
		windwall = true,
		hitbox = true
	},
	["OrianaIzunaCommand"] = {
		charName = "Orianna",
		displayName = "Command Attack",
		slot = SpellSlot.Q,
		type = "linear",
		speed = 1400,
		range = 825,
		delay = 0.25,
		radius = 80,
		danger = 2,
		cc = false,
		collision = false,
		windwall = false,
		hitbox = false
	},
	["OrnnQ"] = {
		charName = "Ornn",
		displayName = "Volcanic Rupture",
		slot = SpellSlot.Q,
		type = "linear",
		speed = 1800,
		range = 800,
		delay = 0.3,
		radius = 65,
		danger = 1,
		cc = true,
		collision = false,
		windwall = true,
		hitbox = true
	},
	["OrnnE"] = {
		charName = "Ornn",
		displayName = "Searing Charge",
		slot = SpellSlot.E,
		type = "linear",
		speed = 1800,
		range = 800,
		delay = 0.35,
		radius = 150,
		danger = 3,
		cc = true,
		collision = false,
		windwall = false,
		hitbox = false
	},
	["OrnnRCharge"] = {
		charName = "Ornn",
		displayName = "Call of the Forge God",
		slot = SpellSlot.R,
		type = "linear",
		speed = 1650,
		range = 2500,
		delay = 0.5,
		radius = 200,
		danger = 3,
		cc = true,
		collision = false,
		windwall = true,
		hitbox = true
	},
	["PantheonE"] = {
		charName = "Pantheon",
		displayName = "Heartseeker Strike",
		slot = SpellSlot.E,
		type = "conic",
		speed = n,
		range = 600,
		delay = 0.389,
		radius = 0,
		angle = 70,
		danger = 3,
		cc = false,
		collision = false,
		windwall = false,
		hitbox = false
	},
	["PoppyQ"] = {
		charName = "Poppy",
		displayName = "Hammer Shock",
		slot = SpellSlot.Q,
		type = "linear",
		speed = n,
		range = 430,
		delay = 0.332,
		radius = 100,
		danger = 2,
		cc = true,
		collision = false,
		windwall = false,
		hitbox = false
	},
	["PoppyRSpell"] = {
		charName = "Poppy",
		displayName = "Keeper's Verdict",
		slot = SpellSlot.R,
		type = "linear",
		speed = 2000,
		range = 1200,
		delay = 0.33,
		radius = 100,
		danger = 3,
		cc = true,
		collision = false,
		windwall = true,
		hitbox = true
	},
	["PykeQMelee"] = {
		charName = "Pyke",
		displayName = "Bone Skewer [Melee]",
		slot = SpellSlot.Q,
		type = "linear",
		speed = n,
		range = 400,
		delay = 0.25,
		radius = 70,
		danger = 2,
		cc = true,
		collision = false,
		windwall = false,
		hitbox = false
	},
	["PykeQRange"] = {
		charName = "Pyke",
		displayName = "Bone Skewer [Range]",
		slot = SpellSlot.Q,
		type = "linear",
		speed = 2000,
		range = 1100,
		delay = 0.2,
		radius = 70,
		danger = 2,
		cc = true,
		collision = true,
		windwall = true,
		hitbox = true
	},
	["PykeE"] = {
		charName = "Pyke",
		displayName = "Phantom Undertow",
		slot = SpellSlot.E,
		type = "linear",
		speed = 3000,
		range = 12500,
		delay = 0,
		radius = 110,
		danger = 2,
		cc = true,
		collision = false,
		windwall = false,
		hitbox = true
	},
	["PykeR"] = {
		charName = "Pyke",
		displayName = "Death from Below",
		slot = SpellSlot.R,
		type = "circular",
		speed = n,
		range = 750,
		delay = 0.5,
		radius = 350,
		radius2 = 100,
		danger = 5,
		cc = false,
		collision = false,
		windwall = false,
		hitbox = false
	},
	["QuinnQ"] = {
		charName = "Quinn",
		displayName = "Blinding Assault",
		slot = SpellSlot.Q,
		type = "linear",
		speed = 1550,
		range = 1025,
		delay = 0.25,
		radius = 60,
		danger = 1,
		cc = false,
		collision = true,
		windwall = true,
		hitbox = true
	},
	["RakanQ"] = {
		charName = "Rakan",
		displayName = "Gleaming Quill",
		slot = SpellSlot.Q,
		type = "linear",
		speed = 1850,
		range = 850,
		delay = 0.25,
		radius = 65,
		danger = 1,
		cc = false,
		collision = true,
		windwall = true,
		hitbox = true
	},
	["RakanW"] = {
		charName = "Rakan",
		displayName = "Grand Entrance",
		slot = SpellSlot.W,
		type = "circular",
		speed = n,
		range = 650,
		delay = 0.7,
		radius = 265,
		danger = 3,
		cc = true,
		collision = false,
		windwall = false,
		hitbox = false
	},
	["RekSaiQBurrowed"] = {
		charName = "RekSai",
		displayName = "Prey Seeker",
		slot = SpellSlot.Q,
		type = "linear",
		speed = 1950,
		range = 1625,
		delay = 0.125,
		radius = 65,
		danger = 2,
		cc = false,
		collision = true,
		windwall = true,
		hitbox = true
	},
	["RenektonSliceAndDice"] = {
		charName = "Renekton",
		displayName = "Slice and Dice",
		slot = SpellSlot.E,
		type = "linear",
		speed = 1125,
		range = 450,
		delay = 0.25,
		radius = 65,
		danger = 2,
		cc = false,
		collision = false,
		windwall = false,
		hitbox = false
	},
	["RengarE"] = {
		charName = "Rengar",
		displayName = "Bola Strike",
		slot = SpellSlot.E,
		type = "linear",
		speed = 1500,
		range = 1000,
		delay = 0.25,
		radius = 70,
		danger = 1,
		cc = true,
		collision = true,
		windwall = true,
		hitbox = true
	},
	["RivenIzunaBlade"] = {
		charName = "Riven",
		displayName = "Wind Slash",
		slot = SpellSlot.R,
		type = "linear",
		speed = 1600,
		range = 900,
		delay = 0.25,
		radius = 0,
		angle = 75,
		danger = 5,
		cc = false,
		collision = false,
		windwall = true,
		hitbox = false
	},
	["RumbleGrenade"] = {
		charName = "Rumble",
		displayName = "Electro Harpoon",
		slot = SpellSlot.E,
		type = "linear",
		speed = 2000,
		range = 850,
		delay = 0.25,
		radius = 60,
		danger = 2,
		cc = true,
		collision = true,
		windwall = true,
		hitbox = true
	},
	["RyzeQWrapper"] = {
		charName = "Ryze",
		displayName = "Overload",
		slot = SpellSlot.Q,
		type = "linear",
		speed = 1700,
		range = 1000,
		delay = 0.25,
		radius = 55,
		danger = 1,
		cc = false,
		collision = true,
		windwall = true,
		hitbox = true
	},
	["SejuaniR"] = {
		charName = "Sejuani",
		displayName = "Glacial Prison",
		slot = SpellSlot.R,
		type = "linear",
		speed = 1600,
		range = 1300,
		delay = 0.25,
		radius = 120,
		danger = 5,
		cc = true,
		collision = false,
		windwall = true,
		hitbox = true
	},
	["ShenE"] = {
		charName = "Shen",
		displayName = "Shadow Dash",
		slot = SpellSlot.E,
		type = "linear",
		speed = 1200,
		range = 600,
		delay = 0,
		radius = 60,
		danger = 2,
		cc = true,
		collision = false,
		windwall = false,
		hitbox = false
	},
	["ShyvanaFireball"] = {
		charName = "Shyvana",
		displayName = "Flame Breath [Standard]",
		slot = SpellSlot.E,
		type = "linear",
		speed = 1575,
		range = 925,
		delay = 0.25,
		radius = 60,
		danger = 1,
		cc = false,
		collision = false,
		windwall = true,
		hitbox = false
	},
	["ShyvanaFireballDragon2"] = {
		charName = "Shyvana",
		displayName = "Flame Breath [Dragon]",
		slot = SpellSlot.E,
		type = "linear",
		speed = 1575,
		range = 975,
		delay = 0.333,
		radius = 60,
		danger = 2,
		cc = false,
		collision = false,
		windwall = true,
		hitbox = false
	},
	["ShyvanaTransformCast"] = {
		charName = "Shyvana",
		displayName = "Transform Leap",
		slot = SpellSlot.R,
		type = "linear",
		speed = 700,
		range = 850,
		delay = 0.25,
		radius = 150,
		danger = 4,
		cc = true,
		collision = false,
		windwall = false,
		hitbox = false
	},
	["SionQ"] = {
		charName = "Sion",
		displayName = "Decimating Smash",
		slot = SpellSlot.Q,
		origin = "",
		type = "linear",
		speed = n,
		range = 750,
		delay = 2,
		radius = 150,
		danger = 3,
		cc = true,
		collision = false,
		windwall = false,
		hitbox = false
	},
	["SionE"] = {
		charName = "Sion",
		displayName = "Roar of the Slayer",
		slot = SpellSlot.E,
		type = "linear",
		speed = 1800,
		range = 800,
		delay = 0.25,
		radius = 80,
		danger = 2,
		cc = true,
		collision = false,
		windwall = true,
		hitbox = false
	},
	["SivirQ"] = {
		charName = "Sivir",
		displayName = "Boomerang Blade",
		slot = SpellSlot.Q,
		type = "linear",
		speed = 1350,
		range = 1250,
		delay = 0.25,
		radius = 90,
		danger = 2,
		cc = false,
		collision = false,
		windwall = true,
		hitbox = false
	},
	["SkarnerFracture"] = {
		charName = "Skarner",
		displayName = "Fracture",
		slot = SpellSlot.E,
		type = "linear",
		speed = 1500,
		range = 1000,
		delay = 0.25,
		radius = 70,
		danger = 1,
		cc = true,
		collision = false,
		windwall = true,
		hitbox = false
	},
	["SonaR"] = {
		charName = "Sona",
		displayName = "Crescendo",
		slot = SpellSlot.R,
		type = "linear",
		speed = 2400,
		range = 1000,
		delay = 0.25,
		radius = 140,
		danger = 4,
		cc = true,
		collision = false,
		windwall = true,
		hitbox = false
	},
	["SorakaQ"] = {
		charName = "Soraka",
		displayName = "Starcall",
		slot = SpellSlot.Q,
		type = "circular",
		speed = 1150,
		range = 810,
		delay = 0.25,
		radius = 235,
		danger = 2,
		cc = true,
		collision = false,
		windwall = false,
		hitbox = false
	},
	["SwainQ"] = {
		charName = "Swain",
		displayName = "Death's Hand",
		slot = SpellSlot.Q,
		type = "conic",
		speed = 5000,
		range = 725,
		delay = 0.25,
		radius = 0,
		angle = 45,
		danger = 2,
		cc = false,
		collision = false,
		windwall = true,
		hitbox = false
	},
	["SwainW"] = {
		charName = "Swain",
		displayName = "Vision of Empire",
		slot = SpellSlot.W,
		type = "circular",
		speed = n,
		range = 3500,
		delay = 1.5,
		radius = 300,
		danger = 1,
		cc = true,
		collision = false,
		windwall = false,
		hitbox = false
	},
	["SwainE"] = {
		charName = "Swain",
		displayName = "Nevermove",
		slot = SpellSlot.E,
		type = "linear",
		speed = 1800,
		range = 850,
		delay = 0.25,
		radius = 85,
		danger = 2,
		cc = true,
		collision = false,
		windwall = true,
		hitbox = false
	},
	["SyndraQ"] = {
		charName = "Syndra",
		displayName = "Dark Sphere",
		slot = SpellSlot.Q,
		type = "circular",
		speed = n,
		range = 800,
		delay = 0.625,
		radius = 200,
		cc = false,
		collision = false,
		windwall = false,
		hitbox = false
	},
	["SyndraWCast"] = {
		charName = "Syndra",
		displayName = "Force of Will",
		slot = SpellSlot.W,
		type = "circular",
		speed = 1450,
		range = 950,
		delay = 0.25,
		radius = 225,
		cc = true,
		collision = false,
		windwall = false,
		hitbox = false
	},
	["SyndraE"] = {
		charName = "Syndra",
		displayName = "Scatter the Weak",
		slot = SpellSlot.E,
		type = "conic",
		speed = 1600,
		range = 700,
		delay = 0.25,
		radius = 0,
		angle = 40,
		danger = 3,
		cc = true,
		collision = false,
		windwall = true,
		hitbox = false
	},
	["TahmKenchQ"] = {
		charName = "TahmKench",
		displayName = "Tongue Lash",
		slot = SpellSlot.Q,
		type = "linear",
		speed = 2800,
		range = 800,
		delay = 0.25,
		radius = 70,
		danger = 2,
		cc = true,
		collision = true,
		windwall = true,
		hitbox = false
	},
	["TaliyahQ"] = {
		charName = "Taliyah",
		displayName = "Threaded Volley",
		slot = SpellSlot.Q,
		type = "linear",
		speed = 3600,
		range = 1000,
		delay = 0.25,
		radius = 100,
		danger = 2,
		cc = false,
		collision = true,
		windwall = true,
		hitbox = false
	},
	["TaliyahWVC"] = {
		charName = "Taliyah",
		displayName = "Seismic Shove",
		slot = SpellSlot.W,
		type = "circular",
		speed = n,
		range = 900,
		delay = 0.85,
		radius = 150,
		danger = 1,
		cc = true,
		collision = false,
		windwall = false,
		hitbox = false
	},
	["TaliyahE"] = {
		charName = "Taliyah",
		displayName = "Unraveled Earth",
		slot = SpellSlot.E,
		type = "conic",
		speed = 2000,
		range = 800,
		delay = 0.45,
		radius = 0,
		angle = 80,
		danger = 2,
		cc = true,
		collision = false,
		windwall = false,
		hitbox = false
	},
	["TaliyahR"] = {
		charName = "Taliyah",
		displayName = "Weaver's Wall",
		slot = SpellSlot.R,
		type = "linear",
		speed = 1700,
		range = 3000,
		delay = 1,
		radius = 120,
		danger = 1,
		cc = true,
		collision = false,
		windwall = false,
		hitbox = false
	},
	["TalonW"] = {
		charName = "Talon",
		displayName = "Rake",
		slot = SpellSlot.W,
		type = "conic",
		speed = 2500,
		range = 650,
		delay = 0.25,
		radius = 75,
		angle = 26,
		danger = 2,
		cc = true,
		collision = false,
		windwall = true,
		hitbox = false
	},
	["ThreshQ"] = {
		charName = "Thresh",
		displayName = "Death Sentence",
		slot = SpellSlot.Q,
		type = "linear",
		speed = 1900,
		range = 1100,
		delay = 0.5,
		radius = 70,
		danger = 1,
		cc = true,
		collision = true,
		windwall = true,
		hitbox = false
	},
	["ThreshEFlay"] = {
		charName = "Thresh",
		displayName = "Flay",
		slot = SpellSlot.E,
		type = "linear",
		speed = n,
		range = 500,
		delay = 0.389,
		radius = 110,
		danger = 3,
		cc = true,
		collision = true,
		windwall = false,
		hitbox = false
	},
	["TristanaW"] = {
		charName = "Tristana",
		displayName = "Rocket Jump",
		slot = SpellSlot.W,
		type = "circular",
		speed = 1100,
		range = 900,
		delay = 0.25,
		radius = 300,
		danger = 2,
		cc = true,
		collision = false,
		windwall = false,
		hitbox = false
	},
	["TryndamereE"] = {
		charName = "Tryndamere",
		displayName = "Spinning Slash",
		slot = SpellSlot.E,
		type = "linear",
		speed = 1300,
		range = 660,
		delay = 0,
		radius = 225,
		danger = 2,
		cc = false,
		collision = false,
		windwall = false,
		hitbox = false
	},
	["WildCards"] = {
		charName = "TwistedFate",
		displayName = "Wild Cards",
		slot = SpellSlot.Q,
		type = "threeway",
		speed = 1000,
		range = 1450,
		delay = 0.25,
		radius = 40,
		angle = 28,
		danger = 1,
		cc = false,
		collision = false,
		windwall = true,
		hitbox = false
	},
	["UrgotQ"] = {
		charName = "Urgot",
		displayName = "Corrosive Charge",
		slot = SpellSlot.Q,
		type = "circular",
		speed = n,
		range = 800,
		delay = 0.6,
		radius = 180,
		danger = 2,
		cc = true,
		collision = false,
		windwall = true,
		hitbox = false
	},
	["UrgotE"] = {
		charName = "Urgot",
		displayName = "Disdain",
		slot = SpellSlot.E,
		type = "linear",
		speed = 1500,
		range = 475,
		delay = 0.45,
		radius = 100,
		danger = 2,
		cc = true,
		collision = false,
		windwall = false,
		hitbox = false
	},
	["UrgotR"] = {
		charName = "Urgot",
		displayName = "Fear Beyond Death",
		slot = SpellSlot.R,
		type = "linear",
		speed = 3200,
		range = 1600,
		delay = 0.5,
		radius = 80,
		danger = 4,
		cc = true,
		collision = false,
		windwall = true,
		hitbox = false
	},
	["VarusE"] = {
		charName = "Varus",
		displayName = "Hail of Arrows",
		slot = SpellSlot.E,
		type = "linear",
		speed = 1500,
		range = 925,
		delay = 0.242,
		radius = 260,
		danger = 3,
		cc = true,
		collision = false,
		windwall = true,
		hitbox = false
	},
	["VarusR"] = {
		charName = "Varus",
		displayName = "Chain of Corruption",
		slot = SpellSlot.R,
		type = "linear",
		speed = 1950,
		range = 1200,
		delay = 0.25,
		radius = 120,
		danger = 4,
		cc = true,
		collision = false,
		windwall = true,
		hitbox = false
	},
	["VeigarBalefulStrike"] = {
		charName = "Veigar",
		displayName = "Baleful Strike",
		slot = SpellSlot.Q,
		type = "linear",
		speed = 2200,
		range = 900,
		delay = 0.25,
		radius = 70,
		danger = 2,
		cc = false,
		collision = true,
		windwall = true,
		hitbox = false
	},
	["VeigarDarkMatter"] = {
		charName = "Veigar",
		displayName = "Dark Matter",
		slot = SpellSlot.W,
		type = "circular",
		speed = n,
		range = 900,
		delay = 1.25,
		radius = 200,
		danger = 1,
		cc = false,
		collision = false,
		windwall = false,
		hitbox = false
	},
	["VeigarEventHorizon"] = {
		charName = "Veigar",
		displayName = "Event Horizon",
		slot = SpellSlot.E,
		type = "annular",
		speed = n,
		range = 700,
		delay = 1,
		radius = 375,
		danger = 2,
		cc = true,
		collision = false,
		windwall = false,
		hitbox = false
	},
	["VelkozQ"] = {
		charName = "Velkoz",
		displayName = "Plasma Fission",
		slot = SpellSlot.Q,
		type = "linear",
		speed = 1300,
		range = 1050,
		delay = 0.25,
		radius = 50,
		danger = 1,
		cc = true,
		collision = true,
		windwall = true,
		hitbox = false
	},
	["VelkozW"] = {
		charName = "Velkoz",
		displayName = "Void Rift",
		slot = SpellSlot.W,
		type = "linear",
		speed = 1700,
		range = 1050,
		delay = 0.25,
		radius = 87.5,
		danger = 1,
		cc = false,
		collision = false,
		windwall = true,
		hitbox = false
	},
	["VelkozE"] = {
		charName = "Velkoz",
		displayName = "Tectonic Disruption",
		slot = SpellSlot.E,
		type = "circular",
		speed = n,
		range = 800,
		delay = 0.8,
		radius = 185,
		danger = 2,
		cc = true,
		collision = false,
		windwall = false,
		hitbox = false
	},
	["ViQ"] = {
		charName = "Vi",
		displayName = "Vault Breaker",
		slot = SpellSlot.Q,
		type = "linear",
		speed = 1500,
		range = 725,
		delay = 0,
		radius = 90,
		danger = 2,
		cc = true,
		collision = false,
		windwall = false,
		hitbox = false
	},
	["ViktorGravitonField"] = {
		charName = "Viktor",
		displayName = "Graviton Field",
		slot = SpellSlot.W,
		type = "circular",
		speed = n,
		range = 800,
		delay = 1.75,
		radius = 270,
		danger = 1,
		cc = true,
		collision = false,
		windwall = false,
		hitbox = false
	},
	["ViktorDeathRay"] = {
		charName = "Viktor",
		displayName = "Death Ray",
		slot = SpellSlot.E,
		type = "linear",
		speed = 1050,
		range = 1025,
		delay = 0,
		radius = 80,
		danger = 2,
		cc = false,
		collision = false,
		windwall = true,
		hitbox = false
	},
	["VladimirHemoplague"] = {
		charName = "Vladimir",
		displayName = "Hemoplague",
		slot = SpellSlot.R,
		type = "circular",
		speed = n,
		range = 700,
		delay = 0.389,
		radius = 350,
		danger = 3,
		cc = false,
		collision = false,
		windwall = false,
		hitbox = false
	},
	["WarwickR"] = {
		charName = "Warwick",
		displayName = "Infinite Duress",
		slot = SpellSlot.R,
		type = "linear",
		speed = 1800,
		range = 3000,
		delay = 0.1,
		radius = 55,
		danger = 4,
		cc = true,
		collision = false,
		windwall = false,
		hitbox = false
	},
	["XayahQ"] = {
		charName = "Xayah",
		displayName = "Double Daggers",
		slot = SpellSlot.Q,
		type = "linear",
		speed = 2075,
		range = 1100,
		delay = 0.5,
		radius = 45,
		danger = 1,
		cc = false,
		collision = false,
		windwall = true,
		hitbox = false
	},
	["XerathArcanopulse2"] = {
		charName = "Xerath",
		displayName = "Arcanopulse",
		slot = SpellSlot.Q,
		type = "linear",
		speed = n,
		range = 1400,
		delay = 0.5,
		radius = 90,
		danger = 2,
		cc = false,
		collision = false,
		windwall = false,
		hitbox = false
	},
	["XerathArcaneBarrage2"] = {
		charName = "Xerath",
		displayName = "Arcane Barrage",
		slot = SpellSlot.W,
		type = "circular",
		speed = n,
		range = 1000,
		delay = 0.75,
		radius = 235,
		danger = 3,
		cc = true,
		collision = false,
		windwall = false,
		hitbox = false
	},
	["XerathMageSpear"] = {
		charName = "Xerath",
		displayName = "Mage Spear",
		slot = SpellSlot.E,
		type = "linear",
		speed = 1400,
		range = 1050,
		delay = 0.2,
		radius = 60,
		danger = 1,
		cc = true,
		collision = true,
		windwall = true,
		hitbox = false
	},
	["XerathRMissileWrapper"] = {
		charName = "Xerath",
		displayName = "Rite of the Arcane",
		slot = SpellSlot.R,
		type = "circular",
		speed = n,
		range = 6160,
		delay = 0.7,
		radius = 200,
		danger = 3,
		cc = false,
		collision = false,
		windwall = false,
		hitbox = false
	},
	["XinZhaoW"] = {
		charName = "XinZhao",
		displayName = "Wind Becomes Lightning",
		slot = SpellSlot.W,
		type = "linear",
		speed = 5000,
		range = 900,
		delay = 0.5,
		radius = 40,
		danger = 1,
		cc = true,
		collision = false,
		windwall = false,
		hitbox = false
	},
	["YasuoQ1Wrapper"] = {
		charName = "Yasuo",
		displayName = "Steel Tempest",
		slot = SpellSlot.Q,
		type = "linear",
		speed = n,
		range = 475,
		delay = 0.339,
		radius = 40,
		danger = 1,
		cc = false,
		collision = false,
		windwall = false,
		hitbox = false
	},
	["YasuoQ2Wrapper"] = {
		charName = "Yasuo",
		displayName = "Steel Wind Rising",
		slot = SpellSlot.Q,
		type = "linear",
		speed = n,
		range = 475,
		delay = 0.339,
		radius = 40,
		danger = 1,
		cc = false,
		collision = false,
		windwall = false,
		hitbox = false
	},
	["YasuoQ3Wrapper"] = {
		charName = "Yasuo",
		displayName = "Gathering Storm",
		slot = SpellSlot.Q,
		type = "linear",
		speed = 1200,
		range = 1000,
		delay = 0.339,
		radius = 90,
		danger = 2,
		cc = true,
		collision = false,
		windwall = true,
		hitbox = false
	},
	["ZacQ"] = {
		charName = "Zac",
		displayName = "Stretching Strikes",
		slot = SpellSlot.Q,
		type = "linear",
		speed = 2800,
		range = 800,
		delay = 0.33,
		radius = 120,
		danger = 2,
		cc = true,
		collision = false,
		windwall = true,
		hitbox = false
	},
	["ZacE"] = {
		charName = "Zac",
		displayName = "Elastic Slingshot",
		slot = SpellSlot.E,
		type = "circular",
		speed = 1330,
		range = 1800,
		delay = 0,
		radius = 300,
		danger = 3,
		cc = true,
		collision = false,
		windwall = false,
		hitbox = false
	},
	["ZedQ"] = {
		charName = "Zed",
		displayName = "Razor Shuriken",
		slot = SpellSlot.Q,
		type = "linear",
		speed = 1700,
		range = 900,
		delay = 0.25,
		radius = 50,
		danger = 1,
		cc = false,
		collision = false,
		windwall = true,
		hitbox = false
	},
	["ZiggsQ"] = {
		charName = "Ziggs",
		displayName = "Bouncing Bomb",
		slot = SpellSlot.Q,
		type = "circular",
		speed = 1700,
		range = 850,
		delay = 0.25,
		radius = 120,
		danger = 1,
		cc = false,
		collision = true,
		windwall = true,
		hitbox = false
	},
	["ZiggsW"] = {
		charName = "Ziggs",
		displayName = "Satchel Charge",
		slot = SpellSlot.W,
		type = "circular",
		speed = 1750,
		range = 1000,
		delay = 0.25,
		radius = 240,
		danger = 2,
		cc = true,
		collision = false,
		windwall = true,
		hitbox = false
	},
	["ZiggsE"] = {
		charName = "Ziggs",
		displayName = "Hexplosive Minefield",
		slot = SpellSlot.E,
		type = "circular",
		speed = 1800,
		range = 900,
		delay = 0.25,
		radius = 250,
		danger = 2,
		cc = true,
		collision = false,
		windwall = true,
		hitbox = false
	},
	["ZiggsR"] = {
		charName = "Ziggs",
		displayName = "Mega Inferno Bomb",
		slot = SpellSlot.R,
		type = "circular",
		speed = 1550,
		range = 5000,
		delay = 0.375,
		radius = 480,
		danger = 4,
		cc = false,
		collision = false,
		windwall = false,
		hitbox = false
	},
	["ZileanQ"] = {
		charName = "Zilean",
		displayName = "Time Bomb",
		slot = SpellSlot.Q,
		type = "circular",
		speed = n,
		range = 900,
		delay = 0.8,
		radius = 150,
		danger = 2,
		cc = true,
		collision = false,
		windwall = true,
		hitbox = false
	},
	["ZoeQ"] = {
		charName = "Zoe",
		displayName = "Paddle Star [First]",
		slot = SpellSlot.Q,
		type = "linear",
		speed = 1200,
		range = 800,
		delay = 0.25,
		radius = 50,
		danger = 1,
		cc = false,
		collision = true,
		windwall = true,
		hitbox = false
	},
	["ZoeQRecast"] = {
		charName = "Zoe",
		displayName = "Paddle Star [Second]",
		slot = SpellSlot.Q,
		type = "linear",
		speed = 2500,
		range = 1600,
		delay = 0,
		radius = 70,
		danger = 2,
		cc = false,
		collision = true,
		windwall = true,
		hitbox = false
	},
	["ZoeE"] = {
		charName = "Zoe",
		displayName = "Sleepy Trouble Bubble",
		slot = SpellSlot.E,
		type = "linear",
		speed = 1700,
		range = 800,
		delay = 0.3,
		radius = 50,
		danger = 2,
		cc = true,
		collision = true,
		windwall = true,
		hitbox = false
	},
	["ZyraQ"] = {
		charName = "Zyra",
		displayName = "Deadly Spines",
		slot = SpellSlot.Q,
		type = "rectangular",
		speed = n,
		range = 800,
		delay = 0.825,
		radius2 = 400,
		radius = 200,
		danger = 1,
		cc = false,
		collision = false,
		windwall = false,
		hitbox = false
	},
	["ZyraE"] = {
		charName = "Zyra",
		displayName = "Grasping Roots",
		slot = SpellSlot.E,
		type = "linear",
		speed = 1150,
		range = 1100,
		delay = 0.25,
		radius = 70,
		danger = 1,
		cc = true,
		collision = false,
		windwall = true,
		hitbox = false
	},
	["ZyraR"] = {
		charName = "Zyra",
		displayName = "Stranglethorns",
		slot = SpellSlot.R,
		type = "circular",
		speed = n,
		range = 700,
		delay = 2,
		radius = 500,
		danger = 4,
		cc = true,
		collision = false,
		windwall = false,
		hitbox = false
	}
}
local B = {
	["AatroxW="] = {
		charName = "Aatrox",
		displayName = "Infernal Chains [Missile]",
		spellName = "AatroxW",
		slot = SpellSlot.W,
		type = "linear",
		speed = 1800,
		range = 825,
		radius = 80,
		danger = 2,
		cc = true,
		collision = true,
		windwall = true
	},
	["AhriOrbMissile="] = {
		charName = "Ahri",
		displayName = "Orb of Deception [Missile]",
		spellName = "AhriOrbMissile",
		slot = SpellSlot.Q,
		type = "linear",
		speed = 2500,
		range = 880,
		radius = 100,
		danger = 2,
		cc = false,
		collision = false,
		windwall = true
	},
	["AhriSeduceMissile="] = {
		charName = "Ahri",
		displayName = "Seduce [Missile]",
		spellName = "AhriSeduceMissile",
		slot = SpellSlot.E,
		type = "linear",
		speed = 1500,
		range = 975,
		radius = 60,
		danger = 1,
		cc = true,
		collision = true,
		windwall = true
	},
	["AkaliEMis="] = {
		charName = "Akali",
		displayName = "Shuriken Flip [Missile]",
		spellName = "AkaliEMis",
		slot = SpellSlot.E,
		type = "linear",
		speed = 1800,
		range = 825,
		radius = 70,
		danger = 2,
		cc = false,
		collision = true,
		windwall = true
	},
	["SadMummyBandageToss="] = {
		charName = "Amumu",
		displayName = "Bandage Toss [Missile]",
		spellName = "SadMummyBandageToss",
		slot = SpellSlot.Q,
		type = "linear",
		speed = 2000,
		range = 1100,
		radius = 80,
		danger = 3,
		cc = true,
		collision = true,
		windwall = true
	},
	["FlashFrostSpell="] = {
		charName = "Anivia",
		displayName = "Flash Frost [Missile]",
		spellName = "FlashFrostSpell",
		slot = SpellSlot.Q,
		type = "linear",
		speed = 850,
		range = 1100,
		radius = 110,
		danger = 2,
		cc = true,
		collision = false,
		windwall = true
	},
	["VolleyRightAttack="] = {
		charName = "Ashe",
		displayName = "Volley [Missile]",
		spellName = "VolleyRightAttack",
		slot = SpellSlot.W,
		type = "conic",
		speed = 2000,
		range = 1200,
		radius = 20,
		angle = 40,
		danger = 2,
		cc = true,
		collision = true,
		windwall = true
	},
	["EnchantedCrystalArrow="] = {
		charName = "Ashe",
		displayName = "Enchanted Crystal Arrow [Missile]",
		spellName = "EnchantedCrystalArrow",
		slot = SpellSlot.R,
		type = "linear",
		speed = 1600,
		range = 12500,
		radius = 130,
		danger = 4,
		cc = true,
		collision = false,
		windwall = true
	},
	["AurelionSolQMissile="] = {
		charName = "AurelionSol",
		displayName = "Starsurge [Missile]",
		spellName = "AurelionSolQMissile",
		slot = SpellSlot.Q,
		type = "linear",
		speed = 850,
		range = 1075,
		radius = 110,
		danger = 2,
		cc = true,
		collision = false,
		windwall = true
	},
	["BardQMissile="] = {
		charName = "Bard",
		displayName = "Cosmic Binding [Missile]",
		spellName = "BardQMissile",
		slot = SpellSlot.Q,
		type = "linear",
		speed = 1500,
		range = 950,
		radius = 60,
		danger = 2,
		cc = true,
		collision = true,
		windwall = true
	},
	["BardRMissile="] = {
		charName = "Bard",
		displayName = "Tempered Fate [Missile]",
		spellName = "BardRMissile",
		slot = SpellSlot.R,
		type = "circular",
		speed = 2100,
		range = 3400,
		radius = 350,
		danger = 2,
		cc = true,
		collision = false,
		windwall = false
	},
	["RocketGrabMissile="] = {
		charName = "Blitzcrank",
		displayName = "Rocket Grab [Missile]",
		spellName = "RocketGrabMissile",
		slot = SpellSlot.Q,
		type = "linear",
		speed = 1800,
		range = 925,
		radius = 70,
		danger = 3,
		cc = true,
		collision = true,
		windwall = true
	},
	["BrandQMissile="] = {
		charName = "Brand",
		displayName = "Sear [Missile]",
		spellName = "BrandQMissile",
		slot = SpellSlot.Q,
		type = "linear",
		speed = 1600,
		range = 1050,
		radius = 60,
		danger = 1,
		cc = false,
		collision = true,
		windwall = true
	},
	["BraumQMissile="] = {
		charName = "Braum",
		displayName = "Winter's Bite [Missile]",
		spellName = "BraumQMissile",
		slot = SpellSlot.Q,
		type = "linear",
		speed = 1700,
		range = 1000,
		radius = 70,
		danger = 3,
		cc = true,
		collision = true,
		windwall = true
	},
	["CaitlynPiltoverPeacemaker="] = {
		charName = "Caitlyn",
		displayName = "Piltover Peacemaker [Missile]",
		spellName = "CaitlynPiltoverPeacemaker",
		slot = SpellSlot.Q,
		type = "linear",
		speed = 2200,
		range = 1250,
		radius = 60,
		radius2 = 90,
		danger = 1,
		cc = false,
		collision = false,
		windwall = true
	},
	["CaitlynEntrapment="] = {
		charName = "Caitlyn",
		displayName = "Entrapment [Missile]",
		spellName = "CaitlynEntrapment",
		slot = SpellSlot.E,
		type = "linear",
		speed = 1600,
		range = 750,
		radius = 70,
		danger = 2,
		cc = true,
		collision = true,
		windwall = true
	},
	["CamilleEMissile="] = {
		charName = "Camille",
		displayName = "Hookshot [Missile]",
		spellName = "CamilleEMissile",
		slot = SpellSlot.E,
		type = "linear",
		speed = 1900,
		range = 800,
		radius = 60,
		danger = 1,
		cc = false,
		collision = false,
		windwall = true
	},
	["PhosphorusBombMissile="] = {
		charName = "Corki",
		displayName = "Phosphorus Bomb [Missile]",
		spellName = "PhosphorusBombMissile",
		slot = SpellSlot.Q,
		type = "circular",
		speed = 1000,
		range = 825,
		radius = 250,
		danger = 2,
		cc = false,
		collision = false,
		windwall = true
	},
	["MissileBarrageMissile="] = {
		charName = "Corki",
		displayName = "Missile Barrage [Standard, Missile]",
		spellName = "MissileBarrageMissile",
		slot = SpellSlot.R,
		type = "linear",
		speed = 2000,
		range = 1300,
		radius = 40,
		danger = 1,
		cc = false,
		collision = true,
		windwall = true
	},
	["MissileBarrageMissile2="] = {
		charName = "Corki",
		displayName = "Missile Barrage [Big, Missile]",
		spellName = "MissileBarrageMissile2",
		slot = SpellSlot.R,
		type = "linear",
		speed = 2000,
		range = 1500,
		radius = 40,
		danger = 1,
		cc = false,
		collision = true,
		windwall = true
	},
	["DravenDoubleShotMissile="] = {
		charName = "Draven",
		displayName = "Double Shot [Missile]",
		spellName = "DravenDoubleShotMissile",
		slot = SpellSlot.E,
		type = "linear",
		speed = 1600,
		range = 1050,
		radius = 130,
		danger = 3,
		cc = true,
		collision = false,
		windwall = true
	},
	["DravenR="] = {
		charName = "Draven",
		displayName = "Whirling Death [Missile]",
		spellName = "DravenR",
		slot = SpellSlot.R,
		type = "linear",
		speed = 2000,
		range = 12500,
		radius = 160,
		danger = 4,
		cc = false,
		collision = false,
		windwall = true
	},
	["InfectedCleaverMissile="] = {
		charName = "DrMundo",
		displayName = "Infected Cleaver [Missile]",
		spellName = "InfectedCleaverMissile",
		slot = SpellSlot.Q,
		type = "linear",
		speed = 2000,
		range = 975,
		radius = 60,
		danger = 2,
		cc = true,
		collision = true,
		windwall = true
	},
	["EkkoQMis="] = {
		charName = "Ekko",
		displayName = "Timewinder [Missile]",
		spellName = "EkkoQMis",
		slot = SpellSlot.Q,
		type = "linear",
		speed = 1650,
		range = 1175,
		radius = 60,
		danger = 1,
		cc = true,
		collision = false,
		windwall = true
	},
	["EliseHumanE="] = {
		charName = "Elise",
		displayName = "Cocoon [Missile]",
		spellName = "EliseHumanE",
		slot = SpellSlot.E,
		type = "linear",
		speed = 1600,
		range = 1075,
		radius = 55,
		danger = 1,
		cc = true,
		collision = true,
		windwall = true
	},
	["EvelynnQ="] = {
		charName = "Evelynn",
		displayName = "Hate Spike [Missile]",
		spellName = "EvelynnQ",
		slot = SpellSlot.Q,
		type = "linear",
		speed = 2400,
		range = 800,
		radius = 60,
		danger = 2,
		cc = false,
		collision = true,
		windwall = true
	},
	["EzrealQ="] = {
		charName = "Ezreal",
		displayName = "Mystic Shot [Missile]",
		spellName = "EzrealQ",
		slot = SpellSlot.Q,
		type = "linear",
		speed = 2000,
		range = 1150,
		radius = 60,
		danger = 1,
		cc = false,
		collision = true,
		windwall = true
	},
	["EzrealW="] = {
		charName = "Ezreal",
		displayName = "Essence Flux [Missile]",
		spellName = "EzrealW",
		slot = SpellSlot.W,
		type = "linear",
		speed = 2000,
		range = 1150,
		radius = 60,
		danger = 1,
		cc = false,
		collision = false,
		windwall = true
	},
	["EzrealR="] = {
		charName = "Ezreal",
		displayName = "Trueshot Barrage [Missile]",
		spellName = "EzrealR",
		slot = SpellSlot.R,
		type = "linear",
		speed = 2000,
		range = 12500,
		radius = 160,
		danger = 4,
		cc = false,
		collision = false,
		windwall = true
	},
	["FizzRMissile="] = {
		charName = "Fizz",
		displayName = "Chum the Waters [Missile]",
		spellName = "FizzRMissile",
		slot = SpellSlot.R,
		type = "linear",
		speed = 1300,
		range = 1300,
		radius = 150,
		danger = 5,
		cc = true,
		collision = false,
		windwall = true
	},
	["GalioQMissileR="] = {
		charName = "Galio",
		displayName = "Winds of War [Missile]",
		spellName = "GalioQMissileR",
		slot = SpellSlot.Q,
		type = "circular",
		speed = 1150,
		range = 825,
		radius = 235,
		danger = 2,
		cc = false,
		collision = false,
		windwall = true
	},
	["GnarQMissile="] = {
		charName = "Gnar",
		displayName = "Boomerang Throw [Missile]",
		spellName = "GnarQMissile",
		slot = SpellSlot.Q,
		type = "linear",
		speed = 2500,
		range = 1125,
		radius = 55,
		danger = 2,
		cc = true,
		collision = false,
		windwall = true
	},
	["GnarBigQMissile="] = {
		charName = "Gnar",
		displayName = "Boulder Toss [Missile]",
		spellName = "GnarBigQMissile",
		slot = SpellSlot.Q,
		type = "linear",
		speed = 2100,
		range = 1125,
		radius = 90,
		danger = 2,
		cc = true,
		collision = true,
		windwall = true
	},
	["GragasQMissile="] = {
		charName = "Gragas",
		displayName = "Barrel Roll [Missile]",
		spellName = "GragasQMissile",
		slot = SpellSlot.Q,
		type = "circular",
		speed = 1000,
		range = 850,
		radius = 275,
		danger = 2,
		cc = true,
		collision = false,
		windwall = true
	},
	["GragasRBoom="] = {
		charName = "Gragas",
		displayName = "Explosive Cask [Missile]",
		spellName = "GragasRBoom",
		slot = SpellSlot.R,
		type = "circular",
		speed = 1800,
		range = 1000,
		radius = 400,
		danger = 5,
		cc = true,
		collision = false,
		windwall = true
	},
	["GravesClusterShotSoundMissile="] = {
		charName = "Graves",
		displayName = "End of the Line [Missile]",
		spellName = "GravesClusterShotSoundMissile",
		slot = SpellSlot.Q,
		type = "linear",
		speed = 2000,
		range = 700,
		radius = 20,
		danger = 1,
		cc = false,
		collision = false,
		windwall = true
	},
	["GravesSmokeGrenadeBoom="] = {
		charName = "Graves",
		displayName = "Smoke Grenade [Missile]",
		spellName = "GravesSmokeGrenadeBoom",
		slot = SpellSlot.W,
		type = "circular",
		speed = 1500,
		range = 950,
		radius = 250,
		danger = 2,
		cc = true,
		collision = false,
		windwall = true
	},
	["GravesChargeShotShot="] = {
		charName = "Graves",
		displayName = "Charge Shot [Missile]",
		spellName = "GravesChargeShotShot",
		slot = SpellSlot.R,
		type = "linear",
		speed = 2100,
		range = 1000,
		radius = 100,
		danger = 5,
		cc = false,
		collision = false,
		windwall = true
	},
	["HecarimUltMissile="] = {
		charName = "Hecarim",
		displayName = "Onslaught of Shadows [Missile]",
		spellName = "HecarimUltMissile",
		slot = SpellSlot.R,
		type = "linear",
		speed = 1100,
		range = 1650,
		radius = 280,
		danger = 4,
		cc = true,
		collision = false,
		windwall = false
	},
	["HeimerdingerWAttack2="] = {
		charName = "Heimerdinger",
		displayName = "Hextech Micro-Rockets [Missile]",
		spellName = "HeimerdingerWAttack2",
		slot = SpellSlot.W,
		type = "linear",
		speed = 2050,
		range = 1325,
		radius = 100,
		danger = 2,
		cc = false,
		collision = false,
		windwall = true
	},
	["HeimerdingerESpell="] = {
		charName = "Heimerdinger",
		displayName = "CH-2 Electron Storm Grenade [Missile]",
		spellName = "HeimerdingerESpell",
		slot = SpellSlot.E,
		type = "circular",
		speed = 1200,
		range = 970,
		radius = 250,
		danger = 2,
		cc = true,
		collision = false,
		windwall = true
	},
	["HeimerdingerESpell_ult="] = {
		charName = "Heimerdinger",
		displayName = "CH-2 Electron Storm Grenade [Missile]",
		spellName = "HeimerdingerESpell_ult",
		slot = SpellSlot.E,
		type = "circular",
		speed = 1200,
		range = 970,
		radius = 250,
		danger = 3,
		cc = true,
		collision = false,
		windwall = true
	},
	["IllaoiEMis="] = {
		charName = "Illaoi",
		displayName = "Test of Spirit [Missile]",
		spellName = "IllaoiEMis",
		slot = SpellSlot.E,
		type = "linear",
		speed = 1900,
		range = 900,
		radius = 50,
		danger = 1,
		cc = false,
		collision = true,
		windwall = true
	},
	["IreliaR="] = {
		charName = "Irelia",
		displayName = "Vanguard's Edge [Missile]",
		spellName = "IreliaR",
		slot = SpellSlot.R,
		type = "linear",
		speed = 2000,
		range = 950,
		radius = 160,
		danger = 4,
		cc = true,
		collision = false,
		windwall = true
	},
	["IvernQ="] = {
		charName = "Ivern",
		displayName = "Rootcaller [Missile]",
		spellName = "IvernQ",
		slot = SpellSlot.Q,
		type = "linear",
		speed = 1300,
		range = 1075,
		radius = 80,
		danger = 1,
		cc = true,
		collision = true,
		windwall = true
	},
	["HowlingGaleSpell="] = {
		charName = "Janna",
		displayName = "Howling Gale [Missile]",
		spellName = "HowlingGaleSpell",
		slot = SpellSlot.Q,
		type = "linear",
		speed = 667,
		range = 1750,
		radius = 100,
		danger = 2,
		cc = true,
		collision = false,
		windwall = true
	},
	["JayceShockBlastMis="] = {
		charName = "Jayce",
		displayName = "Shock Blast [Missile]",
		spellName = "JayceShockBlastMis",
		slot = SpellSlot.Q,
		type = "linear",
		speed = 1450,
		range = 1050,
		radius = 70,
		danger = 1,
		cc = false,
		collision = true,
		windwall = true
	},
	["JhinETrap="] = {
		charName = "Jhin",
		displayName = "Captive Audience [Missile]",
		spellName = "JhinETrap",
		slot = SpellSlot.E,
		type = "circular",
		speed = 1600,
		range = 750,
		radius = 130,
		danger = 1,
		cc = true,
		collision = false,
		windwall = false
	},
	["JhinRShotMis="] = {
		charName = "Jhin",
		displayName = "Curtain Call [Missile]",
		spellName = "JhinRShotMis",
		slot = SpellSlot.R,
		type = "linear",
		speed = 5000,
		range = 3500,
		radius = 80,
		danger = 2,
		cc = true,
		collision = false,
		windwall = true
	},
	["JinxWMissile="] = {
		charName = "Jinx",
		displayName = "Zap! [Missile]",
		spellName = "JinxWMissile",
		slot = SpellSlot.W,
		type = "linear",
		speed = 3300,
		range = 1450,
		radius = 60,
		danger = 1,
		cc = true,
		collision = true,
		windwall = true
	},
	["JinxEHit="] = {
		charName = "Jinx",
		displayName = "Flame Chompers! [Missile]",
		spellName = "JinxEHit",
		slot = SpellSlot.E,
		type = "circular",
		speed = 1100,
		range = 900,
		radius = 120,
		danger = 1,
		cc = true,
		collision = false,
		windwall = true
	},
	["JinxR="] = {
		charName = "Jinx",
		displayName = "Super Mega Death Rocket! [Missile]",
		spellName = "JinxR",
		slot = SpellSlot.R,
		type = "linear",
		speed = 1700,
		range = 12500,
		radius = 140,
		danger = 4,
		cc = false,
		collision = false,
		windwall = true
	},
	["KaisaW="] = {
		charName = "Kaisa",
		displayName = "Void Seeker [Missile]",
		spellName = "KaisaW",
		slot = SpellSlot.W,
		type = "linear",
		speed = 1750,
		range = 3000,
		radius = 100,
		danger = 1,
		cc = false,
		collision = true,
		windwall = true
	},
	["KalistaMysticShotMisTrue="] = {
		charName = "Kalista",
		displayName = "Pierce [Missile]",
		spellName = "KalistaMysticShotMisTrue",
		slot = SpellSlot.Q,
		type = "linear",
		speed = 2400,
		range = 1150,
		radius = 40,
		danger = 1,
		cc = false,
		collision = true,
		windwall = true
	},
	["KarmaQMissile="] = {
		charName = "Karma",
		displayName = "Inner Flame [Missile]",
		spellName = "KarmaQMissile",
		slot = SpellSlot.Q,
		type = "linear",
		speed = 1700,
		range = 950,
		radius = 60,
		danger = 2,
		cc = true,
		collision = true,
		windwall = true
	},
	["KarmaQMissileMantra="] = {
		charName = "Karma",
		displayName = "Inner Flame [Mantra, Missile]",
		spellName = "KarmaQMissileMantra",
		slot = SpellSlot.Q,
		origin = "linear",
		type = "linear",
		speed = 1700,
		range = 950,
		radius = 80,
		danger = 2,
		cc = true,
		collision = true,
		windwall = true
	},
	["KayleQMisVFX="] = {
		charName = "Kayle",
		displayName = "Radiant Blast [Missile]",
		spellName = "KayleQMisVFX",
		slot = SpellSlot.Q,
		type = "linear",
		speed = 2000,
		range = 850,
		radius = 60,
		danger = 1,
		cc = true,
		collision = false,
		windwall = true
	},
	["KennenShurikenHurlMissile1="] = {
		charName = "Kennen",
		displayName = "Shuriken Hurl [Missile]",
		spellName = "KennenShurikenHurlMissile1",
		slot = SpellSlot.Q,
		type = "linear",
		speed = 1700,
		range = 1050,
		radius = 50,
		danger = 2,
		cc = false,
		collision = true,
		windwall = true
	},
	["KhazixWMissile="] = {
		charName = "Khazix",
		displayName = "Void Spike [Missile]",
		spellName = "KhazixWMissile",
		slot = SpellSlot.W,
		type = "linear",
		speed = 1700,
		range = 1000,
		radius = 70,
		danger = 1,
		cc = false,
		collision = true,
		windwall = true
	},
	["KledQMissile="] = {
		charName = "Kled",
		displayName = "Beartrap on a Rope [Missile]",
		spellName = "KledQMissile",
		slot = SpellSlot.Q,
		type = "linear",
		speed = 1600,
		range = 800,
		radius = 45,
		danger = 1,
		cc = true,
		collision = true,
		windwall = true
	},
	["KledRiderQMissile="] = {
		charName = "Kled",
		displayName = "Pocket Pistol [Missile]",
		spellName = "KledRiderQMissile",
		slot = SpellSlot.Q,
		type = "conic",
		speed = 3000,
		range = 700,
		radius = 0,
		angle = 25,
		danger = 3,
		cc = false,
		collision = false,
		windwall = true
	},
	["KogMawQ="] = {
		charName = "KogMaw",
		displayName = "Caustic Spittle [Missile]",
		spellName = "KogMawQ",
		slot = SpellSlot.Q,
		type = "linear",
		speed = 1650,
		range = 1175,
		radius = 70,
		danger = 1,
		cc = false,
		collision = true,
		windwall = true
	},
	["KogMawVoidOozeMissile="] = {
		charName = "KogMaw",
		displayName = "Void Ooze [Missile]",
		spellName = "KogMawVoidOozeMissile",
		slot = SpellSlot.E,
		type = "linear",
		speed = 1400,
		range = 1360,
		radius = 120,
		danger = 2,
		cc = true,
		collision = false,
		windwall = true
	},
	["LeblancEMissile="] = {
		charName = "Leblanc",
		displayName = "Ethereal Chains [Standard, Missile]",
		spellName = "LeblancEMissile",
		slot = SpellSlot.E,
		type = "linear",
		speed = 1750,
		range = 925,
		radius = 55,
		danger = 1,
		cc = true,
		collision = true,
		windwall = true
	},
	["LeblancREMissile="] = {
		charName = "Leblanc",
		displayName = "Ethereal Chains [Ultimate. Missile]",
		spellName = "LeblancREMissile",
		slot = SpellSlot.E,
		type = "linear",
		speed = 1750,
		range = 925,
		radius = 55,
		danger = 1,
		cc = true,
		collision = true,
		windwall = true
	},
	["BlindMonkQOne="] = {
		charName = "LeeSin",
		displayName = "Sonic Wave [Missile]",
		spellName = "BlindMonkQOne",
		slot = SpellSlot.Q,
		type = "linear",
		speed = 1800,
		range = 1100,
		radius = 60,
		danger = 1,
		cc = false,
		collision = true,
		windwall = true
	},
	["LeonaZenithBladeMissile="] = {
		charName = "Leona",
		displayName = "Zenith Blade [Missile]",
		spellName = "LeonaZenithBladeMissile",
		slot = SpellSlot.E,
		type = "linear",
		speed = 2000,
		range = 875,
		radius = 70,
		danger = 2,
		cc = true,
		collision = false,
		windwall = true
	},
	["LissandraQMissile="] = {
		charName = "Lissandra",
		displayName = "Ice Shard [Missile]",
		spellName = "LissandraQMissile",
		slot = SpellSlot.Q,
		type = "linear",
		speed = 2200,
		range = 750,
		radius = 75,
		danger = 2,
		cc = true,
		collision = false,
		windwall = true
	},
	["LissandraEMissile="] = {
		charName = "Lissandra",
		displayName = "Glacial Path [Missile]",
		spellName = "LissandraEMissile",
		slot = SpellSlot.E,
		type = "linear",
		speed = 850,
		range = 1025,
		radius = 125,
		danger = 2,
		cc = false,
		collision = false,
		windwall = true
	},
	["LucianW="] = {
		charName = "Lucian",
		displayName = "Ardent Blaze [Missile]",
		spellName = "LucianW",
		slot = SpellSlot.W,
		type = "linear",
		speed = 1600,
		range = 900,
		radius = 80,
		danger = 2,
		cc = false,
		collision = true,
		windwall = true
	},
	["LuluQMissile="] = {
		charName = "Lulu",
		displayName = "Glitterlance [Missile]",
		spellName = "LuluQMissile",
		slot = SpellSlot.Q,
		type = "linear",
		speed = 1450,
		range = 925,
		radius = 60,
		danger = 1,
		cc = true,
		collision = false,
		windwall = true
	},
	["LuxLightBindingDummy="] = {
		charName = "Lux",
		displayName = "Light Binding [Missile]",
		spellName = "LuxLightBindingDummy",
		slot = SpellSlot.Q,
		type = "linear",
		speed = 1200,
		range = 1175,
		radius = 70,
		danger = 1,
		cc = true,
		collision = true,
		windwall = true
	},
	["LuxLightStrikeKugel="] = {
		charName = "Lux",
		displayName = "Light Strike Kugel [Missile]",
		spellName = "LuxLightStrikeKugel",
		slot = SpellSlot.E,
		type = "circular",
		speed = 1200,
		range = 1100,
		radius = 300,
		danger = 3,
		cc = true,
		collision = true,
		windwall = true
	},
	["LuxRVfxMis="] = {
		charName = "Lux",
		displayName = "Malice Cannon [Missile]",
		spellName = "LuxRVfxMis",
		slot = SpellSlot.R,
		type = "linear",
		speed = n,
		range = 3340,
		radius = 120,
		danger = 4,
		cc = false,
		collision = false,
		windwall = false
	},
	["MaokaiQMissile="] = {
		charName = "Maokai",
		displayName = "Bramble Smash [Missile]",
		spellName = "MaokaiQMissile",
		slot = SpellSlot.Q,
		type = "linear",
		speed = 1600,
		range = 600,
		radius = 110,
		danger = 2,
		cc = true,
		collision = false,
		windwall = true
	},
	["MorganaQ="] = {
		charName = "Morgana",
		displayName = "Dark Binding [Missile]",
		spellName = "MorganaQ",
		slot = SpellSlot.Q,
		type = "linear",
		speed = 1200,
		range = 1250,
		radius = 70,
		danger = 1,
		cc = true,
		collision = true,
		windwall = true
	},
	["NamiQMissile="] = {
		charName = "Nami",
		displayName = "Aqua Prison [Missile]",
		spellName = "NamiQMissile",
		slot = SpellSlot.Q,
		type = "circular",
		speed = n,
		range = 875,
		radius = 180,
		danger = 1,
		cc = true,
		collision = false,
		windwall = true
	},
	["NamiRMissile="] = {
		charName = "Nami",
		displayName = "Tidal Wave [Missile]",
		spellName = "NamiRMissile",
		slot = SpellSlot.R,
		type = "linear",
		speed = 850,
		range = 2750,
		radius = 250,
		danger = 3,
		cc = true,
		collision = false,
		windwall = true
	},
	["NautilusAnchorDragMissile="] = {
		charName = "Nautilus",
		displayName = "Dredge Line [Missile]",
		spellName = "NautilusAnchorDragMissile",
		slot = SpellSlot.Q,
		type = "linear",
		speed = 2000,
		range = 925,
		radius = 90,
		danger = 3,
		cc = true,
		collision = true,
		windwall = true
	},
	["NeekoQ="] = {
		charName = "Neeko",
		displayName = "Blooming Burst [Missile]",
		spellName = "NeekoQ",
		slot = SpellSlot.Q,
		type = "circular",
		speed = 1500,
		range = 800,
		radius = 200,
		danger = 2,
		cc = true,
		collision = false,
		windwall = true
	},
	["NeekoE="] = {
		charName = "Neeko",
		displayName = "Tangle-Barbs [Missile]",
		spellName = "NeekoE",
		slot = SpellSlot.E,
		type = "linear",
		speed = 1300,
		range = 1000,
		radius = 70,
		danger = 1,
		cc = true,
		collision = false,
		windwall = true
	},
	["JavelinToss="] = {
		charName = "Nidalee",
		displayName = "Javelin Toss [Missile]",
		spellName = "JavelinToss",
		slot = SpellSlot.Q,
		type = "linear",
		speed = 1300,
		range = 1500,
		radius = 40,
		danger = 1,
		cc = false,
		collision = true,
		windwall = true
	},
	["NocturneDuskbringer="] = {
		charName = "Nocturne",
		displayName = "Duskbringer [Missile]",
		spellName = "NocturneDuskbringer",
		slot = SpellSlot.Q,
		type = "linear",
		speed = 1600,
		range = 1200,
		radius = 60,
		danger = 2,
		cc = false,
		collision = false,
		windwall = true
	},
	["OlafAxeThrow="] = {
		charName = "Olaf",
		displayName = "Undertow [Missile]",
		spellName = "OlafAxeThrow",
		slot = SpellSlot.Q,
		type = "linear",
		speed = 1600,
		range = 1000,
		radius = 90,
		danger = 2,
		cc = true,
		collision = false,
		windwall = true
	},
	["OrianaIzuna="] = {
		charName = "Orianna",
		displayName = "Command Attack [Missile]",
		spellName = "OrianaIzuna",
		slot = SpellSlot.Q,
		type = "linear",
		speed = 1400,
		range = 825,
		radius = 80,
		danger = 2,
		cc = false,
		collision = false,
		windwall = false
	},
	["PoppyRMissile="] = {
		charName = "Poppy",
		displayName = "Keeper's Verdict [Missile]",
		spellName = "PoppyRMissile",
		slot = SpellSlot.R,
		type = "linear",
		speed = 2000,
		range = 1200,
		radius = 100,
		danger = 3,
		cc = true,
		collision = false,
		windwall = true
	},
	["PykeQRange="] = {
		charName = "Pyke",
		displayName = "Bone Skewer [Range, Missile]",
		spellName = "PykeQRange",
		slot = SpellSlot.Q,
		type = "linear",
		speed = 2000,
		range = 1100,
		radius = 70,
		danger = 2,
		cc = true,
		collision = true,
		windwall = true
	},
	["QuinnQ="] = {
		charName = "Quinn",
		displayName = "Blinding Assault [Missile]",
		spellName = "QuinnQ",
		slot = SpellSlot.Q,
		type = "linear",
		speed = 1550,
		range = 1025,
		radius = 60,
		danger = 1,
		cc = false,
		collision = true,
		windwall = true
	},
	["RakanQMis="] = {
		charName = "Rakan",
		displayName = "Gleaming Quill [Missile]",
		spellName = "RakanQMis",
		slot = SpellSlot.Q,
		type = "linear",
		speed = 1850,
		range = 850,
		radius = 65,
		danger = 1,
		cc = false,
		collision = true,
		windwall = true
	},
	["RekSaiQBurrowedMis="] = {
		charName = "RekSai",
		displayName = "Prey Seeker [Missile]",
		spellName = "RekSaiQBurrowedMis",
		slot = SpellSlot.Q,
		type = "linear",
		speed = 1950,
		range = 1625,
		radius = 65,
		danger = 2,
		cc = false,
		collision = true,
		windwall = true
	},
	["RengarEMis="] = {
		charName = "Rengar",
		displayName = "Bola Strike [Missile]",
		spellName = "RengarEMis",
		slot = SpellSlot.E,
		type = "linear",
		speed = 1500,
		range = 1000,
		radius = 70,
		danger = 1,
		cc = true,
		collision = true,
		windwall = true
	},
	["RumbleGrenadeMissile="] = {
		charName = "Rumble",
		displayName = "Electro Harpoon [Missile]",
		spellName = "RumbleGrenadeMissile",
		slot = SpellSlot.E,
		type = "linear",
		speed = 2000,
		range = 850,
		radius = 60,
		danger = 2,
		cc = true,
		collision = true,
		windwall = true
	},
	["RyzeQ="] = {
		charName = "Ryze",
		displayName = "Overload [Missile]",
		spellName = "RyzeQ",
		slot = SpellSlot.Q,
		type = "linear",
		speed = 1700,
		range = 1000,
		radius = 55,
		danger = 1,
		cc = false,
		collision = true,
		windwall = true
	},
	["SejuaniRMissile="] = {
		charName = "Sejuani",
		displayName = "Glacial Prison [Missile]",
		spellName = "SejuaniRMissile",
		slot = SpellSlot.R,
		type = "linear",
		speed = 1600,
		range = 1300,
		radius = 120,
		danger = 5,
		cc = true,
		collision = false,
		windwall = true
	},
	["ShyvanaFireballMissile="] = {
		charName = "Shyvana",
		displayName = "Flame Breath [Standard, Missile]",
		spellName = "ShyvanaFireballMissile",
		slot = SpellSlot.E,
		type = "linear",
		speed = 1575,
		range = 925,
		radius = 60,
		danger = 1,
		cc = false,
		collision = false,
		windwall = true
	},
	["ShyvanaFireballDragonMissile="] = {
		charName = "Shyvana",
		displayName = "Flame Breath [Dragon, Missile]",
		spellName = "ShyvanaFireballDragonMissile",
		slot = SpellSlot.E,
		type = "linear",
		speed = 1575,
		range = 975,
		radius = 60,
		danger = 2,
		cc = false,
		collision = false,
		windwall = true
	},
	["SionEMissile="] = {
		charName = "Sion",
		displayName = "Roar of the Slayer [Missile]",
		spellName = "SionEMissile",
		slot = SpellSlot.E,
		type = "linear",
		speed = 1800,
		range = 800,
		radius = 80,
		danger = 2,
		cc = true,
		collision = false,
		windwall = true
	},
	["SivirQMissile="] = {
		charName = "Sivir",
		displayName = "Boomerang Blade [Missile]",
		spellName = "SivirQMissile",
		slot = SpellSlot.Q,
		type = "linear",
		speed = 1350,
		range = 1250,
		radius = 90,
		danger = 2,
		cc = false,
		collision = false,
		windwall = true
	},
	["SkarnerFractureMissile="] = {
		charName = "Skarner",
		displayName = "Fracture [Missile]",
		spellName = "SkarnerFractureMissile",
		slot = SpellSlot.E,
		type = "linear",
		speed = 1500,
		range = 1000,
		radius = 70,
		danger = 1,
		cc = true,
		collision = false,
		windwall = true
	},
	["SonaRMissile="] = {
		charName = "Sona",
		displayName = "Crescendo [Missile]",
		spellName = "SonaRMissile",
		slot = SpellSlot.R,
		type = "linear",
		speed = 2400,
		range = 1000,
		radius = 140,
		danger = 4,
		cc = true,
		collision = false,
		windwall = true
	},
	["SorakaQMissile="] = {
		charName = "Soraka",
		displayName = "Starcall [Missile]",
		spellName = "SorakaQMissile",
		slot = SpellSlot.Q,
		type = "circular",
		speed = 1150,
		range = 810,
		radius = 235,
		danger = 2,
		cc = true,
		collision = false,
		windwall = false
	},
	["SyndraE="] = {
		charName = "Syndra",
		displayName = "Scatter the Weak [Missile]",
		spellName = "SyndraE",
		slot = SpellSlot.E,
		type = "conic",
		speed = 1600,
		range = 700,
		radius = 0,
		angle = 40,
		danger = 3,
		cc = true,
		collision = false,
		windwall = true
	},
	["TahmKenchQMissile="] = {
		charName = "TahmKench",
		displayName = "Tongue Lash [Missile]",
		spellName = "TahmKenchQMissile",
		slot = SpellSlot.Q,
		type = "linear",
		speed = 2800,
		range = 800,
		radius = 70,
		danger = 2,
		cc = true,
		collision = true,
		windwall = true
	},
	["TaliyahQMis="] = {
		charName = "Taliyah",
		displayName = "Threaded Volley [Missile]",
		spellName = "TaliyahQMis",
		slot = SpellSlot.Q,
		type = "linear",
		speed = 3600,
		range = 1000,
		radius = 100,
		danger = 2,
		cc = false,
		collision = true,
		windwall = true
	},
	["TaliyahRMis="] = {
		charName = "Taliyah",
		displayName = "Weaver's Wall [Missile]",
		spellName = "TaliyahRMis",
		slot = SpellSlot.R,
		type = "linear",
		speed = 1700,
		range = 3000,
		radius = 120,
		danger = 1,
		cc = true,
		collision = false,
		windwall = false
	},
	["TalonWMissileOne="] = {
		charName = "Talon",
		displayName = "Rake [Missile]",
		spellName = "TalonWMissileOne",
		slot = SpellSlot.W,
		type = "conic",
		speed = 2500,
		range = 650,
		radius = 75,
		angle = 26,
		danger = 2,
		cc = true,
		collision = false,
		windwall = true
	},
	["ThreshQMissile="] = {
		charName = "Thresh",
		displayName = "Death Sentence [Missile]",
		spellName = "ThreshQMissile",
		slot = SpellSlot.Q,
		type = "linear",
		speed = 1900,
		range = 1100,
		radius = 70,
		danger = 1,
		cc = true,
		collision = true,
		windwall = true
	},
	["SealFateMissile="] = {
		charName = "TwistedFate",
		displayName = "Wild Cards [Missile]",
		spellName = "SealFateMissile",
		slot = SpellSlot.Q,
		type = "threeway",
		speed = 1000,
		range = 1450,
		radius = 40,
		angle = 28,
		danger = 1,
		cc = false,
		collision = false,
		windwall = true
	},
	["UrgotQMissile="] = {
		charName = "Urgot",
		displayName = "Corrosive Charge [Missile]",
		spellName = "UrgotQMissile",
		slot = SpellSlot.Q,
		type = "circular",
		speed = n,
		range = 800,
		radius = 180,
		danger = 2,
		cc = true,
		collision = false,
		windwall = true
	},
	["UrgotR="] = {
		charName = "Urgot",
		displayName = "Fear Beyond Death [Missile]",
		spellName = "UrgotR",
		slot = SpellSlot.R,
		type = "linear",
		speed = 3200,
		range = 1600,
		radius = 80,
		danger = 4,
		cc = true,
		collision = false,
		windwall = true
	},
	["VarusQMissile="] = {
		charName = "Varus",
		displayName = "Piercing Arrow [Missile]",
		spellName = "VarusQMissile",
		slot = SpellSlot.Q,
		origin = "",
		type = "linear",
		speed = 1900,
		range = 1525,
		radius = 70,
		danger = 1,
		cc = false,
		collision = false,
		windwall = true
	},
	["VarusEMissile="] = {
		charName = "Varus",
		displayName = "Hail of Arrows [Missile]",
		spellName = "VarusEMissile",
		slot = SpellSlot.E,
		type = "linear",
		speed = 1500,
		range = 925,
		radius = 260,
		danger = 3,
		cc = true,
		collision = false,
		windwall = true
	},
	["VarusRMissile="] = {
		charName = "Varus",
		displayName = "Chain of Corruption [Missile]",
		spellName = "VarusRMissile",
		slot = SpellSlot.R,
		type = "linear",
		speed = 1950,
		range = 1200,
		radius = 120,
		danger = 4,
		cc = true,
		collision = false,
		windwall = true
	},
	["VeigarBalefulStrikeMis="] = {
		charName = "Veigar",
		displayName = "Baleful Strike [Missile]",
		spellName = "VeigarBalefulStrikeMis",
		slot = SpellSlot.Q,
		type = "linear",
		speed = 2200,
		range = 900,
		radius = 70,
		danger = 2,
		cc = false,
		collision = true,
		windwall = true
	},
	["VelkozQMissile="] = {
		charName = "Velkoz",
		displayName = "Plasma Fission [Missile]",
		spellName = "VelkozQMissile",
		slot = SpellSlot.Q,
		type = "linear",
		speed = 1300,
		range = 1050,
		radius = 50,
		danger = 1,
		cc = true,
		collision = true,
		windwall = true
	},
	["VelkozWMissile="] = {
		charName = "Velkoz",
		displayName = "Void Rift [Missile]",
		spellName = "VelkozWMissile",
		slot = SpellSlot.W,
		type = "linear",
		speed = 1700,
		range = 1050,
		radius = 87.5,
		danger = 1,
		cc = false,
		collision = false,
		windwall = true
	},
	["ViktorDeathRayMissile="] = {
		charName = "Viktor",
		displayName = "Death Ray [Missile]",
		spellName = "ViktorDeathRayMissile",
		slot = SpellSlot.E,
		type = "linear",
		speed = 1050,
		range = 1025,
		radius = 80,
		danger = 2,
		cc = false,
		collision = false,
		windwall = true
	},
	["XerathMageSpearMissile="] = {
		charName = "Xerath",
		displayName = "Mage Spear [Missile]",
		spellName = "XerathMageSpearMissile",
		slot = SpellSlot.E,
		type = "linear",
		speed = 1400,
		range = 1050,
		radius = 60,
		danger = 1,
		cc = true,
		collision = true,
		windwall = true
	},
	["XerathLocusPulse="] = {
		charName = "Xerath",
		displayName = "Rite of the Arcane [Missile]",
		spellName = "XerathLocusPulse",
		slot = SpellSlot.R,
		type = "circular",
		speed = n,
		range = 6160,
		radius = 200,
		danger = 3,
		cc = false,
		collision = false,
		windwall = false
	},
	["YasuoQ3Mis="] = {
		charName = "Yasuo",
		displayName = "Gathering Storm [Missile]",
		spellName = "YasuoQ3Mis",
		slot = SpellSlot.Q,
		type = "linear",
		speed = 1200,
		range = 1000,
		radius = 90,
		danger = 2,
		cc = true,
		collision = false,
		windwall = true
	},
	["ZacQMissile="] = {
		charName = "Zac",
		displayName = "Stretching Strikes [Missile]",
		spellName = "ZacQMissile",
		slot = SpellSlot.Q,
		type = "linear",
		speed = 2800,
		range = 800,
		radius = 120,
		danger = 2,
		cc = true,
		collision = false,
		windwall = true
	},
	["ZedQMissile="] = {
		charName = "Zed",
		displayName = "Razor Shuriken [Missile]",
		spellName = "ZedQMissile",
		slot = SpellSlot.Q,
		type = "linear",
		speed = 1700,
		range = 900,
		radius = 50,
		danger = 1,
		cc = false,
		collision = false,
		windwall = true
	},
	["ZiggsQSpell="] = {
		charName = "Ziggs",
		displayName = "Bouncing Bomb [Missile]",
		spellName = "ZiggsQSpell",
		slot = SpellSlot.Q,
		type = "circular",
		speed = 1700,
		range = 850,
		radius = 120,
		danger = 1,
		cc = false,
		collision = true,
		windwall = true
	},
	["ZiggsW="] = {
		charName = "Ziggs",
		displayName = "Satchel Charge [Missile]",
		spellName = "ZiggsW",
		slot = SpellSlot.W,
		type = "circular",
		speed = 1750,
		range = 1000,
		radius = 240,
		danger = 2,
		cc = true,
		collision = false,
		windwall = true
	},
	["ZiggsE="] = {
		charName = "Ziggs",
		displayName = "Hexplosive Minefield [Missile]",
		spellName = "ZiggsE",
		slot = SpellSlot.E,
		type = "circular",
		speed = 1800,
		range = 900,
		radius = 250,
		danger = 2,
		cc = true,
		collision = false,
		windwall = true
	},
	["ZiggsRBoom="] = {
		charName = "Ziggs",
		displayName = "Mega Inferno Bomb [Missile]",
		spellName = "ZiggsRBoom",
		slot = SpellSlot.R,
		type = "circular",
		speed = 1550,
		range = 5000,
		radius = 480,
		danger = 4,
		cc = false,
		collision = false,
		windwall = false
	},
	["ZileanQMissile="] = {
		charName = "Zilean",
		displayName = "Time Bomb [Missile]",
		spellName = "ZileanQMissile",
		slot = SpellSlot.Q,
		type = "circular",
		speed = n,
		range = 900,
		radius = 150,
		danger = 2,
		cc = true,
		collision = false,
		windwall = true
	},
	["ZoeQMissile="] = {
		charName = "Zoe",
		displayName = "Paddle Star [First, Missile]",
		spellName = "ZoeQMissile",
		slot = SpellSlot.Q,
		type = "linear",
		speed = 1200,
		range = 800,
		radius = 50,
		danger = 1,
		cc = false,
		collision = true,
		windwall = true
	},
	["ZoeQMis2="] = {
		charName = "Zoe",
		displayName = "Paddle Star [Second, Missile]",
		spellName = "ZoeQMis2",
		slot = SpellSlot.Q,
		type = "linear",
		speed = 2500,
		range = 1600,
		radius = 70,
		danger = 2,
		cc = false,
		collision = true,
		windwall = true
	},
	["ZoeEMis="] = {
		charName = "Zoe",
		displayName = "Sleepy Trouble Bubble [Missile]",
		spellName = "ZoeEMis",
		slot = SpellSlot.E,
		type = "linear",
		speed = 1700,
		range = 800,
		radius = 50,
		danger = 2,
		cc = true,
		collision = true,
		windwall = true
	},
	["ZyraE="] = {
		charName = "Zyra",
		displayName = "Grasping Roots [Missile]",
		spellName = "ZyraE",
		slot = SpellSlot.E,
		type = "linear",
		speed = 1150,
		range = 1100,
		radius = 70,
		danger = 1,
		cc = true,
		collision = false,
		windwall = true
	}
}
local C = {
	["Ahri"] = {
		[3] = {
			type = 1,
			displayName = "Spirit Rush",
			name = "AhriQ-",
			danger = 4,
			range = 450,
			slot = SpellSlot.R
		}
	},
	["Blitzcrank"] = {
		[1] = {
			type = 2,
			displayName = "Overdrive",
			name = "BlitzcrankW-",
			danger = 3,
			slot = SpellSlot.W
		}
	},
	["Corki"] = {
		[1] = {
			type = 1,
			displayName = "Valkyrie",
			name = "CorkiW-",
			danger = 4,
			range = 600,
			slot = SpellSlot.W
		}
	},
	["Draven"] = {
		[1] = {
			type = 2,
			displayName = "Blood Rush",
			name = "DravenW-",
			danger = 3,
			slot = SpellSlot.W
		}
	},
	["Ekko"] = {
		[2] = {
			type = 1,
			displayName = "Phase Dive",
			name = "EkkoE-",
			danger = 2,
			range = 325,
			slot = SpellSlot.E
		}
	},
	["Evelynn"] = {
		[3] = {
			type = 1,
			displayName = "Last Caress",
			name = "EvelynnR-",
			danger = 5,
			range = 450,
			slot = SpellSlot.R
		}
	},
	["Ezreal"] = {
		[2] = {
			type = 1,
			displayName = "Arcane Shift",
			name = "EzrealE-",
			danger = 3,
			range = 475,
			slot = SpellSlot.E
		}
	},
	["Fiora"] = {
		[0] = {
			type = 1,
			displayName = "Lunge",
			name = "FioraQ-",
			danger = 1,
			range = 400,
			slot = SpellSlot.Q
		}
	},
	["Fizz"] = {
		[2] = {
			type = 2,
			displayName = "Playful",
			name = "FizzE-",
			danger = 3,
			slot = SpellSlot.E
		}
	},
	["Garen"] = {
		[0] = {
			type = 2,
			displayName = "Decisive Strike",
			name = "GarenQ-",
			danger = 3,
			slot = SpellSlot.Q
		}
	},
	["Gnar"] = {
		[2] = {
			type = 1,
			displayName = "Hop/Crunch",
			name = "GnarE-",
			range = 475,
			danger = 3,
			slot = SpellSlot.E
		}
	},
	["Gragas"] = {
		[2] = {
			type = 1,
			displayName = "Body Slam",
			name = "GragasE-",
			range = 600,
			danger = 3,
			slot = SpellSlot.E
		}
	},
	["Graves"] = {
		[2] = {
			type = 1,
			displayName = "Quickdraw",
			name = "GravesE-",
			range = 425,
			danger = 1,
			slot = SpellSlot.E
		}
	},
	["Hecarim"] = {
		[2] = {
			type = 2,
			displayName = "Devastating Charge",
			name = "HecarimE-",
			danger = 3,
			slot = SpellSlot.E
		},
		[3] = {
			type = 1,
			displayName = "Onslaught of Shadows",
			name = "HecarimR-",
			range = 1000,
			danger = 5,
			slot = SpellSlot.R
		}
	},
	["Kaisa"] = {
		[2] = {
			type = 2,
			displayName = "Supercharge",
			name = "KaisaE-",
			danger = 2,
			slot = SpellSlot.E
		}
	},
	["Karma"] = {
		[2] = {
			type = 3,
			displayName = "Inspire",
			name = "KarmaE-",
			danger = 3,
			slot = SpellSlot.E
		}
	},
	["Kassadin"] = {
		[3] = {
			type = 1,
			displayName = "Riftwalk",
			name = "KassadinR-",
			range = 500,
			danger = 3,
			slot = SpellSlot.R
		}
	},
	["Katarina"] = {
		[1] = {
			type = 2,
			displayName = "Preparation",
			name = "KatarinaW-",
			danger = 3,
			slot = SpellSlot.W
		}
	},
	["Kayle"] = {
		[1] = {
			type = 3,
			displayName = "Divine Blessing",
			name = "KayleW-",
			danger = 3,
			slot = SpellSlot.W
		}
	},
	["Kayn"] = {
		[0] = {
			type = 1,
			displayName = "Reaping Slash",
			name = "KaynQ-",
			danger = 2,
			slot = SpellSlot.Q
		}
	},
	["Kennen"] = {
		[2] = {
			type = 2,
			displayName = "Lightning Rush",
			name = "KennenE-",
			danger = 3,
			slot = SpellSlot.E
		}
	},
	["Khazix"] = {
		[2] = {
			type = 1,
			displayName = "Leap",
			name = "KhazixE-",
			range = 700,
			danger = 3,
			slot = SpellSlot.E
		}
	},
	["Kindred"] = {
		[0] = {
			type = 1,
			displayName = "Dance of Arrows",
			name = "KindredQ-",
			range = 340,
			danger = 1,
			slot = SpellSlot.Q
		}
	},
	["Kled"] = {
		[2] = {
			type = 1,
			displayName = "Jousting",
			name = "KledE-",
			range = 550,
			danger = 3,
			slot = SpellSlot.E
		}
	},
	["Leblanc"] = {
		[1] = {
			type = 1,
			displayName = "Distortion",
			name = "LeblancW-",
			range = 600,
			danger = 3,
			slot = SpellSlot.W
		}
	},
	["Lucian"] = {
		[2] = {
			type = 1,
			displayName = "Relentless Pursuit",
			name = "LucianE-",
			range = 425,
			danger = 3,
			slot = SpellSlot.E
		}
	},
	["MasterYi"] = {
		[0] = {
			type = 4,
			displayName = "Alpha Strike",
			name = "MasterYiQ-",
			range = 600,
			danger = 3,
			slot = SpellSlot.Q
		}
	},
	["Morgana"] = {
		[2] = {
			type = 5,
			displayName = "Black Shield",
			name = "MorganaE-",
			danger = 2,
			slot = SpellSlot.E
		}
	},
	["Pyke"] = {
		[2] = {
			type = 1,
			displayName = "Phantom Undertow",
			name = "PykeE-",
			range = 550,
			danger = 3,
			slot = SpellSlot.E
		}
	},
	["Rakan"] = {
		[1] = {
			type = 1,
			displayName = "Grand Entrance",
			name = "RakanW-",
			range = 600,
			danger = 3,
			slot = SpellSlot.W
		}
	},
	["Renekton"] = {
		[2] = {
			type = 1,
			displayName = "Slice and Dice",
			name = "RenektonE-",
			range = 450,
			danger = 3,
			slot = SpellSlot.E
		}
	},
	["Riven"] = {
		[0] = {
			type = 1,
			displayName = "Broken Wings",
			name = "RivenQ-",
			range = 260,
			danger = 2,
			slot = SpellSlot.Q
		},
		[2] = {
			type = 1,
			displayName = "Valor",
			name = "RivenE-",
			range = 325,
			danger = 2,
			slot = SpellSlot.E
		}
	},
	["Rumble"] = {
		[1] = {
			type = 2,
			displayName = "Scrap Shield",
			name = "RumbleW-",
			danger = 2,
			slot = SpellSlot.W
		}
	},
	["Sejuani"] = {
		[0] = {
			type = 1,
			displayName = "Arctic Assault",
			name = "SejuaniQ-",
			danger = 3,
			slot = SpellSlot.Q
		}
	},
	["Shaco"] = {
		[0] = {
			type = 1,
			displayName = "Deceive",
			name = "ShacoQ-",
			range = 400,
			danger = 3,
			slot = SpellSlot.Q
		},
		[3] = {
			type = 2,
			displayName = "Hallucinate",
			name = "ShacoR-",
			danger = 5,
			slot = SpellSlot.R
		}
	},
	["Shen"] = {
		[2] = {
			type = 1,
			displayName = "Shadow Dash",
			name = "ShenE-",
			range = 600,
			danger = 4,
			slot = SpellSlot.E
		}
	},
	["Shyvana"] = {
		[1] = {
			type = 2,
			displayName = "Burnout",
			name = "ShyvanaW-",
			danger = 3,
			slot = SpellSlot.W
		}
	},
	["Sivir"] = {
		[2] = {
			type = 5,
			displayName = "Spell Shield",
			name = "SivirE-",
			danger = 2,
			slot = SpellSlot.E
		}
	},
	["Skarner"] = {
		[1] = {
			type = 2,
			displayName = "Crystalline Exoskeleton",
			name = "SkarnerW-",
			danger = 3,
			slot = SpellSlot.W
		}
	},
	["Sona"] = {
		[2] = {
			type = 2,
			displayName = "Song of Celerity",
			name = "SonaE-",
			danger = 3,
			slot = SpellSlot.E
		}
	},
	["Teemo"] = {
		[1] = {
			type = 2,
			displayName = "Move Quick",
			name = "TeemoW-",
			danger = 3,
			slot = SpellSlot.W
		}
	},
	["Tryndamere"] = {
		[2] = {
			type = 1,
			displayName = "Spinning Slash",
			name = "TryndamereE-",
			range = 660,
			danger = 3,
			slot = SpellSlot.E
		}
	},
	["Udyr"] = {
		[2] = {
			type = 2,
			displayName = "Bear Stance",
			name = "UdyrE-",
			danger = 1,
			slot = SpellSlot.E
		}
	},
	["Vayne"] = {
		[0] = {
			type = 1,
			displayName = "Tumble",
			name = "VayneQ-",
			range = 300,
			danger = 2,
			slot = SpellSlot.Q
		}
	},
	["Vi"] = {
		[0] = {
			type = 1,
			displayName = "Vault Breaker",
			name = "ViQ-",
			range = 250,
			danger = 3,
			slot = SpellSlot.Q
		}
	},
	["Vladimir"] = {
		[1] = {
			type = 2,
			displayName = "Sanguine Pool",
			name = "VladimirW-",
			danger = 3,
			slot = SpellSlot.W
		}
	},
	["Volibear"] = {
		[0] = {
			type = 2,
			displayName = "Rolling Thunder",
			name = "VolibearQ-",
			danger = 3,
			slot = SpellSlot.Q
		}
	},
	["Wukong"] = {
		[2] = {
			type = 1,
			displayName = "Nimbus Strike",
			name = "WukongE-",
			range = 625,
			danger = 3,
			slot = SpellSlot.E
		}
	},
	["Xayah"] = {
		[3] = {
			type = 2,
			displayName = "Featherstorm",
			name = "XayahR-",
			danger = 5,
			slot = SpellSlot.R
		}
	},
	["Yasuo"] = {
		[1] = {
			type = 6,
			displayName = "Wind Wall",
			name = "YasuoW-",
			danger = 2,
			slot = SpellSlot.W
		}
	},
	["Zed"] = {
		[3] = {
			type = 4,
			displayName = "Death Mark",
			name = "ZedR-",
			range = 625,
			danger = 4,
			slot = SpellSlot.R
		}
	},
	["Zilean"] = {
		[2] = {
			type = 3,
			displayName = "Time Warp",
			name = "ZileanE-",
			danger = 3,
			slot = SpellSlot.E
		}
	}
}
local D = {
	["Katarina"] = "katarinarsound",
	["Xerath"] = "XerathLocusOfPower2",
	["Vladimir"] = "VladimirW"
}
local E = {}

function DelayAction(F, G, H) if not delayedActionsExecuter then
function delayedActionsExecuter() for I, J in pairs(E) do
	if I <= RiotClock.time then
for K, L in ipairs(J) do L.func(unpack(L.args or {})) end
	E[I] = nil end end end
AddEvent(Events.OnTick, delayedActionsExecuter) end
local M = RiotClock.time + (G or 0) if E[M] then u(E[M], {
	func = F,
	args = H
})
else E[M] = {
	{
		func = F, args = H
	}
}
end end
local
function N()
local O = {}
O.__index = O
return setmetatable(O, {
	__call = function(P, ...)
	local Q = setmetatable({}, O) if O.__init then O.__init(Q, ...) end
	return Q end
}) end
JustEvade = N()
function JustEvade:__init() _G.JustEvade, self.Block, self.DoD, self.Loaded, self.SafePos, self.ExtendPos, self.Switcher = false, false, false, false, nil, nil, nil
self.Timer, self.DebugTimer, self.NewTimer, self.OldTimer, self.DetectedSpells, self.DodgeableSpells, self.Enemies = 0, 0, 0, 0, {}, {}, {}
self.Font, self.Rectangle = DrawHandler:CreateFont("Prestige Elite Std", 10), {
	1,
	4,
	2,
	3,
	1,
	2,
	2,
	3
}
for I, R in pairs(ObjectManager:GetEnemyHeroes()) do
	if R and R.team~= myHero.team then u(self.Enemies, R) end end
w(self.Enemies, function(S, T) return S.charName < T.charName end) self.SpellSlot = {
	[SpellSlot.Q] = "Q",
	[SpellSlot.W] = "W",
	[SpellSlot.E] = "E",
	[SpellSlot.R] = "R"
}
self.JEMenu = Menu("JustEvade", "JustEvade v"..b..
	" BETA") self.JEMenu:sub("Core", "Core Settings") self.JEMenu.Core:checkbox("LimitRange", "Limit Detection Range", true) self.JEMenu.Core:checkbox("ExtendPos", "Extend Evade Position", false) self.JEMenu.Core:slider("AS", "Angle Search Step", 5, 120, 10, 5) self.JEMenu.Core:slider("ES", "Extended Safe Position", 10, 200, 50, 10) self.JEMenu.Core:slider("LR", "Limited Detection Range", 500, 10000, 6000, 250) self.JEMenu.Core:slider("MP", "Maximum Evade Points", 1, 50, 20, 1) self.JEMenu.Core:slider("PS", "Separator Value", 5, 150, 50, 5) self.JEMenu:sub("Main", "Main Settings") self.JEMenu.Main:checkbox("Evade", "Enable Evade", true) self.JEMenu.Main:checkbox("Dodge", "Dodge Spells", true) self.JEMenu.Main:checkbox("Draw", "Draw Spells", true) self.JEMenu.Main:checkbox("FOW", "Enable FOW Detection", true) self.JEMenu.Main:checkbox("Status", "Draw Evade Status", true) self.JEMenu.Main:checkbox("SafePos", "Draw Safe Position", true) self.JEMenu.Main:key("SD", "Stop Dodging", string.byte("A")) self.JEMenu.Main:key("DD", "Dodge Only Dangerous", string.byte("N")) self.JEMenu:sub("Spells", "Spell Settings") DelayAction(function() self.JEMenu.Spells:sub("DSpells", "Dodgeable Spells:") for U, V in ipairs(self.Enemies) do
		for I, W in pairs(A) do
			if W.
	charName == V.charName then self.JEMenu.Spells:sub(I, ""..W.charName..
		" "..self.SpellSlot[W.slot]..
		" - "..W.displayName) self.JEMenu.Spells[I]:checkbox("Dodge"..I, "Dodge Spell", true) self.JEMenu.Spells[I]:checkbox("Draw"..I, "Draw Spell", true) self.JEMenu.Spells[I]:list("Mode"..I, "Evade Mode", 1, {
		"Optimal Path",
		"Mouse Position"
	}) self.JEMenu.Spells[I]:slider("HP"..I, "%HP To Dodge Spell", 0, 100, 100, 5) self.JEMenu.Spells[I]:slider("ER"..I, "Extra Radius", 0, 100, 0, 5) self.JEMenu.Spells[I]:slider("Danger"..I, "Danger Level", 1, 5, W.danger or 1, 1) end end
	for I, W in pairs(B) do
		if W.
	charName == V.charName then self.JEMenu.Spells:sub(I, ""..W.charName..
		" "..self.SpellSlot[W.slot]..
		" - "..W.displayName) self.JEMenu.Spells[I]:checkbox("Dodge"..I, "Dodge Spell", true) self.JEMenu.Spells[I]:checkbox("Draw"..I, "Draw Spell", true) self.JEMenu.Spells[I]:list("Mode"..I, "Evade Mode", 1, {
		"Optimal Path",
		"Mouse Position"
	}) self.JEMenu.Spells[I]:slider("HP"..I, "%HP To Dodge Spell", 0, 100, 100, 5) self.JEMenu.Spells[I]:slider("ER"..I, "Extra Radius", 0, 100, 0, 5) self.JEMenu.Spells[I]:slider("Danger"..I, "Danger Level", 1, 5, W.danger or 1, 1) end end end self.JEMenu.Spells:sub("ESpells", "Evading Spells:") local X = C[myHero.charName]
	if X then
	for I = 0, 3 do
		if X[I]
	then self.JEMenu.Spells:sub(X[I].name, ""..myHero.charName..
		" "..(self.SpellSlot[I] or "?")..
		" - "..X[I].displayName) self.JEMenu.Spells[X[I].name]:checkbox("US"..X[I].name, "Use Spell", true) self.JEMenu.Spells[X[I].name]:slider("Danger"..X[I].name, "Danger Level", 1, 5, X[I].danger or 1, 1) end end end end, 0.01) AddEvent(Events.OnTick, function() self:Tick() end) AddEvent(Events.OnDraw, function() self:Draw() end) AddEvent(Events.OnProcessSpell, function(...) self:OnProcessSpell(...) end) AddEvent(Events.OnCreateObject, function(...) self:OnCreateObject(...) end) AddEvent(Events.OnIssueOrder, function(...) self:OnIssueOrder(...) end) _G.JustEvadeLoaded = true end

function JustEvade:DrawArrow(Y, Z, _)
local a0 = Z - z(z(Y - Z):normalized() * 50):perpendicular() + z(Y - Z):normalized() * 50
local a1 = Z - z(z(Y - Z):normalized() * 50):perpendicular2() + z(Y - Z):normalized() * 50
DrawHandler:Line(self:To2D(Y), self:To2D(Z), _) DrawHandler:Line(self:To2D(a0), self:To2D(Z), _) DrawHandler:Line(self:To2D(a1), self:To2D(Z), _) end

function JustEvade:DrawCircle(a2, a3, _) DrawHandler:Circle3D(self:To3D(a2), a3, _) end

function JustEvade:DrawCone(Y, Z, a4, _)
local a4 = r(a4)
local a5, a6 = z(Z - Y):rotated(0, -a4 / 2, 0) DrawHandler:Line(self:To2D(Y), self:To2D(z(Y + a5)), _) for I = -a4 / 2, a4 / 2, a4 / 10 do a6 = z(Z - Y):rotated(0, I, 0) DrawHandler:Line(self:To2D(z(Y + a6)), self:To2D(z(Y + a5)), _) a5 = a6 end
	DrawHandler:Line(self:To2D(Y), self:To2D(z(Y + a5)), _) end

function JustEvade:DrawRectangleOutline(a7, _)
local a0, a1, a8, a9 = self:To2D(a7[1]), self:To2D(a7[2]), self:To2D(a7[3]), self:To2D(a7[4]) DrawHandler:Line(a0, a1, _) DrawHandler:Line(a1, a8, _) DrawHandler:Line(a8, a9, _) DrawHandler:Line(a0, a9, _) end

function JustEvade:DrawText(aa, ab, a2, ac, ad, _) DrawHandler:Text(ab, self:To2D(z(a2.x + ac, 0, a2.z + ad)), aa, _) end

function JustEvade:CalculateEndPos(Y, ae, af, ag, a3, ah, ai)
local Z = z(Y):extended(ae, ag + self.BoundingRadius) if ai == "linear"
or ai == "threeway"
or ai == "conic"
then
if ah then local Y, aj = z(Y):extended(ae, 45), {}
for I, ak in pairs(ObjectManager:GetAllyMinions()) do
	if ak and ak.team == myHero.team and self:GetDistance(ak.position, Y) <= ag and ak.maxHealth > 5 and not ak.isDead then local al = self:VectorPointProjectionOnLineSegment(Y, ae, ak.position) if al and self:GetDistance(al, ak.position) < (ak.boundingRadius or 45) / 2 + a3 then u(aj, ak.position) end end end
if# aj > 0 then w(aj, function(S, T) return self:GetDistanceSqr(S, Y) < self:GetDistanceSqr(T, Y) end)
local am = self:GetDistance(Y, aj[1])
local Z = Y:extended(ae, am) return Z, am end end elseif ai == "circular"
or ai == "rectangular"
or ai == "annular"
then
if ag > 0 then
if self:GetDistance(af, ae) < ag then Z = ae end
else Z = af end end
return Z, ag end

function JustEvade:CircleLineSegmentIntersection(S, T, P, an)
local ao, L = {
	x = T.x - S.x,
	y = T.z - S.z
}, {
	x = S.x - P.x,
	y = S.z - P.z
}
local ap, aq, ar = self:DotProduct(ao, ao), 2 * self:DotProduct(L, ao), self:DotProduct(L, L) - an * an
local as = aq * aq - 4 * ap * ar
if as >= 0 then local at, au = (-aq - t(as)) / (2 * ap), (-aq + t(as)) / (2 * ap) return at >= 0 and at <= 1 and z(S.x + at * ao.x, P.y, S.z + at * ao.y), au >= 0 and au <= 1 and z(S.x + au * ao.x, P.y, S.z + au * ao.y) end
return nil, nil end

function JustEvade:CrossProduct(av, aw) return av.x * (aw.z or aw.y) - (av.z or av.y) * aw.x end

function JustEvade:DotProduct(av, aw) return av.x * aw.x + (av.y or av.z) * (aw.y or aw.z) end

function JustEvade:GetAngle(ax, ay)
local az = z(ax - ay)
local a4 = l(h(az.x, az.z)) return a4 < 0 and a4 + 360 or a4 end

function JustEvade:GetAlternateEvadePos(Y, Z, a3, ai)
local aA, aB = z(myHero.position), z(0, 0, 0) if ai == "circular"
then aB = Z + z(myHero.position - Z):normalized() * (a3 + self.BoundingRadius) if self:IsWall(aB) then aB = Z + z(aB - Z):normalized() * (a3 + self.BoundingRadius) end
else local aC = self:VectorIntersection(Y, Z, z(aA + z(Y - Z)):perpendicular(), aA)
local aD = z(aC.x, Z.y, aC.z) if self:GetDistanceSqr(aA + z(Y - Z):perpendicular(), aD) > self:GetDistanceSqr(aA + z(Y - Z):perpendicular2(), aD) then aB = aD + z(Y - Z):perpendicular():normalized() * (a3 + self.BoundingRadius * 1.1) if self:IsWall(aB) then aB = aD + z(Y - Z):perpendicular2():normalized() * (a3 + self.BoundingRadius * 1.1) end
else aB = aD + z(Y - Z):perpendicular2():normalized() * (a3 + self.BoundingRadius * 1.1) if self:IsWall(aB) then aB = aD + z(Y - Z):perpendicular():normalized() * (a3 + self.BoundingRadius * 1.1) end end end
return aB end

function JustEvade:GetBestEvadePos(Y, aE, Z, a3, ai, aF) if a3 > 300 then
return self:GetAlternateEvadePos(aE, Z, a3, ai) end
local aG = z(myHero.position)
local aH = z(aG):extended(self:GetMousePos(), a3)
local aI = {
	[1] = function(S, T) return self:GetDistanceSqr(S, aG) < self:GetDistanceSqr(T, aG) end,
	[2] = function(S, T) return self:GetDistanceSqr(S, aH) < self:GetDistanceSqr(T, aH) end
}
local aJ, aK, aB = {}, {}
for I = 10, self.BoundingRadius * 1.5 + 300, 20 do aB = z(aG):extended(aH, I) for U = 0, 355, self.JEMenu.Core.AS:get() do aB = self:Rotate2D(aG, aB, r(U)) if not self:IsWall(aB) then
if# aK > 1 and self:IsAssociated(aK, aB) == false or# aK <= 1 then
if self:IsSafePos(aB, 0) and self:TableContains(aJ, U) == false and not self:IsAboutToHit(aB) then u(aJ, U) u(aK, aB) end end end
if# aK >= self.JEMenu.Core.MP:get() then goto ap end end end::ap::
	if# aK == 0 then local aL = self:GetAlternateEvadePos(aE, Z, a3, ai) aK[1] = not self:IsAboutToHit(aL) and aL or nil end
w(aK, aI[aF]) return# aK > 0 and aK[1] or nil end

function JustEvade:GetDistance(ax, ay) return t(self:GetDistanceSqr(ax, ay)) end

function JustEvade:GetDistanceSqr(ax, ay)
local ay = ay and ay or myHero.position
local aM, aN = ay.x - ax.x, (ay.z or ay.y) - (ax.z or ax.y) return aM * aM + aN * aN end

function JustEvade:GetMissilePosition(Y, Z, aO, aP, G) if aP == n or aO + G >= RiotClock.time then
return Y end
return z(Y):extended(Z, aP * (RiotClock.time - aO - G)) end

function JustEvade:GetMousePos() return pwHud.hudManager.activeVirtualCursorPos end

function JustEvade:GetMovePath()
local aQ = myHero.aiManagerClient.navPath.paths
return aQ and z(aQ[#aQ]) or nil end

function JustEvade:GetRectanglePoints(Y, Z, a3, radius2, ai)
local aR, aS, aT, aU
if ai == "linear"
or ai == "threeway"
then local aV = z(Z - Y):perpendicular():normalized() * a3
aR, aS, aT, aU = z(Y + aV), z(Z + aV), z(Z - aV), z(Y - aV) elseif ai == "rectangular"
then local aW = Z - z(Z - Y):perpendicular():normalized() * (radius2 or 400)
local aX = Z + z(Z - Y):perpendicular():normalized() * (radius2 or 400)
local aV = z(aX - aW):perpendicular():normalized() * a3 / 2
aR, aS, aT, aU = z(aW + aV), z(aX + aV), z(aX - aV), z(aW - aV) end
return {
	aR,
	aS,
	aT,
	aU
}
end

function JustEvade:IsAboutToHit(a2) for I, aY in pairs(self.DodgeableSpells) do
	if aY.type == "linear"
and aY.speed~= n then local aZ = {
	x = aY.endPos.x - aY.startPos.x,
	z = aY.endPos.z - aY.startPos.z
}
local a_ = t(aZ.x * aZ.x + aZ.z * aZ.z) aZ.x, aZ.z = aZ.x / a_ * aY.speed, aZ.z / a_ * aY.speed
local b0 = {
	x = a2.x - myHero.position.x,
	z = a2.z - myHero.position.z
}
local b1 = t(b0.x * b0.x + b0.z * b0.z)
local b2 = self:GetMovementSpeed() b0.x, b0.z = b0.x / b1 * b2, b0.z / b1 * b2
local a2 = {
	x = aY.startPos.x - a2.x,
	z = aY.startPos.z - a2.z
}
if a2.x * a2.x + a2.z * a2.z < a_ * a_ then local b3 = {
	x = aZ.x - b0.x,
	z = aZ.z - b0.z
}
local S = b3.x * b3.x + b3.z * b3.z
local T = 2 * (a2.x * b3.x + a2.z * b3.z)
local P = a2.x * a2.x + a2.z * a2.z - (aY.radius + self.BoundingRadius) ^ 2
local as = T * T - 4 * S * P
if as >= 0 then
local at, aum, tg = (-T + t(as)) / (2 * S), (-T - t(as)) / (2 * S)
if at >= 0 and aum >= 0 then tg = p(at, aum) - aY.delay
else tg = o(at, aum) - aY.delay end
if tg > 0 then
return true end end end elseif aY.type == "circular"
then local b5 = z(myHero.position):extended(a2, (aY.range / aY.speed + aY.delay + 0.05) * self:GetMovementSpeed()) if self:GetDistance(b5, aY.endPos) < aY.radius + self.BoundingRadius then
return true end end end
return false end

function JustEvade:IsAssociated(aK, a2) for I = 1, #aK, 1 do
	if self:IsInRange(a2, aK[I], self.JEMenu.Core.PS:get()) then
return true end end
return false end

function JustEvade:IsOnScreen(a2)
local b6, b7 = Renderer.width, Renderer.height
local b8 = Renderer:WorldToScreen(self:To3D(a2)) return b8.x < b6 and b8.x > 0 and b8.y < b7 and b8.y > 0 end

function JustEvade:IsWall(a2)
local b9 = NavMesh:GetCollisionFlags(self:To3D(a2)) return b9 == 2 or b9 == 70 end

function JustEvade:IsZero(ba) return ba and ba.x == 0 and ba.y == 0 and ba.z == 0 end

function JustEvade:IsSafePos(aG, bb)
local bb = bb or 0
for I, aY in pairs(self.DodgeableSpells) do local Y, Z, bc, al = aY.startPos, aY.endPos, self.BoundingRadius + bb
	if aY.type == "rectangular"
then local aW = Z - z(Z - Y):perpendicular():normalized() * (radius2 or 400)
local aX = Z + z(Z - Y):perpendicular():normalized() * (radius2 or 400) Y, Z = aW, aX end
local bd = aY.type == "rectangular"
and aY.radius / 2 + bc or aY.radius + bc
local be = (aY.type == "linear"
	or aY.type == "threeway"
	or aY.type == "rectangular") and "linear"
or aY.type
if be == "linear"
then al = self:VectorPointProjectionOnLineSegment(Y, Z, aG) end
if be == "circular"
and self:GetDistanceSqr(aG, Z) < bd * bd or be == "linear"
and self:GetDistanceSqr(aG, al) < bd * bd then
return false end end
return true end

function JustEvade:IsPointInCone(Y, Z, a2, ag, a4)
local as = f(self:GetAngle(Y, a2) - self:GetAngle(Y, Z)) if as < a4 and self:IsInRange(Y, a2, ag) then
return true end
return false end

function JustEvade:IsInRange(ax, ay, ag)
local aM, aN = ay.x - ax.x, (ay.z or ay.y) - (ax.z or ax.y) return aM * aM + aN * aN <= ag * ag end

function JustEvade:LineSegmentIntersection(a5, bf, a6, bg)
local an, aY = z(bf - a5), z(bg - a6)
local ac = self:CrossProduct(an, aY)
local b4, bh = self:CrossProduct(a6 - a5, aY) / ac, self:CrossProduct(a6 - a5, an) / ac
if ac~= 0 and(b4 >= 0 and b4 <= 1) and(bh >= 0 and bh <= 1) then
return a5 + b4 * an end
return nil end

function JustEvade:Rotate2D(Y, Z, bi)
local aM, bj = Z.x - Y.x, Z.z - Y.z
local bk, bl = aM * k(bi) - bj * s(bi), aM * s(bi) + bj * k(bi) return z(bk + Y.x, Z.y, bl + Y.z) end

function JustEvade:To2D(a2) return Renderer:WorldToScreen(self:To3D(a2)) end

function JustEvade:To3D(a2) return D3DXVECTOR3(a2.x, a2.y, a2.z) end

function JustEvade:VectorIntersection(a5, bf, a6, bg)
local bm, bn, bo, bp, bq, br, bs, bt = a5.x, a5.z or a5.y, bf.x, bf.z or bf.y, a6.x, a6.z or a6.y, bg.x, bg.z or bg.y
local an, aY, bh, b3, V, bu = bm * bp - bn * bo, bq * bt - br * bs, bq - bs, bm - bo, br - bt, bn - bp
local bk, bl, bv = an * bh - b3 * aY, an * V - bu * aY, b3 * V - bu * bh
return bv~= 0 and {
	x = bk / bv, z = bl / bv
}
end

function JustEvade:VectorPointProjectionOnLineSegment(aZ, b0, b3)
local bw, bx, by, bz, bA, bB = b3.x, b3.z or b3.y, aZ.x, aZ.z or aZ.y, b0.x, b0.z or b0.y
local bC = ((bw - by) * (bA - by) + (bx - bz) * (bB - bz)) / ((bA - by) ^ 2 + (bB - bz) ^ 2)
local bD = {
	x = by + bC * (bA - by),
	y = bz + bC * (bB - bz)
}
local bE = bC < 0 and 0 or(bC > 1 and 1 or bC)
local bF = bE == bC
local bG = bF and bD or {
	x = by + bE * (bA - by), y = bz + bE * (bB - bz)
}
return bG, bD, bF end

function JustEvade:CastSpell(W, bH) if bH then
return myHero.spellbook:CastSpell(W, bH) end
return myHero.spellbook:CastSpell(W) end

function JustEvade:GetDodgeableSpells()
local bI = {}
for I, aY in pairs(self.DetectedSpells) do
	if self.JEMenu.Spells[aY.name]["Dodge"..aY.name]:get() and myHero.healthPercent <= self.JEMenu.Spells[aY.name]["HP"..aY.name]:get() then
if self.DoD and aY.danger == 5 or not self.DoD then u(bI, aY) end end end
return bI end

function JustEvade:GetMovementSpeed() return myHero.characterIntermediate.movementSpeed end

function JustEvade:IsMoving() return myHero.position.x - m(myHero.position.x) ~= 0 end

function JustEvade:IsReady(W) return myHero.spellbook:CanUseSpell(W) == 0 end

function JustEvade:TableContains(bJ, bK) for I = 1, #bJ do
	if bJ[I] == bK then
return true end end
return false end

function JustEvade:Switch(aF)
if aF == 1 then _G.JustEvade = true
else _G.JustEvade, self.SafePos, self.ExtendPos = false, nil, nil end
end

function JustEvade:ValidTarget(bL, ag)
local ag = ag and ag or n
return bL and bL.isValid and bL.isVisible and not bL.isDead and self:GetDistanceSqr(myHero.position, bL.position) <= ag * ag end

function JustEvade:Tick() if self.JEMenu.Main.DD:get() then
if RiotClock.time > self.Timer + 0.25 then
if self.DoD == false then self.DoD = true
else self.DoD = false end
self.Timer = RiotClock.time end end
self.BoundingRadius = myHero.boundingRadius or 65
if self.JEMenu.Main.Evade:get() and self.JEMenu.Main.Dodge:get() then self.DodgeableSpells = self:GetDodgeableSpells() if# self.DodgeableSpells > 0 then
for I, aY in pairs(self.DodgeableSpells) do self:DetectCollision(I, aY) if _G.JustEvade and self.SafePos then self:DodgeSpell(I, aY) end end
else self:Switch(0); self.Block = false end end end

function JustEvade:DetectCollision(I, aY)
local aF, bM = self.JEMenu.Spells[aY.name]["Mode"..aY.name]:get() or 1, aY.range / aY.speed + aY.delay
if not self:IsSafePos(myHero.position, self.BoundingRadius * 0.8) then
if self.OldTimer~= self.NewTimer then self.SafePos = self:GetBestEvadePos(aY.startPos, aY.rawStartPos, aY.endPos, aY.radius, aY.type, aF) self.ExtendPos = z(myHero.position):extended(self.SafePos, self:GetDistance(myHero.position, self.SafePos) + self.BoundingRadius * 1.5 + (self.JEMenu.Core.ExtendPos:get() and self.JEMenu.Core.ES:get() or 0)) self:Switch(1) self.OldTimer = self.NewTimer end
else self:Switch(0); self.Block = false end end

function JustEvade:DodgeSpell(I, aY) if self:GetDistanceSqr(myHero.position, self.SafePos) > 500000 then
return end
local bN, bO = self:Avoid(aY), D[myHero.charName] and myHero.buffManager:HasBuff(D[myHero.charName]) if bO or bN == 1 or self.JEMenu.Main.SD:get() or self:IsInRange(myHero.position, self.SafePos, 15) then self:Switch(0) return end
myHero:IssueOrder(GameObjectOrder.MoveTo, self:To3D(self.ExtendPos)) if self:IsInRange(self:GetMovePath(), self.ExtendPos, 20) then self.Block = true
else self.Block = false end end

function JustEvade:Avoid(aY) for I = 0, 3 do local X = C[myHero.charName]
	if
X and X[I] and self.JEMenu.Spells[X[I].name]["US"..X[I].name]:get() then
if self:IsReady(X[I].slot) and aY.danger >= self.JEMenu.Spells[X[I].name]["Danger"..X[I].name]:get() then
if X[I].type == 1 then local bP = z(myHero.position):extended(self.SafePos, X[I].range) self:CastSpell(X[I].slot, self:To3D(bP)) return 0 elseif X[I].type == 2 then self:CastSpell(X[I].slot, myHero.networkId) return 1 elseif X[I].type == 3 then self:CastSpell(X[I].slot, myHero.networkId) return 0 elseif X[I].type == 4 then
for I, R in pairs(self.Enemies) do
	if R and self:ValidTarget(R, X[I].range) then self:CastSpell(X[I].slot, R.networkId) return 1 end end elseif X[I].type == 5 and aY.cc then self:CastSpell(X[I].slot, myHero.networkId) return 1 elseif X[I].type == 6 and aY.windwall then local bQ = z(myHero.position):extended(aY.startPos, 100) self:CastSpell(X[I].slot, self:To3D(bQ)) return 1 end end end end
return 0 end

function JustEvade:Draw() if self.JEMenu.Main.Status:get() then
if self.JEMenu.Main.Evade:get() then
if self.DoD then self:DrawText("Evade: Dodge Only Dangerous", self.Font, myHero.position, -223, -65, 0xFFFFFF00)
else self:DrawText("Evade: ON", self.Font, myHero.position, -75, -65, 0xFFFFFFFF) end
else self:DrawText("Evade: OFF", self.Font, myHero.position, -82, -65, 0xFFA9A9A9) end end
if# self.DodgeableSpells > 0 and _G.JustEvade == true and self.SafePos~= nil and self.JEMenu.Main.SafePos:get() then self:DrawCircle(self.SafePos, self.BoundingRadius, 0xC0FFFFFF) self:DrawArrow(z(myHero.position), self.SafePos, 0xC0FFFF00) end
for I, aY in pairs(self.DetectedSpells) do
	if aY.type == "linear"
and aY.speed~= n then self.DetectedSpells[I].startPos = self:GetMissilePosition(aY.rawStartPos, aY.endPos, aY.startTime, aY.speed, aY.delay) end
if self.JEMenu.Main.Draw:get() then
if self.JEMenu.Spells[aY.name]["Draw"..aY.name]:get() then local bM = aY.range / aY.speed + aY.delay
if aY.startTime + bM > RiotClock.time then
if aY.type == "linear"
then self:DrawRectangleOutline(self:GetRectanglePoints(aY.startPos, aY.endPos, aY.radius, nil, aY.type), 0xC0FFFFFF) elseif self:IsOnScreen(aY.endPos) then
if aY.type == "threeway"
then local bR, bS = self:Rotate2D(aY.startPos, aY.endPos, r(aY.angle)), self:Rotate2D(aY.startPos, aY.endPos, r(-aY.angle)) self:DrawRectangleOutline(self:GetRectanglePoints(aY.startPos, aY.endPos, aY.radius, nil, aY.type), 0xC0FFFFFF) self:DrawRectangleOutline(self:GetRectanglePoints(self:GetMissilePosition(aY.startPos, bR, aY.startTime, aY.speed, aY.delay), bR, aY.radius, nil, aY.type), 0xC0FFFFFF) self:DrawRectangleOutline(self:GetRectanglePoints(self:GetMissilePosition(aY.startPos, bS, aY.startTime, aY.speed, aY.delay), bS, aY.radius, nil, aY.type), 0xC0FFFFFF) elseif aY.type == "circular"
then self:DrawCircle(aY.endPos, aY.radius, 0xC0FFFFFF) elseif aY.type == "conic"
then self:DrawCone(aY.startPos, aY.endPos, aY.angle or 40, 0xC0FFFFFF) elseif aY.type == "rectangular"
then self:DrawRectangleOutline(self:GetRectanglePoints(aY.startPos, aY.endPos, aY.radius, aY.radius2, aY.type), 0xC0FFFFFF) elseif aY.type == "annular"
then self:DrawCircle(aY.endPos, aY.radius, 0xC0FFFFFF) self:DrawCircle(aY.endPos, aY.radius / 1.5, 0xC0FFFFFF) end end
else v(self.DetectedSpells, I) end end end end end

function JustEvade:OnProcessSpell(bT, W) if bT and W and myHero.team~= bT.team and A[W.spellData.name] then
if self.JEMenu.Core.LimitRange:get() and self:GetDistance(myHero.position, bT.position) > self.JEMenu.Core.LR:get() then
return end
self.NewTimer = RiotClock.time
local aY, bU, bV = A[W.spellData.name], z(W.endPos), z(bT.position)
local bW = z(W.startPos) or bV
local bX, bY = self:CalculateEndPos(bW, bU, bV, aY.range, aY.radius, aY.collision, aY.type)
local bZ, b_ = self.JEMenu.Spells[W.spellData.name]["ER"..W.spellData.name]:get() or 0, self.JEMenu.Spells[W.spellData.name]["Danger"..W.spellData.name]:get() or 1
u(self.DetectedSpells, {
	startPos = bW,
	rawStartPos = bW,
	endPos = bX,
	unitPos = bV,
	speed = aY.speed,
	range = bY,
	radius = aY.radius + bZ,
	radius2 = aY.radius2,
	angle = aY.angle,
	delay = aY.delay,
	name = W.spellData.name,
	startTime = RiotClock.time,
	type = aY.type,
	danger = b_,
	cc = aY.cc,
	collision = aY.collision,
	windwall = aY.windwall
}) end end

function JustEvade:OnCreateObject(c0, c1) if not self.JEMenu.Main.FOW:get() then
return end
local W = c0.asMissile
local bT, c2 = W.spellCaster, c0.name
if bT and W and not bT.isVisible and bT.type == GameObjectType.AIHeroClient and myHero.team~= bT.team then
if self.JEMenu.Core.LimitRange:get() and self:GetDistance(myHero.position, bT.position) > self.JEMenu.Core.LR:get() then
return end
local c3, c4 = false, c2..
"="
if B[c4] then c3 = true
else
	for I, c5 in pairs(B) do
		if string.find(c2, B[I].spellName) then c3 = true end end end
if c3 then self.NewTimer = RiotClock.time
local aY, bU, bV = B[c4], z(W.destPos), z(bT.position)
local bW = z(W.launchPos) or bV
local bX, bY = self:CalculateEndPos(bW, bU, bV, aY.range, aY.radius, aY.collision, aY.type)
local bZ, b_ = self.JEMenu.Spells[c4]["ER"..c4]:get() or 0, self.JEMenu.Spells[c4]["Danger"..c4]:get() or 1
u(self.DetectedSpells, {
	startPos = bW,
	rawStartPos = bW,
	endPos = bX,
	unitPos = bV,
	speed = aY.speed,
	range = bY,
	radius = aY.radius + bZ,
	radius2 = aY.radius2,
	angle = aY.angle,
	delay = 0,
	name = c4,
	startTime = RiotClock.time,
	type = aY.type,
	danger = b_,
	cc = aY.cc,
	collision = aY.collision,
	windwall = aY.windwall
}) end end end

function JustEvade:OnIssueOrder(c6, a2) if _G.JustEvade then
if self.Block and self.ExtendPos and self:IsInRange(a2, self.ExtendPos, 20) or c6 == 3 then
BlockOrder() end elseif self:IsSafePos(myHero.position, 0) then local aA, c7 = z(myHero.position), {}
local a2 = z(aA):extended(a2, self:GetDistance(a2, aA) + self.BoundingRadius) for I, aY in pairs(self.DodgeableSpells) do
	if aY.type == "linear"
then local aK = self:GetRectanglePoints(aY.startPos, aY.endPos, aY.radius + self.BoundingRadius, 0, aY.type)
local aR = self:LineSegmentIntersection(aK[1], aK[2], aA, a2)
local aS = self:LineSegmentIntersection(aK[3], aK[4], aA, a2) if aR then u(c7, aR) end
if aS then u(c7, aS) end elseif aY.type == "circular"
then local c8, c9 = self:CircleLineSegmentIntersection(aA, a2, aY.endPos, aY.radius + self.BoundingRadius) if c8 then u(c7, c8) end
if c9 then u(c7, c9) end end end
if# c7 > 0 then w(c7, function(S, T) return self:GetDistanceSqr(aA, S) < self:GetDistanceSqr(aA, T) end)
local ca = z(c7[1]) - z(c7[1] - aA):normalized() * self.BoundingRadius / 1.9
myHero:IssueOrder(GameObjectOrder.MoveTo, self:To3D(ca)); BlockOrder() end end end

function JustEvade:OnWndProc(cb, cc, cd, ce) if RiotClock.time > self.DebugTimer + 0.25 then
if cd == string.byte("A") then self.NewTimer, self.DebugTimer = RiotClock.time, RiotClock.time
local bW, bX = z(9640, 0, 1300), z(7340, 0, 1300)
local cf = self:GetDistance(bW, bX) u(self.DetectedSpells, {
	startPos = bW,
	rawStartPos = bW,
	endPos = bW,
	unitPos = bW,
	speed = n,
	range = cf,
	radius = m(math.random(180, 260)),
	radius2 = nil,
	angle = nil,
	delay = 30,
	name = "EzrealW",
	startTime = RiotClock.time,
	type = "circular",
	danger = 1,
	cc = false,
	collision = false,
	windwall = true
}) end end end

function OnLoad() JustEvade() d() end
