-- Common
if myHero.charName ~= "Tristana" then return end

local _tristanaVERSION = 0.02
local _tristanaVERSIONONLINE = GetInternalWebResult("LegitTristana.version") 

local function DownloadNewVersion(currentVersion,onlineVersion)
	local currentVersion,onlineVersion = tonumber(currentVersion),tonumber(onlineVersion)
	if onlineVersion > currentVersion then
		PrintChat("<font color=\"#50ed8f\">[UPDATE]</font> <font color=\"#8f70f4\"><b>[Legit - Tristana]</b></font><b> <font color=\"#50ed8f\"> v" ..onlineVersion.. " is avaible!</font>")
		DownloadInternalFile("Legit%20Tristana.lua", SCRIPT_PATH .. "Legit Tristana.lua")
	end
end
DownloadNewVersion(_tristanaVERSION,_tristanaVERSIONONLINE)

local FileExists = function(path)
	local file = io.open(path, 'r')
	if file then
		io.close(file)
		return true
	end
	return false
end


if FileExists(COMMON_PATH .. "FF15Menu.lua") then require 'FF15Menu'else print("[Legit - Tristana] - requires FF15Menu.lua in your //COMMON folder.") return end

local Common = {}
local LegitTristana = {}

-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------- C O M M O N -------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

function Common:tableContain(tab,val)
    for index, value in pairs(tab) do
        if value == val then
            return true
        end
    end
    return false
end

function Common:Vec3(vec)
	return D3DXVECTOR3(vec.x, vec.y, vec.z)
end    

function Common:GetDistanceSqr(p1, p2)
	local dx, dz = p1.x - p2.x, p1.z - p2.z 
	return dx * dx + dz * dz
end

function Common:GetDistance(p1, p2)
	return sqrt(self:GetDistanceSqr(p1, p2))
end	

function Common:GetDistanceToZero(p1,p2)
	return math.sqrt(math.pow((p1.position.x - p2.position.x),2) + 0 + math.pow((p1.position.z - p2.position.z),2))
end

function Common:GetDistanceToZeroPos(p1,p2)
	return math.sqrt(math.pow((p1.x - p2.x),2) + 0 + math.pow((p1.z - p2.z),2))
end

function Common:OnScreen(pos)
	local maxX,maxY = Renderer.width, Renderer.height
	local toScreen = Renderer:WorldToScreen(D3DXVECTOR3(pos.x, pos.y, pos.z))
	return toScreen.x < maxX and toScreen.x > 0 and toScreen.y < maxY and toScreen.y > 0
end

function Common:IsRunning(unit)
	return unit.position.x - floor(unit.position.x) ~= 0
end

function Common:CountEnemiesAround(range,pos)
    local pos = pos or myHero.position
    local count = 0
    for k, v in pairs(ObjectManager:GetEnemyHeroes()) do
        if v.isVisible and not v.isDead and v.isValid then
            if GetDistance(pos,v.position) <= range then
                count = count + 1
            end
        end
    end
    return count
end

function Common:IsReady(spell)
    return 0 == myHero.spellbook:CanUseSpell(spell)
end

function Common:SpellInfo(spell)
    return myHero.spellbook:Spell(spell)
end

function Common:BonusAD(unit)
    return unit.characterIntermediate.flatPhysicalDamageMod
end

function Common:TotalAD(unit)
    return unit.characterIntermediate.flatPhysicalDamageMod + unit.characterIntermediate.baseAttackDamage
end

function Common:BonusAP(unit)
    return unit.characterIntermediate.flatMagicDamageMod
end

function Common:GetPercentage(a,b)
    return 100*a/b
end

function Common:CollisionMinion(pos1, pos2, r, team, ignoreUnit)
    local team = team or 300 - myHero.team
    local distanceP1_P2 = Common:GetDistanceToZeroPos(pos1,pos2)
    local vec = D3DXVECTOR3((pos2.x - pos1.x)/distanceP1_P2,0,(pos2.z - pos1.z)/distanceP1_P2)
    local t = {}
    if team ~= myHero.team then
        for k, v in pairs(ObjectManager:GetEnemyMinions()) do

            local unitPos = v.position
            local distanceP1_Unit = Common:GetDistanceToZeroPos(pos1,unitPos)
            if v.isValid and not v.isDead and distanceP1_Unit <= distanceP1_P2 and v.team ~= myHero.team then
                local checkPos = D3DXVECTOR3(pos1.x + vec.x*distanceP1_Unit,0,pos1.z + vec.z*distanceP1_Unit)
                if Common:GetDistanceToZeroPos(unitPos,checkPos) < r + 55 and v ~= ignoreUnit then
                    table.insert(t,v)
                end
            end
        end
    else
        for k, v in pairs(ObjectManager:GetAllyMinions()) do
            local unitPos = v.position
            local distanceP1_Unit = Common:GetDistanceToZeroPos(pos1,unitPos)
            if v.isValid and not v.isDead and distanceP1_Unit <= distanceP1_P2 and v.team == myHero.team then
                local checkPos = D3DXVECTOR3(pos1.x + vec.x*distanceP1_Unit,0,pos1.z + vec.z*distanceP1_Unit)
                if Common:GetDistanceToZeroPos(unitPos,checkPos) < r + 55 and v ~= ignoreUnit then
                    table.insert(t,v)
                end
            end
        end
    end
    return {count = #t, units = t}
end

function Common:CollisionHeroes(pos1, pos2, r, team, ignoreUnit)
    local team = team or 300 - myHero.team
    local distanceP1_P2 = Common:GetDistanceToZeroPos(pos1,pos2)
    local vec = D3DXVECTOR3((pos2.x - pos1.x)/distanceP1_P2,0,(pos2.z - pos1.z)/distanceP1_P2)
    local t = {}
    if team ~= myHero.team then
        for k, v in pairs(ObjectManager:GetEnemyHeroes()) do
            local unitPos = v.position
            local distanceP1_Unit = Common:GetDistanceToZeroPos(pos1,unitPos)
            if v.isValid and not v.isDead and distanceP1_Unit <= distanceP1_P2 then
                local checkPos = D3DXVECTOR3(pos1.x + vec.x*distanceP1_Unit,0,pos1.z + vec.z*distanceP1_Unit)
                if Common:GetDistanceToZeroPos(unitPos,checkPos) < r + v.boundingRadius/2 and v ~= ignoreUnit then
                    table.insert(t,v)
                end
            end
        end
    else
        for k, v in pairs(ObjectManager:GetAllyHeroes()) do
            local unitPos = v.position
            local distanceP1_Unit = Common:GetDistanceToZeroPos(pos1,unitPos)
            if v.isValid and not v.isDead and distanceP1_Unit <= distanceP1_P2 then
                local checkPos = D3DXVECTOR3(pos1.x + vec.x*distanceP1_Unit,0,pos1.z + vec.z*distanceP1_Unit)
                if Common:GetDistanceToZeroPos(unitPos,checkPos) < r + v.boundingRadius/2 and v ~= ignoreUnit then
                    table.insert(t,v)
                end
            end
        end
    end
    return {count = #t, units = t}
end

function Common:Hex(a,r,g,b)
	return tonumber(string.format("0x%.2X%.2X%.2X%.2X",a,r,g,b))
end

function Common:Init()
    self.WayPoints,self.LastWayPoints,self.Dashes = {}, {}, {}

    self.UnkillableBuffsNames = {["JaxCounterStrike"] = function(source) return 2 end, ["kindredrnodeathbuff"] = function(source) return 0 end, ["UndyingRage"] = function(source) return 5 end, ["ChronoShift"] = function(source) return 5 end, ["JudicatorIntervention"] = function(source) return source.spellbook:Spell(self.R).level*0.5 + 1.5 end, ["TaricR"] = function(source) return 2.5 end, ["FioraW"] = function(source) return 0.75 end, ["aatroxpassivedeath"] = function(source) return 0 end, ["VladimirSanguinePool"] = function(source) return 2 end, ["fizzeicon"] = function(source) return 0.75 end, ["bardrstasis"] = function(source) return 2.5 end, ["KarthusDeathDefiedBuff"] = function(source) return 999 end, ["KogMawIcathianSurprise"] = function(source) return 5 end, ["zhonyasringshield"] = function(source) return 2.5 end }
    self.UnkillableBuffs = {}

    self.HardCC_Types = {[BuffType.Stun] = "Stun", [BuffType.Snare] = "HardCC", [BuffType.SleepBuffType] = "HardCC", [BuffType.Suppression] = "HardCC", [BuffType.Knockup] = "HardCC"}
    self.MoveCC_Types = {[BuffType.Taunt] = "MoveCC", [BuffType.Fear] = "MoveCC", [BuffType.Charm] = "MoveCC", [BuffType.Flee] = "MoveCC", [BuffType.Knockback] = "MoveCC"}
    self.SoftCC_Types = {[BuffType.Polymorph] = "SoftCC", [BuffType.Slow] = "SoftCC"}
    
    self.HardCCBuffs = {}
    self.MoveCCBuffs = {}
    self.SoftCCBuffs = {}

    self.Recall = {}
    self.ActiveSpells = {}
    self.Channeling = {}
    self.Projectiles = {}

    self.Humanizer = {Q=0,W=0,E=0,R=0,S1=0,S2=0,I1=0,I2=0,I3=0,I4=0,I5=0,I6=0,T=0}
    self.Tick = 0
    
    AddEvent(Events.OnNewPath,function(a,b,c,d) self:OnWaypoint(a,b,c,d) end)
    AddEvent(Events.OnBuffGain,function(a,b) self:OnBuffGain(a,b) end)
    --bugged AddEvent(Events.OnBuffUpdate,function(a,b) self:OnBuffUpdate(a,b) end)
    AddEvent(Events.OnBuffLost,function(a,b) self:OnBuffLost(a,b) end)

    LegitTristana:__init()
end



function Common:OnWaypoint(_unit,_path,_isWalk,_dashSpeed)
    if _unit.type == 1 then
        local nID = _unit.networkId
        if self.WayPoints[nID] ~= nil then self.LastWayPoints[nID] = self.WayPoints[nID] end
        self.WayPoints[nID] = {unit = _unit, path = _path, isWalk = _isWalk, dashSpeed = _dashSpeed, time = RiotClock.time}
        if _isWalk == false then
            if self.Dashes[nID] == nil then self.Dashes[nID] = {} end
            self.Dashes[nID] = {unit = _unit, path = _path, dashSpeed = _dashSpeed, time = RiotClock.time}
        else
            if self.Dashes[nID] ~= nil then
                self.Dashes[nID] = nil
            end
        end
    end
end

function Common:OnBuffGain(_unit,_buff)
    --PrintChat(myHero.type)
    if _unit.type == myHero.type then
        local _name = _buff.name
        local _type = _buff.type
        local nID = _unit.networkId
    --// Unkillable
        if self.UnkillableBuffsNames[_name] ~= nil then
            if self.UnkillableBuffs[nID] == nil then self.UnkillableBuffs[nID] = {} end
            table.insert(self.UnkillableBuffs[nID],{ name = _name, time = RiotClock.time, duration = self.UnkillableBuffsNames[_name](_buff.caster) })
        end
    --// CC Debuffs
        if self.HardCC_Types[_type] then
            if self.HardCCBuffs[nID] == nil then self.HardCCBuffs[nID] = {} end
            table.insert(self.HardCCBuffs[nID],{name = _name, time = RiotClock.time, duration = _buff.remainingTime})
        end
        if self.MoveCC_Types[_type] then
            if self.MoveCCBuffs[nID] == nil then self.MoveCCBuffs[nID] = {} end
            table.insert(self.MoveCCBuffs[nID],{name = _name, time = RiotClock.time, duration = _buff.remainingTime})
        end
        if self.SoftCC_Types[_type] then
            if self.SoftCCBuffs[nID] == nil then self.SoftCCBuffs[nID] = {} end
            table.insert(self.SoftCCBuffs[nID],{name = _name, time = RiotClock.time, duration = _buff.remainingTime})
        end
    end
end

-- bugged
function Common:OnBuffUpdate(_unit,_buff)
    if _unit.type == myHero.type then
        local _name = _buff.name
        local _type = _buff.type
        local nID = _unit.networkId
        if self.UnkillableBuffsNames[_name] ~= nil then
            if self.UnkillableBuffs[nID] == nil then self.UnkillableBuffs[nID] = {} end
            table.insert(self.UnkillableBuffs[nID],{ name = _name, time = RiotClock.time, duration = _buff.endTime - _buff.startTime })
        end
    end
end

function Common:OnBuffLost(_unit,_buff)
    if _unit.type == myHero.type then
        local _name = _buff.name
        local _type = _buff.type
        local nID = _unit.networkId
    --// Unkillable
        if self.UnkillableBuffs[nID] ~= nil then
            for i,b in pairs(self.UnkillableBuffs[nID]) do
                if b.name == _name then
                    table.remove(self.UnkillableBuffs[nID],i)
                end
            end
        end
    --// CC Debuffs
        if self.HardCCBuffs[nID] then
            for i,b in pairs(self.HardCCBuffs[nID]) do
                if b.name == _name then
                    table.remove(self.HardCCBuffs[nID],i)
                end
            end
        end
        if self.MoveCCBuffs[nID] then
            for i,b in pairs(self.MoveCCBuffs[nID]) do
                if b.name == _name then
                    table.remove(self.MoveCCBuffs[nID],i)
                end
            end
        end
        if self.SoftCCBuffs[nID] then
            for i,b in pairs(self.SoftCCBuffs[nID]) do
                if b.name == _name then
                    table.remove(self.SoftCCBuffs[nID],i)
                end
            end
        end
    end
end

function Common:Unkillable(unit,time)
    local time = time or 0
    if self.UnkillableBuffs[unit.networkId] == nil then return 0 end
    local duration = 0
    for i,b in pairs(self.UnkillableBuffs[unit.networkId]) do
        local timeLeft = b.duration - (RiotClock.time - b.time)
        if duration < timeLeft then
            duration = timeLeft
        end
    end
    duration = duration - time
    if duration < 0 then return 0 end
    return duration
end

function Common:HardCC(unit,time)
    local time = time or 0
    if self.HardCCBuffs[unit.networkId] == nil then return 0 end
    local duration = 0
    for i,b in pairs(self.HardCCBuffs[unit.networkId]) do
        local timeLeft = b.duration - (RiotClock.time - b.time)
        if duration < timeLeft then
            duration = timeLeft
        end
    end
    duration = duration - time
    return duration
end

function Common:MoveCC(unit,time)
    local time = time or 0
    if self.MoveCCBuffs[unit.networkId] == nil then return 0 end
    local duration = 0
    for i,b in pairs(self.MoveCCBuffs[unit.networkId]) do
        local timeLeft = b.duration - (RiotClock.time - b.time)
        if duration < timeLeft then
            duration = timeLeft
        end
    end
    duration = duration - time
    return duration
end

function Common:SoftCC(unit,time)
    local time = time or 0
    if self.SoftCCBuffs[unit.networkId] == nil then return 0 end
    local duration = 0
    for i,b in pairs(self.SoftCCBuffs[unit.networkId]) do
        local timeLeft = b.duration - (RiotClock.time - b.time)
        if duration < timeLeft then
            duration = timeLeft
        end
    end
    duration = duration - time
    return duration
end

function Common:GetPrediction(unit,delay,speed,width)
    if unit.charName == "PracticeTool_TargetDummy" then return unit.position end
    local speed = speed or math.huge
    local cc = self:HardCC(unit)

    local t = self:GetDistanceToZeroPos(unit.position,myHero.position)/speed - cc + delay
    if t <= 0.03 then
        return unit.position
    end
    local ms = unit.characterIntermediate.movementSpeed
    local width = width or 0
    local distance = t * ms - unit.boundingRadius/2 - width/2

    if Common.Dashes[unit.networkId] then
        local dash = Common.Dashes[unit.networkId]
        if dash.path[2] then
            --PrintChat(tostring(dash.path[1].x))

            --DrawHandler:Circle3D(unit.position,unit.boundingRadius,Common:Hex(250,254,50,147))
            DrawHandler:Circle3D(D3DXVECTOR3(dash.path[2].x,dash.path[2].y,dash.path[2].z),unit.boundingRadius,Common:Hex(250,254,50,147))
           distance = t * dash.dashSpeed - unit.boundingRadius/10 - width/2
        end
    end

    
    local savePaths = 0
    local newPath = {}
    local returnPos = unit.position
 
    for i,p in pairs(unit.aiManagerClient.navPath.paths) do
        if i > 1 then
            local lP = unit.aiManagerClient.navPath.paths[i-1]
            local distanceUnit_LP = self:GetDistanceToZeroPos(unit.position,lP)
            local distanceP_LP = self:GetDistanceToZeroPos(p,lP)
            local checkPos = {x = lP.x + (p.x - lP.x)/distanceP_LP * distanceUnit_LP, y = unit.position.y, z = lP.z + (p.z - lP.z)/distanceP_LP * distanceUnit_LP}
            
           local test = D3DXVECTOR3(checkPos.x,checkPos.y,checkPos.z)
            if self:GetDistanceToZeroPos(unit.position,checkPos) < 20 and distanceUnit_LP < distanceP_LP then
                savePaths = i
            end
            if i >= savePaths and savePaths > 1 then
                table.insert(newPath,p)
            end
        end
    end

    
    if #newPath > 0 then returnPos = newPath[#newPath] end

    local ppD = 0
   
    for i,p in pairs(newPath) do
        if ppD < distance then
            local lP = unit.position
            if i > 1 then lP = newPath[i-1] end
            ppD = ppD + math.sqrt((lP.x - p.x) ^ 2 + (lP.z - p.z) ^ 2)
        end
        if ppD > distance then
            local extra = 0
            local lP = unit.position
            local startV = unit.position
            if i > 1 then startV = newPath[i-1] end
            if i > 1 then extra = self:GetDistanceToZeroPos(lP,startV) end
            local calcD = ppD - (ppD - distance + extra)
            local distanceP_LP = self:GetDistanceToZeroPos(p,lP)
            local predPos = {x = startV.x + (p.x - startV.x)/distanceP_LP * (calcD), y = unit.position.y, z = startV.z + (p.z - startV.z)/distanceP_LP * (calcD)}
            local test = D3DXVECTOR3(predPos.x,predPos.y,predPos.z)
            returnPos = test
            break
        end
    end
    

    if unit.position.x - math.floor(unit.position.x) ~= 0 and self:GetDistanceToZeroPos(returnPos,unit.position) > 0 then
        return returnPos
    elseif unit.position.x - math.floor(unit.position.x) == 0 then
        return unit.position
    else
        return nil
    end

    return unit.position
end

function Common:FreshWaypoint(unit)
    local wp = self.WayPoints[unit.networkId]
    if wp == nil then return false end
    if RiotClock.time - wp.time < 0.1 then
        return true
    end
    return false
end

function Common:IsCCed(unit)
    if self.HardCCBuffs[unit.networkId] ~= nil then return true end
    if self.MoveCCBuffs[unit.networkId] ~= nil then return true end
    return false
end

function Common:CalcPhysicalDamage(source, target, dmg)
	if target.isInvulnerable then return 0 end
	
  local result = 0
  local baseArmor = target.characterIntermediate.armor
  local Lethality = source.characterIntermediate.physicalLethality * (0.6 + 0.4 * source.experience.level / 18)
  baseArmor = baseArmor - Lethality
  
  if baseArmor < 0 then baseArmor = 0 end
  if (baseArmor >= 0 ) then
	local armorPenetration = source.characterIntermediate.percentArmorPenetration
	local armor = baseArmor - ((armorPenetration*baseArmor) / 100)
	result = dmg * (100 / (100 + armor))
  end
  
  return result
end

function Common:CalcMagicalDamage(source, target, dmg)
	if target.isInvulnerable then return 0 end
	
	local result = 0
  
  local baseArmor = target.characterIntermediate.spellBlock
  local Lethality = source.characterIntermediate.flatMagicPenetration
	baseArmor = baseArmor - Lethality
  
  if baseArmor < 0 then baseArmor = 0 end
  if (baseArmor >= 0 ) then
	local armorPenetration = source.characterIntermediate.percentMagicPenetration
	local armor = baseArmor - ((armorPenetration*baseArmor) / 100)
	result = dmg * (100 / (100 + armor))
  end
  
  return result
end

-- CHAMPION

function LegitTristana:__init()

    PrintChat("<font color=\"#8f70f4\"><b>[Legit - Tristana]</b></font><b> <font color=\"#FFFFFF\"> v" .._tristanaVERSION.. " loaded!</font>")

    self.legitMenu = Menu("_legitTristanaMenu", "[Legit Tristana] v" .._tristanaVERSION)
    self.legitMenu:sub('combo', 'Combo')
        self.legitMenu.combo:checkbox('useQ', 'Use Q', true)
        self.legitMenu.combo:checkbox('useE', 'Use E', true)
        self.legitMenu.combo:checkbox('useR', 'Use R', true)
        self.legitMenu.combo:checkbox('forceE', 'Force Attack on bomb', true)

    self.legitMenu:sub('ks', 'Killsteal')
        self.legitMenu.ks:checkbox('useAA', 'Use Auto Attack :)', false)
        self.legitMenu.ks:checkbox('useR', 'Use R', true)

    self.Q,self.W,self.E,self.R,self.S1,self.S2 = SpellSlot.Q,SpellSlot.W,SpellSlot.E,SpellSlot.R,SpellSlot.Summoner1,SpellSlot.Summoner2
    self.ItemSlot = {[1] = SpellSlot.Item1, [2]= SpellSlot.Item2, [3]= SpellSlot.Item3,[4]= SpellSlot.Item4,[5]= SpellSlot.Item5,[6]= SpellSlot.Item6}
    self.eBuff = {}
    self.myMissiles = {}

    AddEvent(Events.OnBuffGain, 	function(a,b) self:OnBuffGain(a,b) end)
    AddEvent(Events.OnBuffLost, 	function(a,b) self:OnBuffLost(a,b) end)
    AddEvent(Events.OnCreateObject, function(a,b) self:OnCreateObject(a,b) end)
    AddEvent(Events.OnDeleteObject, function(a)   self:OnDeleteObject(a) end)
    AddEvent(Events.OnTick,function() self:OnTick() end)
    --AddEvent(Events.OnDraw,function() self:OnDraw() end)

end

function LegitTristana:OnBuffGain(unit,buff)
    if unit.type == myHero.type then
        if buff.name == "tristanaechargesound" then
            if self.eBuff[unit.networkId] == nil then
                self.eBuff[unit.networkId] = 0
                if self.legitMenu.combo.forceE:get() then
                    LegitOrbwalker:SetForcedTarget(unit)
                end
            end
        end
    end
end

function LegitTristana:OnBuffLost(unit,buff)
    if unit.type == myHero.type then
        if buff.name == "tristanaechargesound" then
            self.eBuff[unit.networkId] = nil
            if self.legitMenu.combo.forceE:get() then
                LegitOrbwalker:UnsetForcedTarget()
            end
        end
    end
end

function LegitTristana:OnCreateObject(o,ID)
    if (o.type == GameObjectType.MissileClient and o.asMissile.spellCaster.networkId == myHero.networkId) then
        if string.find(string.lower(o.name),"attack") then
            local eCount = self.eBuff[o.asMissile.target.networkId]
            if eCount and eCount < 4 then self.eBuff[o.asMissile.target.networkId] = eCount + 1 end
            local multi = string.find(string.lower(o.name),"crit") and 2 or 1
            if self.myMissiles[o.asMissile.target.networkId] == nil then self.myMissiles[o.asMissile.target.networkId] = {} end
            table.insert(self.myMissiles[o.asMissile.target.networkId],{missile = o, dmg = math.floor(Common:CalcPhysicalDamage(myHero,o.asMissile.target,Common:TotalAD(myHero))*multi)})
        end
    end
end

function LegitTristana:OnDeleteObject(o)	
    if o.asMissile.target ~= nil then
        local myMissiles = self.myMissiles[o.asMissile.target.networkId]
        if myMissiles then
            for i, m in pairs(myMissiles) do
                if m.missile.networkId == o.networkId then
                    if #self.myMissiles[o.asMissile.target.networkId] > 1 then
                        table.remove(self.myMissiles[o.asMissile.target.networkId],i)
                    else
                        self.myMissiles[o.asMissile.target.networkId] = nil
                    end
                end
            end
        end
    end
end

function LegitTristana:OnTick()
    if LegitOrbwalker:GetMode() == "Combo" then
        self:Combo()
    end
    self:AutoKS()
end

--[[
function LegitTristana:OnDraw()

end
]]

function LegitTristana:Combo()
    local target = LegitOrbwalker:GetTarget()
    if target then
        if self.legitMenu.combo.useE:get() and Common:IsReady(self.E) and not LegitOrbwalker:IsAttacking() and self.eBuff[target.networkId] == nil and self:AirDMG(target) < target.health + target.attackShield then
            local eTarget = LegitOrbwalker:GetTarget(myHero.characterIntermediate.attackRange + 350)
            if eTarget == target and (Common:GetDistanceToZero(myHero,target) < myHero.characterIntermediate.attackRange - 100 or Common:IsCCed(target) == true) then
                myHero.spellbook:CastSpell(SpellSlot.E, target.networkId)
                if self.legitMenu.combo.forceE:get() then
                    LegitOrbwalker:SetForcedTarget(target)
                end
            end
        end
        if self.legitMenu.combo.useQ:get() and LegitOrbwalker:IsAttacking() and Common:IsReady(self.Q) and self:AirDMG(target) < target.health + target.attackShield then
            myHero.spellbook:CastSpell(SpellSlot.Q, myHero.networkId)
        end
        if self.legitMenu.combo.useR:get() and Common:IsReady(self.R) then
            local eDMG = self:eDMG(target)
            local targetHP = target.health + target.attackShield
            if targetHP + (Common:TotalAD(myHero)/3)*(1+myHero.characterIntermediate.crit) > self:AirDMG(target) + eDMG then
                local rDMG = Common:CalcMagicalDamage(myHero,target,200+100*Common:SpellInfo(self.R).level + Common:BonusAP(myHero))
                if targetHP + target.magicShield + self:NullOrb(target) < eDMG + rDMG + self:AirDMG(target) then
                    myHero.spellbook:CastSpell(self.R, target.networkId)
                end
            end
        end
    end
end

function LegitTristana:AutoKS()
    for i,enemy in pairs(ObjectManager:GetEnemyHeroes()) do
        local range = math.floor(myHero.characterIntermediate.attackRange + myHero.boundingRadius + enemy.boundingRadius)
        if enemy.team ~= myHero.team and not enemy.isInvulnerable and not enemy.isDead and enemy.isVisible and enemy.isTargetable and Common:GetDistanceToZero(myHero,enemy) < range then
            if self.legitMenu.ks.useAA:get() then
                if LegitOrbwalker:CanAttack() and LegitOrbwalker:IsAttacking() == false then
                    local aaDMG = Common:CalcPhysicalDamage(myHero, enemy, Common:TotalAD(myHero))
                    local eDMG = self:eDMG(enemy)
                    local targetHP = enemy.health + 10 + enemy.attackShield
                    if targetHP < aaDMG + eDMG and targetHP > eDMG + self:AirDMG(enemy) then
                        LegitOrbwalker:Attack(enemy)
                        return
                    end
                end
            end
            if self.legitMenu.ks.useR:get() then
                if Common:IsReady(self.R) then
                    local eDMG = self:eDMG(enemy)
                    local targetHP = enemy.health + enemy.attackShield
                    local rDMG = math.floor(Common:CalcMagicalDamage(myHero,enemy,200+100*Common:SpellInfo(self.R).level + Common:BonusAP(myHero)))
                    if targetHP + enemy.magicShield + self:NullOrb(enemy) < eDMG + rDMG + self:AirDMG(enemy) and targetHP > self:AirDMG(enemy) then
                        myHero.spellbook:CastSpell(self.R, enemy.networkId)
                        return
                    end
                end
            end
        end
    end
end

function LegitTristana:AirDMG(unit)
    local dmg = 0
    if self.myMissiles[unit.networkId] then
        for i,m in pairs(self.myMissiles[unit.networkId]) do
            dmg = dmg + m.dmg
        end
    end
    return dmg
end

function LegitTristana:eStack(unit)
    if self.eBuff[unit.networkId] ~= nil then return self.eBuff[unit.networkId] end
    return -1
end

function LegitTristana:eDMG(unit)
    local eStacksOnTarget = self:eStack(unit)
    if eStacksOnTarget < 0 then return 0 end
    local eLVL = Common:SpellInfo(self.E).level
    local minPhys = 60+10*eLVL + ((0.3+0.2*eLVL)*Common:BonusAD(myHero)) + 0.5*Common:BonusAP(myHero)
    local perStack = (18+3*eLVL + ((0.09+0.06*eLVL)*Common:BonusAD(myHero)) + 0.15*Common:BonusAP(myHero))*eStacksOnTarget
    return math.floor(Common:CalcPhysicalDamage(myHero,unit, math.floor(minPhys + perStack)))
end

function LegitTristana:NullOrb(unit)
    return math.floor(35.294 + 4.706*unit.experience.level)
end

-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

function OnLoad()
    Common:Init()
end