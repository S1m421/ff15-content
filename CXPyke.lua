local Pyke = {}
local version = 1
require "FF15Menu"
require "utils"
local DreamTS = require("DreamTS")
local dmgLib = require("FF15DamageLib")

function OnLoad()
    if not _G.Prediction then
        LoadPaidScript(PaidScript.DREAM_PRED)
    end
end

function Pyke:__init()
    self.q = {
        type = "linear",
        speed = 2000,
        range = 1050,
        delay = 0.2,
        width = 70
    }
    self.r = {
        type = "circular",
        speed = math.huge,
        range = 750,
        delay = 0.5,
        radius = 100
    }
    self:Menu()
    self.LastQ = 0
    self.scale = {[1] = 250,[2] = 290,[3] = 330,[4] = 370,[5] = 400, [6] = 430, [7] = 450, [8] = 470, [9] = 490, [10] = 510, [11] = 530, [12] = 540, [13] = 550}
    self.TS =
        DreamTS(
        self.menu.dreamTs,
        {
            ValidTarget = function(unit)
                return _G.Prediction.IsValidTarget(unit)
            end,
            Damage = function(unit)
                return dmgLib:CalculateMagicDamage(myHero, unit, 100)
            end
        }
    )
    AddEvent(Events.OnTick, function() self:OnTick() end)
    AddEvent(Events.OnDraw, function() self:OnDraw() end)
    AddEvent(Events.OnBuffGain, function(obj, buff) self:OnBuffGain(obj, buff) end)
    AddEvent(Events.OnBuffLost, function(obj, buff) self:OnBuffLost(obj, buff) end)
    PrintChat("<font color=\"#E41B17\">[<b>¤ Cyrex ¤</b>]:</font>" .. " <font color=\"#" .. "FFFFFF" .. "\">" .. "Pyke Loaded" .. "</font>")
    self.font = DrawHandler:CreateFont("Calibri", 12)
end

function Pyke:Menu()
    self.menu = Menu("cxPyke", "Cyrex Pyke")
    self.menu:sub("dreamTs", "Target Selector")

    self.menu:sub("Key", "Key Settings")
        --self.menu.Key:checkbox("e", "Start Combo With Q", true, string.byte("K"))

    self.menu:sub("combo", "Combo Settings")
        self.menu.combo:label("xd", "Q Settings")
        self.menu.combo:checkbox("q", "Use Smart Q", true)
        self.menu.combo:label("xd2", "E Settings")
        self.menu.combo:checkbox("e", "Use E", true)
        self.menu.combo:slider("range", "Dynamic E Range", 100, 500, 400, 10)

    self.menu:sub("harass", "Harass Settings")
        self.menu.harass:checkbox("q", "Use Long Q", true)
        self.menu.harass:slider("mana", "Min. Mana Percent: ", 0, 100, 10, 5)

    self.menu:sub("auto", "Automatic Settings")
        self.menu.auto:label("dx", "Killsteal Settings")
        self.menu.auto:checkbox("uks", "Use Smart Killsteal", true)
        self.menu.auto:checkbox("urks", "Use R in Killsteal", true)

    self.menu:sub("draws", "Draw")
        self.menu.draws:checkbox("q", "Q", true)
        self.menu.draws:checkbox("e", "E", true)
    self.menu:label("version", "Version: " .. version .. "")
    self.menu:label("author", "Author: Coozbie")
end

function Pyke:OnDraw()
    if self.menu.draws.q:get() and myHero.visibleOnScreen then
        DrawHandler:Circle3D(myHero.position, self:q_range(), self:Hex(255, 7, 141, 237))
    end
    if self.menu.draws.e:get() and myHero.visibleOnScreen then
        DrawHandler:Circle3D(myHero.position, self.menu.combo.range:get(), self:Hex(255, 7, 141, 237))
    end    
end

local delayedActions, delayedActionsExecuter = {}, nil
function Pyke:DelayAction(func, delay, args) --delay in seconds
  if not delayedActionsExecuter then
    function delayedActionsExecuter()
      for t, funcs in pairs(delayedActions) do
        if t <= os.clock() then
          for i = 1, #funcs do
            local f = funcs[i]
            if f and f.func then
              f.func(unpack(f.args or {}))
            end
          end
          delayedActions[t] = nil
        end
      end
    end
    AddEvent(Events.OnTick, delayedActionsExecuter)
  end
  local t = os.clock() + (delay or 0)
  if delayedActions[t] then
    delayedActions[t][#delayedActions[t] + 1] = {func = func, args = args}
  else
    delayedActions[t] = {{func = func, args = args}}
  end
end

function Pyke:GetPercentHealth(obj)
  local obj = obj or myHero
  return (obj.health / obj.maxHealth) * 100
end

function Pyke:MoveToMouse()
    local pos = pwHud.hudManager.virtualCursorPos
    myHero:IssueOrder(GameObjectOrder.MoveTo, pos)
end

function Pyke:GetBonusAD(obj)
  local obj = obj or myHero
  return (obj.characterIntermediate.flatPhysicalDamageMod)
end


function Pyke:GetAARange(target)
  return myHero.characterIntermediate.attackRange + myHero.boundingRadius + (target and target.boundingRadius or 0)
end

function Pyke:r_damage()
    if myHero.experience.level < 6 then return 0 end
    local damage = self.scale[myHero.experience.level - 5]
    local bonus = self:GetBonusAD()
    local lethality = myHero.characterIntermediate.physicalLethality
    local total = (damage + (bonus * 0.8) + (lethality * 1.5))
    return total
end


function Pyke:q_range()
    local t = os.clock() - self.LastQ
    local range = 400;

    if t > 0.5 then
        range = range + (t/.1 * 62);
    end
    
    if range > 1050 then
        return 1050
    end

    return range
end

function Pyke:spear(unit)
    if myHero.spellbook:CanUseSpell(0) ~= 0 then return end
    if (myHero.spellbook:CanUseSpell(2) == 0 and GetDistance(unit) < self.menu.combo.range:get()) and not myHero.buffManager:HasBuff('PykeQ') and self.menu.combo.e:get() then return end
    if GetDistance(unit) > 1050 then return end

    local pred = _G.Prediction.GetPrediction(unit, self.q, myHero)
    if not pred then return end
        
    if pred.castPosition and not (pred:minionCollision() or GetDistance(unit) <= 400) and (pred.realHitChance == 1 or _G.Prediction.WaypointManager.ShouldCast(unit)) then
        if myHero.buffManager:HasBuff('PykeQ') then
            if (GetDistance(unit) + 181 < self:q_range()) and not unit.buffManager:HasBuff("BlackShield") then
                myHero.spellbook:UpdateChargeableSpell(0, pred.castPosition, true)
            end
        else
            myHero.spellbook:CastSpell(0, pwHud.hudManager.virtualCursorPos)
        end
    end
end

function Pyke:LongShortQ(unit)
    if myHero.spellbook:CanUseSpell(0) ~= 0 then return end
    if (myHero.spellbook:CanUseSpell(2) == 0) and self.menu.combo.e:get() then return end
    if GetDistance(unit) > 1050 then return end

    local pred = _G.Prediction.GetPrediction(unit, self.q, myHero)
    if not pred then return end

    if pred.castPosition and (pred.realHitChance == 1) and not pred:minionCollision() and not unit.buffManager:HasBuff("BlackShield") then
        if myHero.buffManager:HasBuff('PykeQ') then
            if (GetDistance(unit) < 1050) then
                myHero.spellbook:UpdateChargeableSpell(0, pred.castPosition, true)
            end
        end
    end
end


function Pyke:ShortQ(unit)
    if myHero.spellbook:CanUseSpell(0) ~= 0 then return end
    if (myHero.spellbook:CanUseSpell(2) == 0) and not myHero.buffManager:HasBuff('PykeQ') and self.menu.combo.e:get() then return end
    if GetDistance(unit) > 400 then return end

    local pred = _G.Prediction.GetPrediction(unit, self.q, myHero)
    if not pred then return end

    if pred.castPosition and (pred.realHitChance == 1) then
        if myHero.buffManager:HasBuff('PykeQ') then
            if (GetDistance(unit) < 400 and self:q_range() <= 400) and not unit.buffManager:HasBuff("BlackShield") then
                myHero.spellbook:UpdateChargeableSpell(0, pred.castPosition, true)
            end
        else
            myHero.spellbook:CastSpell(0, pwHud.hudManager.virtualCursorPos)
        end
    end
end

function Pyke:dash(unit)
    if not self.menu.combo.e:get() then return end
    if myHero.spellbook:CanUseSpell(2) ~= 0 then return end
    if myHero.buffManager:HasBuff('PykeQ') then return end
    if GetDistance(unit) > self.menu.combo.range:get() then return end
    if IsValidTarget(unit) then
        myHero.spellbook:CastSpell(2, unit.position)
    end
end


function Pyke:CastR(unit)
    if myHero.spellbook:CanUseSpell(3) == 0 then
        local pred = _G.Prediction.GetPrediction(unit, self.r, myHero)
        if pred and pred.castPosition and (pred.realHitChance == 1 or _G.Prediction.WaypointManager.ShouldCast(unit)) and GetDistanceSqr(pred.castPosition) <= self.r.range * self.r.range then
            myHero.spellbook:CastSpell(3, pred.castPosition)
        end
    end
end


function Pyke:KillSteal()
    for i, enemy in pairs(ObjectManager:GetEnemyHeroes()) do
        if enemy and enemy.team ~= myHero.team and not enemy.isInvulnerable and not enemy.isDead and enemy.isTargetable and not enemy.buffManager:HasBuff('SionPassiveZombie') and not enemy.buffManager:HasBuff("FioraW") and not enemy.buffManager:HasBuff("UndyingRage") and not enemy.buffManager:HasBuff("KarthusDeathDefiedBuff") and not enemy.buffManager:HasBuff("KayleR") and not enemy.buffManager:HasBuff("KindredRNoDeathBuff") and not enemy.buffManager:HasBuff("WillRevive") then
            local hp = enemy.health
            local dist = GetDistanceSqr(enemy)
            local r = myHero.spellbook:CanUseSpell(3) == 0
            if r and dist < (750 * 750) and self:r_damage() > hp and self.menu.auto.urks:get() then
                self:CastR(enemy)
            end
        end
    end
end


function Pyke:OnTick()
    local target = self:GetTarget(1050)
    if LegitOrbwalker:GetMode() == "Combo" then
        if target and ValidTarget(target) then
            if GetDistance(target) < self:GetAARange() and myHero.spellbook:CanUseSpell(0) == 0 then
                LegitOrbwalker:BlockAttack(true)
            else
                LegitOrbwalker:BlockAttack(false)
            end
            if self:GetPercentHealth(target) > 30 and myHero.spellbook:CanUseSpell(3) == 0 then
                if self.menu.combo.q:get() then
                    if GetDistance(target) > 400 and myHero.spellbook:CanUseSpell(0) == 0 then
                        self:spear(target)
                    elseif GetDistance(target) < 400 and myHero.spellbook:CanUseSpell(0) == 0 and myHero.buffManager:HasBuff('PykeQ') then
                        self:LongShortQ(target)
                    elseif GetDistance(target) < 350 and myHero.spellbook:CanUseSpell(0) == 0 then
                        self:ShortQ(target) 
                    elseif GetDistance(target) < 400 and self:GetPercentHealth(target) < 50 and myHero.spellbook:CanUseSpell(0) == 0 then
                        self:ShortQ(target)
                    end
                end
                self:DelayAction(function() self:dash(target) end, 0.25)
            elseif myHero.spellbook:CanUseSpell(3) ~= 0 then
                if self.menu.combo.q:get() then
                    if GetDistance(target) > 400 and myHero.spellbook:CanUseSpell(0) == 0 then
                        self:spear(target)
                    elseif GetDistance(target) < 400 and myHero.spellbook:CanUseSpell(0) == 0 and myHero.buffManager:HasBuff('PykeQ') then
                        self:LongShortQ(target)
                    elseif GetDistance(target) < 350 and myHero.spellbook:CanUseSpell(0) == 0 then
                        self:ShortQ(target) 
                    elseif GetDistance(target) < 400 and self:GetPercentHealth(target) < 50 and myHero.spellbook:CanUseSpell(0) == 0 then
                        self:ShortQ(target)
                    end
                end
                self:DelayAction(function() self:dash(target) end, 0.25)
            end
        end
    end
    if LegitOrbwalker:GetMode() == "Harass" then
        if target and ValidTarget(target) then
            if myHero.mana / myHero.maxMana * 100 >= self.menu.harass.mana:get() then
                if self.menu.harass.q:get() then         
                    self:spear(target)
                end
            end        
        end
    end
    if self.menu.auto.urks:get() then self:KillSteal() end
    if myHero.buffManager:HasBuff('PykeQ') then
        LegitOrbwalker:BlockAttack(true)
    else
        LegitOrbwalker:BlockAttack(false)
    end
end

function Pyke:OnBuffGain(obj, buff)
    if obj == myHero and buff.name == "PykeQ" then
        self.LastQ = os.clock()
    end
end

function Pyke:OnBuffLost(obj, buff)
    if obj == myHero and buff.name == "PykeQ" then
        self.LastQ = 0
    end
end


function Pyke:Hex(a, r, g, b)
    return string.format("0x%.2X%.2X%.2X%.2X", a, r, g, b)
end

function Pyke:GetTarget(dist, all)
    self.TS.ValidTarget = function(unit)
        return _G.Prediction.IsValidTarget(unit, dist)
    end
    local res = self.TS:update()
    if all then
        return res
    else
        if res and res[1] then
            return res[1]
        end
    end
end

if myHero.charName == "Pyke" then
    Pyke:__init()
end
