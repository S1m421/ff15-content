local version = 0.01
local author = "Maxxxel"
local open = io.open

--Auto Update + Download levelPresets + FF15Menu
local liveVersion = GetInternalWebResult("FF15AutoLevel.version")

if tonumber(liveVersion) > version then
	PrintChat("<font color=\"#FFFF0000\"><b>FF15AutoLevel:</b></font><b><font color=\"#FFFFFFFF\"> Updated to v"..version..", please press F5 to reload.</font>")
	DownloadInternalFile('FF15AutoLevel.lua', SCRIPT_PATH .. "FF15AutoLevel.lua")
	return
end

if not open(COMMON_PATH .. "FF15Menu.lua", "r") then
	PrintChat("<font color=\"#FFFF0000\"><b>FF15AutoLevel:</b></font><b><font color=\"#FFFFFFFF\"> FF15Menu downloaded, please press F5 to reload.</font>")
	DownloadInternalFile("FF15Menu.lua", COMMON_PATH .. "FF15Menu.lua")
	return
end

if not open(COMMON_PATH .. "levelPresets.lua", "r") then
	PrintChat("<font color=\"#FFFF0000\"><b>FF15AutoLevel:</b></font><b><font color=\"#FFFFFFFF\"> Level presets downloaded, please press F5 to reload.</font>")
	DownloadInternalFile("levelPresets.lua", COMMON_PATH .. "levelPresets.lua")
	return
end

--Requirements
require 'FF15Menu'
local levelPresets = require 'levelPresets'

--Funcs
local function GetLevelData(unit)
	local preset
	local myHeroName = myHero.charName:lower()

	for name, data in pairs(levelPresets) do
		if name:lower():find(myHeroName) then
			preset = data
			break
		end
	end

	levelPresets = nil

	return preset
end

--Menu
local FF15AutoLevelMenu = Menu('FF15AutoLevel', 'FF15 Auto Level')
FF15AutoLevelMenu:checkbox('Enabled', 'Enabled', true)
local myHeroLevelData = GetLevelData(myHero)
if not myHeroLevelData then
	FF15AutoLevelMenu.Enabled:set(false)
	PrintChat("<font color=\"#FFFF0000\"><b>FF15AutoLevel:</b></font><b><font color=\"#FFFFFFFF\"> No levelData found for " .. myHero.charName .. ".</font>")
	FF15AutoLevelMenu:label('Info', 'No Level Data for ' .. myHero.charName)
	return
else
	local list = {"Used: ", "Winrate: "}

	for i = 1, 18 do
		list[1] = list[1] .. myHeroLevelData['mostUsed'][i]
		list[2] = list[2] .. myHeroLevelData['highestRate'][i]
	end

	FF15AutoLevelMenu:list('Mode', 'Level Preset', 1, list)
	FF15AutoLevelMenu:checkbox('Wait', 'Wait on level 1', true)
	FF15AutoLevelMenu:slider('Delay', 'Delay [s]', 0, 5, 1, .5)
end
FF15AutoLevelMenu:label('Info2', 'Vesion: ' .. version .. ' by ' .. author)

local waitingForLevelUp = false
local Q, W, E, R = SpellSlot.Q, SpellSlot.W, SpellSlot.E, SpellSlot.R
local delayedAction

AddEvent(Events.OnTick, function()
	if delayedAction then
		local data = delayedAction
		local wasLeveled = myHero.experience.spellTrainingPoints ~= data[4]

		if wasLeveled then
			delayedAction = nil
			waitingForLevelUp = false
			return
		elseif data[1] then
			local diff = RiotClock.time - data[3]

			if diff >= data[2] then 
				data[1]()
				delayedAction[1] = nil
			end
		end
	end

	if not waitingForLevelUp then
		local xp = myHero.experience
		local actualLevel = xp.level
		local levelPoints = xp.spellTrainingPoints

		if actualLevel == 18 and levelPoints == 0 then 
			waitingForLevelUp = true
			return 
		end

		if levelPoints > 0 then
			if actualLevel == 1 and FF15AutoLevelMenu.Wait:get() then return end

			local mode = FF15AutoLevelMenu.Mode:get() == 1 and "mostUsed" or "highestRate"
			local skillingOrder = myHeroLevelData[mode]
			local QL, WL, EL, RL = 0, 0, 0, myHero.charName == "Karma" and 1 or 0
			local Delay = FF15AutoLevelMenu.Delay:get()
			--Check which level the spell should have
			for i = 1, actualLevel do
				if skillingOrder[i] == "Q" then 		--Q
					QL = QL + 1
				elseif skillingOrder[i] == "W" then		--W
					WL = WL + 1
				elseif skillingOrder[i] == "E" then 	--E
					EL = EL + 1
				elseif skillingOrder[i] == "R" then		--R
					RL = RL + 1
				end
			end

			local sp = myHero.spellbook
			local diffR = sp:Spell(R).level - RL < 0
			local diffQ = sp:Spell(Q).level - QL
			local diffW = sp:Spell(W).level - WL
			local diffE = sp:Spell(E).level - EL
			local lowest = 99
			local spell

			if diffQ < lowest then
				lowest = diffQ
				spell = Q
			end

			if diffW < lowest then
				lowest = diffW
				spell = W
			end

			if diffE < lowest then
				lowest = diffE
				spell = E
			end

			if diffR then
				spell = R
			end

			if spell then
				waitingForLevelUp = true
				delayedAction = {
					function() myHero.spellbook:LevelSpell(spell) end, Delay, RiotClock.time, levelPoints
				}
			end
		end
	end
end)