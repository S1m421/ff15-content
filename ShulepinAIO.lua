----------------------------------------------------
--|                    Setup                     |--
----------------------------------------------------

local _SAIOVER = 0.06
local _SAIOVERONLINE = GetInternalWebResult("ShulepinAIO.version") 

local PrintChat = function(msg)
        return PrintChat("<font color=\"#00E4FF\">[ShulepinAIO]</font> " .. "<font color=\"#ffffff\">" .. msg .. "</font>")
end

local DownloadNewVersion = function(currentVersion, onlineVersion)
        local currentVersion, onlineVersion = tonumber(currentVersion), tonumber(onlineVersion)

        if onlineVersion > currentVersion then
                PrintChat("New Version Is Avaliable - " .. onlineVersion)
                DownloadInternalFile("ShulepinAIO.lua", SCRIPT_PATH .. "ShulepinAIO.lua")
                PrintChat("Download Successful, please press F5 to reload.")
        end
end

DownloadNewVersion(_SAIOVER, _SAIOVERONLINE)

local FileExists = function(path)
        local file = io.open(path, 'r')

        if file then
                io.close(file)
                return true
        end

        return false
end

if FileExists(COMMON_PATH .. "GeometryLib.lua") then 
        require 'GeometryLib'
else
        PrintChat("Downloading GeometryLib...") 
        DownloadInternalFile("GeometryLib.lua", COMMON_PATH .. "GeometryLib.lua")
        PrintChat("Download Successful, please press F5 to reload.")
        return
end

if FileExists(COMMON_PATH .. "FF15Menu.lua") then 
        require 'FF15Menu'
else
        PrintChat("Downloading FF15Menu...") 
        DownloadInternalFile("FF15Menu.lua", COMMON_PATH .. "FF15Menu.lua")
        PrintChat("Download Successful, please press F5 to reload.")
        return
end

local DamageLib
if FileExists(COMMON_PATH .. "FF15DamageLib.lua") then 
        DamageLib = require 'FF15DamageLib'
else
        PrintChat("Downloading FF15DamageLib...") 
        DownloadInternalFile("FF15DamageLib.lua", COMMON_PATH .. "FF15DamageLib.lua")
        PrintChat("Download Successful, please press F5 to reload.")
        return
end

----------------------------------------------------
--|           Local Functions and Vars           |--
----------------------------------------------------

local huge      = math.huge
local sqrt      = math.sqrt
local min       = math.min
local lower     = string.lower
local format    = string.format
local sort      = table.sort

local class = function(name)
        if name then _G[name] = {} end
        local __class = name and _G[name] or {}
        __class.__index = __class

        return setmetatable(__class, {
                __call = function(self, ...)
                        local obj = setmetatable({}, self)
                        return obj:__init(...) or obj
                end
        })
end

local Team = {
        Ally = myHero.team,
        Enemy = 300 - myHero.team,
        Neutral = 300
}

local ImmobileBuffTypes = {
        [BuffType.Stun] = true,
        [BuffType.Taunt] = true,
        [BuffType.Snare] = true,
        [BuffType.Fear] = true,
        [BuffType.Charm] = true,
        [BuffType.Suppression] = true,
}

local ImmobileSpells = {
        { name = "katarinar", duration = 1 },
        { name = "drain", duration = 1 },
        { name = "crowstorm", duration = 1 },
        { name = "absolutezero", duration = 1 },
        { name = "corkitrueshotbarrage", duration = 1 },
        { name = "luxmalicecannon", duration = 1 },
        { name = "reapthewhirlwind", duration = 1 },
        { name = "missfortunebullettime", duration = 1 },
        { name = "shenstandunited", duration = 1 },
}

local BlinkSpells = {
        { name = "corkiarcaneshift", range = 475, delay = 0.25, delay2 = 0.8 }
}

local ARGB = function(a, r, g, b)
        return format("0x%.2X%.2X%.2X%.2X", a, r, g, b)
end

local Color = {
        Red               = ARGB(255, 255, 0, 0),
        Crimson           = ARGB(255, 220, 20, 60),
        DarkRed           = ARGB(255, 139, 0, 0),
        Pink              = ARGB(255, 255, 192, 203),
        DeepPink          = ARGB(255, 255, 20, 147),
        Orange            = ARGB(255, 255, 60, 0),
        Yellow            = ARGB(255, 255, 255, 0),
        Khaki             = ARGB(255, 240, 230, 140),
        Purple            = ARGB(255, 128, 0, 128),
        DarkViolet        = ARGB(255, 148, 0, 211),
        Green             = ARGB(255, 0, 128, 0),
        DarkGreen         = ARGB(255, 0, 100, 0),
        Lime              = ARGB(255, 0, 255, 0),
        LimeGreen         = ARGB(255, 50, 205, 50),
        SpringGreen       = ARGB(255, 0, 255, 127),
        MediumSpringGreen = ARGB(255, 0, 250, 154),
        MediumSeaGreen    = ARGB(255, 60, 179, 113),
        Olive             = ARGB(255, 128, 128, 0),
        Blue              = ARGB(255, 0, 0, 255),
        Navy              = ARGB(255, 0, 0, 128),
        Cyan              = ARGB(255, 0, 255, 255),
        DarkCyan          = ARGB(255, 0, 139, 139),
        SkyBlue           = ARGB(255, 135, 206, 235),
        Brown             = ARGB(255, 162, 42, 42),
        Black             = ARGB(255, 0, 0, 0),
        Gray              = ARGB(255, 128, 128, 128),
        Silver            = ARGB(255, 192, 192, 192),
        White             = ARGB(255, 255, 255, 255)
}

local GetDistanceSqr = function(p1, p2)
        if not p1 or not p2 then return 0 end

        p2 = p2 or myHero
        p1 = p1.position or p1
        p2 = p2.position or p2
  
        local dx = p1.x - p2.x
        local dz = p1.z - p2.z

        return dx * dx + dz * dz
end

local GetDistance = function(p1, p2)
        return sqrt(GetDistanceSqr(p1, p2))
end

local GetMousePos = function()
        return Vector(pwHud.hudManager.activeVirtualCursorPos)
end

local GetMoveSpeed = function(unit)
        return unit.characterIntermediate.movementSpeed
end

local GetTotalAD = function(unit)
        return unit.characterIntermediate.flatPhysicalDamageMod + unit.characterIntermediate.baseAttackDamage
end

local GetTotalAP = function(unit)
        return unit.characterIntermediate.flatMagicDamageMod + unit.characterIntermediate.baseAbilityDamage
end

local GetBonusAD = function(unit)
        return unit.characterIntermediate.flatPhysicalDamageMod
end

local GetRealAutoAttackRange = function(unit, target)
        local unit = unit or myHero
        local result = unit.characterIntermediate.attackRange + unit.boundingRadius

        if target then
                return result + target.boundingRadius
        end

        return result
end

local DrawCircle3D = function(vector, radius, color)
        return DrawHandler:Circle3D(vector:ToDX3(), radius or 150, color or ARGB(175, 255, 255, 255))
end

local IsValidTarget = function(unit, range, from)
        local range = range or huge
        local from = from or myHero
        return unit and unit.isValid and unit.isVisible and not unit.isDead and GetDistance(unit, from) <= range
end

local IsCastingSpell = function(unit)
        return unit.spellbook.spellCasterClient ~= nil and unit.spellbook.spellCasterClient.isCastingSpell
end

local CalcPhysicalDamage = function(source, target, dmg)
        if target.isInvulnerable then return 0 end
        
        local result = 0
        local baseArmor = target.characterIntermediate.armor
        local Lethality = source.characterIntermediate.physicalLethality * (0.6 + 0.4 * source.experience.level / 18)

        baseArmor = baseArmor - Lethality
  
        if baseArmor < 0 then 
                baseArmor = 0 
        end

        if baseArmor >= 0 then
                local armorPenetration = source.characterIntermediate.percentArmorPenetration
                local armor = baseArmor - ((armorPenetration*baseArmor) / 100)
                result = dmg * (100 / (100 + armor))
        end
  
        return result
end

local CalcMagicalDamage = function(source, target, dmg)
        if target.isInvulnerable then return 0 end
        
        local result = 0
        local baseArmor = target.characterIntermediate.spellBlock
        local Lethality = source.characterIntermediate.flatMagicPenetration

        baseArmor = baseArmor - Lethality
  
        if baseArmor < 0 then 
                baseArmor = 0 
        end

        if baseArmor >= 0 then
                local armorPenetration = source.characterIntermediate.percentMagicPenetration
                local armor = baseArmor - ((armorPenetration*baseArmor) / 100)
                result = dmg * (100 / (100 + armor))
        end
  
        return result
end

local CircleCircleIntersection = function(c1, c2, r1, r2)
        local D = GetDistance(c1, c2)
        if D > r1 + r2 or D <= math.abs(r1 - r2) then return nil end
        local A = (r1 * r2 - r2 * r1 + D * D) / (2 * D)
        local H = sqrt(r1 * r1 - A * A)
        local Direction = (c2 - c1):normalized()
        local PA = c1 + A * Direction
        local S1 = PA + H * Direction:perpendicular()
        local S2 = PA - H * Direction:perpendicular()
        return S1, S2
end

local delayedActions, delayedActionsExecuter = {}, nil
local DelayAction = function(func, delay, args)
        if not delayedActionsExecuter then
                delayedActionsExecuter = function()
                        for t, funcs in pairs(delayedActions) do
                                if t <= GetTickCount() then
                                        for i, f in ipairs(funcs) do
                                                f.func(unpack(f.args or {}))
                                        end
                                        delayedActions[t] = nil
                                end
                        end
                end

                AddEvent(Events.OnTick, delayedActionsExecuter)
        end

        local t = GetTickCount() + (delay or 0)

        if delayedActions[t] then
                table.insert(delayedActions[t], { func = func, args = args })
        else
                delayedActions[t] = { { func = func, args = args } }
        end
end

local GetBuffData = function(unit, name)
        local buff = unit.buffManager:HasBuff(name)

        if buff then
                return buff
        end

        return false
end

function base.Vector:Extended(to, distance)
        return self + (to - self):normalized() * distance
end

function base.Vector:ToDX3()
        return D3DXVECTOR3(self.x, self.y, self.z)
end

function base.Vector:Draw(radius, color)
        return DrawCircle3D(self, radius, color)
end

function string.starts(str, start)
        return string.sub(str, 1, string.len(start)) == start
end

function string.ends(str, pattern)
        local len1, len2 = #str, #pattern

        if len1 >= len2 then
                return str:sub(len1 - len2 + 1, len1) == pattern
        end

        return false
end

function table.contains(t, item, key)
        for k = 1, #t do
                local v = t[k]

                if key and v[key] == item or v == item then
                        return k, v
                end
        end
end

----------------------------------------------------
--|                 Hero Manager                 |--
----------------------------------------------------

local HeroManager = {
        Enemy = {
                Count = 0,
                List  = {}
        },

        Ally  = {
                Count = 0,
                List  = {}
        },

        All = {
                Count = 0,
                List  = {}
        }
}

do 
        local heroTable = ObjectManager:GetObjectsByType(GameObjectType.AIHeroClient)
        local heroCount = #heroTable

        for i = 1, heroCount do 
                local hero = heroTable[i]

                HeroManager.All.Count = HeroManager.All.Count + 1
                HeroManager.All.List[HeroManager.All.Count] = hero

                if hero.team == Team.Ally then 
                        HeroManager.Ally.Count = HeroManager.Ally.Count + 1
                        HeroManager.Ally.List[HeroManager.Ally.Count] = hero
                elseif hero.team == Team.Enemy then 
                        HeroManager.Enemy.Count = HeroManager.Enemy.Count + 1
                        HeroManager.Enemy.List[HeroManager.Enemy.Count] = hero
                end
        end

        local GetCountHeroesInRange = function(range)
                local count = 0
                for i = 1, heroCount do 
                        local hero = heroTable[i]
                        if hero and hero.team == Team.Enemy and IsValidTarget(hero, range) then
                                count = count + 1
                        end
                end
                return count
        end

        HeroManager.GetCountHeroesInRange = GetCountHeroesInRange
end

----------------------------------------------------
--|                Object Manager                |--
----------------------------------------------------

local CObjectManager = {
        MinionEnemy   = {},
        MinionAlly    = {},
        JungleMonster = {},
        JunglePlant   = {},
        MinionType    = {},
        JungleType    = {},
        TurretEnemy   = {},
        TurretAlly    = {},
        Barrack       = {},
        Missile       = {},
        Troy          = {},
        Other         = {}
}

CObjectManager.Sort = {
        Health_ASC    = function(l, r) return l.health < r.health end,
        Health_DEC    = function(l, r) return l.health > r.health end,
        MaxHealth_ASC = function(l, r)
                local v1, v2 = l.maxHealth, r.maxHealth
                return v1 == v2 and l.health < r.health or v1 < v2
        end,
        MaxHealth_DEC = function(l, r)
                local v1, v2 = l.maxHealth, r.maxHealth
                return v1 == v2 and l.health < r.health or v1 > v2
        end,
        HPPercent_ASC = function(l, r)
                local v1, v2 = l.health / l.maxHealth, r.health / r.maxHealth
                return v1 == v2 and l.health < r.health or v1 < v2
        end,
        HPPercent_DEC = function(l, r)
                local v1, v2 = l.health / l.maxHealth, r.health / r.maxHealth
                return v1 == v2 and l.health < r.health or v1 > v2
        end
}

CObjectManager.GetMinions = function(minions, range, sortMethod, from)
        local result, size = {}, 0

        for k, minion in pairs(minions) do 
                if minion and minion.isValid and GetDistance(from or myHero, minion) < range then 
                        size = size + 1
                        result[size] = minion
                end
        end

        if sortMethod then table.sort(result, sortMethod) end
        return result, size
end

CObjectManager.GetObjects = function(objects, range, sortMethod, from)
        local result, size = {}, 0

        for k, object in pairs(objects) do
                if object and GetDistance(object, from) < range then
                        size = size + 1
                        result[size] = object
                end
        end

        if sortMethod then table.sort(result, sortMethod) end
        return result, size
end

CObjectManager.GetObjectsByCondition = function(objects, condition, sortMethod, from)
        local result, size = {}, 0

        for i = 1, #objects do
                if objects[i] then
                        for k, object in pairs(objects[i]) do
                                if condition(object) then
                                        size = size + 1
                                        result[size] = object
                                end
                        end
                end
        end

        if sortMethod then table.sort(result, sortMethod) end
        return result, size
end

do
        local OnCreateObject = function(unit, nID)
                if unit.type == GameObjectType.obj_AI_Minion then 
                        if unit.isValid then
                                if unit.team == Team.Ally and unit.name:starts("Minion_T" .. Team.Ally) then 
                                        CObjectManager.MinionAlly[unit.index] = unit
                                elseif unit.team == Team.Enemy and unit.name:starts("Minion_T" .. Team.Enemy) then
                                        CObjectManager.MinionEnemy[unit.index] = unit
                                elseif unit.team == Team.Neutral then
                                        CObjectManager.JungleMonster[unit.index] = unit
                                end
                        end
                elseif unit.type == GameObjectType.MissileClient then
                        CObjectManager.Missile[unit.index] = unit
                elseif unit.name:ends(".troy") then
                        CObjectManager.Troy[unit.index] = unit
                end
        end 

        local OnDeleteObject = function(unit)
                if unit.type == GameObjectType.obj_AI_Minion then 
                        if unit.isValid then
                                if unit.team == Team.Ally and unit.name:starts("Minion_T" .. Team.Ally) then 
                                        CObjectManager.MinionAlly[unit.index] = nil
                                elseif unit.team == Team.Enemy and unit.name:starts("Minion_T" .. Team.Enemy) then
                                        CObjectManager.MinionEnemy[unit.index] = nil
                                elseif unit.team == Team.Neutral and unit.name:starts("SRU") then
                                        CObjectManager.JungleMonster[unit.index] = nil
                                end
                        end
                elseif unit.type == GameObjectType.MissileClient then
                        CObjectManager.Missile[unit.index] = nil
                elseif unit.name:ends(".troy") then
                        CObjectManager.Troy[unit.index] = unit
                end
        end

        local minionTable = ObjectManager:GetObjectsByType(GameObjectType.obj_AI_Minion)
        local minionCount = #minionTable

        for i = 1, minionCount do 
                local minion = minionTable[i]

                if minion and minion.isValid then 
                        OnCreateObject(minion, minion.index)
                end
        end

        AddEvent(Events.OnCreateObject, OnCreateObject)
        AddEvent(Events.OnDeleteObject, OnDeleteObject)
end

----------------------------------------------------
--|                 Path Manager                 |--
----------------------------------------------------

class 'PathManager'

PathManager.Data = {}

for i = 1, HeroManager.All.Count do
        local hero = HeroManager.All.List[i]

        if not PathManager.Data[hero.networkId] then 
                PathManager.Data[hero.networkId] = {
                        wayPoints = {},
                        pathIndex = -1,
                }
        end
end

do
        local GetPathCount = function(unit)
                local unit = unit or myHero
                return #unit.aiManagerClient.navPath.paths
        end

        local GetPathIndex = function(unit)
                local unit = unit or myHero
                return PathManager.Data[unit.networkId] and PathManager.Data[unit.networkId].pathIndex or -1
        end

        local IsMoving = function(unit)
                local unit = unit or myHero
                return unit.aiManagerClient.navPath.isMoving
        end

        local GetWayPoints = function(unit)
                local result = {}

                if PathManager.Data[unit.networkId] then 
                        local data = PathManager.Data[unit.networkId]
                        local pathCount  = GetPathCount(unit)
                        local pathIndex  = GetPathIndex(unit) 
                        local isMoving   = IsMoving(unit)

                        if isMoving then 
                                result[#result + 1] = unit.position

                                for i = pathIndex + 1, pathCount do
                                        if data.wayPoints[i] then
                                                local path = data.wayPoints[i]
                                                result[#result + 1] = path
                                        end
                                end
                        end
                end

                return result
        end

        local UpdateIndex = function()
                for i = 1, HeroManager.All.Count do
                        local hero = HeroManager.All.List[i]

                        if PathManager.Data[hero.networkId] then
                                local data = PathManager.Data[hero.networkId]
                                local pathCount = GetPathCount(hero)

                                if pathCount == 1 then data.pathIndex = 0 end

                                if data.wayPoints[data.pathIndex] then
                                        if GetDistance(hero.position, data.wayPoints[data.pathIndex]) > GetDistance(data.wayPoints[data.pathIndex], data.wayPoints[data.pathIndex + 1]) then
                                                data.pathIndex = min(pathCount - 1, data.pathIndex + 1)
                                        end
                                end
                        end
                end
        end

        local OnNewPath = function(source, paths, isWalk, dashSpeed)
                for i = 1, #paths do
                        if PathManager.Data[source.networkId] then 
                                PathManager.Data[source.networkId].pathIndex = 1
                                PathManager.Data[source.networkId].wayPoints[i] = D3DXVECTOR3(paths[i].x, paths[i].y, paths[i].z)
                        end
                end
        end

        local OnUpdate = function()
                for i = 1, HeroManager.All.Count do
                        local hero = HeroManager.All.List[i]

                        if PathManager.Data[hero.networkId] then
                                local data = PathManager.Data[hero.networkId]
                                local pathCount = GetPathCount(hero)

                                if data.pathIndex ~= -1 then
                                        UpdateIndex()
                                end
                        end
                end
        end

        function PathManager:GetPathCount(unit)
                return GetPathCount(unit)
        end

        function PathManager:GetPathIndex(unit)
                return GetPathIndex(unit)
        end

        function PathManager:IsMoving(unit)
                return IsMoving(unit)
        end

        function PathManager:GetWayPoints(unit)
                return GetWayPoints(unit)
        end

        AddEvent(Events.OnNewPath, OnNewPath)
        AddEvent(Events.OnDraw, OnUpdate)
end

----------------------------------------------------
--|                  Prediction                  |--
----------------------------------------------------

class 'Prediction'

function Prediction:__init()
        self.Data = {}

        for i = 1, HeroManager.All.Count do
                local hero = HeroManager.All.List[i]
                local nID = hero.networkId

                if not self.Data[nID] then
                        self.Data[nID] = {
                                LastCastAttemptT    = 0,
                                ImmobileSpellCastT  = 0,
                                ImmobileBuffTypeT   = 0,
                                SlowBuffTypeT       = 0,

                                DashData = {
                                        isBlink   = false,
                                        startPos  = nil,
                                        endPos    = nil,
                                        startTime = 0,
                                        endTime   = 0,
                                        extraTime = 0,
                                        speed     = 0,
                                        duration  = 0,
                                },
                        }
                end
        end

        AddEvent(Events.OnBuffGain, function(...) self:OnBuffGain(...) end)
        AddEvent(Events.OnProcessSpell, function(...) self:OnProcessSpell(...) end)
        AddEvent(Events.OnNewPath, function(...) self:OnNewPath(...) end)
end

function Prediction:CalculateUnitPosition(unit, delay, speed, radius, from, tempPos)
        local calcPos    = Vector(0, 0, 0)
        local unitVector = Vector(unit)
        local delay      = delay + (NetClient.ping / 1000)
        local pathData   = unit.aiManagerClient.navPath
        local pathCount  = PathManager:GetPathCount(unit)
        local pathIndex  = PathManager:GetPathIndex(unit) + 1
        local pathEndPos = Vector(pathData.paths[pathCount])
        local pathPos    = tempPos and tempPos or Vector(unit)
        local unitMS     = GetMoveSpeed(unit)
        local pathPot    = (unitMS * ((GetDistance(from, pathPos) / speed) + delay))
        local unitBR     = unit.boundingRadius

        if unit.type == myHero.type then
                if pathCount < 3 then
                        local extPos = unitVector:Extended(pathEndPos, pathPot)

                        if GetDistance(unitVector, extPos) > 0 then
                                if GetDistance(unitVector, pathEndPos) >= GetDistance(unitVector, extPos) then
                                        calcPos = extPos
                                else
                                        calcPos = pathEndPos
                                end
                        else
                                calcPos = pathEndPos
                        end 
                else
                        for i = pathIndex, pathCount do
                                if pathData.paths[i] and pathData.paths[i - 1] then
                                        local startPos = i == pathIndex and unitVector or Vector(pathData.paths[i - 1])
                                        local endPos = Vector(pathData.paths[i])
                                        local pathDist = GetDistance(startPos, endPos)
                        
                                        if pathData.paths[pathIndex - 1] then
                                                if pathPot > pathDist then
                                                        pathPot = pathPot - pathDist
                                                else
                                                        local extPos = startPos:Extended(endPos, pathPot)
                        
                                                        calcPos = extPos
                        
                                                        if tempPos then
                                                                return calcPos, calcPos
                                                        else
                                                                return self:CalculateUnitPosition(unit, delay, speed, radius, from, calcPos)
                                                        end
                                                end
                                        end
                                end 
                        end

                        if GetDistance(unitVector, pathEndPos) > unitBR then
                                calcPos = pathEndPos
                        else
                                calcPos = unitVector
                        end
                end
        else
                local extPos = unitVector:Extended(pathEndPos, pathPot - unitBR)
                
                if GetDistance(unitVector, extPos) > 0 then
                        if GetDistance(unitVector, pathEndPos) >= GetDistance(unitVector, extPos) then
                                calcPos = extPos
                        else
                                calcPos = pathEndPos
                        end
                else
                        calcPos = pathEndPos
                end
        end

        calcPos = calcPos and calcPos or unitVector

        if tempPos then
                return calcPos, calcPos
        else
                return self:CalculateUnitPosition(unit, delay, speed, radius, from, calcPos)
        end
end

function Prediction:IsDashing(unit, delay, radius, speed, from)
        local OnDash, CanHit, UnitPosition = false, false, nil

        if self.Data[unit.networkId] then
                local unitMS = GetMoveSpeed(unit)
                local data = self.Data[unit.networkId] 
                local dashData = data.DashData

                if dashData.endTime >= RiotClock.time then 
                        OnDash = true

                        if dashData.isBlink then
                                if (dashData.endTime - RiotClock.time) <= (delay + GetDistance(from, dashData.endPos) / speed) then
                                        UnitPosition = Vector(dashData.endPos.x, 0, dashData.endPos.z)
                                        CanHit = (unitMS * (delay + GetDistance(from, dashData.endPos) / speed - (dashData.extraTime - RiotClock.time))) < radius
                                end

                                if ((dashData.endTime - RiotClock.time) >= (delay + GetDistance(from, dashData.startPos) / speed)) and not CanHit then
                                        UnitPosition = Vector(dashData.startPos.x, 0, dashData.startPos.z)
                                        CanHit = true
                                end
                        else
                                local t1, p1, t2, p2, dist = VectorMovementCollision(dashData.startPos, dashData.endPos, dashData.speed, Vector(from), speed, (RiotClock.time - dashData.startTime) + delay)
                                t1, t2 = (t1 and 0 <= t1 and t1 <= (dashData.endTime - RiotClock.time - delay)) and t1 or nil, (t2 and 0 <= t2 and t2 <=  (dashData.endTime - RiotClock.time - delay)) and t2 or nil
                                local t = t1 and t2 and min(t1, t2) or t1 or t2

                                if t then 
                                        UnitPosition = t == t1 and Vector(p1.x, 0, p1.y) or Vector(p2.x, 0, p2.y)
                                        CanHit = true
                                else
                                        UnitPosition = Vector(dashData.endPos.x, 0, dashData.endPos.z)
                                        CanHit = (unitMS * (delay + GetDistance(from, UnitPosition) / speed - (dashData.endTime - RiotClock.time))) < radius
                                end
                        end
                end
        end

        return OnDash, CanHit, UnitPosition
end

function Prediction:IsImmobile(unit, delay, radius, speed, from)
        if self.Data[unit.networkId] then 
                local unitMS = GetMoveSpeed(unit)
                local data = self.Data[unit.networkId]
                local extraDelay = speed == huge and 0 or (GetDistance(from, unit) / speed)

                if data.ImmobileSpellCastT + (radius / unitMS) > RiotClock.time + delay + extraDelay then
                        return true, Vector(unit), Vector(unit)
                end

                if data.ImmobileBuffTypeT + (radius / unitMS) > RiotClock.time + delay + extraDelay then
                        return true, Vector(unit), Vector(unit)
                end
        end
                 
        return false, Vector(unit), Vector(unit)
end

function Prediction:IsSlowed(unit, delay, speed, from)
        if self.Data[unit.networkId] then 
                local data = self.Data[unit.networkId]
                local delta = GetDistance(unit, from) / speed

                if data.SlowBuffTypeT > RiotClock.time + delay + delta then 
                        return true
                end
        end

        return false
end

function Prediction:OnBuffGain(source, buff)
        if source and self.Data[source.networkId] then 
                local data = self.Data[source.networkId]

                if ImmobileBuffTypes[buff.type] then 
                        data.ImmobileBuffTypeT = RiotClock.time + buff.remainingTime
                end

                if buff.type == BuffType.Slow then 
                        data.SlowBuffTypeT = RiotClock.time + buff.remainingTime
                end
        end
end

function Prediction:OnProcessSpell(source, args)
        if source and self.Data[source.networkId] then 
                local data = self.Data[source.networkId]
                local spellName = lower(args.spellData.name) 

                data.LastCastAttemptT = RiotClock.time + 0.25
                        
                for i = 1, #ImmobileSpells do 
                        local immobileSpell = ImmobileSpells[i]
                        local immobileSpellName = lower(immobileSpell.name)
                        local immobileSpellDuration = immobileSpell.duration

                        if spellName == immobileSpellName then 
                                data.ImmobileSpellCastT = RiotClock.time + immobileSpell.duration
                        end
                end

                for i = 1, #BlinkSpells do
                        local blinkSpell = BlinkSpells[i]
                        local blinkSpellName = lower(blinkSpell.name)

                        if spellName == blinkSpellName then
                                local startPos = Vector(source)
                                local endPos = GetDistance(source, Vector(args.endPos)) < blinkSpell.range and Vector(args.endPos) or Vector(source) + blinkSpell.range * (Vector(args.endPos) - Vector(source)):normalized()
                                local endTime = RiotClock.time + blinkSpell.delay
                                local extraTime = RiotClock.time + blinkSpell.delay2

                                data.DashData.isBlink = true
                                data.DashData.duration = blinkSpell.delay
                                data.DashData.startPos = startPos
                                data.DashData.endPos = endPos
                                data.DashData.endTime = endTime
                                data.DashData.extraTime = extraTime
                        end
                end
        end
end

function Prediction:OnNewPath(source, paths, isWalk, dashSpeed)
        if source and self.Data[source.networkId] then 
                local data = self.Data[source.networkId]

                if not isWalk then 
                        local startPos = D3DXVECTOR3(paths[1].x, paths[1].y, paths[1].z)
                        local endPos = D3DXVECTOR3(paths[#paths].x, paths[#paths].y, paths[#paths].z)
                        local startTime = RiotClock.time - (NetClient.ping / 2000)
                        local dashDistance = GetDistance(startPos, endPos)
                        local endTime = startTime + (dashDistance / dashSpeed)

                        data.DashData.startPos = startPos
                        data.DashData.endPos = endPos
                        data.DashData.startTime = startTime
                        data.DashData.endTime = endTime
                        data.DashData.speed = dashSpeed
                end
        end
end

function Prediction:GetBestCastPosition(unit, delay, radius, range, speed, from, collision)
        local Position, CastPosition, HitChance = Vector(unit), Vector(unit), 0
        local TargetDashing, CanHitDashing, DashPosition = self:IsDashing(unit, delay, radius, speed, from)
        local TargetImmobile, ImmobilePos, ImmobileCastPosition = self:IsImmobile(unit, delay, radius, speed, from)

        if unit.type == myHero.type then
                if TargetDashing then
                        if CanHitDashing then
                                HitChance = 5
                        else
                                HitChance = 0
                        end

                        Position, CastPosition = DashPosition, DashPosition
                elseif TargetImmobile then
                        Position, CastPosition = ImmobilePos, ImmobileCastPosition
                        HitChance = 4
                else
                        CastPosition, Position = self:CalculateUnitPosition(unit, delay, speed, radius, from)
                        HitChance = 1
                end
        else
                CastPosition, Position = self:CalculateUnitPosition(unit, delay, speed, radius, from)
                HitChance = 1
        end

        if not CastPosition or not Position then 
                HitChance = 0
        end

        if GetDistanceSqr(from, Position) >= range * range then 
                HitChance = 0
        end

        if collision and HitChance > 0 then
                if self:MinionCollisionStatus(unit, CastPosition, delay, radius, range, speed, from) then 
                        HitChance = -1
                end
        end

        return CastPosition, HitChance, Position
end

function Prediction:CollisionStatus(unit, checkUnit, checkPosition, delay, radius, range, speed, from)
        if unit.networkId == checkUnit.networkId then return false end

        local checkUnitVector = Vector(checkUnit)
        local checkUnitBR = checkUnit.boundingRadius
        local UnitPosition, CastPosition = Prediction:CalculateUnitPosition(checkUnit, delay, speed, radius, from)

        if GetDistanceSqr(from, checkPosition) <= (range * range) and GetDistanceSqr(from, checkUnitVector) <= (range + 150) * (range + 150) then
                local checkUnitPathCount = PathManager:GetPathCount(checkUnit)

                if checkUnitPathCount > 1 then
                        local pointSegment, pointLine, isOnSegment = VectorPointProjectionOnLineSegment(Vector(from), checkPosition, UnitPosition)
                        local pointSegmentVector = Vector(pointSegment.x, myHero.position.y, pointSegment.y)
                        if isOnSegment and GetDistanceSqr(UnitPosition, pointSegmentVector) <= (checkUnitBR + radius) * (checkUnitBR + radius) then
                                return true
                        end
                end

                local pointSegment, pointLine, isOnSegment = VectorPointProjectionOnLineSegment(Vector(from), checkPosition, checkUnitVector)
                local pointSegmentVector = Vector(pointSegment.x, myHero.position.y, pointSegment.y)
                if isOnSegment and GetDistanceSqr(UnitPosition, pointSegmentVector) <= (checkUnitBR + radius) * (checkUnitBR + radius) then
                        return true
                end
        end

        return false
end

function Prediction:MinionCollisionStatus(unit, checkPosition, delay, radius, range, speed, from)
        local result, size = CObjectManager.GetMinions(CObjectManager.MinionEnemy, range)
        for i = 1, size do 
                local minion = result[i]

                if not minion then break end

                if self:CollisionStatus(unit, minion, checkPosition, delay, radius, range, speed, from) then 
                        return true
                end
        end

        return false
end

local Prediction = Prediction()

----------------------------------------------------
--|                 Spell Manager                |--
----------------------------------------------------

class 'Spell'

function Spell:__init(SpellData)
        self.Slot = SpellData.Slot
        self.Range = SpellData.Range or huge
        self.Delay = SpellData.Delay or 0.25
        self.Speed = SpellData.Speed or huge
        self.Radius = SpellData.Radius or 0
        self.From = SpellData.From or myHero
        self.Collision = SpellData.Collision or false

        return self
end

function Spell:SetFrom(value)
        self.From = value
end

function Spell:IsReady()
        return myHero.spellbook:CanUseSpell(self.Slot) == 0
end

function Spell:CanCast(target, range, from)
        local from = from or myHero.position
        local range = range or self.Range
        return target and target.isValid and target.team ~= myHero.team and target.isVisible and not target.isDead and not target.isInvulnerable and GetDistance(from, target.position) <= range
end

function Spell:GetPrediction(target)
        local CastPosition, HitChance, Position = Prediction:GetBestCastPosition(target, self.Delay, self.Radius, self.Range, self.Speed, self.From, self.Collision)
        return CastPosition, HitChance, Position
end

function Spell:Cast(target)
        return myHero.spellbook:CastSpell(self.Slot, target)
end

function Spell:Draw(color, radius, vector)
        local vector = vector or Vector(myHero)
        local radius = radius or self.Range
        local color = color or ARGB(255, 255, 255, 255)
        return DrawCircle3D(vector, radius, color)
end

function Spell:ToString()
        return ({[0] = "Q", [1] = "W", [2] = "E", [3] = "R"})[self.Slot]
end

----------------------------------------------------
--|                DamageIndicator               |--
----------------------------------------------------

class "DamageIndicator"

function DamageIndicator:__init(menu)
        self.text = {}
        self.color = {}
        self.damage = {}
        self.active = {}
        self.added = {}
        self.condition = {}
        self.size = 0

        self.Menu = menu:sub("shulepin_damage_indicator", "Damage Indicator")
        self.Enable = self.Menu:checkbox("enable", "Enable", true)
end

function DamageIndicator:Add(text, color, damage, condition)
        self.size = self.size + 1
        self.text[self.size] = text
        self.color[self.size] = color
        self.damage[self.size] = damage
        self.condition[self.size] = condition

        for i = 1, self.size do
                local index = "damage_indicator_" .. self.text[i]:lower()
                if not self.added[index] then
                        self.active[i] = self.Menu:checkbox(index, self.text[i], true)
                        self.added[index] = true
                end
        end
end

function DamageIndicator:GetData(unit)
        if not self.Enable:get() then return {} end

        local show, position, value = {}, {}, {}
        local x = 106 / (unit.maxHealth + unit.allShield) 

        for i = 1, self.size do
                position[i] = i == 1 and (unit.health + unit.allShield) * x or position[i - 1]

                if self.active[i]:get() and (self.condition[i])(unit) then
                        value[i] = math.min(position[i], (self.damage[i])(unit) * x)
                        position[i] = position[i] - value[i]
                        if position[i] < 1e-9 then
                                position[i] = 0
                                show[i] = true
                                break
                        end
                        show[i] = true
                end
        end

        return show, position, value
end

function DamageIndicator:Draw(unit)
        local show, position, value = self:GetData(unit)
        for i = 1, self.size do
                if show[i] then
                        local hpBarPos = unit.infoComponent.hpBarScreenPosition
                        DrawHandler:FilledRect(D3DXVECTOR4(hpBarPos.x + position[i] - 47, hpBarPos.y - 24, value[i], 11), self.color[i])
                end
        end
end

----------------------------------------------------
--|                Champion: Corki               |--
----------------------------------------------------

class "Corki"

function Corki:__init(menu)
        self.Q = Spell({
                Slot = 0,
                Range = 825,
                Speed = 1000,
                Radius = 250,
                Delay = 0.25,
                From = myHero,
                Collision = false,
        })

        self.W = Spell({
                Slot = 1,
                Range = 500,
        })

        self.E = Spell({
                Slot = 2,
                Range = 550,
                Speed = 10000,
                Radius = 60,
                Delay = 0.25,
                From = myHero,
                Collision = false,
        })

        self.R = Spell({
                Slot = 3,
                Range = 1225,
                Speed = 2000,
                Radius = 40,
                Delay = 0.175,
                From = myHero,
                Collision = true,
        })

        self.Orbwalker = _G.LegitOrbwalker

        self.Menu = Menu("shulepin_" .. myHero.charName:lower(), "[ShulepinAIO] - " .. myHero.charName .. " - v" .. _SAIOVER)
        self.Menu:sub("Combo", "Combo Settings")
        self.Menu.Combo:checkbox("Q", "Use Q", true)
        self.Menu.Combo:checkbox("E", "Use E", true)
        self.Menu.Combo:checkbox("R", "Use R", true)
        self.Menu.Combo:slider("RStacks", "Keep R Stacks", 0, 7, 0, 1)
        self.Menu:sub("Harass", "Harass Settings")
        self.Menu.Harass:checkbox("Q", "Use Q", true)
        self.Menu.Harass:checkbox("E", "Use E", true)
        self.Menu.Harass:checkbox("R", "Use R", true)
        self.Menu.Harass:slider("RStacks", "Keep R Stacks", 0, 7, 0, 1)
        self.Menu.Harass:slider("Mana", "Mana(%) Manager", 0, 100, 50, 5)
        self.Menu:sub("KS", "KS Settings")
        self.Menu.KS:checkbox("Q", "Use Q", true)
        self.Menu.KS:checkbox("R", "Use R", true)
        self.Menu:sub("Draw", "Draw Settings")
        self.Menu.Draw:checkbox("Disable", "Disable All Drawings", false)
        self.Menu.Draw:checkbox("Q", "Draw Q", true)
        self.Menu.Draw:checkbox("W", "Draw W", false)
        self.Menu.Draw:checkbox("E", "Draw E", false)
        self.Menu.Draw:checkbox("R", "Draw R", true)
     
        self.DamageIndicator = DamageIndicator(self.Menu)
        self.DamageIndicator:Add("Draw AA Damage", ARGB(200, 255, 174, 0), function(unit) return DamageLib:GetAutoAttackDamage(myHero, unit) end, function(unit) return true end)
        for k, spell in pairs({self.Q, self.R}) do
                self.DamageIndicator:Add("Draw " .. spell:ToString() .. " Damage", ARGB(200, 255, 174, 0), function(unit) return self:GetDamage(spell.Slot, unit) end, function(unit) return myHero.spellbook:Spell(spell.Slot).level > 0 and myHero.spellbook:Spell(spell.Slot).cooldownTimeRemaining == 0 end)
        end

        AddEvent(Events.OnTick, function() self:OnTick() end)
        AddEvent(Events.OnDraw, function() self:OnDraw() end)

        PrintChat(myHero.charName .. " Loaded")
end

function Corki:GetDamage(slot, target) 
        local source = myHero
        local level = myHero.spellbook:Spell(slot).level

        if slot == self.Q.Slot then
                local baseDamage = 30 + 45 * level
                local bonusDamage = 0.5 * GetBonusAD(myHero) + 0.5 * GetTotalAP(myHero)
                local totalDamage = DamageLib:CalculateMagicDamage(source, target, baseDamage + bonusDamage)
                return totalDamage
        end

        if slot == self.R.Slot then
                local bigRocket = GetBuffData(myHero, "mbcheck2")
                local baseDamage = bigRocket and 120 + 60 * level or 65 + 25 * level
                local bonusDamage = bigRocket and -30 + 60 * level + 0.4 * GetTotalAP(myHero) or -15 + 30 * level + 0.2 * GetTotalAP(myHero)
                local totalDamage = DamageLib:CalculateMagicDamage(source, target, baseDamage + bonusDamage)
                return totalDamage
        end

        return 0
end

function Corki:OnTick()
        for i = 1, HeroManager.Enemy.Count do
                local enemy = HeroManager.Enemy.List[i]
                if enemy and IsValidTarget(enemy, self.R.Range) then
                        if self.Menu.KS.Q:get() and self.Q:IsReady() and self.Q:CanCast(enemy) then
                                local damage = self:GetDamage(self.Q.Slot, enemy)
                                local CastPosition, HitChance = self.Q:GetPrediction(enemy)

                                if HitChance > 0 and damage > enemy.health + enemy.magicShield then
                                        self.Q:Cast(CastPosition:ToDX3())
                                        break
                                end
                        end

                        if self.Menu.KS.R:get() and self.R:IsReady() and self.R:CanCast(enemy) then
                                local damage = self:GetDamage(self.R.Slot, enemy)
                                local CastPosition, HitChance = self.R:GetPrediction(enemy)

                                if HitChance > 0 and damage > enemy.health + enemy.magicShield then
                                        self.R:Cast(CastPosition:ToDX3())
                                        break
                                end
                        end
                end
        end

        local target = self.Orbwalker:GetTarget(self.R.Range)
        if not target or self.Orbwalker:IsAttacking() then return end

        if self.Orbwalker:GetMode() == "Combo" then
                if self.Menu.Combo.R:get() and self.R:IsReady() and self.R:CanCast(target) and myHero.spellbook:Spell(3).currentAmmoCount > self.Menu.Combo.RStacks:get() then
                       local CastPosition, HitChance = self.R:GetPrediction(target)

                        if HitChance > 0 then
                               self.R:Cast(CastPosition:ToDX3())
                               return
                        end
                end

                if self.Menu.Combo.Q:get() and self.Q:IsReady() and self.Q:CanCast(target) then
                        local CastPosition, HitChance = self.Q:GetPrediction(target)

                        if HitChance > 0 then
                                self.Q:Cast(CastPosition:ToDX3())
                                return
                        end
                end

                if self.Menu.Combo.E:get() and self.E:IsReady() and self.E:CanCast(target) then
                        self.E:Cast(target.position)
                        return
                end
        elseif self.Orbwalker:GetMode() == "Harass" and (myHero.mana / myHero.maxMana * 100) > self.Menu.Harass.Mana:get() then
                if self.Menu.Harass.R:get() and self.R:IsReady() and self.R:CanCast(target) and myHero.spellbook:Spell(3).currentAmmoCount > self.Menu.Harass.RStacks:get() then
                       local CastPosition, HitChance = self.R:GetPrediction(target)

                        if HitChance > 0 then
                               self.R:Cast(CastPosition:ToDX3())
                               return
                        end
                end

                if self.Menu.Harass.Q:get() and self.Q:IsReady() and self.Q:CanCast(target) then
                        local CastPosition, HitChance = self.Q:GetPrediction(target)

                        if HitChance > 0 then
                                self.Q:Cast(CastPosition:ToDX3())
                                return
                        end
                end

                if self.Menu.Harass.E:get() and self.E:IsReady() and self.E:CanCast(target) then
                        self.E:Cast(target.position)
                        return
                end
        end
end

function Corki:OnDraw()
        for i = 1, HeroManager.Enemy.Count do
                local hero = HeroManager.Enemy.List[i]
                if hero and hero.isVisible then
                        self.DamageIndicator:Draw(hero)
                end
        end

        if self.Menu.Draw.Disable:get() then return end

        for k, spell in pairs({self.Q, self.W, self.E, self.R}) do
                if self.Menu.Draw[spell:ToString()]:get() and spell:IsReady() then
                        spell:Draw()
                end
        end
end

----------------------------------------------------
--|               Champion: Twitch               |--
----------------------------------------------------

class "Twitch"

function Twitch:__init(menu)
        self.W = Spell({
                Slot = 1,
                Range = 950,
                Speed = 1400,
                Radius = 100,
                Delay = 0.25,
                From = myHero,
                Collision = false,
        })

        self.E = Spell({
                Slot = 2,
                Range = 1200,
        })
        self.E.PoisonData = {}

        self.R = Spell({
                Slot = 3,
                Range = 850,
        })

        self.Orbwalker = _G.LegitOrbwalker

        self.Menu = Menu("shulepin_" .. myHero.charName:lower(), "[ShulepinAIO] - " .. myHero.charName .. " - v" .. _SAIOVER)
        self.Menu:sub("Combo", "Combo Settings")
        self.Menu.Combo:checkbox("W", "Use W", true)
        self.Menu.Combo:checkbox("R", "Use R", true)
        self.Menu.Combo:slider("MinEnemies", "Min. Enemies to use R", 1, 5, 2, 1)
        self.Menu:sub("KS", "KS Settings")
        self.Menu.KS:checkbox("E", "Use E", true)
        self.Menu:sub("Draw", "Draw Settings")
        self.Menu.Draw:checkbox("Disable", "Disable All Drawings", false)
        self.Menu.Draw:checkbox("W", "Draw W", false)
        self.Menu.Draw:checkbox("E", "Draw E", true)
        self.Menu.Draw:checkbox("R", "Draw R", false)
        self.DamageIndicator = DamageIndicator(self.Menu)
        self.DamageIndicator:Add("Draw E Damage", ARGB(200, 255, 174, 0), function(unit) return self:GetPoisonDamage(unit) end, function(unit) return myHero.spellbook:Spell(self.E.Slot).cooldownTimeRemaining == 0 end)

        AddEvent(Events.OnTick, function(...) self:OnTick(...) end)
        AddEvent(Events.OnDraw, function(...) self:OnDraw(...) end)
        AddEvent(Events.OnExecuteCastFrame, function(...) self:OnPostAttack(...) end)

        PrintChat(myHero.charName .. " Loaded")
end

function Twitch:GetPoisonDamage(target)
        if not self.E:IsReady() then return 0 end

        local nID = target.networkId
        local level = myHero.spellbook:Spell(self.E.Slot).level
        local stacks = self.E.PoisonData[nID].Stacks

        if stacks ~= 0 then
                local baseDamage = 10 + 10 * level
                local stackDamage = (10 + 5 * level + 0.35 * myHero.characterIntermediate.flatPhysicalDamageMod + 0.2 * GetTotalAP(myHero)) * stacks
                local totalRawDamage = baseDamage + stackDamage
                local totalDamage =  CalcPhysicalDamage(myHero, target, totalRawDamage)
                return totalDamage
        end

        return 0
end

function Twitch:OnTick()
        for i = 1, HeroManager.Enemy.Count do
                local enemy = HeroManager.Enemy.List[i]
                local nID = enemy.networkId

                if not self.E.PoisonData[nID] then
                        self.E.PoisonData[nID] = {
                                Stacks = 0,
                                Time = 0,
                        }
                end

                local buff = GetBuffData(enemy, "TwitchDeadlyVenom")
                if buff and buff.count > 0 and buff.remainingTime > 0 then
                        self.E.PoisonData[nID].Stacks = buff.count
                        self.E.PoisonData[nID].Time = buff.remainingTime
                else
                        self.E.PoisonData[nID].Stacks = 0
                        self.E.PoisonData[nID].Time = 0
                end

                if self.Menu.KS.E:get() then
                        local damage = self:GetPoisonDamage(enemy)
                        if self.E:IsReady() and self.E:CanCast(enemy) and damage >= enemy.health + enemy.attackShield then
                                self.E:Cast(myHero.networkId)
                                return
                        end
                end
        end

        local target = self.Orbwalker:GetTarget(self.W.Range)
        if not target or self.Orbwalker:IsAttacking() then return end

        if self.Orbwalker:GetMode() == "Combo" then
                if self.Menu.Combo.W:get() and self.W:IsReady() and self.W:CanCast(target) then
                        local CastPosition, HitChance = self.W:GetPrediction(target)

                        if HitChance > 0 then
                               self.W:Cast(CastPosition:ToDX3())
                               return
                        end
                end

                if self.Menu.Combo.R:get() and self.R:IsReady() then
                        local aaRange = GetRealAutoAttackRange()
                        if HeroManager.GetCountHeroesInRange(aaRange) >= self.Menu.Combo.MinEnemies:get() then
                                self.R:Cast(myHero.networkId)
                                return
                        end
                end
        end
end

function Twitch:OnDraw()
        for i = 1, HeroManager.Enemy.Count do
                local hero = HeroManager.Enemy.List[i]
                if hero and hero.isVisible then
                        self.DamageIndicator:Draw(hero)
                end
        end

        if self.Menu.Draw.Disable:get() then return end

        for k, spell in pairs({self.W, self.E, self.R}) do
                if self.Menu.Draw[spell:ToString()]:get() and spell:IsReady() then
                        spell:Draw()
                end
        end
end

function Twitch:OnPostAttack(unit, spell)
        if unit ~= myHero or not spell.spellData.name:lower():find("attack") then return end
        local target = spell.target
        if not target then return end

        if self.Orbwalker:GetMode() == "Combo" then
                if self.Menu.Combo.W:get() and self.W:IsReady() and self.W:CanCast(target) then
                        local CastPosition, HitChance = self.W:GetPrediction(target)

                        if HitChance > 0 then
                               self.W:Cast(CastPosition:ToDX3())
                               return
                        end
                end
        end
end

----------------------------------------------------
--|           Champion: Twitsted Fate            |--
----------------------------------------------------

class "TwistedFate"

function TwistedFate:__init(menu)
        self.Q = Spell({
                Slot = 0,
                Range = 1400,
                Speed = 1000,
                Radius = 50,
                Delay = 0.25,
                From = myHero,
                Collision = false,
        })

        self.W = Spell({
                Slot = 1,
                Range = 750,
        })
        self.W.LastCastAttemptT = 0

        self.R = Spell({
                Slot = 3,
                Range = 5500,
        })

        self.Cards = {
                None    = 0,
                Red     = 1,
                Yellow  = 2,
                Blue    = 3
        }

        self.SelectStatus = {
                Ready     = 0,
                Selecting = 1,
                Selected  = 2,
                Cooldown  = 3
        }

        self.CardSelector = {
                SelectedCard = 0,
                Status = 0,
        }

        self.Orbwalker = _G.LegitOrbwalker

        self.Menu = Menu("shulepin_" .. myHero.charName:lower(), "[ShulepinAIO] - " .. myHero.charName .. " - v" .. _SAIOVER)
        self.Menu:sub("Combo", "Combo Settings")
        self.Menu.Combo:checkbox("Q", "Use Q at Immobile Enemy", true)
        self.Menu.Combo:checkbox("W", "Use W [Pick Gold Card]", true)
        self.Menu:sub("CardSelector", "Card Selector")
        self.Menu.CardSelector:key("YellowCard", "Pick Yellow Card", 0x57)
        self.Menu.CardSelector:key("BlueCard", "Pick Blue Card", 0x45)
        self.Menu.CardSelector:key("RedCard", "Pick Red Card", 0x54)
        self.Menu:checkbox("AA", "Disable AA During Loop Cycle", true)
        self.Menu:checkbox("WR", "Auto Select Yellow Card When R", true)

        AddEvent(Events.OnTick, function(...) self:OnTick(...) end)
        AddEvent(Events.OnProcessSpell, function(...) self:OnProcessSpell(...) end)

        PrintChat(myHero.charName .. " Loaded")
end

function TwistedFate:StartSelecting(card)
        if myHero.spellbook:Spell(1).spellData.name == "PickACard" and self.CardSelector.Status == self.SelectStatus.Ready then
                self.CardSelector.SelectedCard = card
                if GetTickCount() - self.W.LastCastAttemptT > 170 + NetClient.ping / 2 then
                        self.W:Cast(myHero.networkId)
                        self.W.LastCastAttemptT = GetTickCount()
                        return
                end
        end
end

function TwistedFate:OnTick()
        local spellName = myHero.spellbook:Spell(1).spellData.name
        local spellState = myHero.spellbook:CanUseSpell(1)
        
        if spellState == SpellState.Ready and spellName == "PickACard" and (self.CardSelector.Status ~= self.SelectStatus.Selecting or GetTickCount() - self.W.LastCastAttemptT > 500) or myHero.isDead then
                self.CardSelector.Status = self.SelectStatus.Ready
        elseif spellState == SpellState.Cooldown and spellName == "PickACard" then
                self.CardSelector.SelectedCard = self.Cards.None
                self.CardSelector.Status = self.SelectStatus.Cooldown
        elseif spellState == SpellState.Surpressed and not myHero.isDead then 
                self.CardSelector.Status = self.SelectStatus.Selected
        end

        if self.CardSelector.SelectedCard == self.Cards.Blue and spellName:lower() == "bluecardlock" and GetTickCount() > self.W.LastCastAttemptT then
                self.W:Cast(myHero.networkId)
                return
        elseif self.CardSelector.SelectedCard == self.Cards.Yellow and spellName:lower() == "goldcardlock" and GetTickCount() > self.W.LastCastAttemptT then
                self.W:Cast(myHero.networkId)
                return
        elseif self.CardSelector.SelectedCard == self.Cards.Red and spellName:lower() == "redcardlock" and GetTickCount() > self.W.LastCastAttemptT then
                self.W:Cast(myHero.networkId)
                return
        end

        if self.Menu.CardSelector.YellowCard:get() then
                self:StartSelecting(self.Cards.Yellow)
                return
        end

        if self.Menu.CardSelector.BlueCard:get() then
                self:StartSelecting(self.Cards.Blue)
                return
        end

        if self.Menu.CardSelector.RedCard:get() then
                self:StartSelecting(self.Cards.Red)
                return
        end

        self.Orbwalker:BlockAttack(self.Menu.AA:get() and self.CardSelector.Status == self.SelectStatus.Selecting)

        if self.Menu.Combo.Q:get() then
                for i = 1, HeroManager.Enemy.Count do
                        local enemy = HeroManager.Enemy.List[i]
                        local TargetImmobile, ImmobilePos, ImmobileCastPosition = Prediction:IsImmobile(enemy, self.Q.Delay, self.Q.Radius, self.Q.Speed, myHero)

                        if self.Q:IsReady() and self.Q:CanCast(enemy) and TargetImmobile then
                                self.Q:Cast(ImmobileCastPosition:ToDX3())
                                return
                        end
                end
        end

        local target = self.Orbwalker:GetTarget(self.Q.Range)
        if not target or self.Orbwalker:IsAttacking() then return end

        if self.Orbwalker:GetMode() == "Combo" then
                if self.Menu.Combo.W:get() and self.W:IsReady() and self.W:CanCast(target) then
                        self:StartSelecting(self.Cards.Yellow)
                        return
                end
        end
end

function TwistedFate:OnProcessSpell(unit, spell)
        if unit ~= myHero then return end

        local spellName = spell.spellData.name:lower()

        if spellName == "pickacard" then
                self.CardSelector.Status = self.SelectStatus.Selecting
        end

        if spellName == "goldcardlock" or spellName == "bluecardlock" or spellName == "redcardlock" then
                self.CardSelector.Status = self.SelectStatus.Selected
                self.CardSelector.SelectedCard = self.Cards.None
        end

        if self.Menu.WR:get() and spellName == "gate" then
                self:StartSelecting(self.Cards.Yellow)
        end
end

----------------------------------------------------
--|               Champion: Lucian               |--
----------------------------------------------------

class "Lucian"

function Lucian:__init()
        self.Q = Spell({
                Slot = 0,
                Range = 650,
                Delay = 0.35,
                Speed = huge,
                Radius = 30,
                Collision = false,
                From = myHero,
        })

        self.Q2 = Spell({
                Slot = 0,
                Range = 900,
                Delay = 0.35,
                Speed = huge,
                Radius = 30,
                Collision = false,
                From = myHero,
        })

        self.W = Spell({
                Slot = 1,
                Range = 1000,
                Delay = 0.30,
                Speed = 1600,
                Radius = 40,
                Collision = true,
                From = myHero,
        })

        self.E = Spell({
                Slot = 2,
                Range = 425,
        })

        self.R = Spell({
                Slot = 3,
                Range = 1200,
                Delay = 0.25,
                Speed = huge,
                Radius = 50,
                Collision = true,
                From = myHero,
        })
        
        self.Orbwalker = _G.LegitOrbwalker

        self.Menu = Menu("shulepin_" .. myHero.charName:lower(), "[ShulepinAIO] - " .. myHero.charName .. " - v" .. _SAIOVER)
        self.Menu:sub("Combo", "Combo Settings")
        self.Menu.Combo:checkbox("Q", "Use Q", true)
        self.Menu.Combo:checkbox("W", "Use W", true)
        self.Menu.Combo:checkbox("WIgnorePred", "Igrone W Prediction", false)
        self.Menu.Combo:checkbox("WIgnoreCol", "Igrone W Collision", false)
        self.Menu.Combo:checkbox("E", "Use E", true)
        self.Menu.Combo:list("EMode", "E Cast Mode", 1, {"To Side", "To Mouse", "To Target"})
        self.Menu.Combo:list("Rotation", "Combo Rotation", 3, {"-> Q", "-> W", "-> E"})

        AddEvent(Events.OnTick, function(...) self:OnTick(...) end)
        AddEvent(Events.OnExecuteCastFrame, function(...) self:OnPostAttack(...) end)

        PrintChat(myHero.charName .. " Loaded")
end

function Lucian:OnTick()
        self.W.Collision = not self.Menu.Combo.WIgnoreCol:get()
        self.Orbwalker:BlockAttack(myHero.spellbook:Spell(self.R.Slot).toggleState == 2)
end

function Lucian:OnPostAttack(unit, spell)
        if unit ~= myHero or not spell.spellData.name:lower():find("attack") then return end
        local target = spell.target
        if not target then return end

        if self.Orbwalker:GetMode() == "Combo" then
                local ComboRotation = self.Menu.Combo.Rotation:get() - 1

                if self.Menu.Combo.Q:get() and (ComboRotation == 0 or myHero.spellbook:CanUseSpell(ComboRotation) ~= 0) and myHero.spellbook:CanUseSpell(0) == 0 and self.Q:CanCast(target) then
                        self.Q:Cast(target.networkId)
                elseif self.Menu.Combo.E:get() and (ComboRotation == 2 or myHero.spellbook:CanUseSpell(ComboRotation) ~= 0) and myHero.spellbook:CanUseSpell(2) == 0 and self.E:CanCast(target, self.E.Range + 525) then
                        if self.Menu.Combo.EMode:get() == 1 then
                                local c1, c2, r1, r2 = Vector(myHero), Vector(target), 525, 525
                                local O1, O2 = CircleCircleIntersection(c1, c2, r1, r2)

                                if O1 and O2 then
                                        local pos = GetDistance(GetMousePos(), O1) > GetDistance(GetMousePos(), O2) and O2 or O1
                                        local castPos = c1 + 125 * (pos - c1):normalized() 
                                        self.E:Cast(castPos:ToDX3())
                                end
                        elseif self.Menu.Combo.EMode:get() == 2 then
                                local castPos = Vector(myHero) + 125 * (Vector(GetMousePos()) - Vector(myHero)):normalized() 
                                self.E:Cast(castPos:ToDX3())
                        elseif self.Menu.Combo.EMode:get() == 3 then
                                local castPos = Vector(myHero) + 125 * (Vector(target) - Vector(myHero)):normalized() 
                                self.E:Cast(castPos:ToDX3())
                        end
                elseif self.Menu.Combo.W:get() and (ComboRotation == 1 or myHero.spellbook:CanUseSpell(ComboRotation) ~= 0) and myHero.spellbook:CanUseSpell(1) == 0 and self.W:CanCast(target) then
                        if not self.Menu.Combo.WIgnorePred:get() then
                                local CastPosition, HitChance = self.W:GetPrediction(target)

                                if HitChance > 0 then
                                       self.W:Cast(CastPosition:ToDX3())
                                end
                        else
                                self.W:Cast(target.position)
                        end
                end
        end
end

----------------------------------------------------
--|              Champion: Kalista               |--
----------------------------------------------------

class "Kalista"

function Kalista:__init()
        self.Q = Spell({
                Slot = 0,
                Range = 1150,
                Delay = 0.35,
                Speed = 2100,
                Radius = 70,
                Collision = true,
                From = myHero,
        })

        self.W = Spell({
                Slot = 1,
                Range = 5000,
                Delay = 0.25,
                Speed = 450,
                Radius = 100,
                Collision = false,
                From = myHero,
        })

        self.E = Spell({
                Slot = 2,
                Range = 1000
        })
        self.RendTargets = {}

        self.R = Spell({
                Slot = 3,
                Range = 1200
        })

        self.Orbwalker = _G.LegitOrbwalker

        self.Menu = Menu("shulepin_" .. myHero.charName:lower(), "[ShulepinAIO] - " .. myHero.charName .. " - v" .. _SAIOVER)
        self.Menu:sub("Combo", "Combo Settings")
        self.Menu.Combo:checkbox("Q", "Use Q After Attack", true)
        self.Menu.Combo:checkbox("E", "Use E if Killable", true)
        self.Menu:sub("Draw", "Draw Settings")
        self.Menu.Draw:checkbox("Disable", "Disable All Drawings", false)
        self.Menu.Draw:checkbox("Q", "Draw Q", true)
        self.Menu.Draw:checkbox("E", "Draw E", true)
        self.DamageIndicator = DamageIndicator(self.Menu)
        self.DamageIndicator:Add("Draw E Damage", ARGB(200, 255, 174, 0), function(unit) return self:GetDamage(self.E.Slot, unit) end, function(unit) return myHero.spellbook:Spell(self.E.Slot).cooldownTimeRemaining == 0 end)

        PrintChat(myHero.charName .. " Loaded")

        AddEvent(Events.OnTick, function(...) self:OnTick(...) end)
        AddEvent(Events.OnDraw, function(...) self:OnDraw(...) end)
        AddEvent(Events.OnExecuteCastFrame, function(...) self:OnPostAttack(...) end)
end

function Kalista:GetDamage(slot, target)
        local level = myHero.spellbook:Spell(slot).level

        if slot == self.E.Slot then
                local buff = GetBuffData(target, "kalistaexpungemarker")
                if buff and buff.count > 0 then
                        local baseDamage = 10 + 10 * level + 0.6 * GetTotalAD(myHero)
                        local spearDamage = (level * (level * 0.5 + 2.5) + 7) + (3.75 * level + 16.25) * GetTotalAD(myHero) / 100
                        local totalDamage = DamageLib:CalculatePhysicalDamage(myHero, target, baseDamage + spearDamage * (buff.count - 1))
                        return totalDamage
                end
        end

        return 0
end

function Kalista:OnTick()
        if self.Menu.Combo.E:get() and self.E:IsReady() then
                local result, size = CObjectManager.GetObjectsByCondition({HeroManager.Enemy.List}, 
                        function(unit) 
                                local buff = GetBuffData(unit, "kalistaexpungemarker")
                                return buff and self.E:CanCast(unit)
                        end)

                for i = 1, size do
                        local unit = result[i]
                        if unit then
                                local damage = self:GetDamage(self.E.Slot, unit)
                                if damage >= unit.health + unit.attackShield then
                                        self.E:Cast(myHero.networkId)
                                        break   
                                end
                        end
                end
        end
end

function Kalista:OnDraw()
        for i = 1, HeroManager.Enemy.Count do
                local hero = HeroManager.Enemy.List[i]
                if hero and hero.isVisible then
                        self.DamageIndicator:Draw(hero)
                end
        end

        if self.Menu.Draw.Disable:get() then return end

        for k, spell in pairs({self.Q, self.E}) do
                if self.Menu.Draw[spell:ToString()]:get() and spell:IsReady() then
                        spell:Draw()
                end
        end
end

function Kalista:OnPostAttack(unit, spell)
        if unit ~= myHero or not spell.spellData.name:lower():find("attack") then return end
        local target = spell.target
        if not target then return end

        if self.Orbwalker:GetMode() == "Combo" then
                if self.Menu.Combo.Q:get() and self.Q:IsReady() and self.Q:CanCast(target) then
                        local CastPosition, HitChance = self.Q:GetPrediction(target)

                        if HitChance > 0 then
                                self.Q:Cast(CastPosition:ToDX3())
                        end
                end
        end
end

----------------------------------------------------
--|               Champion: Darius               |--
----------------------------------------------------

class "Darius"

function Darius:__init()
        self.Q = Spell({
                Slot = 0,
                Range = 415,
                Delay = 0.75,
                Speed = huge,
                Radius = 250,
                Collision = false,
                From = myHero
        })
        self.Q.MoveTo = nil

        self.W = Spell({
                Slot = 1,
                Range = 300
        })
        self.W.LastCastAttemptT = 0

        self.E = Spell({
                Slot = 2,
                Range = 490,
                Delay = 0.3,
                Speed = huge,
                Radius = huge,
                Collision = false,
                From = myHero
        })

        self.R = Spell({
                Slot = 3,
                Range = 460
        })

        self.Orbwalker = _G.LegitOrbwalker

        self.Menu = Menu("shulepin_" .. myHero.charName:lower(), "[ShulepinAIO] - " .. myHero.charName .. " - v" .. _SAIOVER)
        self.Menu:sub("Combo", "Combo Settings")
        self.Menu.Combo:checkbox("Q", "Use Q", true)
        self.Menu.Combo:checkbox("QHelper", "Use Positioning Helper", true)
        self.Menu.Combo:checkbox("W", "Use W After Attack", true)
        self.Menu.Combo:checkbox("E", "Use E", true)
        self.Menu.Combo:checkbox("R", "Use R if Killable", true)
        self.Menu:sub("Harass", "Harass Settings")
        self.Menu.Harass:checkbox("Q", "Use Q", true)
        self.Menu.Harass:slider("Mana", "Mana(%) Manager", 0, 100, 50, 5)
        self.Menu:sub("Draw", "Draw Settings")
        self.Menu.Draw:checkbox("Disable", "Disable All Drawings", false)
        self.Menu.Draw:checkbox("Q", "Draw Q", false)
        self.Menu.Draw:checkbox("E", "Draw E", true)
        self.Menu.Draw:checkbox("R", "Draw R", false)
        self.DamageIndicator = DamageIndicator(self.Menu)
        self.DamageIndicator:Add("Draw Q Damage", ARGB(200, 255, 174, 0), function(unit) return self:GetDamage(self.Q.Slot, unit) end, function(unit) return myHero.spellbook:Spell(self.Q.Slot).cooldownTimeRemaining == 0 end)
        self.DamageIndicator:Add("Draw W Damage", ARGB(200, 255, 174, 0), function(unit) return self:GetDamage(self.W.Slot, unit) end, function(unit) return myHero.spellbook:Spell(self.W.Slot).cooldownTimeRemaining == 0 end)
        self.DamageIndicator:Add("Draw R Damage", ARGB(200, 255, 174, 0), function(unit) return self:GetDamage(self.R.Slot, unit) end, function(unit) return myHero.spellbook:Spell(self.R.Slot).cooldownTimeRemaining == 0 end)
        self.DamageIndicator:Add("Draw AA Damage", ARGB(200, 255, 174, 0), function(unit) return DamageLib:GetAutoAttackDamage(myHero, unit) end, function(unit) return true end)

        AddEvent(Events.OnTick, function(...) self:OnTick(...) end)
        AddEvent(Events.OnDraw, function(...) self:OnDraw(...) end)
        AddEvent(Events.OnExecuteCastFrame, function(...) self:OnExecuteCastFrame(...) end)

        PrintChat(myHero.charName .. " Loaded")
end

function Darius:GetDamage(slot, target)
        local level = myHero.spellbook:Spell(slot).level

        if slot == self.Q.Slot then
                local baseDamage = 10 + 30 * level
                local bonusDamage = (0.9 + 0.1 * level) * 1 + GetTotalAD(myHero)
                local totalDamage = DamageLib:CalculatePhysicalDamage(myHero, target, baseDamage + bonusDamage)
                return totalDamage
        end

        if slot == self.W.Slot then
                local baseDamage = DamageLib:GetAutoAttackDamage(myHero, target)
                local bonusDamage = (0.45 + 0.05 * level) * GetTotalAD(myHero)
                local totalDamage = baseDamage + DamageLib:CalculatePhysicalDamage(myHero, target, bonusDamage)
                return totalDamage
        end

        if slot == self.R.Slot then
                local buff = GetBuffData(target, "DariusHemo")
                local baseDamage = 100 * level + 0.75 * GetBonusAD(myHero)
                local stacks = buff and buff.count or 0
                local rawDamage = baseDamage + (1 + 0.2 * stacks)
                return rawDamage
        end

        return 0
end

function Darius:OnTick()
        local qCastBuff = GetBuffData(myHero, "dariusqcast")

        self.Orbwalker:BlockAttack(qCastBuff ~= false)
        self.Orbwalker:BlockMove(self.Q.MoveTo ~= nil)

        if self.Menu.Combo.QHelper:get() and self.Q.MoveTo then
                if GetDistance(myHero.position, self.Q.MoveTo) < 20 then
                        --
                elseif not NavMesh:IsWall(self.Q.MoveTo:ToDX3()) then
                        --self.Q.MoveTo:Draw()
                        myHero:IssueOrder(GameObjectOrder.MoveTo, self.Q.MoveTo:ToDX3())
                end
        end

        for i = 1, HeroManager.Enemy.Count do
                local enemy = HeroManager.Enemy.List[i]
                if enemy and self.R:IsReady() and self.R:CanCast(enemy) then
                        local damage = self:GetDamage(self.R.Slot, enemy)
                        if damage >= enemy.health + enemy.attackShield then
                                self.R:Cast(enemy.networkId)
                                break
                        end
                end
        end

        local target = self.Orbwalker:GetTarget(500)
        if not target then self.Q.MoveTo = nil return end

        if self.Menu.Combo.QHelper:get() then
                if qCastBuff then
                        local CastPosition = Prediction:GetBestCastPosition(target, 0.2, 0, 500, huge, myHero, false)
                        self.Q.MoveTo = CastPosition:Extended(Vector(myHero), ((self.Q.Radius + self.Q.Range)/2))
                else
                        self.Q.MoveTo = nil
                end
        end

        if self.Orbwalker:IsAttacking() then return end

        if self.Orbwalker:GetMode() == "Combo" then
                if self.Menu.Combo.E:get() and self.E:IsReady() and self.E:CanCast(target) and GetDistance(myHero.position, target.position) > 300 then
                        local CastPosition, HitChance = self.E:GetPrediction(target)

                        if HitChance > 0 then
                                self.E:Cast(CastPosition:ToDX3())
                                return
                        end
                end

                if self.Menu.Combo.Q:get() and self.Q:IsReady() and self.Q:CanCast(target) and ((not self.W:IsReady() and GetTickCount() - self.W.LastCastAttemptT > 1) or GetDistance(myHero.position, target.position) > 300) then 
                        self.Q:Cast(myHero.networkId)
                        return
                end 
        elseif self.Orbwalker:GetMode() == "Harass" and (myHero.mana / myHero.maxMana * 100) > self.Menu.Harass.Mana:get() then
                if self.Menu.Harass.Q:get() and self.Q:IsReady() and self.Q:CanCast(target) and ((not self.W:IsReady() and GetTickCount() - self.W.LastCastAttemptT > 1) or GetDistance(myHero.position, target.position) > 300) then 
                        self.Q:Cast(myHero.networkId)
                        return
                end 
        end
end

function Darius:OnDraw()
        for i = 1, HeroManager.Enemy.Count do
                local hero = HeroManager.Enemy.List[i]
                if hero and hero.isVisible then
                        self.DamageIndicator:Draw(hero)
                end
        end

        if self.Menu.Draw.Disable:get() then return end

        for k, spell in pairs({self.Q, self.E, self.R}) do
                if self.Menu.Draw[spell:ToString()]:get() and spell:IsReady() then
                        spell:Draw()
                end
        end
end

function Darius:OnExecuteCastFrame(unit, spell)
        if unit ~= myHero or not spell.spellData.name:lower():find("attack") then return end

        local target = spell.target
        if not target then return end

        if self.Orbwalker:GetMode() == "Combo" and target.type == myHero.type then
                if self.Menu.Combo.W:get() and self.W:IsReady() and self.W:CanCast(target, self.W.Range + target.boundingRadius + myHero.boundingRadius) then
                        self.W:Cast(myHero.networkId)
                        self.W.LastCastAttemptT = GetTickCount()
                end
        end
end

----------------------------------------------------
--|             Load Champion Class              |--
----------------------------------------------------

if _G[myHero.charName] then 
        _G[myHero.charName](Config)
end