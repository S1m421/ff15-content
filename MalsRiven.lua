local _scriptVERSION = 4.6
local _scriptVERSIONONLINE = GetInternalWebResult("MalsRiven.version") 
local _scriptName = "MalsRiven"
local _scriptTitle = "Riding on Riven"
--printtext("\nRiding on Riven\n")
--printtext("\nBy Malbert\n")
--printtext("\nBeta 4.3\n")

local function DownloadNewVersion(currentVersion,onlineVersion)
	local currentVersion,onlineVersion = tonumber(currentVersion),tonumber(onlineVersion)
	if onlineVersion > currentVersion then
		print("Downloading ".._scriptName.." v"..onlineVersion)
		PrintChat("<font color=\"#50ed8f\">[UPDATE]</font> <font color=\"#8f70f4\"><b>".._scriptName.."</b></font><b> <font color=\"#50ed8f\"> v" ..onlineVersion.. " is avaible!</font>")
		DownloadInternalFile(_scriptName .. ".lua", SCRIPT_PATH .. _scriptName.. ".lua")
		PrintChat("<font color=\"#50ed8f\">[UPDATED]</font> <font color=\"#8f70f4\"><b>".._scriptName.."</b></font><b> <font color=\"#50ed8f\"> Refresh with F5</font>")
		print("Downloaded ".._scriptName.." v"..onlineVersion)
	end
end
DownloadNewVersion(_scriptVERSION,_scriptVERSIONONLINE)

local MalChampionScript={}

local FileExists = function(path)
	local file = io.open(path, 'r')
	if file then
		io.close(file)
		return true
	end
	return false
end

--local damageLib
if FileExists(COMMON_PATH .. "FF15Menu.lua") then require 'FF15Menu' else print("FF15Menu.lua is required in your //COMMON folder.") return end
if FileExists(COMMON_PATH .. "FF15DamageLib.lua") then damageLib = require 'FF15DamageLib' else print("Downloading FF15DamageLib.lua into your //COMMON folder.") DownloadInternalFile("FF15DamageLib.lua", COMMON_PATH .. "FF15DamageLib.lua") return end
if FileExists(COMMON_PATH .. "FF15-PremiumPrediction.lua") then require 'FF15-PremiumPrediction' else print("Downloading FF15-PremiumPrediction.lua into your //COMMON folder.") DownloadInternalFile("FF15-PremiumPrediction.lua", COMMON_PATH .. "FF15-PremiumPrediction.lua") return end
local target
local stuntarget
local targetult
local ignitedamage=0
local Ractive=false
local Rtimer=os.clock()
local Atimer=os.clock()
local threshold=1/4
local Rdamage=0
local Rdamage2=0
local useR=true
local castR=false
local Rtimer=os.clock()
local mousePos
local ufa
local ufax,ufay,ufaz
local ufa2
local ufa2x,ufa2y,ufa2z
local delay=0.001
local AC=0.6
local AAS=0.3
local Q2ETimer=0
 
local timerAnimationBegin=0
local timerAnimationSpeed = 0
local timetoAA=0
local AttackCompleted=0
local particles={}
particles["globalhit_bloodslash.troy"]=true
particles["globalhit_bloodslash_crit.troy"]=true
--particles[Thresh_ba_tar.troy]=true
local attacks={}
attacks["RivenBasicAttack"]=true
attacks["RivenBasicAttack2"]=true
attacks["RivenBasicAttack3"]=true
attacks["RivenCritAttack"]=true
--------Spell Stuff
local ChampionDmgType="PHYS"
local attackspeed=0
local AARDY=0
local AARANGE=0
local QRDY=0
local QRANGE=0
local QRADIUS=0
local QSPEED=0
local QAOE=0
local QLENGTH=0
local WRDY=0
local WRANGE=0
local WRADIUS=0
local WSPEED=0
local WAOE=0
local WLENGTH=0
local ERDY=0
local ERANGE=0
local ERADIUS=0
local ESPEED=0
local EAOE=0
local ELENGTH=0
local RRDY=0
local RRANGE=0
local RRADIUS=0
local RSPEED=0
local RAOE=0
local RLENGTH=0
local Qmod=0
local QmodReset=0
local Qspot=nil
local QLayoutSpot=0
local nearestTarget
 
 
local targetItems={3144,3153,3128,3092,3146}
--Bilgewater,BoTRK,DFG,FrostQueen,Hextech
local aoeItems={3184,3143,3180,3131,3069,3023,3290,3142}
--Entropy,Randuins,Odyns,SwordDivine,TalismanAsc,TwinShadows,TwinShadows,YoGBlade
local hydraItems={3074,3077}
--Hydra,Tiamat
 
 --[[
local egg = {team = 0, enemy = 0}
local zac = {team = 0, enemy = 0}
local aatrox = {team = 0, enemy = 0}
local _registry = {}
local cc = 0
local skillshotArray = {
}
--]]
local colorcyan = 0x0000FFFF
local coloryellow = 0xFFFFFF00
local colorgreen = 0xFF00FF00
local colorred = 0xFFFF0000
local colorblue = 0xFF0000FF
local drawskillshot = false
local playerradius = 150
local skillshotcharexist = false
local dodgeskillshotkey = 74 -- dodge skillshot key J
local show_allies=0
 
local autoE=false
 
 ----MENU
local RivConfig = Menu("Riven", "Riven Config")
RivConfig:sub("keySettings", "Keybinds")
RivConfig.keySettings:key("teamfight", "Teamfight", string.byte("T"), false)
RivConfig.keySettings:key("tf2", "EQQWQ Combo", string.byte("Y"), false)
RivConfig.keySettings:key("h", "Harass QWQQ EscapeE", string.byte("A"), false)
RivConfig.keySettings:key("escape", "Escape", string.byte("Z"), false)

RivConfig:sub("scriptSettings", "Settings")
--RivConfig.scriptSettings:key("nm", "NEARMOUSE Targetting", 56,false):permashow(true)--Press Digit 8
RivConfig.scriptSettings:checkbox("ult", "Use Ult in TF", true,55):permashow(true)
RivConfig.scriptSettings:checkbox("ks", "KillSteal", true,56):permashow(true)
RivConfig.scriptSettings:checkbox("nm", "NEARMOUSE Targetting", false,57):permashow(true)
RivConfig.scriptSettings:checkbox("stun", "Auto Stun", false,58):permashow(true)
RivConfig.scriptSettings:slider("s", "AA Delay in Combos", 0,1,0.3,0.05):permashow(true)
RivConfig.scriptSettings:slider("AAS", "When To AA Again", 0,1,0.3,0.05)
RivConfig.scriptSettings:slider("AC", "When Attack Completed", 0,1,0.6,0.05)
------------END MENU

function MalChampionScript:RivenRun()
	Atimer=os.clock()
	AddEvent(Events.OnTick,function() self:OnTick() end)
	AddEvent(Events.OnDraw,function() self:OnDraw() end)
	AddEvent(Events.OnProcessSpell, function(a,b) self:OnProcessSpell(a,b) end)
	QRANGE=myHero.spellbook:Spell(SpellSlot.Q).castRange+AARANGE
	QRADIUS=myHero.spellbook:Spell(SpellSlot.Q).castRadius
	QSPEED=myHero.spellbook:Spell(SpellSlot.Q).spellData.speed
	WRANGE=myHero.spellbook:Spell(SpellSlot.W).castRange
	WRADIUS=myHero.spellbook:Spell(SpellSlot.W).castRadius
	WSPEED=myHero.spellbook:Spell(SpellSlot.W).spellData.speed
	ERANGE=myHero.spellbook:Spell(SpellSlot.E).castRange
	ERADIUS=myHero.spellbook:Spell(SpellSlot.E).castRadius
	ESPEED=myHero.spellbook:Spell(SpellSlot.E).spellData.speed
	RRANGE=900--1000--myHero.spellbook:Spell(SpellSlot.R).castRange
	RRADIUS=myHero.spellbook:Spell(SpellSlot.R).castRadius
	RSPEED=myHero.spellbook:Spell(SpellSlot.R).spellData.speed
	--PrintChat("Q "..QRANGE.." "..QRADIUS.." "..QSPEED)
	--PrintChat("W "..WRANGE.." "..WRADIUS.." "..WSPEED)
	--PrintChat("E "..ERANGE.." "..ERADIUS.." "..ESPEED)
	--PrintChat("R "..RRANGE.." "..RRADIUS.." "..RSPEED)
	PrintChat(_scriptTitle.." Loaded!")
	
end
function MalChampionScript:OnTick()
	if (not myHero.isDead) then
		mousePos = pwHud.hudManager.virtualCursorPos
		delay=RivConfig.scriptSettings.s:get()
		AARANGE=myHero.characterIntermediate.attackRange
		QRANGE=myHero.spellbook:Spell(SpellSlot.Q).castRange+AARANGE
		WRANGE=myHero.spellbook:Spell(SpellSlot.W).castRange
		AC=RivConfig.scriptSettings.AC:get()
		AAS=RivConfig.scriptSettings.AAS:get()
		attackspeed=MalChampionScript:GetAttackSpeed(myHero)
		timerAnimationSpeed = 0.1*(1/attackspeed)
		if (RivConfig.scriptSettings.nm:get()) then
			target = self:GetWeakEnemy("PHYS", 500,myHero,"NEARPOSITION",mousePos)
		else 
			target = self:GetWeakEnemy("PHYS", 500,myHero)
		end
		targetult = self:GetWeakEnemy("PHYS", 1000)
		stuntarget = self:GetWeakEnemy("PHYS", 265)
		if (myHero.canAttack) then
			AARDY=1
		else 
			AARDY=0
		end
		if (myHero.spellbook:CanUseSpell(SpellSlot.Q)==0) then
			QRDY=1
		else 
			QRDY=0
		end
		if (myHero.spellbook:CanUseSpell(SpellSlot.W)==0) then
			WRDY=1
		else 
			WRDY=0
		end
		if (myHero.spellbook:CanUseSpell(SpellSlot.E)==0) then
			ERDY=1
		else 
			ERDY=0
		end
		if (myHero.spellbook:CanUseSpell(SpellSlot.R)==0) then
			RRDY=1
		else 
			RRDY=0
		end
		if Qmod~=0 and QmodReset<os.clock() then
				Qmod=0
				Qspot=nil
		end
		if Qspot~=nil and Qmod~=2 then
				Qspot=nil
		end
		if target~=nil then
			AARANGE=myHero.characterIntermediate.attackRange+target.boundingRadius
			QRANGE=myHero.spellbook:Spell(SpellSlot.Q).castRange+AARANGE
				ufax,ufay,ufaz = self:GetFireahead(target,5,22) --target.position.x,target.position.y,target.position.z--
				ufa={x=ufax,y=ufay,z=ufaz}
				if (RRDY==1) then
					if target.health/target.maxHealth<1/4 then
						Rdamage=self:getDmg("M",target,myHero,"PHYS",(120+(120*self:GetSpellLevel('R'))+1.8*myHero.characterIntermediate.flatPhysicalDamageMod)*RRDY)
					else
						Rdamage=self:getDmg("M",target,myHero,"PHYS",((40+(40*self:GetSpellLevel('R'))+.6*myHero.characterIntermediate.flatPhysicalDamageMod)+(40+(40*self:GetSpellLevel('R'))+.6*myHero.characterIntermediate.flatPhysicalDamageMod)*(1-target.health/target.maxHealth)*8/300)*RRDY)
					end
				else 
					Rdamage=0
				end
		else
				Rdamage=0
		end
		if targetult~=nil then
			ufa2x,ufa2y,ufa2z = self:GetFireahead(targetult,5,22)
			ufa2={x=ufa2x,y=ufa2y,z=ufa2z}
			if targetult.health/targetult.maxHealth<1/4 then
				Rdamage2=self:getDmg("M",targetult,myHero,"PHYS",(120+(120*self:GetSpellLevel('R'))+1.8*myHero.characterIntermediate.flatPhysicalDamageMod)*RRDY)
			else
				Rdamage2=self:getDmg("M",targetult,myHero,"PHYS",((40+(40*self:GetSpellLevel('R'))+.6*myHero.characterIntermediate.flatPhysicalDamageMod)+(40+(40*self:GetSpellLevel('R'))+.6*myHero.characterIntermediate.flatPhysicalDamageMod)*(1-targetult.health/targetult.maxHealth)*8/300)*RRDY)
				--Rdamage2=damageLib:CalculateDamage(myHero, targetult, "Physical",((40+(40*self:GetSpellLevel('R'))+.6*myHero.characterIntermediate.flatPhysicalDamageMod)+(40+(40*self:GetSpellLevel('R'))+.6*myHero.characterIntermediate.flatPhysicalDamageMod)*(1-targetult.health/targetult.maxHealth)*8/300)*RRDY)
			end
		else
			Rdamage2=0
		end
	
		if castR==false and Ractive==true then
				if Rtimer+13.5<os.clock() then
						castR=true
				else
						castR=false
				end
		elseif castR==true and Rtimer+15<os.clock() then
				castR=false
		end
 
		if RivConfig.scriptSettings.ult:get() then useR=true else useR=false end
		if RivConfig.scriptSettings.ks:get() then self:Killsteal() end
	   -- if RivConfig.smite then smitesteal() end
	   --[[ if RivConfig.shield and ERDY==1 then
				autoE=true
		else
				autoE=false
		end--]]
	   
		
		if RivConfig.keySettings.teamfight:get() then
				self:TF()
		end 
		if RivConfig.keySettings.tf2:get() then
				self:TF2()
		end 
		if RivConfig.keySettings.h:get() then
				self:Harass()
		end 
		if RivConfig.keySettings.escape:get() then
				self:Escape()
		end
		if RivConfig.scriptSettings.stun:get() and WRDY==1 and stuntarget~=nil and not RivConfig.keySettings.teamfight:get() and not RivConfig.keySettings.tf2:get() and not RivConfig.keySettings.h:get() and not RivConfig.keySettings.escape:get() then		
			MalChampionScript:CastSpellTarget('W',myHero)
		end
	
	end
end

function MalChampionScript:OnProcessSpell(unit,spell)
	if unit~=nill and spell ~= nil and unit==myHero then  
                --print("\nSpell: "..spell.name)
				--PrintChat("Spell: "..spell.spellData.name)
				local spellName=spell.spellData.name
                if attacks[spellName] then
                        AttackCompleted=os.clock()+(AC/attackspeed)
                        timetoAA = os.clock()+(1-AAS)*(1/attackspeed)
                elseif string.find(spellName,"RivenTriCleave") ~= nil then
                --print("\nCheck1")
                        Qmod=(Qmod+1)%3
                        QmodReset=os.clock()+3.5
                        Atimer=os.clock()+delay+(AC/attackspeed)
                        if target~=nil then
                                if self:GetD(target)<=AARANGE then
                                    self:MoveToXYZ(myHero.position.x,myHero.position.y,myHero.position.z,0)
                                else
                                    self:MoveToXYZ(target.position.x,target.position.y,target.position.z,0)
                                end    
                                --[[if self:GetD(target)<400 then
                                        for _, item in pairs(hydraItems) do
                                                if GetInventorySlot(item)~=nil and myHero["SpellTime"..GetInventorySlot(item)]>1.0 then
                                                        CastSpellTarget(tostring(GetInventorySlot(item)),target)
                                                end
                                        end
                                end--]]
                        end
                        timetoAA=os.clock()
                elseif string.find(spellName,"RivenMartyr") ~= nil then
                        Atimer=os.clock()+delay+(AC/attackspeed)
                        if target~=nil then
                                if self:GetD(target)<=AARANGE then
                                    self:MoveToXYZ(myHero.position.x,myHero.position.y,myHero.position.z,0)
                                else
                                    self:MoveToXYZ(target.position.x,target.position.y,target.position.z,0)
                                end
                                --[[if GetD(target)<400 then
                                        for _, item in pairs(hydraItems) do
                                                if GetInventorySlot(item)~=nil and myHero["SpellTime"..GetInventorySlot(item)]>1.0 then
                                                        CastSpellTarget(tostring(GetInventorySlot(item)),target)
                                                end
                                        end
                                end--]]
                        end
                        timetoAA=os.clock()
 
                elseif string.find(spellName,"RivenFeint") ~= nil then
                        Atimer=os.clock()+delay+(AC/attackspeed)
                        if target~=nil then
                                if self:GetD(target)<=AARANGE then
                                    self:MoveToXYZ(myHero.position.x,myHero.position.y,myHero.position.z,0)
                                else
                                    self:MoveToXYZ(target.position.x,target.position.y,target.position.z,0)
                                end
                        end
                        timetoAA=os.clock()
 
                elseif string.find(spellName,"RivenFengShuiEngine") ~= nil then    
                        Ractive=true
                        Rtimer=os.clock()
                        Atimer=os.clock()+delay+(AC/attackspeed)
                        if target~=nil then
                                if self:GetD(target)<=AARANGE then
                                    self:MoveToXYZ(myHero.position.x,myHero.position.y,myHero.position.z,0)
                                else
                                    self:MoveToXYZ(target.position.x,target.position.y,target.position.z,0)
                                end
                               --[[ if GetD(target)<400 then
                                        for _, item in pairs(hydraItems) do
                                                if GetInventorySlot(item)~=nil and myHero["SpellTime"..GetInventorySlot(item)]>1.0 then
                                                        CastSpellTarget(tostring(GetInventorySlot(item)),target)
                                                end
                                        end
                                end--]]
                        end
                        timetoAA=os.clock()
               
                elseif string.find(spellName,"rivenizunablade") ~= nil then  
                        Ractive=false
                        castR=false
                        Atimer=os.clock()+delay+(AC/attackspeed)
                        if target~=nil then
                                if self:GetD(target)<=AARANGE then
                                    self:MoveToXYZ(myHero.position.x,myHero.position.y,myHero.position.z,0)
                                else
                                    self:MoveToXYZ(target.position.x,target.position.y,target.position.z,0)
                                end
                        end
                        timetoAA=os.clock()
                end
                --[[if string.find(spell.name,"BasicAttack") ~= nil or string.find(spell.name,"CritAttack") ~= nil then  
                --    Atimer=os.clock()+(1/attackspeed)
                timetoAA = os.clock()+(1/attackspeed) - RivConfig.AAS*0.3*(1/attackspeed)timetoAA = os.clock()+(1/attackspeed) - RivConfig.AAS*0.3*(1/attackspeed)
                end--]]
 
        end
end
function MalChampionScript:AttackT(enemy)
        if enemy~=nil and os.clock()>=timetoAA then
                --MoveToXYZ(myHero.x,myHero.y,myHero.z,0)
                self:AttackTarget(enemy)
        elseif os.clock()>=AttackCompleted then
                if self:GetD(enemy)<=AARANGE then
					self:MoveToXYZ(myHero.position.x,myHero.position.y,myHero.position.z,0)
                else
                    self:MoveToXYZ(enemy.position.x,enemy.position.y,enemy.position.z,0)
                end
        end
end
function MalChampionScript:TF()
        if target~=nil then
		--PrintChat(AARANGE)
                if useR==true and Ractive==true and (target.health<Rdamage or castR==true or myHero.health<15/100*myHero.maxHealth) then --or target.health/target.maxHealth<1/4
						self:CastSpellXYZ("R",ufa.x,0,ufa.z,0)
						--CastSpellXYZ("R",ufa.x,0,ufa.z,0)
                        Ractive=false
                elseif os.clock()<Atimer and self:GetD(target)<AARANGE then
                        self:AttackT(target)                
                elseif QRDY==1 and Qmod==0 and ((os.clock()>Atimer) or self:GetD(target)>QRADIUS) then--myHero.characterIntermediate.attackRange+150) then
                       
                        self:CastSpellXYZ("Q",ufa.x,0,ufa.z,0)
 
 
                        --[[if RRDY==1 and Ractive==false then
                                CastSpellXYZ("R",myHero.x,0,myHero.z)
                                Ractive=true
                        end
                        if WRDY==1 and Ractive==true and GetD(target)<250 then
                                CastSpellXYZ("W",ufa.x,0,ufa.z)
                        end--]]
               
                elseif useR==true and RRDY==1 and Ractive==false and (os.clock()>Atimer) and self:GetD(target,myHero)<RRANGE then
                        self:CastSpellXYZ("R",ufa.x,0,ufa.z,0)
                        Ractive=true
 
                elseif WRDY==1 and (os.clock()>Atimer) and self:GetD(target,myHero)<WRADIUS then                
                        self:CastSpellXYZ("W",myHero.position.x,0,myHero.position.z,0)
 
                elseif ERDY==1 and ( self:GetD(target)>QRADIUS) then
                        self:CastSpellXYZ("E",ufa.x,0,ufa.z,0)
                elseif QRDY==1 and ((os.clock()>Atimer) or self:GetD(target)>QRADIUS) then         
                        self:CastSpellXYZ("Q",ufa.x,0,ufa.z,0)      
 
                elseif ERDY==1 and ((os.clock()>Atimer)) then
                        self:CastSpellXYZ("E",ufa.x,0,ufa.z,0)
                else
                        self:AttackT(target)
						--[[ ITEMS
                        if  self:GetD(target)<400 then
                                for _, item in pairs(hydraItems) do
                                        if GetInventorySlot(item)~=nil and myHero["SpellTime"..GetInventorySlot(item)]>1.0 then
                                                CastSpellTarget(tostring(GetInventorySlot(item)),target)
                                        end
                                end
                                for _, item in pairs(aoeItems) do
                                        if GetInventorySlot(item)~=nil and myHero["SpellTime"..GetInventorySlot(item)]>1.0 then
                                                CastSpellTarget(tostring(GetInventorySlot(item)),target)
                                        end
                                end
                                for _, item in pairs(targetItems) do
                                        if GetInventorySlot(item)~=nil and myHero["SpellTime"..GetInventorySlot(item)]>1.0 then
                                                CastSpellTarget(tostring(GetInventorySlot(item)),target)
                                        end
                                end
                        elseif GetD(target)<600 then
                                for _, item in pairs(targetItems) do
                                        if GetInventorySlot(item)~=nil and myHero["SpellTime"..GetInventorySlot(item)]>1.0 then
                                                CastSpellTarget(tostring(GetInventorySlot(item)),target)
                                        end
                                end
                        end
						--]]
                end
        elseif targetult~=nil then
			if ERDY==1 then --try and bring targetult into target range
				self:CastSpellXYZ("E",ufa2.x,0,ufa2.z,0)
			elseif useR==true and Ractive==true and self:GetD(targetult)>600 and self:GetD(ufa2)<RRANGE then
				self:CastSpellXYZ("R",ufa2.x,0,ufa2.z,0)
			elseif useR==false and QRDY==1 then
				self:CastSpellXYZ("Q",ufa2.x,0,ufa2.z,0)
			else 
				self:AttackT(targetult) 
			end
        else
            self:MoveToMouse()
        end
end
function MalChampionScript:TF2()
	if target~=nil then
		if useR==true and Ractive==true and (target.health<Rdamage  or castR==true or myHero.health<15/100*myHero.maxHealth) then --or target.health/target.maxHealth<1/4
			self:CastSpellXYZ("R",ufa.x,0,ufa.z,0)
			Ractive=false
		elseif os.clock()<Atimer and self:GetD(target)<AARANGE then
			self:AttackT(target)		
		elseif ERDY==1 and (os.clock()>Atimer or self:GetD(target)>QRADIUS) then
			self:CastSpellXYZ("E",ufa.x,0,ufa.z,0)
			
		elseif useR==true and RRDY==1 and Ractive==false and (os.clock()>Atimer) and self:GetD(target)<RRANGE then
			self:CastSpellXYZ("R",ufa.x,0,ufa.z,0)
			Ractive=true
		elseif QRDY==1 and (os.clock()>Atimer or self:GetD(target)>AARANGE) and Qmod<2 then
			
			self:CastSpellXYZ("Q",ufa.x,0,ufa.z,0)

		elseif WRDY==1 and (os.clock()>Atimer) and self:GetD(target)<WRANGE then
		
			if not self:runningAway(target) then
				local tx,tz=0,0
				local qx,qy,qz=self:GetFireahead(target,2,0)
				Qspot={x=qx,y=qy,z=qz}
				local dist=self:GetD(Qspot,target)
				if target.x==Qspot.x then
						tx = Qspot.x
						if target.z>Qspot.z then
								tz = target.z+dist
						else
								tz = target.z-dist
						end
			   
				elseif Qspot.z==target.z then
						tz = target.z
						if target.x>Qspot.x then
								tx = target.x+(dist)
						else
								tx = target.x-(dist)
						end
			   
				elseif target.x>Qspot.x then
						angle = math.asin((target.x+Qspot.x)/dist)
						zs = (dist)*math.cos(angle)
						xs = (dist)*math.sin(angle)
						if target.z>Qspot.z then
								tx = target.x+xs
								tz = target.z+zs
						elseif target.z<Qspot.z then
								tx = target.x+xs
								tz = target.z-zs
						end
			   
				elseif target.x<Qspot.x then
						angle = math.asin((Qspot.x+target.x)/dist)
						zs = (dist)*math.cos(angle)
						xs = (dist)*math.sin(angle)
						if target.z>Qspot.z then
								tx = target.x-xs
								tz = target.z+zs
						elseif target.z<Qspot.z then
								tx = target.x-xs
								tz = target.z-zs
						end 
				end
				Qspot={x=tx,y=qy,z=tz}
			else
				local qx,qy,qz=self:GetFireahead(target,2,0)
				Qspot={x=qx,y=qy,z=qz}
			end
			
			self:CastSpellXYZ("W",myHero.x,0,myHero.z,0)
		else			
			if Qmod==2 and Qspot==nil then				
				self:AttackT(target)
			elseif Qmod==2 and Qspot~=nil and self:GetD(Qspot)>50 then
				self:MoveToXYZ(Qspot.x,0,Qspot.z)
			elseif Qmod==2 and Qspot~=nil and self:GetD(Qspot)<=50 then
				self:CastSpellXYZ("Q",ufa.x,0,ufa.z,0)
			else
				self:AttackT(target)
			end
			--item usage
		end


	elseif targetult~=nil then
		if ERDY==1 then --try and bring targetult into target range
			self:CastSpellXYZ("E",ufa2.x,0,ufa2.z,0)
		elseif useR==true and Ractive==true and self:GetD(targetult)>600 and self:GetD(ufa2)<RRANGE then
			self:CastSpellXYZ("R",ufa2.x,0,ufa2.z,0)
		elseif useR==false and QRDY==1 then
			self:CastSpellXYZ("Q",ufa2.x,0,ufa2.z,0)
		else 
			self:AttackT(targetult) 
		end
	else
		self:MoveToMouse()
	end
end
function MalChampionScript:Harass()
	if targetult~=nil then
		local mx,my=0,0
		if QRDY==1 and self:GetD(ufa2)<600 then
			Q2ETimer=os.clock()+1
			
			--ClickSpellXYZ("Q",ufa2.x,0,ufa2.z,0)
			self:CastSpellXYZ("Q",ufa2.x,0,ufa2.z,0)
			self:CastSpellXYZ("Q",ufa2.x,0,ufa2.z,0)
		elseif WRDY==1 and self:GetD(targetult)<WRANGE then
			
			self:CastSpellTarget("W",myHero)
		elseif QRDY==0 and ERDY==1 and Q2ETimer<os.clock() then
			local xspot=0
			local zspot=0
			local xdist=0
			local zdist=0
			xdist=myHero.x- ufa2.x
				xspot=myHero.x+1.1*xdist	
				
			zdist=myHero.z-ufa2.z
				zspot=myHero.z+ 1.1*zdist
				
			
			
			
			--ClickSpellXYZ("E",xspot,0,zspot,0)
			self:CastSpellXYZ("E",xspot,0,zspot,0)
			self:CastSpellXYZ("E",xspot,0,zspot,0)
		else
			self:MoveToMouse()
		end
	else
		self:MoveToMouse()
	end

end
function MalChampionScript:Escape()
        if ERDY==1  then
                --print('\nE  '..myHero.SpellTimeE..'  '..myHero.SpellNameE)
                self:CastSpellXYZ("E",mousePos.x,0,mousePos.z,0)
        elseif ERDY==0 and QRDY==1 then
                --print('\n'..myHero.SpellTimeQ..'  '..myHero.SpellNameQ)
                self:CastSpellXYZ("Q",mousePos.x,0,mousePos.z,0)
        else
                self:MoveToMouse()
        end  
end
function MalChampionScript:Killsteal()
	if targetult~=nil then
		local W = self:getDmg('W',targetult,myHero)*WRDY
		if targetult.health<W+Rdamage2+ignitedamage and self:GetD(targetult)<WRANGE then			
			self:CastSpellXYZ("R",ufa2.x,0,ufa2.z,0)
			self:CastSpellXYZ("W",ufa2.x,0,ufa2.z,0)
			self:CastSpellXYZ("R",ufa2.x,0,ufa2.z,0)
			--if ignitedamage~=0 then CastSummonerIgnite(targetult) end
		elseif targetult.health<(W+Rdamage2+ignitedamage)*ERDY and self:GetD(ufa2)<WRANGE+ERANGE then
			self:CastSpellXYZ("E",ufa2.x,0,ufa2.z,0)
		elseif targetult.health<Rdamage2+ignitedamage and self:GetD(targetult)<600 then
			self:CastSpellXYZ("R",ufa2.x,0,ufa2.z,0)
			self:CastSpellXYZ("R",ufa2.x,0,ufa2.z,0)
			--if ignitedamage~=0 then CastSummonerIgnite(targetult) end
		elseif targetult.health<Rdamage2 and self:GetD(ufa2)<RRANGE then
			self:CastSpellXYZ("R",ufa2.x,0,ufa2.z,0)
			self:CastSpellXYZ("R",ufa2.x,0,ufa2.z,0)
		end
	end
end
function MalChampionScript:OnDraw()
	if not myHero.isDead then
		if QRDY==1 then --QRDY==1 then
			--CustomCircle(300,3,3,myHero)
			DrawHandler:Circle3D(myHero.position,300, colorgreen)
		end
		if WRDY==1 then --WRDY==1 then
			--CustomCircle(275,3,2,myHero)
			DrawHandler:Circle3D(myHero.position,275, colorgreen)
		end
		if RRDY==1 then --RRDY==1 then
			--CustomCircle(950,10,1,myHero) missing thickness
			DrawHandler:Circle3D(myHero.position,950, colorcyan)
		end
	end
	if target~=nil and not target.isDead then
		DrawHandler:Circle3D(target.position,100, coloryellow)
	end
        --if targetult~=nil then
               --CustomCircle(100,3,4,targetult)
        --end
end

------------------CONVERSION FUNCTIONS!
function MalChampionScript:CustomCircle(radius,bold,colorsent,unit)
	local posar
	if (unit.position~=nil) then posar=unit.position
	else posar=unit end
	local color=colorsent
	if (colorsent<10) then
		local colorarr={[1]=colorblue,[2]=colorgreen,[3]=coloryellow,[4]=colorcyan,[5]=colorred}
		color=colorarr[colorsent]
	end
	DrawHandler:Circle3D(posar,radius, color)
	for i=1, math.ceil(bold/2), 1 do
		local radiusincr=radius+i
		local radiusdecr=radius-i
		DrawHandler:Circle3D(posar,radiusincr, color)
		DrawHandler:Circle3D(posar,radiusdecr, color)
	end
end
function MalChampionScript:runningAway(slowtarget)
   local d1 = self:GetD(slowtarget) -- DISTANCE BETWEEN UNIT AND MYHERO
   local x, y, z = GetFireahead(slowtarget,2,0) --PREDICTION TO GET FACING DIRECTION
   local d2 = self:GetD({x=x, y=y, z=z}) --DISTANCE BETWEEN PREDICTION AND MYHERO
   local d3 = self:GetD({x=x, y=y, z=z},slowtarget) --DISTANCE BETWEEN PREDICTION AND UNIT
   local angle = math.acos((d2*d2-d3*d3-d1*d1)/(-2*d3*d1))
   return angle%(2*math.pi)>math.pi/2 and angle%(2*math.pi)<math.pi*3/2
end
function MalChampionScript:GetFireahead(unitfa,delayfa,speedfa,radius, angle, collision,source,range)
	local sourcefa=source or myHero
	local radiusfa=radius or 55
	local anglefa=angle or 0
	local collisionfa=collision or false
	local rangefa=range or 5000
	local delayfa=delayfa*.1
	local speedfa=speedfa*100
	local CastPos, PredPos, HitChance, TimeToHit = PremiumPrediction:GetPrediction(sourcefa, unitfa, speedfa, rangefa, delayfa, radiusfa, anglefa, collisionfa)
	if (not collision or (CastPos and HitChance >= 0.6)) then 
		return CastPos
	else 
		return nil
	end
end

function MalChampionScript:GetD(p1, p2)
if p2 == nil then p2 = myHero.position end
if p1.position~=nil then p1=p1.position end
if p2.position~=nil then p2=p2.position end
if (p1.z == nil or p2.z == nil) and p1.x~=nil and p1.y ~=nil and p2.x~=nil and p2.y~=nil then
px=p1.x-p2.x
py=p1.y-p2.y
if px~=nil and py~=nil then
px2=px*px
py2=py*py
if px2~=nil and py2~=nil then
return math.sqrt(px2+py2)
else
return 99999
end
else
return 99999
end
 
elseif p1.x~=nil and p1.z ~=nil and p2.x~=nil and p2.z~=nil then
px=p1.x-p2.x
pz=p1.z-p2.z
if px~=nil and pz~=nil then
px2=px*px
pz2=pz*pz
if px2~=nil and pz2~=nil then
return math.sqrt(px2+pz2)
else
return 99999
end
else    
return 99999
end
 
else
return 99999
end
end

function MalChampionScript:run_every(interval, fn, ...)
    return self:internal_run({fn=fn, interval=interval}, ...)
end
 
function MalChampionScript:internal_run(t, ...)    
    local fn = t.fn
    local key = t.key or fn
   
    local now = os.clock()
    local data = _registry[key]
       
    if data == nil or t.reset then
        local args = {}
        local n = select('#', ...)
        local v
        for i=1,n do
            v = select(i, ...)
            table.insert(args, v)
        end  
        -- the first t and args are stored in registry        
        data = {count=0, last=0, complete=false, t=t, args=args}
        _registry[key] = data
    end
       
    --assert(data~=nil, 'data==nil')
    --assert(data.count~=nil, 'data.count==nil')
    --assert(now~=nil, 'now==nil')
    --assert(data.t~=nil, 'data.t==nil')
    --assert(data.t.start~=nil, 'data.t.start==nil')
    --assert(data.last~=nil, 'data.last==nil')
    -- run
    local countCheck = (t.count==nil or data.count < t.count)
    local startCheck = (data.t.start==nil or now >= data.t.start)
    local intervalCheck = (t.interval==nil or now-data.last >= t.interval)
    --print('', 'countCheck', tostring(countCheck))
    --print('', 'startCheck', tostring(startCheck))
    --print('', 'intervalCheck', tostring(intervalCheck))
    --print('')
    if not data.complete and countCheck and startCheck and intervalCheck then                
        if t.count ~= nil then -- only increment count if count matters
            data.count = data.count + 1
        end
        data.last = now        
       
        if t._while==nil and t._until==nil then
            return fn(...)
        else
            -- while/until handling
            local signal = t._until ~= nil
            local checker = t._while or t._until
            local result
            if fn == checker then            
                result = fn(...)
                if result == signal then
                    data.complete = true
                end
                return result
            else
                result = checker(...)
                if result == signal then
                    data.complete = true
                else
                    return fn(...)
                end
            end            
        end
    end    
end

function MalChampionScript:CastSpellXYZ(spellLetter,px,py,pz,misc)
	if (px.x~=nil) then
		pz=px.z
		if (px.y~=nil) then
			py=px.y
		else 
			py=0
		end
		px=px.x
	elseif (px.position~=nil) then
		pz=px.position.z
		py=px.position.y
		px=px.position.x
	end
	local pos=D3DXVECTOR3(px,py,pz)
	local spellarr={["Q"]=SpellSlot.Q,["W"]=SpellSlot.W,["E"]=SpellSlot.E,["R"]=SpellSlot.R}
	local spellNumber=spellarr[spellLetter]
	myHero.spellbook:CastSpell(spellNumber, pos)
end
function MalChampionScript:CastSpellTarget(spellLetter,hero)
	if (hero~=nil) then 
		local spellarr={["Q"]=SpellSlot.Q,["W"]=SpellSlot.W,["E"]=SpellSlot.E,["R"]=SpellSlot.R}
		local spellNumber=spellarr[spellLetter]
		myHero.spellbook:CastSpell(spellNumber, hero.networkId)
	end
end
function MalChampionScript:GetSpellLevel(spellLetter,hero)
	local hero=hero or myHero
	local spellarr={["Q"]=SpellSlot.Q,["W"]=SpellSlot.W,["E"]=SpellSlot.E,["R"]=SpellSlot.R}
	local spellNumber=spellarr[spellLetter]
	return myHero.spellbook:Spell(spellNumber).level
end
function MalChampionScript:getDmg(spellLetter,target,source,damage_type,damage)
	local source=source or myHero
	if (spellLetter=="M") then
		local damage_type=damage_type or ChampionDmgType
		local damage=damage or 0
		if damage_type=="PHYS" then
			return damageLib:CalculateDamage(myHero, target, "Physical",damage)
		elseif damage_type=="MAGIC" then
			return damageLib:CalculateDamage(myHero, target, "Magical",damage)
		elseif damage_type=="MAGIC" then
			return damageLib:CalculateDamage(myHero, target, "True",damage)
		else
			return damageLib:CalculateDamage(myHero, target, "Mixed",damage)
		end
	else 
		local spellarr={["Q"]=SpellSlot.Q,["W"]=SpellSlot.W,["E"]=SpellSlot.E,["R"]=SpellSlot.R}
		local spellNumber=spellarr[spellLetter]
		return damageLib:GetSpellDamage(source, target, spellNumber, 'Default', false)
	end
end
function MalChampionScript:AttackTarget(enemy)
	myHero:IssueOrder(GameObjectOrder.AttackUnit, enemy)
end
function MalChampionScript:MoveToXYZ(px,py,pz,misc)
	local pos = D3DXVECTOR3(px,py,pz)
	myHero:IssueOrder(GameObjectOrder.MoveTo, pos)
end
function MalChampionScript:MoveToMouse()
	local pos = pwHud.hudManager.virtualCursorPos
	myHero:IssueOrder(GameObjectOrder.MoveTo, pos)
end
function MalChampionScript:GetWeakEnemy(damage_type, range, attacker, tag, posarray, i)
	local enemyArray={}
	local i = i or 1
	local attacker = attacker or myHero
	local tag = tag or "BEST"
	local posarray = posarray or myHero.position
	for k, v in pairs(ObjectManager:GetEnemyHeroes()) do
		if v.isValid and not v.isDead and not v.isInvulnerable and v.isVisible and v.isTargetable then
			if ((tag~="NEARPOSITION" or (self:GetD(v,posarray)<250)) and self:GetD(v)<range) then
				local effectivehealth=0 --LOWER THE BETTER
				--FROM NODDY
				local damagemultiplier=0
				local totaldamageneeded=v.health
				local resistance=0
				local enemyHP=v.health
				local shield=0
				local healthDmg=0
				if damage_type == "PHYS" then
					shield=v.attackShield
					enemyHP=v.health+shield
					healthDmg=damageLib:CalculateDamage(myHero, v, "Physical", enemyHP)
				elseif damage_type == "MAGIC" then
					shield=v.magicShield
					enemyHP=v.health+shield
					healthDmg=damageLib:CalculateDamage(myHero, v, "Magical", enemyHP)
				elseif damage_type == "TRUE" then
					enemyHP=v.health+shield
					healthDmg=damageLib:CalculateDamage(myHero, v, "True", enemyHP)
				else --BASIC
					shield=v.allShield
					enemyHP=v.health+shield
					healthDmg=damageLib:CalculateDamage(myHero, v, "Mixed", enemyHP)
				end
				if (healthDmg~=nil and healthDmg>0) then
					totaldamageneeded=(enemyHP*enemyHP)/healthDmg
				end
				effectivehealth=math.ceil(totaldamageneeded)
				--END
				if (effectivehealth>0) then
					--PrintChat(effectivehealth)
					table.insert(enemyArray,{effectivehealth,v})
				end
			end
		end
	end
	table.sort(enemyArray, SortArrayFirstIndex)
	for kc, vc in pairs(enemyArray) do
			--PrintChat(kc.." HP "..vc[1].." NAME "..vc[2].charName)
	end
	if (enemyArray[i]~=nil and enemyArray[i][2]~=nil) then
	return enemyArray[i][2]
	else
	return nil
	end
end

function MalChampionScript:GetAttackSpeed(hero)
	local attackSpeed = (hero.characterIntermediate.baseAttackSpeed*hero.characterIntermediate.attackSpeedMod)
	attackSpeed = math.min(2.5,attackSpeed)
	return attackSpeed
end
function SortArrayFirstIndex(a,b)
  return a[1] < b[1]
end
MalChampionScript:RivenRun()

--[[
Change log:

3/28/19 Version 4.3: This has only been tested on Dummy Targets in Practice Mode, but thought I'd open this topic so that I can start getting feedback ASAP. All killsteal and item usage mentioned below are not in the script yet.

Why should I play Riven and why does she require a script?
Riven is a high burst melee champ that requires, imo, a solo lane. She has great escape and is op. This script makes her even more op since it does
it's best to get autoattacks inbetween abilities as fast as possible. It will also autoshield in the future.

Current Features:

Teamfight hotkey
This will use all items and exhaust, it will Q then if in range will activate R and AA, then Q again then AA then Q then AA W AA E AA it will activate
R again if it will kill the enemy with a ks, you are below 15%hp, or it is about to go on cooldown (after 13.5 seconds).
(Hotkey T)

Escape Traveling hotkey
3 Q's and an E towards your mouse.
(Hotkey Z)

AA Delay in Combos
Changing this will affect how much time you allow yourself to AA between abilities. Default is 0.2. Depending on lag and latency,
as well as attack speed you might be able to increase this or have to decrease this. Decereasing this will tell your bot to give your
AA animation less time to go off. Increasing this will give more time. At the right number you should be able to cancel your AA animation
while still putting out the damage.

Attack Nearest Mouse Target
Prioritizes the weakest target within 250 radius of your mouse cursor instead of weakest enemy within 500 range from you.

Future Features:

Teamfight2 EQQWQ hotkey
This will use all items and exhaust, it will E when in range to stun then AA then if in range will activate R then Q and AA then Q then AA
then W AA behind them then Q to push them back and AA it will activate
R again if it will kill the enemy with a ks, you are below 15% hp, or it is about to go on cooldown (after 13.5 seconds).
(Hotkey Y)

Combo No Ult
Will do everything that teamfight does except it won't cast R so your autoattacks will be regular, although Killsteal may take over and activate
R to fling R at the enemy in order to kill them. So basically will Q AA Q AA Q W and use E to get into range if not in range.
(Hotkey X)

AutoStun toggle
Will stun with W if an enemy walks in range.
(Toggle with 0, default is off)

AutoShield toggle
Will shield with E if an enemy is: attacking you, is going to hit you with a target spell, or skillshotspell that isn't an instant spell like Annie's Tibbers
which is a skillshot and instant. It will also shield you from minion damage if it will kill you. Will always shield you against jungle monsters.
(Toggle with F6, default is on)

Killsteal switch
Will look to E dash to get into range to W and Ult and ignite for a kill
(default is on, switch through in game menu)

Attack Nearest Mouse Object
To help jungle or use abilities on any enemy object like baron; this will use the Teamfight combo against any enemy like: dragon or baron or
a minion or a champion. Will target the closest enemy to your mouse. Red circle around your mouse, yellow around target, will use combo if
in 500 range, otherwise it will walk to your mouse.

]]--