local _scriptVERSION = 1.7
local _scriptVERSIONONLINE = GetInternalWebResult("MalsJinx.version") 
local _scriptName = "MalsJinx"
local _scriptTitle = "HiJinxs"


local function DownloadNewVersion(currentVersion,onlineVersion)
	local currentVersion,onlineVersion = tonumber(currentVersion),tonumber(onlineVersion)
	if onlineVersion > currentVersion then
		print("Downloading ".._scriptName.." v"..onlineVersion)
		PrintChat("<font color=\"#50ed8f\">[UPDATE]</font> <font color=\"#8f70f4\"><b>".._scriptName.."</b></font><b> <font color=\"#50ed8f\"> v" ..onlineVersion.. " is avaible!</font>")
		DownloadInternalFile(_scriptName .. ".lua", SCRIPT_PATH .. _scriptName.. ".lua")
		PrintChat("<font color=\"#50ed8f\">[UPDATED]</font> <font color=\"#8f70f4\"><b>".._scriptName.."</b></font><b> <font color=\"#50ed8f\"> Refresh with F5</font>")
		print("Downloaded ".._scriptName.." v"..onlineVersion)
	end
end
DownloadNewVersion(_scriptVERSION,_scriptVERSIONONLINE)

local MalChampionScript={}

local FileExists = function(path)
	local file = io.open(path, 'r')
	if file then
		io.close(file)
		return true
	end
	return false
end

--local damageLib
if FileExists(COMMON_PATH .. "FF15Menu.lua") then require 'FF15Menu' else print("FF15Menu.lua is required in your //COMMON folder.") return end
if FileExists(COMMON_PATH .. "FF15DamageLib.lua") then damageLib = require 'FF15DamageLib' else print("Downloading FF15DamageLib.lua into your //COMMON folder.") DownloadInternalFile("FF15DamageLib.lua", COMMON_PATH .. "FF15DamageLib.lua") return end
if FileExists(COMMON_PATH .. "FF15-PremiumPrediction.lua") then print("Requiring Prediction!") require"FF15-PremiumPrediction" print("Prediction Loaded!") else print("Downloading FF15-PremiumPrediction.lua into your //COMMON folder.") DownloadInternalFile("FF15-PremiumPrediction.lua", COMMON_PATH .. "FF15-PremiumPrediction.lua") return end

local colorcyan = 0x0000FFFF
local coloryellow = 0xFFFFFF00
local colorgreen = 0xFF00FF00
local colorred = 0xFFFF0000
local colorblue = 0xFF0000FF
local ChampionDmgType="PHYS"
local attackspeed=0
local AARDY=0
local AARANGE=0
local QRDY=0
local QRANGE=0
local QRADIUS=0
local QDELAY=0
local QSPEED=0
local QAOE=0
local QLENGTH=0
local WRDY=0
local WRANGE=0
local WRADIUS=0
local WDELAY=0
local WSPEED=0
local WAOE=0
local WLENGTH=0
local ERDY=0
local ERANGE=0
local ERADIUS=0
local EDELAY=0
local ESPEED=0
local EAOE=0
local ELENGTH=0
local RRDY=0
local RRANGE=0
local RRADIUS=0
local RDELAY=0
local RSPEED=0
local RAOE=0
local RLENGTH=0

local target
local targetig
local targetlong
local ignitedamage=0
local Qstacks=0
local stackTimer=0

local gun=true

local timerAnimationBegin = 0
local timerAnimationSpeed = 0.1*(1/attackspeed)
local timetoAA=0
local range = 0


local JinxConfig = Menu("Jinx", "Jinx Config")
JinxConfig:sub("keySettings", "Keybinds")
JinxConfig.keySettings:key("c", "Combos", 84,false )
JinxConfig.keySettings:key("w", "W Poke", 65,false)
JinxConfig:sub("scriptSettings", "Settings")
--JinxConfig.scriptSettings:checkbox('ks', 'Killsteal', SCRIPT_PARAM_ONOFF, true)
--JinxConfig.scriptSettings:checkbox("nm", "NEARMOUSE Targetting", false,57):permashow(true)
JinxConfig.scriptSettings:slider('wd', "W Delay", 0,10,6.45,0.05)
JinxConfig.scriptSettings:slider('ws', "W Proj Speed", 20,30,24,1)
--JinxConfig.scriptSettings:slider('rd', "R Delay", 0,10,6.45,0.05)
--JinxConfig.scriptSettings:slider('rs', "R Proj Speed", 20,30,24,1)
JinxConfig.scriptSettings:slider("AnimS", "Animation Speed", 0,4,1.2,0.1)
JinxConfig.scriptSettings:slider("AAS", "Time To AA Speed", 0,2,1,0.1)



--W 1500 645 2400         R 1500 range  1630 2100   528 2200           650 21.5  E 500 1900

function MalChampionScript:JinxRun()
	AddEvent(Events.OnTick,function() self:OnTick() end)
	AddEvent(Events.OnDraw,function() self:OnDraw() end)
	AddEvent(Events.OnProcessSpell, function(a,b) self:OnProcessSpell(a,b) end)
	AddEvent(Events.OnBasicAttack, function(a,b) self:OnBasicAttack(a,b) end)
	QRANGE=myHero.spellbook:Spell(SpellSlot.Q).castRange
	QRADIUS=myHero.spellbook:Spell(SpellSlot.Q).castRadius
	QSPEED=myHero.spellbook:Spell(SpellSlot.Q).spellData.speed
	WRANGE=myHero.spellbook:Spell(SpellSlot.W).castRange
	WRADIUS=myHero.spellbook:Spell(SpellSlot.W).castRadius
	WSPEED=myHero.spellbook:Spell(SpellSlot.W).spellData.speed
	ERANGE=myHero.spellbook:Spell(SpellSlot.E).castRange
	ERADIUS=myHero.spellbook:Spell(SpellSlot.E).castRadius
	ESPEED=myHero.spellbook:Spell(SpellSlot.E).spellData.speed
	RRANGE=myHero.spellbook:Spell(SpellSlot.R).castRange
	RRADIUS=myHero.spellbook:Spell(SpellSlot.R).castRadius
	RSPEED=myHero.spellbook:Spell(SpellSlot.R).spellData.speed
	PrintChat(_scriptTitle.." Loaded!")
end
function MalChampionScript:OnTick()	
	if (not myHero.isDead) then
		mousePos = pwHud.hudManager.virtualCursorPos
		AARANGE=myHero.characterIntermediate.attackRange
		attackspeed=MalChampionScript:GetAttackSpeed(myHero)
		
		if (false) then
			target = self:GetWeakEnemy("PHYS", 800,myHero,"NEARPOSITION",mousePos)
		else 
			target = self:GetWeakEnemy("PHYS", 800,myHero)
		end
		targetlong =  self:GetWeakEnemy("PHYS",1500)
			-----------------------------
		if (myHero.canAttack) then
			AARDY=1
		else 
			AARDY=0
		end
		if (myHero.spellbook:CanUseSpell(SpellSlot.Q)==0) then
			QRDY=1
		else 
			QRDY=0
		end
		if (myHero.spellbook:CanUseSpell(SpellSlot.W)==0) then
			WRDY=1
		else 
			WRDY=0
		end
		if (myHero.spellbook:CanUseSpell(SpellSlot.E)==0) then
			ERDY=1
		else 
			ERDY=0
		end
		if (myHero.spellbook:CanUseSpell(SpellSlot.R)==0) then
			RRDY=1
		else 
			RRDY=0
		end
		-------------------------------------
		if target~=nil then
			AARANGE=myHero.characterIntermediate.attackRange--+target.boundingRadius
		end
		range = AARANGE
		WDELAY = JinxConfig.scriptSettings.wd:get()
		WSPEED = JinxConfig.scriptSettings.ws:get()
		timerAnimationSpeed = JinxConfig.scriptSettings.AnimS:get()*0.1*(1/attackspeed)
		if JinxConfig.keySettings.c:get() then
			self:Combo()
		end
		if JinxConfig.keySettings.w:get() then
			self:WPoke()
		end
		
		if Qstacks>0 and stackTimer<os.clock() then
			Qstacks=Qstacks-1
			stackTimer=os.clock()+2.5
		end
		--[[
		if JinxConfig.ks then
			Killsteal()
		end
		--]]
	end
end

function MalChampionScript:Combo()
	if target~=nil then
		if (gun==true and ((self:GetD(target)>600))) or (gun==false and (self:GetD(target)<=600 )) then
			self:CastSpellTarget('Q',myHero)
		end
		if os.clock()>timetoAA then
			self:AttackTarget(target)
		elseif os.clock() > (timerAnimationBegin + timerAnimationSpeed) then
			self:MoveToMouse()
		end
	else
		if targetlong~=nil then		
			if WRDY==1 and targetlong.characterIntermediate.movementSpeed>=myHero.characterIntermediate.movementSpeed 
			and self:runningAway(targetlong) 
			and self:GetFireahead(targetlong,WDELAY,WSPEED,WRADIUS, 0, true,myHero,WRANGE)~=nil then --ADD FIREAHEAD CHECK WITH COLLISION
				self:CastSpellXYZ('W',self:GetFireahead(targetlong,WDELAY,WSPEED,WRADIUS, 0, true,myHero,WRANGE))
			end
		end
		self:MoveToMouse()
	end
end

function MalChampionScript:WPoke()

	if WRDY==1 and targetlong~=nil and self:GetFireahead(targetlong,WDELAY,WSPEED,WRADIUS, 0, true,myHero,WRANGE)~=nil then		

			self:CastSpellXYZ('W',self:GetFireahead(targetlong,WDELAY,WSPEED,WRADIUS, 0, true,myHero,WRANGE))

	elseif WRDY==1 and target~=nil and self:GetFireahead(target,WDELAY,WSPEED,WRADIUS, 0, true,myHero,WRANGE)~=nil then		

			self:CastSpellXYZ('W',self:GetFireahead(target,WDELAY,WSPEED,WRADIUS, 0, true,myHero,WRANGE))
	else
		self:MoveToMouse()
	end
end


function MalChampionScript:OnBasicAttack(unit, spell)
	if unit ~= nil and spell ~= nil and unit==myHero then  
		local spellName=spell.spellData.name
		--PrintChat("SPELL:"..spellName)
		if string.find(spellName,"JinxBasicAttack") or string.find(spellName,"JinxCritAttack") then
			timerAnimationBegin = os.clock()
			timetoAA = math.max(os.clock()+.5,os.clock()+(1/attackspeed) - JinxConfig.scriptSettings.AAS:get()*0.3*(1/attackspeed))
			--print("\n AA "..timetoAA.." Time "..os.clock().." DIFF ".. timetoAA-os.clock())
			Qstacks=math.min(Qstacks+1,3)
			stackTimer=os.clock()+2.5
			gun=true
		elseif string.find(spellName,"JinxQAttack") or string.find(spellName,"JinxQAttack") or string.find(spellName,"JinxQCrit") then
			timerAnimationBegin = os.clock()
			timetoAA = os.clock()+(1/attackspeed) - JinxConfig.scriptSettings.AAS:get()*0.3*(1/attackspeed)
			gun=false
		end
	end
end

function MalChampionScript:OnProcessSpell(unit, spell)
	if unit ~= nil and spell ~= nil and unit==myHero then  
		local spellName=spell.spellData.name
		--PrintChat("SPELL:"..spellName)
		if spellName=="JinxQ" then
			if gun==true then
				gun=false
			else
				gun=true
			end
		end
	end
end
function MalChampionScript:OnDraw()
	if not myHero.isDead then
		if QRDY == 1 then
			self:CustomCircle(800,3,3,myHero)
		end	
		
		if RRDY == 1 then
			self:CustomCircle(1500,3,2,myHero)
			for k, v in pairs(ObjectManager:GetEnemyHeroes()) do
				if v.isValid and not v.isDead and not v.isInvulnerable and v.isVisible and v.isTargetable then
					local hero = v
					--PrintChat(self:getDmg("M",hero,myHero,"PHYS",(150+(100*self:GetSpellLevel('R'))+myHero.characterIntermediate.flatPhysicalDamageMod+(hero.maxHealth-hero.health)*(.2+.05*self:GetSpellLevel('R')))*0.8))
					
					if self:GetD(hero)>1500 and hero.health<self:getDmg("M",hero,myHero,"PHYS",(150+(100*self:GetSpellLevel('R'))+myHero.characterIntermediate.flatPhysicalDamageMod+(hero.maxHealth-hero.health)*(.2+.05*self:GetSpellLevel('R')))*0.8) then
						self:CustomCircle(200,50,3,hero)
					elseif self:GetD(hero)<=1500 and hero.health<self:getDmg("M",hero,myHero,"PHYS",(75+(50*self:GetSpellLevel('R'))+0.5*myHero.characterIntermediate.flatPhysicalDamageMod+(hero.maxHealth-hero.health)*(.2+.05*self:GetSpellLevel('R')))*0.8) then
						self:CustomCircle(200,50,5,hero)
					end
					if self:GetD(hero)>1500 and hero.health<self:getDmg("M",hero,myHero,"PHYS",(150+(100*self:GetSpellLevel('R'))+myHero.characterIntermediate.flatPhysicalDamageMod+(hero.maxHealth-hero.health)*(.2+.05*self:GetSpellLevel('R')))) then
						self:CustomCircle(250,50,2,hero)
					elseif self:GetD(hero)<=1500 and hero.health<self:getDmg("M",hero,myHero,"PHYS",(75+(50*self:GetSpellLevel('R'))+0.5*myHero.characterIntermediate.flatPhysicalDamageMod+(hero.maxHealth-hero.health)*(.2+.05*self:GetSpellLevel('R')))) then
						self:CustomCircle(250,50,1,hero)
					end
				end
			end
		end	
		if WRDY ==1 then
			self:CustomCircle(WRANGE,5,1,myHero)
		end	
		
		if ERDY ==1 then
			self:CustomCircle(ERANGE,5,4,myHero)--875
		end
		if target~=nil and not target.isDead then
			self:CustomCircle(100,3,5,target)
		end	
	end
        --if targetult~=nil then
               --CustomCircle(100,3,4,targetult)
        --end
end
------------------------------------------------------ Check If In Spell Stuff

------------------CONVERSION FUNCTIONS!
function MalChampionScript:CustomCircle(radius,bold,colorsent,unit)
	local posar
	if (unit.position~=nil) then posar=unit.position
	else posar=unit end
	local color=colorsent
	if (colorsent<10) then
		local colorarr={[1]=colorblue,[2]=colorgreen,[3]=coloryellow,[4]=colorcyan,[5]=colorred}
		color=colorarr[colorsent]
	end
	DrawHandler:Circle3D(posar,radius, color)
	for i=1, math.ceil(bold/2), 1 do
		local radiusincr=radius+i
		local radiusdecr=radius-i
		DrawHandler:Circle3D(posar,radiusincr, color)
		DrawHandler:Circle3D(posar,radiusdecr, color)
	end
end
function MalChampionScript:runningAway(slowtarget)
   local d1 = self:GetD(slowtarget) -- DISTANCE BETWEEN UNIT AND MYHERO
   local x, y, z = self:GetFireahead(slowtarget,2,0) --PREDICTION TO GET FACING DIRECTION
   local d2 = self:GetD({x=x, y=y, z=z}) --DISTANCE BETWEEN PREDICTION AND MYHERO
   local d3 = self:GetD({x=x, y=y, z=z},slowtarget) --DISTANCE BETWEEN PREDICTION AND UNIT
   local angle = math.acos((d2*d2-d3*d3-d1*d1)/(-2*d3*d1))
   return angle%(2*math.pi)>math.pi/2 and angle%(2*math.pi)<math.pi*3/2
end
function MalChampionScript:GetFireahead(unitfa,delayfa,speedfa,radius, angle, collision,source,range)
	local sourcefa=source or myHero
	local radiusfa=radius or 55
	local anglefa=angle or 0
	local collisionfa=collision or false
	local rangefa=range or 5000
	local delayfa=delayfa*.1
	local speedfa=speedfa*100
	local CastPos, PredPos, HitChance, TimeToHit = PremiumPrediction:GetPrediction(sourcefa, unitfa, speedfa, rangefa, delayfa, radiusfa, anglefa, collisionfa)
	if (not collision or (CastPos and HitChance >= 0.6)) then 
		return CastPos
	else 
		return nil
	end
end

function MalChampionScript:GetD(p1, p2)
if p2 == nil then p2 = myHero.position end
if p1.position~=nil then p1=p1.position end
if p2.position~=nil then p2=p2.position end
if (p1.z == nil or p2.z == nil) and p1.x~=nil and p1.y ~=nil and p2.x~=nil and p2.y~=nil then
px=p1.x-p2.x
py=p1.y-p2.y
if px~=nil and py~=nil then
px2=px*px
py2=py*py
if px2~=nil and py2~=nil then
return math.sqrt(px2+py2)
else
return 99999
end
else
return 99999
end
 
elseif p1.x~=nil and p1.z ~=nil and p2.x~=nil and p2.z~=nil then
px=p1.x-p2.x
pz=p1.z-p2.z
if px~=nil and pz~=nil then
px2=px*px
pz2=pz*pz
if px2~=nil and pz2~=nil then
return math.sqrt(px2+pz2)
else
return 99999
end
else    
return 99999
end
 
else
return 99999
end
end

function MalChampionScript:run_every(interval, fn, ...)
    return self:internal_run({fn=fn, interval=interval}, ...)
end
 
function MalChampionScript:internal_run(t, ...)    
    local fn = t.fn
    local key = t.key or fn
   
    local now = os.clock()
    local data = _registry[key]
       
    if data == nil or t.reset then
        local args = {}
        local n = select('#', ...)
        local v
        for i=1,n do
            v = select(i, ...)
            table.insert(args, v)
        end  
        -- the first t and args are stored in registry        
        data = {count=0, last=0, complete=false, t=t, args=args}
        _registry[key] = data
    end
       
    --assert(data~=nil, 'data==nil')
    --assert(data.count~=nil, 'data.count==nil')
    --assert(now~=nil, 'now==nil')
    --assert(data.t~=nil, 'data.t==nil')
    --assert(data.t.start~=nil, 'data.t.start==nil')
    --assert(data.last~=nil, 'data.last==nil')
    -- run
    local countCheck = (t.count==nil or data.count < t.count)
    local startCheck = (data.t.start==nil or now >= data.t.start)
    local intervalCheck = (t.interval==nil or now-data.last >= t.interval)
    --print('', 'countCheck', tostring(countCheck))
    --print('', 'startCheck', tostring(startCheck))
    --print('', 'intervalCheck', tostring(intervalCheck))
    --print('')
    if not data.complete and countCheck and startCheck and intervalCheck then                
        if t.count ~= nil then -- only increment count if count matters
            data.count = data.count + 1
        end
        data.last = now        
       
        if t._while==nil and t._until==nil then
            return fn(...)
        else
            -- while/until handling
            local signal = t._until ~= nil
            local checker = t._while or t._until
            local result
            if fn == checker then            
                result = fn(...)
                if result == signal then
                    data.complete = true
                end
                return result
            else
                result = checker(...)
                if result == signal then
                    data.complete = true
                else
                    return fn(...)
                end
            end            
        end
    end    
end

function MalChampionScript:CastSpellXYZ(spellLetter,px,py,pz,misc)
	if (px.x~=nil) then
		pz=px.z
		if (px.y~=nil) then
			py=px.y
		else 
			py=0
		end
		px=px.x
	elseif (px.position~=nil) then
		pz=px.position.z
		py=px.position.y
		px=px.position.x
	end
	local pos=D3DXVECTOR3(px,py,pz)
	local spellarr={["Q"]=SpellSlot.Q,["W"]=SpellSlot.W,["E"]=SpellSlot.E,["R"]=SpellSlot.R}
	local spellNumber=spellarr[spellLetter]
	myHero.spellbook:CastSpell(spellNumber, pos)
end
function MalChampionScript:CastSpellTarget(spellLetter,hero)
	if (hero~=nil) then 
		local spellarr={["Q"]=SpellSlot.Q,["W"]=SpellSlot.W,["E"]=SpellSlot.E,["R"]=SpellSlot.R}
		local spellNumber=spellarr[spellLetter]
		myHero.spellbook:CastSpell(spellNumber, hero.networkId)
	end
end
function MalChampionScript:GetSpellLevel(spellLetter,hero)
	local hero=hero or myHero
	local spellarr={["Q"]=SpellSlot.Q,["W"]=SpellSlot.W,["E"]=SpellSlot.E,["R"]=SpellSlot.R}
	local spellNumber=spellarr[spellLetter]
	return myHero.spellbook:Spell(spellNumber).level
end
function MalChampionScript:getDmg(spellLetter,target,source,damage_type,damage)
	local source=source or myHero
	if (spellLetter=="M") then
		local damage_type=damage_type or ChampionDmgType
		local damage=damage or 0
		if damage_type=="PHYS" then
			return damageLib:CalculateDamage(myHero, target, "Physical",damage)
		elseif damage_type=="MAGIC" then
			return damageLib:CalculateDamage(myHero, target, "Magical",damage)
		elseif damage_type=="MAGIC" then
			return damageLib:CalculateDamage(myHero, target, "True",damage)
		else
			return damageLib:CalculateDamage(myHero, target, "Mixed",damage)
		end
	else 
		local spellarr={["Q"]=SpellSlot.Q,["W"]=SpellSlot.W,["E"]=SpellSlot.E,["R"]=SpellSlot.R}
		local spellNumber=spellarr[spellLetter]
		return damageLib:GetSpellDamage(source, target, spellNumber, 'Default', false)
	end
end
function MalChampionScript:AttackTarget(enemy)
	myHero:IssueOrder(GameObjectOrder.AttackUnit, enemy)
end
function MalChampionScript:MoveToXYZ(px,py,pz,misc)
	local pos = D3DXVECTOR3(px,py,pz)
	myHero:IssueOrder(GameObjectOrder.MoveTo, pos)
end
function MalChampionScript:MoveToMouse()
	local pos = pwHud.hudManager.virtualCursorPos
	myHero:IssueOrder(GameObjectOrder.MoveTo, pos)
end
function MalChampionScript:GetWeakEnemy(damage_type, range, attacker, tag, posarray, i)
	local enemyArray={}
	local i = i or 1
	local attacker = attacker or myHero
	local tag = tag or "BEST"
	local posarray = posarray or myHero.position
	for k, v in pairs(ObjectManager:GetEnemyHeroes()) do
		if v.isValid and not v.isDead and not v.isInvulnerable and v.isVisible and v.isTargetable then
			if ((tag~="NEARPOSITION" or (self:GetD(v,posarray)<250)) and self:GetD(v)<range) then
				local effectivehealth=0 --LOWER THE BETTER
				--FROM NODDY
				local damagemultiplier=0
				local totaldamageneeded=v.health
				local resistance=0
				local enemyHP=v.health
				local shield=0
				local healthDmg=0
				if damage_type == "PHYS" then
					shield=v.attackShield
					enemyHP=v.health+shield
					healthDmg=damageLib:CalculateDamage(myHero, v, "Physical", enemyHP)
				elseif damage_type == "MAGIC" then
					shield=v.magicShield
					enemyHP=v.health+shield
					healthDmg=damageLib:CalculateDamage(myHero, v, "Magical", enemyHP)
				elseif damage_type == "TRUE" then
					enemyHP=v.health+shield
					healthDmg=damageLib:CalculateDamage(myHero, v, "True", enemyHP)
				else --BASIC
					shield=v.allShield
					enemyHP=v.health+shield
					healthDmg=damageLib:CalculateDamage(myHero, v, "Mixed", enemyHP)
				end
				if (healthDmg~=nil and healthDmg>0) then
					totaldamageneeded=(enemyHP*enemyHP)/healthDmg
				end
				effectivehealth=math.ceil(totaldamageneeded)
				--END
				if (effectivehealth>0) then
					--PrintChat(effectivehealth)
					table.insert(enemyArray,{effectivehealth,v})
				end
			end
		end
	end
	table.sort(enemyArray, SortArrayFirstIndex)
	for kc, vc in pairs(enemyArray) do
			--PrintChat(kc.." HP "..vc[1].." NAME "..vc[2].charName)
	end
	if (enemyArray[i]~=nil and enemyArray[i][2]~=nil) then
	return enemyArray[i][2]
	else
	return nil
	end
end

function MalChampionScript:GetAttackSpeed(hero)
	local attackSpeed = (hero.characterIntermediate.baseAttackSpeed*hero.characterIntermediate.attackSpeedMod)
	attackSpeed = math.min(2.5,attackSpeed)
	return attackSpeed
end
function SortArrayFirstIndex(a,b)
  return a[1] < b[1]
end
MalChampionScript:JinxRun()