local Ahri = {}
local version = 1
require "FF15Menu"
require "utils"
local DreamTS = require("DreamTS")
local dmgLib = require("FF15DamageLib")

function OnLoad()
    if not _G.Prediction then
        LoadPaidScript(PaidScript.DREAM_PRED)
    end
end

function Ahri:__init()
    self.q = {
        speed = 1700,
        range = 870,
        delay = 0.25,
        width = 80
    }
    self.e = {
        speed = 1600,
        range = 970,
        delay = 0.25,
        width = 60
    }
    self:Menu()
    self.charm = nil
    self.delaytable = {}
    self.TS =
        DreamTS(
        self.menu.dreamTs,
        {
            ValidTarget = function(unit)
                return _G.Prediction.IsValidTarget(unit, 975)
            end,
            Damage = function(unit)
                return dmgLib:CalculateMagicDamage(myHero, unit, 100)
            end
        }
    )
    AddEvent(Events.OnTick, function() self:OnTick() end)
    AddEvent(Events.OnDraw, function() self:OnDraw() end)
    AddEvent(Events.OnBuffGain, function(obj, buff) self:OnBuffGain(obj, buff) end)
    AddEvent(Events.OnBuffLost, function(obj, buff) self:OnBuffLost(obj, buff) end)
    PrintChat("<font color=\"#E41B17\">[<b>¤ Cyrex ¤</b>]:</font>" .. " <font color=\"#" .. "FFFFFF" .. "\">" .. "Ahri Loaded" .. "</font>")
    self.font = DrawHandler:CreateFont("Calibri", 12)
end

function Ahri:Menu()
    self.menu = Menu("cxahri", "Cyrex Ahri")
    self.menu:sub("dreamTs", "Target Selector")

    self.menu:sub("Key", "Key Settings")
        self.menu.Key:checkbox("e", "Start Combo With E", true, string.byte("K"))

    self.menu:sub("combo", "Combo Settings")
        self.menu.combo:label("xd", "Q Settings")
        self.menu.combo:checkbox("q", "Use Q", true)
        self.menu.combo:label("xd1", "W Settings")
        self.menu.combo:checkbox("w", "Use W", true)
        self.menu.combo:checkbox("wc", "Cast Only if Enemy is Charmed?", false)
        self.menu.combo:slider("wr", "Min. W Range To Cast", 300, 700, 600, 50)
        self.menu.combo:label("xd2", "E Settings")
        self.menu.combo:checkbox("e", "Use E", true)
        self.menu.combo:label("xd3", "R Settings")
        self.menu.combo:checkbox("r", "Use R [Mouse & Beta]", false)

    self.menu:sub("harass", "Harass Settings")
        self.menu.harass:checkbox("q", "Use Q", true)
        self.menu.harass:checkbox("w", "Use W", true)
        self.menu.harass:checkbox("e", "Use E", true)
        self.menu.harass:slider("mana", "Min. Mana Percent: ", 0, 100, 10, 5)

    self.menu:sub("draws", "Draw")
        self.menu.draws:checkbox("q", "Q", true)
        self.menu.draws:checkbox("w", "W", true)
        self.menu.draws:checkbox("e", "E", true)
end

function Ahri:OnDraw()
    if self.menu.draws.q:get() and myHero.visibleOnScreen then
        DrawHandler:Circle3D(myHero.position, self.q.range, self:Hex(255,255,255,255))
    end
    if self.menu.draws.w:get() and myHero.visibleOnScreen then
        DrawHandler:Circle3D(myHero.position, self.menu.combo.wr:get(), self:Hex(255,255,255,255))
    end
    if self.menu.draws.e:get() and myHero.visibleOnScreen then
        DrawHandler:Circle3D(myHero.position, self.e.range, self:Hex(255,255,255,255))
    end    
end

function Ahri:Delay(func, delay)
    local t = os.clock() + delay
    self.delaytable[t] = {f = func}
end

function Ahri:GetPercentHealth(obj)
  local obj = obj or myHero
  return (obj.health / obj.maxHealth) * 100
end

function Ahri:CastQ(target)
    if myHero.spellbook:CanUseSpell(0) == 0 then
        local pred = _G.Prediction.GetPrediction(target, self.q, myHero)
        if pred and pred.castPosition and GetDistanceSqr(pred.castPosition) <= (self.q.range * self.q.range) and not pred:windWallCollision() then
            myHero.spellbook:CastSpell(0, pred.castPosition)
        end
    end
end

function Ahri:CastE(target)
    if myHero.spellbook:CanUseSpell(2) == 0 then
        local pred = _G.Prediction.GetPrediction(target, self.e, myHero)
        if pred and pred.castPosition and GetDistanceSqr(pred.castPosition) <= (self.e.range * self.e.range) and (pred.realHitChance == 1 or _G.Prediction.WaypointManager.ShouldCast(target) or GetDistanceSqr(pred.castPosition) <= 400 * 400) and not pred:windWallCollision() and not pred:minionCollision() then
            myHero.spellbook:CastSpell(2, pred.castPosition)
        end
    end
end

function Ahri:CastR()
    if myHero.spellbook:CanUseSpell(3) == 0 and self.menu.combo.r:get() then
        myHero.spellbook:CastSpell(3, pwHud.hudManager.virtualCursorPos)
    end
end


function Ahri:OnTick()
    local target = LegitOrbwalker:GetTarget(975)
    if LegitOrbwalker:GetMode() == "Combo" then
        if target then
            if self.menu.combo.e:get() then                
                self:CastE(target)
            end
            if self.menu.Key.e:get() and myHero.spellbook:CanUseSpell(2) == 0 then return end 
            if self.menu.combo.q:get() then         
                self:CastQ(target)
            end 
            if self.menu.combo.w:get() then 
                if myHero.spellbook:CanUseSpell(1) == 0 and GetDistance(target) <= (self.menu.combo.wr:get()) then
                    if self.menu.combo.wc:get() and self.charm then 
                        myHero.spellbook:CastSpell(1, myHero.networkId)
                    elseif not self.menu.combo.wc:get() or self:GetPercentHealth(target) < 40 then
                        myHero.spellbook:CastSpell(1, myHero.networkId)
                    elseif not self.menu.combo.wc:get() and myHero.spellbook:CanUseSpell(0) ~= 0 and myHero.spellbook:CanUseSpell(2) ~= 0 then
                        myHero.spellbook:CastSpell(1, myHero.networkId)
                    end
                end
            end         
            if self.menu.combo.r:get() and GetDistance(target) <= 700 then    
                self:Delay(self:CastR(target), 1)
            end 
        end
    end
    if LegitOrbwalker:GetMode() == "Harass" then
        if target then
            if self.menu.harass.e:get() then                
                self:CastE(target)
            end
            if self.menu.harass.q:get() then         
                self:CastQ(target)
            end 
            if self.menu.combo.w:get() then 
                if myHero.spellbook:CanUseSpell(1) == 0 and GetDistanceSqr(target) <= (self.menu.combo.wr:get() * self.menu.combo.wr:get()) then
                    myHero.spellbook:CastSpell(1)
                end
            end         
        end
    end
    for v, k in pairs(self.delaytable) do
        if v <= os.clock() then
            k.f()
            self.delaytable[v] = nil
        end
    end
end

function Ahri:OnBuffGain(obj, buff)
    if obj and obj.team ~= myHero.team and obj.type == myHero.type and buff.type == 22 then
        self.charm = true
    end
end

function Ahri:OnBuffLost(obj, buff)
    if obj and obj.team ~= myHero.team and obj.type == myHero.type and buff.type == 22 then
        self.charm = false
    end
end

function Ahri:Hex(a, r, g, b)
    return string.format("0x%.2X%.2X%.2X%.2X", a, r, g, b)
end

function Ahri:GetTarget(dist, all)
    self.TS.ValidTarget = function(unit)
        return _G.Prediction.IsValidTarget(unit, dist)
    end
    local res = self.TS:update()
    if all then
        return res
    else
        if res and res[1] then
            return res[1]
        end
    end
end

if myHero.charName == "Ahri" then
    Ahri:__init()
end
